"""
New PanPan
"""


class Register:
    class Main:
        invited = 'com.aaecosys.apac_panpan:id/welcome_invited'  # 收到邀请
        register = 'com.aaecosys.apac_panpan:id/welcome_device_setup'  # 注册
        login = 'com.aaecosys.apac_panpan:id/welcome_sign_in'  # 登录

    class Policy:
        privacy = 'com.aaecosys.apac_panpan:id/eula_privacy_policy'  # 隐私政策
        service = 'com.aaecosys.apac_panpan:id/eula_terms_of_service'  # 服务条件
        license = 'com.aaecosys.apac_panpan:id/eula_license_agreement'  # 终端用户许可协议
        agree = 'com.aaecosys.apac_panpan:id/signup_flow_eula_agree_container'  # 我同意
        link = "//*[@class='com.vivo.chromium.WebViewAdapter' and @index='0']"  # 隐私条款h5链接
        back = 'com.vivo.browser:id/tool_bar_btn_prev'  # 返回按钮

    class Invited:
        image = 'com.aaecosys.apac_panpan:id/onboarding_chooser_1'  # 邀请图片
        invite_back = '//android.widget.ImageButton[@content-desc="Navigate up"]'  # 邀请图片返回按钮

    class Register:
        start = 'com.aaecosys.apac_panpan:id/setup_button_continue'  # 开始
        first_name = 'com.aaecosys.apac_panpan:id/signup_userinfo_firstname_inner'  # 名
        last_name = 'com.aaecosys.apac_panpan:id/signup_userinfo_lastname_inner'  # 姓
        mail = 'com.aaecosys.apac_panpan:id/signup_userinfo_email_inner'  # 邮箱
        phone_area_select = "//*[contains(@text, '+93')]" # 选择Afghanistan
        phone_area  = 'com.aaecosys.apac_panpan:id/signup_userinfo_phone_container_country_code_container'  # 手机区号选择
        phone = 'com.aaecosys.apac_panpan:id/signup_userinfo_phone_container_phone_entry'  # 手机号
        password = 'com.aaecosys.apac_panpan:id/signup_userinfo_password'  # 密码
        # password_visible = 'com.aaecosys.apac_panpan:id/text_input_password_toggle'  # 可见/不可见
        password_visible = '//*[@content-desc="Show password"]'  # 可见/不可见
        policy = 'com.aaecosys.apac_panpan:id/signup_userinfo_eula'  # 协议
        next = 'com.aaecosys.apac_panpan:id/signup_userinfo_next'  # 下一步
        assert_alert_firstname = "//*[@text='空的名']"  # 名为空
        assert_alert_lastname = "//*[@text='空的姓']"  # 姓为空
        assert_alert_first_name = "空的名"  # 名为空提示
        assert_alert_last_name = "空的姓"  # 姓为空提示
        assert_alert_email_error = "无效的邮箱地址"  # 邮箱非法提示
        assert_alert_email = 'com.aaecosys.apac_panpan:id/textinput_error'  # 邮箱非法
        area_number_default = '+86'  # 默认中国区号
        phone_area_default = 'com.aaecosys.apac_panpan:id/signup_userinfo_phone_container_country_code_text'  # 默认+86
        phone_area_search = 'com.aaecosys.apac_panpan:id/country_picker_search' # 搜索国家区号
        phone_area_search_result ='//*[@resource-id="com.aaecosys.apac_panpan:id/country_picker_listview"]/android.widget.LinearLayout[1]'  # 搜索结果
        phone_area_search_match = '//*[@resource-id="com.aaecosys.apac_panpan:id/country_picker_listview"]/android.widget.LinearLayout[1]/android.widget.TextView[1]'  # 匹配结果
        assert_password_strength = 'com.aaecosys.apac_panpan:id/signup_userinfo_password_strength_text'  # 密码长度提示
        assert_alert_password = '必须至少有8个字符，包含一个大写字母、一个小写字母、一个数字和一个特殊字符'  # 密码强度提示

    class UserIcon:
        skip = 'com.aaecosys.apac_panpan:id/signup_collectphoto_skip'  # 跳过这步
        select_from_album = 'com.aaecosys.apac_panpan:id/signup_collectphoto_gallery'  # 从图片中选择
        take_photo = 'com.aaecosys.apac_panpan:id/signup_collectphoto_camera'  # 拍照
        photo = 'com.aaecosys.apac_panpan:id/coordinator_a'  # 安全照片整个页面
        select_image = 'com.google.android.documentsui:id/icon_thumb'  # 选择手机相册中第一张图片
        show_image = 'com.aaecosys.apac_panpan:id/signup_collectphoto_user_img'   # 显示选择的图片
        upload_image_next = 'com.aaecosys.apac_panpan:id/signup_collectphoto_next'   # 上传头像下一步
        # shutter_button = 'com.android.camera:id/shutter_button'   # 调用手机拍照功能
        shutter_button = 'com.android.camera:id/v9_shutter_button_internal'   # 调用手机拍照功能
        # sure_button = 'com.android.camera:id/done_button'  # 确定上传头像
        sure_button = 'com.android.camera:id/intent_done_apply'  # 确定上传头像

    class VerifyCode:
        code_input = '//*[@resource-id="com.aaecosys.apac_panpan:id/signup_twofactor_pin"]/android.widget.EditText'  # 验证码输入
        code_view = 'com.aaecosys.apac_panpan:id/signup_twofactor_pin'  # 验证码
        no_code = 'com.aaecosys.apac_panpan:id/signup_twofactor_nocode'  # 未收到验证码
        change_phone = 'com.aaecosys.apac_panpan:id/md_buttonDefaultNegative'  # 更改电话
        send_again = 'com.aaecosys.apac_panpan:id/md_buttonDefaultPositive'    # 再发一次
        change_phone_title = 'com.aaecosys.apac_panpan:id/md_title'  # 更改电话标题
        assert_change_phone_title = '更新电话号码'  # 更新电话号码提示
        cancel_change = 'com.aaecosys.apac_panpan:id/md_buttonDefaultNegative'  # 取消更改电话
        change_phone_new = 'com.aaecosys.apac_panpan:id/change_phonephone_container_phone_entry'  # 输入新的电话号码
        change_phone_save = 'com.aaecosys.apac_panpan:id/md_buttonDefaultPositive'  # 保存修改的电话号码
        send_again_title = 'com.aaecosys.apac_panpan:id/md_title'  # 重新发送手机验证码弹框
        assert_send_again_title = '重新发送手机验证码'  # 重新发送手机验证码
        send_again_ok = 'com.aaecosys.apac_panpan:id/md_buttonDefaultPositive'  # 更改电话好的按钮
        code_page = 'com.aaecosys.apac_panpan:id/coordinator_a'  # 验证码页面


class Login:
    class Login:
        phone_input = 'com.aaecosys.apac_panpan:id/login_phone_container_phone_entry'  # 手机号
        mail_input = 'com.aaecosys.apac_panpan:id/login_email_inner'  # 邮箱
        password_input = 'com.aaecosys.apac_panpan:id/login_password_field'  # 密码
        type_switch = 'com.aaecosys.apac_panpan:id/login_switch_credential'  # 切换登录方式
        mail_text = '邮箱登录'  # 邮箱登录文本
        phone_text = '手机登录'  # 手机登录文本
        submit = 'com.aaecosys.apac_panpan:id/login_submit_button_container'  # 登录
        forget_password = 'com.aaecosys.apac_panpan:id/login_forgot_pass_container'  # 忘记密码
        lost_phone = 'com.aaecosys.apac_panpan:id/login_lost_phone_container'  # 遗失手机
        lost_phone_submit = '//*[contains(@text, "提交")]'  # 遗失手机页提交按键
        back_button = '//*[@content-desc="Navigate up"]'  # 登录页返回按钮
        area_number = 'com.aaecosys.apac_panpan:id/login_phone_container_country_code_text'  # 区号
        area_number_china = '+86'  # 默认中国区号
        phone_or_password_error = 'com.aaecosys.apac_panpan:id/textinput_error'  # 手机号或密码错误
        button_sure = 'com.aaecosys.apac_panpan:id/login_submit_button_container'  # 确认
        create_account = "//*[@text='创建帐号' and @index='1']"  # 创建账号
        country_search = 'com.aaecosys.apac_panpan:id/country_picker_search'  # 搜索国家/地区

        input_or_password_error = 'com.aaecosys.apac_panpan:id/textinput_error'  # 手机号/邮箱或密码错误
        phone_or_password_error_text = '手机号或密码错误'  # 手机号或密码错误
        mail_or_password_error_text = '电子邮件或密码错误'  # 电子邮件或密码错误
        mail_text_error = 'com.aaecosys.apac_panpan:id/textinput_error'  # 请输入有效的邮箱地址
        password_show = 'com.aaecosys.apac_panpan:id/text_input_password_toggle'  # 密码可见按键

    class ForgetPassword:
        switch_mode = 'com.aaecosys.apac_panpan:id/signup_flow_switch_mode'  # 切换验证方式
        # back_button = 'com.aaecosys.apac_panpan:id/forgot_pass_flow_action_bar_button'  # 返回按键
        back_button = '//*[@resource-id="com.aaecosys.apac_panpan:id/forgot_pass_flow_action_bar_button"]//*[@class="android.widget.ImageButton"]'  # 返回按键
        next = '//*[contains(@text, "继续")]'  # 忘记密码页继续按键
        area_number = 'com.aaecosys.apac_panpan:id/signup_flow_phone_container_country_code_container'  # 忘记密码页区号
        phone_input = 'com.aaecosys.apac_panpan:id/signup_flow_phone_container_phone_entry'  # 忘记密码页手机号输入框
        mail_input = 'com.aaecosys.apac_panpan:id/signup_flow_email_entry'  # 忘记密码页邮箱输入框
        invalid_phone = 'com.aaecosys.apac_panpan:id/snackbar_text'  # 无效的号码
        resend_code = 'com.aaecosys.apac_panpan:id/signup_flow_resend_email_code_button'  # 验证码重新发送
        continue_button = 'com.aaecosys.apac_panpan:id/signup_flow_validate_email_code_continue_button'  # 忘记密码验证码页继续按键
        invalid_code = 'com.aaecosys.apac_panpan:id/snackbar_text'  # 错误的验证码
        code_sent_text = '验证码已发送'  # 验证码已发送
        code_input = 'com.aaecosys.apac_panpan:id/signup_flow_enter_email_code_field'  # 忘记密码页验证码输入框
        invalid_mail_text = '无效的邮箱地址'  # 无效的邮箱地址
        new_password_create_input = 'com.aaecosys.apac_panpan:id/change_pass_new_pass_field'  # 创建新密码输入框
        new_password_confirm_input = 'com.aaecosys.apac_panpan:id/change_pass_confirm_new_pass_field'  # 确认新密码输入框
        new_password_close = 'com.aaecosys.apac_panpan:id/change_pass_action_bar_button'  # 修改密码关闭按键
        new_password_save = 'com.aaecosys.apac_panpan:id/save_password'  # 修改密码保存按键

    class Verify:
        # code_input = 'com.aaecosys.apac_panpan:id/credential_validate_textfield'  # 验证码
        # code_input = '//*[@resource-id="com.aaecosys.apac_panpan:id/signup_twofactor_pin"]/android.widget.EditText'  # 验证码输入
        code_input = '//*[@resource-id="com.aaecosys.apac_panpan:id/credential_validate_textfield"]/android.widget.EditText'  # 验证码输入
        submit = 'com.aaecosys.apac_panpan:id/credential_validate_submit_button_container'  # 提交
        no_code = 'com.aaecosys.apac_panpan:id/credential_validate_didnt_recieve'  # 未收到验证码
        resend = 'com.aaecosys.apac_panpan:id/credential_validate_resend_button_container'  # 重新发送


class HomePage:
    class HomePage:
        menu_in_other_page = "com.august.bennu:id/house_activity_drawer_button_container"  # menu
        house_list = 'com.august.bennu:id/house_card_background'  # HOUSE LIST
        empty_device = 'com.august.bennu:id/keychain_empty_new_device'   # empty device
        # empty_device = 'com.august.bennu:id/house_name'  # empty device


        main = "com.aaecosys.apac_panpan:id/keychain_empty_subhead"  # 主页
        no_rooms = "com.aaecosys.apac_panpan:id/keychain_empty_layout"  # 暂未添加任何设备
        # add_device = "//*[@text='Set Up New Device']"  # SET UP NEW DEVICE
        add_device = '//*[@resource-id="com.august.bennu:id/design_navigation_view"]//androidx.appcompat.widget.LinearLayoutCompat[5]'  # SET UP NEW DEVICE
        menu_in_home_page = "//*[@content-desc='Navigate up']"  # HOME
        # menu_in_other_page = "com.aaecosys.apac_panpan:id/house_activity_menu_button"  # 菜单键
        home_list = '//*[@resource-id="com.aaecosys.apac_panpan:id/keychain_recycleview"]/android.widget.FrameLayout[1]'  # 家的列表

    class Menu:
        user_icon = "com.aaecosys.apac_panpan:id/keydraw_user_pic"  # 用户信息
        user_name = "com.aaecosys.apac_panpan:id/keydraw_header_username"  # 用户名称
        home = "//*[@text='Set Up New Device']"  # 首页
        help = "//*[@text='帮助']"  # 帮助
        help_text = "//*[contains(@text, '联系我们')]"  # 联系我们
        add_device = "//*[@text='添加设备']"  # 添加设备

    class User:
        name = "com.aaecosys.apac_panpan:id/user_settings_name"  # 修改名字
        mail = 'com.aaecosys.apac_panpan:id/user_settings_email'  # 用户邮箱
        phone = 'com.aaecosys.apac_panpan:id/user_settings_phone_container_phone'  # 用户手机
        edit = 'com.aaecosys.apac_panpan:id/user_settings_fab'  # 编辑
        logout = "com.aaecosys.apac_panpan:id/user_settings_signout"  # 登出
        icon = 'com.aaecosys.apac_panpan:id/user_settings_user_picture_container'  # 头像区域

    class UserName:
        name_modify_title = "//*[@text='你叫什么名字？']"  # 你叫什么名字
        first_name = "com.aaecosys.apac_panpan:id/user_name_first_name_field"  # 名输入框
        last_name = "com.aaecosys.apac_panpan:id/user_name_last_name_field"  # 姓输入框

    class UserIcon:
        modify_icon = "com.aaecosys.apac_panpan:id/user_settings_user_picture"  # 头像
        open_camera = "//*[@text='拍一张照片']"  # 拍一张照片
        take_photo = "//*[@text='拍摄']"  # 小米8拍摄
        # take_photo = "com.google.android.GoogleCamera:id/shutter_button"  # pixel3拍摄
        take_photo_done = "//*[@text='Done']"  # 小米8完成
        # take_photo_done = "//*[@content-desc='完成']"  # pixel3完成
        close_camera = "//*[@text='最近照片或视频']"  # 关闭相机
        select_from_album = "//*[@text='选择图片']"  # 选择图片
        select_first_photo = "com.google.android.documentsui:id/icon_thumb"  # 小米8第一个图片
        # select_first_photo = '//android.widget.ImageView[@resource-id="com.android.documentsui:id/icon_thumb"]'  # 选择相册第一张图片
        icon_updating = "//*[@text='上传图片中']"  # 上传图片中

    class Edit:
        account = 'com.aaecosys.apac_panpan:id/fab_menu_fab_expanded'  # 管理账户
        change_password = "com.aaecosys.apac_panpan:id/fab_menu_action_1"  # 修改密码
        change_password_close = 'com.aaecosys.apac_panpan:id/change_pass_back_button_ripple'  # 关闭
        change_password_save = 'com.aaecosys.apac_panpan:id/save_password'  # 保存
        password_first = 'com.aaecosys.apac_panpan:id/change_pass_new_pass_field'  # 第一个密码
        password_second = 'com.aaecosys.apac_panpan:id/change_pass_confirm_new_pass_field'  # 第二个密码

    class Else:
        add_device_text = "com.august.bennu:id/cannotScanBarcodeLink"  # can't scan the code?
        set_device_image = 'com.august.bennu:id/onboarding_choose_device_background'  # set device image
        enter_serial_number = "com.august.bennu:id/barcode_scan_manual_input_inner"  # enter serial number
        lock_start = 'com.august.bennu:id/setup_button_continue'  # start
        lock_continue = 'com.august.bennu:id/barcode_scan_manual_positive'  # continue
        lock_skip = 'com.august.bennu:id/installation_instruction_continue'  # skip
        lock_install = "//*[@text='START SETUP']"  # start setup
        choose_lock_name = 'com.august.bennu:id/choose_house_name_device'  # lock name
        add_new_house = '//*[@text="Add a New House"]/../android.widget.RadioButton'  # add a new house
        choose_house_continue = 'com.august.bennu:id/choose_house_continue'  # continue
        # choose_house_continue = "//*[@text='CONTINUE']"  # continue
        my_house_name = 'com.august.bennu:id/house_name'  # display house name
        instruction_link = 'com.august.bennu:id/installation_instruction_link'  # instruction link
        scan_smartlock_progress = 'com.august.bennu:id/smartlock_setup_progress_container'  # setup progress
        # select_existing_house = '//*[@resource-id="com.august.bennu:id/choose_house_houses_recycler"]/android.widget.LinearLayout[7]/android.widget.RadioButton'  # select_existing_house
        select_existing_house = '//*[@text="yaleGlobal Automation"]/../android.widget.RadioButton'  # select_existing_house
        house_new_name = 'com.august.bennu:id/choose_house_new_name'  # house new name
        install_skip = "//*[@text='SKIP']"  # skip

        find_many_locks = 'com.august.bennu:id/md_titleFrame'  # 找到多把锁弹窗
        cancel = "com.aaecosys.apac_panpan:id/md_buttonDefaultNegative"  # 取消
        sure_or_try = "com.aaecosys.apac_panpan:id/md_buttonDefaultPositive"  # 确定/试试
        add_lock_text = "//*[@text='安装智能锁']"  # 安装智能锁
        link_lock_text = "//*[@text='连接智能锁']"  # 连接智能锁
        scan_smartlock_close = 'com.aaecosys.apac_panpan:id/header_action_bar_button'  # 选择锁关闭按钮,点不了
        scan_smartlock_failure = "//*[@text='扫描失败']"  # 扫描失败
        default_house_name = 'Mi Casa'    # 默认住宅名Mi Casa
        default_lock_name = '前门'  # 锁默认命名前门
        house_new_image = 'com.aaecosys.apac_panpan:id/choose_house_new_image_overlay'  # 上传住宅图片按钮
        choose_house_image = '//*[@text="选择图片"]'   # 选择住宅图片
        take_house_photo = '//*[@text="拍一张照片"]'  # 拍一张照片
        image_from_phone = '//android.widget.ImageView[@resource-id="com.google.android.documentsui:id/icon_thumb"]'  # 选择相册第一张图片
        house_image_view = 'com.aaecosys.apac_panpan:id/choose_house_new_image'  # 显示图片
        all_house = '//*[@resource-id="com.aaecosys.apac_panpan:id/choose_house_houses_recycler"]/android.widget.LinearLayout[2]/android.widget.RadioButton'   # 已添加过的住宅
        # my_house_name = 'com.aaecosys.apac_panpan:id/house_activity_house_name'  # 显示我的住宅名称

        compound_lock_name = "//*[contains(@text, '{}')]"  # 锁的名称

        reject_open_bt = 'android:id/button2'   # 拒绝打开蓝牙
        allow_open_bt = 'android:id/button1'  # 允许打开蓝牙


class Lock:
    class Main:
        my_lock_name = 'com.august.bennu:id/lockman_lock_name_textview'  # my lock name
        lock_open_status = "//*[contains(@text, 'UNLOCKED')]"  # unlocked
        lock_close_status = "//*[contains(@text, 'LOCKED')]"   # locked
        lock_grey_status = "//*[contains(@text, 'COULD NOT CONNECT')]"   # COULD NOT CONNECT
        lock_connect_status = '//*[@text="CONNECTING…"]'  # CONNECTING
        user_lock_button = 'com.august.bennu:id/lockman_lock_button_lottie'  # lock manage
        activity_show = "//*[@content-desc='ble_lock']"  # ble_lock
        lock_manager = "//*[@content-desc='LOCK']"  # Lock manage
        lock_unknown_status = "//*[contains(@text, 'UNKNOWN')]"  # UNKNOWN






        low_battery_warn = '//*[@text="低电量警告"]'     # 低电量警告
        lock_home_button = 'com.aaecosys.apac_panpan:id/house_activity_dots_button'  # 开关锁_home按钮
        room_list = 'com.aaecosys.apac_panpan:id/keychain_recycleview'  # 房间列表
        lock = 'Lock Manager'  # 锁
        record = 'Activity Feed'  # 日志
        user = 'Guest List'  # 用户
        setting = 'House Settings'  # 设定
        lottie = 'com.aaecosys.apac_panpan:id/lockman_lock_button_lottie'  # 开关锁按键
        feed_title = 'com.aaecosys.apac_panpan:id/activity_feed_title'  # 日志页活动标题
        locked = '锁关闭'
        unlocked = '锁开启'
        disconnection = '无法连接\n点击重新连接'
        status_text = '//*[@resource-id="com.aaecosys.apac_panpan:id/lockman_lock_status_textview"]/android.widget.TextView'  # 锁状态的文案
        house_name = '//*[@resource-id="com.aaecosys.apac_panpan:id/keychain_recycleview"]/android.widget.FrameLayout[1]'
        network_popup = 'com.android.htmlviewer:id/auto_connect_slidingButton'   # 网络自动连接弹窗



    class Setting:
        house_setting = "com.august.bennu:id/settings_house_settings"  # house setting
        house_name = 'com.august.bennu:id/house_settings_name_textview'  # house name
        house_image_button = 'com.august.bennu:id/house_settings_house_image_button'  # house image
        edit_house_owner = 'com.august.bennu:id/settings_house_edit_owners'  # edit house owner
        admin_permission_check = "//*[@text='{}']/../android.widget.CheckBox"  # admin permission check
        lock_icon = 'com.august.bennu:id/cell_settings_device_icon'  # lock icon






        # setting_button = "//*[@content-desc='House Settings']"  # 设置按钮
        setting_button = "//*[@content-desc='SETTINGS']"  # 设置按钮
        lock_setting = "//*[@text='锁设置']"  # 锁设置
        house_name_image = "//*[@text='房屋名称和照片']"  # 房屋名称和照片
        back_button = 'com.aaecosys.apac_panpan:id/house_settings_action_bar_button'  # 返回按钮
        lock_name = 'com.aaecosys.apac_panpan:id/cell_settings_name'  # 锁名字
        house_name_edit = 'com.aaecosys.apac_panpan:id/entry_field'  # 房屋名字输入框
        save_button = 'com.aaecosys.apac_panpan:id/md_buttonDefaultPositive'  # 保存按钮
        cancel_button = 'com.aaecosys.apac_panpan:id/md_buttonDefaultNegative'   # 取消按钮
        alert_house_name = 'com.aaecosys.apac_panpan:id/snackbar_text'  # 提示控件
        alert_name_notnull = '房间名字不能为空'  # 提示房间名字不能为空
        alert_wifi_close = '获取设置时出错'  # 提示获取设置时出错
        save_name = 'com.aaecosys.apac_panpan:id/house_settings_action_bar_right_button'  # 保存修改
        upload_house_image = "//*[@text='上传图片中']"  # 上传图片中
        upload_image_failure = "//*[@text='上传失败']"   # 上传失败
        # another_phone_image = '//*[@resource-id="com.android.documentsui:id/dir_list"]/android.widget.RelativeLayout[3]'  # 选择相册第三张图片
        another_phone_image = '//*[@resource-id="com.google.android.documentsui:id/dir_list"]/android.widget.LinearLayout[3]'  # 选择相册第三张图片
        show_house_owner = 'com.aaecosys.apac_panpan:id/cell_edit_house_check'  # 显示房屋管理员
        user_list = '//android.widget.TextView[@resource-id="com.aaecosys.apac_panpan:id/cell_edit_house_user_name"]' # 显示房屋管理员和普通用户
        owner_back_button = 'com.aaecosys.apac_panpan:id/edit_owners_action_bar_button'   # 编辑房屋管理员返回键
        admin_permission_name = "//*[@text='{}']/.."  # 登录的管理员
        scroll_house_owner = 'com.aaecosys.apac_panpan:id/edit_owners_recycler'   # 滑动房屋管理员

    class User:
        invite = 'com.aaecosys.apac_panpan:id/invite_user_imageview'  # 邀请
        invite_user = 'com.aaecosys.apac_panpan:id/full_access_material_button'  # 邀请用户
        invite_tmp = 'com.aaecosys.apac_panpan:id/entry_code_material_button'  # 访客
        cancel = 'android:id/button3'  # 取消

    class LockSetting:
        assert_bt_not_open = "//*[contains(@text, 'BLUETOOTH DISABLED')]"  # BLUETOOTH DISABLED
        lock_name_show = "//*[@resource-id='com.august.bennu:id/lock_settings_name']//*[@resource-id='com.august.bennu:id/ripple_title_value_value']"
        alarm = 'com.august.bennu:id/lock_settings_active_monitoring_container'  # smart alert
        reset = 'com.august.bennu:id/lock_settings_reset_container'  # reset
        instructions = 'com.august.bennu:id/lock_settings_install_container'  # install instructions
        troubleshooting = 'com.august.bennu:id/lock_settings_troubleshoot'    # troubleshooting help
        device_info = "//*[@resource-id='com.august.bennu:id/lock_settings_information_group']//*[@resource-id='com.august.bennu:id/lock_settings_device_info']"  # 设备信息
        module_serial = "//*[@text='Module Serial Number']/../android.widget.TextView[2]"  # module Serial Number
        module_version = "//*[@text='Module Firmware Version']/../android.widget.TextView[2]"  # Module Firmware Version
        scroll_view = "//*[@class='android.widget.ScrollView']"
        lock_model = "//*[@text='Host Lock Serial Number']/../android.widget.TextView[2]"  # lock model
        lock_firmware_version = "//*[@text='Host Lock Firmware Version']/../android.widget.TextView[2]" # lock firmware version
        reset_button = 'com.august.bennu:id/factory_reset_button'  # reset button
        reset_confirm = 'com.august.bennu:id/md_buttonDefaultPositive'  # confirm

        loading = 'com.aaecosys.apac_panpan:id/md_title'  # 刷新中
        # connection_error = "//*[@text='连接故障']"  # 连接故障
        connection_error = "//*[@text='锁的连接需要']"  # 连接故障
        # wifi_lost = "//*[@text='似乎已断开与互联网的连接']"  # 似乎已断开与互联网的连接
        wifi_lost = "//*[contains(@text, '无法建立安全连接')]"   # 无法建立安全连接
        cancel_connect = 'com.aaecosys.apac_panpan:id/md_buttonDefaultNeutral'  # 取消
        lock_name = 'com.aaecosys.apac_panpan:id/lock_settings_name'  # 锁的名字
        lock_name_edit = 'com.aaecosys.apac_panpan:id/entry_field'  # 锁名字输入框
        alert_error = 'com.aaecosys.apac_panpan:id/snackbar_text'  # 错误提示
        header_back_button = 'com.aaecosys.apac_panpan:id/header_back_button_ripple'   # 左上角返回
        add_alert_button = 'com.aaecosys.apac_panpan:id/add_smart_alerts'   # 添加智能提醒按钮
        add_alert_back = 'com.aaecosys.apac_panpan:id/header_action_bar_button'    # 返回按钮
        choose_event = 'com.aaecosys.apac_panpan:id/event_chooser'  # 选择活动
        alert_save_button = 'com.aaecosys.apac_panpan:id/save_button'   # 保存按钮
        choose_time = 'com.aaecosys.apac_panpan:id/time_chooser'  # 选择时间
        lock_by_special = 'com.aaecosys.apac_panpan:id/cus_textview'   # 门由特定用户锁定或解锁
        choose_alert_done = 'com.aaecosys.apac_panpan:id/choose_smartalert_done'  # 完成
        visitor_not_show = "//*[@text='访客']"  # 访客
        lock_by_someone = "//*[contains(@text, '{}关锁或解锁']"  # 门由特定用户锁定或解锁
        delete_button = 'com.aaecosys.apac_panpan:id/delete_button'    # 取消按钮
        any_time = "//*[@text='任何时候']"  # 任何时候
        fixed_time = "//*[@text='在一个时间范围内']"  # 在一个时间范围内
        smart_alert_text = 'com.aaecosys.apac_panpan:id/smart_alert_cell_text'  # 智能提醒内容
        specific_smart_alert = "//*[contains(@text, '当{}\n锁门或将门解锁时提醒我')]"  # 当{}锁门或将门解锁时提醒我
        auto_lock = 'com.aaecosys.apac_panpan:id/lock_settings_reloreck_container'  # 自动上锁
        auto_lock_switch = 'com.aaecosys.apac_panpan:id/re_lock_settings_switch'  # 自动上锁开关
        switch_save = 'com.aaecosys.apac_panpan:id/re_lock_settings_save'   # 自动上锁开关保存
        auto_lock_status =  "//*[@resource-id='com.aaecosys.apac_panpan:id/lock_settings_reloreck_container']//*[@resource-id='com.aaecosys.apac_panpan:id/ripple_title_value_value']"
        reset_cancel = 'com.aaecosys.apac_panpan:id/md_buttonDefaultNegative'  # 取消

        reset_cleaning = 'com.aaecosys.apac_panpan:id/md_content'  # 清除数据
        device_volume = 'com.aaecosys.apac_panpan:id/lock_settings_unity_device'  # 设备设置
        volume_setting = 'com.aaecosys.apac_panpan:id/ripple_title_value_container'  # 音量设置
        default_volume = 'com.aaecosys.apac_panpan:id/ripple_title_value_value'  # 默认音量高
        volume_high = "//*[@text='高']"  # 音量高
        volume_low = "//*[@text='低']"  # 音量低
        volume_silent = "//*[@text='静音']"  # 静音
        alert_update_volume = 'com.aaecosys.apac_panpan:id/snackbar_text'  # 提示更新音量
        lock_icon = 'com.aaecosys.apac_panpan:id/cell_settings_device_icon'  # 锁设置icon


class UserManagement:
    class Main:
        house_name = '//*[@resource-id="com.august.bennu:id/keychain_recycleview"]/android.widget.FrameLayout[1]'  # house list
        user_tab = '//*[@content-desc="INVITE"]'  # invite
        # user_admin = "//*[@resource-id='com.august.bennu:id/cell_guest_list']//*[@text='{}']"  # admin name
        # user_admin = "//*[@resource-id='com.august.bennu:id/guest_list_recycler']//android.view.ViewGroup//android.widget.TextView"  # admin name
        setting_tab = '//*[@content-desc="SETTINGS"]'  # settings tab
        log_tab = '//*[@content-desc="ACTIVITY"]'  # activity tab
        user_admin = "//*[@resource-id='com.august.bennu:id/guest_list_recycler']//*[@text='{}']"  # admin name
        back_bar_button = 'com.august.bennu:id/manage_access_action_bar_button'  # BACK
        pop_up = 'com.august.bennu:id/md_title'   # prompt


        home_tab = '//*[@content-desc="LOCK"]'  # 开关锁tab
        user_list = '//*[@content-desc="INVITE"]'  # 用户列表
        my_user_name = '//*[@text="automation shen"]'  # 我
        admin_name = ''  # 管理员列表名字
        user_card = "//*[@text='门卡']"  # 门卡
        add_user_card = 'com.aaecosys.apac_panpan:id/add_credential_next'  # 点击添加门卡
        already_add = 'com.aaecosys.apac_panpan:id/remove_credential_hardware_profile_text'  # 门卡/指纹已添加
        user_fingerprint = "//*[@text='指纹']"  # 指纹
        add_user_fingerprint = 'com.aaecosys.apac_panpan:id/add_credential_next'  # 点击添加指纹
        card_already_add = 'com.aaecosys.apac_panpan:id/remove_credential_hardware_profile_text'  # 门卡已添加

        # user_admin = "//*[@resource-id='com.aaecosys.apac_panpan:id/cell_guest_list']//*[@text='{}']"  # 管理员
        user_normal = "//*[@resource-id='com.aaecosys.apac_panpan:id/cell_guest_list']//*[@text='{}']"  # 普通用户
        user_guest = "//*[@resource-id='com.aaecosys.apac_panpan:id/cell_guest_list']//*[@text='{}']"  # 访客
        user_invite = "//*[@resource-id='com.aaecosys.apac_panpan:id/guest_list_recycler_invert']//*[@text='{}']"  # 邀请
        user_password = "//*[@text='密码']"  # 密码
        remove_card = 'com.aaecosys.apac_panpan:id/remove_credential'  # 删除门卡
        password_input = 'com.aaecosys.apac_panpan:id/manage_pin_value'  # 输入密码
        password_save = 'com.aaecosys.apac_panpan:id/manage_pin_save'  # 保存密码
        save_update = 'com.aaecosys.apac_panpan:id/manage_access_action_bar_right_button'  # 保存更改
        delete_user = 'com.august.bennu:id/manage_access_delete_user_button'  # 删除用户

    class Invite:
        invite = 'com.august.bennu:id/invite_user_imageview'  # invite user
        full_access = 'com.august.bennu:id/full_access_material_button'   # full access
        RFID_access = 'com.august.bennu:id/rfid_material_button'   # RFID access
        select_phone = 'com.august.bennu:id/select_contact_new_contact_button'  # invite via mobile
        area_number = 'com.august.bennu:id/create_contact_phone_container_country_code_text'  # country code
        country_search = 'com.august.bennu:id/country_picker_search'  # country search
        area_name = 'com.august.bennu:id/row_title'  # country name
        phone_input = 'com.august.bennu:id/create_contact_phone_container_phone_entry'  # phone input
        first_name = 'com.august.bennu:id/create_contact_user_first_name'  # first name
        last_name = 'com.august.bennu:id/create_contact_user_last_name'  # last name
        send_button = 'com.august.bennu:id/invite_person_action_bar_right_button'  # send button
        add_yourself = '//*[@text="You can\'t add yourself"]'  # You can't add yourself
        invite_cancel = 'android:id/button3'  # ok on error message
        has_access = '//*[@text="User already has access"]'  # User already has access
        user_invited = '//*[@text="INVITED"]'  # INVITED
        select_contact = 'com.august.bennu:id/select_contact_choose_contact_button'  # invite via contact list
        registered_contact = '//*[@text="Philadelphia She"]'  # registered contact
        invited_contact = '//*[@text="多手机号"]'  # registered multiple contact
        multiple_contact = '//*[@text="多手机号"]'  # 多手机号
        not_registered = '//*[@text="not registered"]'  # not registered
        access_set = 'com.august.bennu:id/cell_access_control_access_level_level_textview'  # access change
        owner_access = 'com.august.bennu:id/access_level_owner_button'  # owner access
        back_button = 'com.august.bennu:id/invite_person_action_bar_button'  # back arrow
        confirm = 'com.august.bennu:id/md_buttonDefaultPositive'  # confirm
        general_access = 'com.august.bennu:id/access_level_guest_button'  # guest access
        schedule_always = 'com.august.bennu:id/access_schedule_always_cell'  # always
        schedule_recurring = 'com.august.bennu:id/access_schedule_recurring_cell'  # recurring
        schedule_temp = 'com.august.bennu:id/access_schedule_temp_cell'  # temporary
        contact_continue = 'com.august.bennu:id/select_credential_contact_continue'  # contact continue
        lock_list = 'com.august.bennu:id/choose_lock_list'  # lock list
        lock_name = 'com.august.bennu:id/lock_name'  # lock name
        lock_checked = 'com.august.bennu:id/lock_checked_state'  # lock checked
        lock_continue = 'com.august.bennu:id/select_credential_lock_continue'  # lock continue
        wait_connection = '//*[@text="Waiting for lock connection"]'  # Waiting for lock connection
        card_continue = 'com.august.bennu:id/register_credential_positive_button'   # continue
        start_RFID_registration = '//*[@text="Start Contactless Key Registration"]'  # Start Contactless Key Registration
        start_FP_registration = '//*[@text="Start Fingerprint Registration"]'  # Start Fingerprint Registration

        registration_timeout = '//*[@text="Registration timed out"]'  # Registration timed out
        register_success = "//*[contains(@text, 'Successfully registered')]"  # Successfully registered Contactless Key or fingerprint
        register_sub_message = "//*[contains(@text, '')]"    # display lock name
        register_failure = "//*[contains(@text, 'Could not register')]"   # Could not register Contactless Key or fingerprint
        register_finish = '//*[@text="FINISH"]'  # FINISH
        RFID_tab = 'com.august.bennu:id/cell_access_control_rfid_container'  # RFID_tab
        lock_connection = '//*[@text="Waiting for lock connection"]'  # Waiting for lock connection
        # not_registered_status = '//*[@text="NOT REGISTERED"]'  # NOT REGISTERED
        RFID_registered_value = 'com.august.bennu:id/cell_access_control_rfid_value'  # REGISTERED
        FP_registered_value = 'com.august.bennu:id/cell_access_control_finger_print_value'  # REGISTERED
        access_always = '//*[@text="ALWAYS"]'  # ALWAYS
        RFID_access_level = '//*[@text="CONTACTLESS KEY ONLY"]'  # CONTACTLESS KEY ONLY
        registered_info = 'com.august.bennu:id/unregister_credential_status'  # registered
        unregister_button = 'com.august.bennu:id/unregister_credential_text_button'  # unregister button
        right_close = 'com.august.bennu:id/header_right_close'  # right close
        RFID_first_name = 'com.august.bennu:id/user_name_first_name_field'  # first name
        RFID_last_name = 'com.august.bennu:id/user_name_last_name_field'  # last name
        pincode_set = 'com.august.bennu:id/cell_access_control_pin_code_container'  # set pincode
        pin_input = 'com.august.bennu:id/manage_pin_value'  # pincode
        change_pin = 'com.august.bennu:id/manage_pin_save'  # save change
        PINCODE_access = 'com.august.bennu:id/entry_code_material_button'  # PINCODE access
        PINCODE_continue = 'com.august.bennu:id/pin_code_continue'   # PINCODE continue
        create_pin = 'com.august.bennu:id/create_pin_entry_code_container'  # create pincode
        save_pin = 'com.august.bennu:id/create_pin_save'  # save pin
        pin_schedule = 'com.august.bennu:id/create_pin_access_schedule_container'  # pin schedule
        recur_sunday = 'com.august.bennu:id/access_schedule_rec_sunday_button'
        recur_monday = 'com.august.bennu:id/access_schedule_rec_monday_button'
        recur_tuesday = 'com.august.bennu:id/access_schedule_rec_tuesday_button'
        recur_wednesday = 'com.august.bennu:id/access_schedule_rec_wednesday_button'
        recur_thursday = 'com.august.bennu:id/access_schedule_rec_thursday_button'
        recur_friday = 'com.august.bennu:id/access_schedule_rec_friday_button'
        recur_saturday = 'com.august.bennu:id/access_schedule_rec_saturday_button'
        guest_back_button = '//*[@content-desc="Navigate up"]'  # back
        only_user_schedule = 'com.august.bennu:id/cell_access_control_access_schedule_layout'  # schedule
        pin_code = 'com.august.bennu:id/cell_access_control_pin_code_value'
        only_access_level = 'com.august.bennu:id/cell_access_control_access_level_layout'   # access level
        FP_access = 'com.august.bennu:id/fingerprint_material_button'   # FP access













        invite_user = 'com.aaecosys.apac_panpan:id/full_access_material_button'  # 邀请管理员或普通用户
        invite_guest = 'com.aaecosys.apac_panpan:id/entry_code_material_button'  # 邀请访客
        user_access = 'com.aaecosys.apac_panpan:id/cell_access_control_access_level_layout'  # 用户权限等级

        none_access = 'com.aaecosys.apac_panpan:id/access_level_none_button'  # 无权限
        error_prompt = 'com.aaecosys.apac_panpan:id/snackbar_text'  # 底部弹出的错误提示框
        continue_button = 'com.aaecosys.apac_panpan:id/pin_code_continue'  # 邀请访客页继续按键
        guest_first_name = 'com.aaecosys.apac_panpan:id/user_name_first_name_field'  # 访客的名
        guest_last_name = 'com.aaecosys.apac_panpan:id/user_name_last_name_field'  # 访客的姓
        guest_contact1 = 'com.aaecosys.apac_panpan:id/select_contact_choose_contact_button'  # 通过通讯录邀请访客





        cancel = 'com.aaecosys.apac_panpan:id/md_buttonDefaultNegative'  # 取消

        invalid_contact = '//*[@text="非法手机号"]'  # 非法手机号
        blank_contact = '//*[@text="无手机号"]'  # 无手机号

        unregistered_contact = '//*[@text="未注册手机号"]'  # 未注册手机号

        send_text = 'com.aaecosys.apac_panpan:id/create_pin_user_send_text_switch'  # 向访客发送短信
        popup_confirm = 'com.aaecosys.apac_panpan:id/md_buttonDefaultPositive'  # 弹框的确定按键
        popup_cancel = 'com.aaecosys.apac_panpan:id/md_buttonDefaultNegative'  # 弹框的取消按键

    class Edit:
        schedule_set = 'com.august.bennu:id/cell_access_control_access_schedule_value'   # schedule set
        right_button = 'com.august.bennu:id/manage_access_action_bar_right_button'  # right button
        rec_start_time = 'com.august.bennu:id/access_schedule_rec_start_time'    # rec start time
        rec_end_time = 'com.august.bennu:id/access_schedule_rec_end_time'  # rec end time
        disable_pin = 'com.august.bennu:id/manage_pin_deactivate'  # disable pin
        delete_button = 'com.august.bennu:id/manage_access_delete_user_button'  # delete user




        pin_set = 'com.aaecosys.apac_panpan:id/cell_access_control_pin_code_value'  # 我的权限页/访客详情页密码设置


        pin_input = 'com.aaecosys.apac_panpan:id/manage_pin_value'  # 用户密码输入框
        save_pin = 'com.aaecosys.apac_panpan:id/manage_pin_save'  # 用户密码保存按键

         # 使用时间设置/访客详情页使用时间
        card_set = 'com.aaecosys.apac_panpan:id/cell_access_control_card_value'  # 门卡设置
        finger_set = 'com.aaecosys.apac_panpan:id/cell_access_control_fingerprint_value'  # 指纹设置
        back_button = 'com.aaecosys.apac_panpan:id/manage_access_action_bar_button'  # 返回按键
        manage_access_button = 'com.aaecosys.apac_panpan:id/manage_access_action_bar_right_button'  # 保存权限修改
        guest_name = 'com.aaecosys.apac_panpan:id/access_control_user_name'  # 访客姓名
        guest_first_name = 'com.aaecosys.apac_panpan:id/user_name_first_name_field'  # 访客名
        guest_last_name = 'com.aaecosys.apac_panpan:id/user_name_last_name_field'  # 访客姓


class Notification:
    class Main:
        alert_open_bt = "//*[contains(@text, '开锁')]"  # 蓝牙开门
        alert_close_bt = "//*[contains(@text, '锁上')]"  # 蓝牙关门
        alert_open_card = "//*[contains(@text, '门卡 开')]"  # 门卡开门
        alert_open_finger = "//*[contains(@text, '指纹 开')]"  # 指纹开门
        clear_button = "com.android.systemui:id/dismiss_view"  # 清除所有通知
        fix_time_mo = 'com.aaecosys.apac_panpan:id/mo_day_picker'  # 周一
        fix_time_tu = 'com.aaecosys.apac_panpan:id/tu_day_picker'  # 周二
        fix_time_we = 'com.aaecosys.apac_panpan:id/we_day_picker'  # 周三
        fix_time_th = 'com.aaecosys.apac_panpan:id/th_day_picker'  # 周四
        fix_time_fr = 'com.aaecosys.apac_panpan:id/fr_day_picker'  # 周五
        fix_time_sa = 'com.aaecosys.apac_panpan:id/sa_day_picker'  # 周六
        fix_time_su = 'com.aaecosys.apac_panpan:id/su_day_picker'  # 周日
        smartalert_start_time = 'com.aaecosys.apac_panpan:id/smartalert_start_time'   # 开始时间
        smartalert_end_time = 'com.aaecosys.apac_panpan:id/smartalert_end_time'  # 结束时间
        fix_time_1 = '//*[@content-desc="1"]'  # 1点
        fix_time_23 = '//*[@content-desc="11"]'  # 24点
        fix_time_55 = '//*[@content-desc="55"]'  # 55分
        confirm_time = 'android:id/button1'   # 确定
        notification_panel = 'com.android.systemui:id/notification_stack_scroller'   # 通知面板
