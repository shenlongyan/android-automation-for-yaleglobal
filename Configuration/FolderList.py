import os
import time


# 创建报告文件夹
# report_folder = '\\\\CNSHBSFS01\\Data\\APAC-Innovation Processes\\Public\\APAC_QA\\Luffy\\Result_Report''
jenkins_path = os.environ['WORKSPACE']
report_folder = jenkins_path + '\\Report'
if not os.path.exists(report_folder):
    os.mkdir(report_folder)
else:
    pass

"""本地需要按照时间戳分类测试文件时使用"""
# 创建测试文件夹
folder_name = time.strftime('%Y%m%d%H%M')
path = report_folder + '\\{}\\'.format(folder_name)
if not os.path.exists(path):
    os.makedirs(path)
else:
    pass
report_dir = path + 'TestReport.html'.format(folder_name)
output_path = path
logging_name = path + 'logging.log'


"""Jenkins上的测试报告路径无法使用变量"""
# Jenkins
# report_dir = report_folder + '\\TestReport.html'
# output_path = report_folder + '\\'
# logging_name = report_folder + '\\logging.log'


# """移动testReport到workspace根目录下"""
# # shutil.move(report_dir, jenkins_path)
# shutil.copyfile(report_dir, jenkins_path)