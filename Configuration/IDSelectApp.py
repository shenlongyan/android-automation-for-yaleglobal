from AppName import YaleGlobal

app_select = 'YaleGlobal'

if app_select == 'YaleGlobal':
    register_main_invited = YaleGlobal.Register.Main.invited  # 收到邀请
    register_main_register = YaleGlobal.Register.Main.register  # 注册
    register_main_login = YaleGlobal.Register.Main.login  # 登录
    register_policy_privacy = YaleGlobal.Register.Policy.privacy  # 隐私政策
    register_policy_service = YaleGlobal.Register.Policy.service  # 服务条件
    register_policy_license = YaleGlobal.Register.Policy.license  # 终端用户许可协议
    register_policy_agree = YaleGlobal.Register.Policy.agree  # 我同意
    register_policy_link = YaleGlobal.Register.Policy.link  # 跳转的h5链接
    register_policy_back = YaleGlobal.Register.Policy.back  # h5链接返回按钮
    register_invited_image = YaleGlobal.Register.Invited.image  # 邀请图片
    register_invited_back = YaleGlobal.Register.Invited.invite_back  # 邀请图片返回按钮
    register_register_start = YaleGlobal.Register.Register.start  # 开始
    register_register_first_name = YaleGlobal.Register.Register.first_name  # 名
    register_register_last_name = YaleGlobal.Register.Register.last_name  # 姓
    register_register_mail = YaleGlobal.Register.Register.mail  # 邮箱
    register_register_phone_area = YaleGlobal.Register.Register.phone_area  # 手机区号
    register_register_phone_area_text = YaleGlobal.Register.Register.phone_area_default  # 显示+86
    register_register_phone_area_default = YaleGlobal.Register.Register.area_number_default  # 手机区号默认中国
    register_phone_area_select = YaleGlobal.Register.Register.phone_area_select  # 选择Afghanistan
    register_phone_area_search = YaleGlobal.Register.Register.phone_area_search  # 搜索国家区号
    register_phone_area_search_result = YaleGlobal.Register.Register.phone_area_search_result  # 搜索国家区号结果
    register_phone_area_search_match = YaleGlobal.Register.Register.phone_area_search_match  # 匹配搜索结果
    register_register_phone = YaleGlobal.Register.Register.phone  # 手机号
    register_register_password = YaleGlobal.Register.Register.password  # 密码
    register_register_password_visible = YaleGlobal.Register.Register.password_visible  # 可见/不可见
    register_register_next = YaleGlobal.Register.Register.next  # 下一步
    register_register_alert_firstname = YaleGlobal.Register.Register.assert_alert_firstname  # 名为空
    register_register_alert_first_name = YaleGlobal.Register.Register.assert_alert_first_name  # 名为空提示
    register_register_alert_lastname = YaleGlobal.Register.Register.assert_alert_lastname  # 姓为空
    register_register_alert_last_name = YaleGlobal.Register.Register. assert_alert_last_name # 姓为空提示
    register_register_alert_email = YaleGlobal.Register.Register. assert_alert_email # 邮箱非法
    register_register_alert_email_error = YaleGlobal.Register.Register.assert_alert_email_error  # 邮箱非法提示
    register_register_assert_password_strength = YaleGlobal.Register.Register.assert_password_strength  # 密码非法提示条
    register_register_assert_alert_password = YaleGlobal.Register.Register.assert_alert_password  # 密码非法提示
    register_userIcon_skip = YaleGlobal.Register.UserIcon.skip  # 跳过这步
    register_userIcon_select_from_album = YaleGlobal.Register.UserIcon.select_from_album  # 从图片中选择
    register_userIcon_take_photo = YaleGlobal.Register.UserIcon.take_photo  # 拍照
    register_UserIcon_photo = YaleGlobal.Register.UserIcon.photo  # 安全照片整个页面
    register_UserIcon_select_image = YaleGlobal.Register.UserIcon.select_image  # 选择手机相册中第一张图片
    register_UserIcon_show_image = YaleGlobal.Register.UserIcon.show_image  # 显示选择的图片
    register_UserIcon_upload_image_next = YaleGlobal.Register.UserIcon.upload_image_next  # 上传头像下一步
    register_UserIcon_shutter_button = YaleGlobal.Register.UserIcon.shutter_button   # 调用手机拍照功能
    register_UserIcon_sure_button = YaleGlobal.Register.UserIcon.sure_button  # 确定上传头像
    register_verifyCode_code_input = YaleGlobal.Register.VerifyCode.code_input  # 验证码输入
    register_verifyCode_code_view = YaleGlobal.Register.VerifyCode.code_view  # 验证码
    register_verifyCode_no_code = YaleGlobal.Register.VerifyCode.no_code  # 未收到验证码
    register_verifyCode_change_phone = YaleGlobal.Register.VerifyCode.change_phone  # 更改电话
    register_verifyCode_send_again = YaleGlobal.Register.VerifyCode.send_again   # 再发一次
    register_verifyCode_change_phone_title = YaleGlobal.Register.VerifyCode.change_phone_title  # 更改电话标题
    register_verifyCode_assert_change_phone_title = YaleGlobal.Register.VerifyCode.assert_change_phone_title  # 更新电话号码提示
    register_verifyCode_cancel_change = YaleGlobal.Register.VerifyCode.cancel_change  # 取消更新电话号码
    register_verifyCode_change_phone_new = YaleGlobal.Register.VerifyCode.change_phone_new  # 更新电话号码
    register_verifyCode_change_phone_save = YaleGlobal.Register.VerifyCode.change_phone_save  # 更新电话号码保存
    register_verifyCode_send_again_title = YaleGlobal.Register.VerifyCode.send_again_title   # 重新发送手机验证码弹框
    register_verifyCode_assert_send_again_title = YaleGlobal.Register.VerifyCode.assert_send_again_title  # 重新发送手机验证码
    register_verifyCode_send_again_ok = YaleGlobal.Register.VerifyCode.send_again_ok  # 更改电话好的按钮
    register_verifyCode_code_page = YaleGlobal.Register.VerifyCode.code_page  # 验证码页面
    login_login_phone_input = YaleGlobal.Login.Login.phone_input  # 手机号
    login_login_mail_input = YaleGlobal.Login.Login.mail_input  # 邮箱
    login_login_password_input = YaleGlobal.Login.Login.password_input  # 密码
    login_login_back_button = YaleGlobal.Login.Login.back_button  # 登录页返回按钮
    login_login_type_switch = YaleGlobal.Login.Login.type_switch  # 切换登录方式
    login_login_type_mail = YaleGlobal.Login.Login.mail_text  # 邮箱登录文本
    login_login_type_phone = YaleGlobal.Login.Login.phone_text  # 手机登录文本
    login_login_submit = YaleGlobal.Login.Login.submit  # 登录
    login_button_sure = YaleGlobal.Login.Login.submit  # 登录(login函数中的登录button)
    login_login_forget_password = YaleGlobal.Login.Login.forget_password  # 忘记密码
    login_login_lost_phone = YaleGlobal.Login.Login.lost_phone  # 遗失手机
    login_login_lost_phone_submit = YaleGlobal.Login.Login.lost_phone_submit
    login_login_area_number = YaleGlobal.Login.Login.area_number  # 区号
    login_login_area_number_china = YaleGlobal.Login.Login.area_number_china  # 中国区号
    login_login_country_search = YaleGlobal.Login.Login.country_search  # 搜索国家/地区
    # login_login_area_name = YaleGlobal.Login.Login.area_name  # 国家/地区名称
    login_login_input_or_password_error = YaleGlobal.Login.Login.input_or_password_error  # 手机号/邮箱或密码错误
    login_login_phone_or_password_error_text = YaleGlobal.Login.Login.phone_or_password_error_text  # 手机号或密码错误
    login_login_mail_or_password_error_text = YaleGlobal.Login.Login.mail_or_password_error_text  # 电子邮件或密码错误
    login_login_mail_text_error = YaleGlobal.Login.Login.mail_text_error  # 请输入有效的邮箱地址
    login_login_password_show = YaleGlobal.Login.Login.password_show  # 密码可见按键
    login_forget_password_switch_mode = YaleGlobal.Login.ForgetPassword.switch_mode  # 切换验证方式
    login_forget_back_button = YaleGlobal.Login.ForgetPassword.back_button  # 忘记密码页返回按键
    login_forget_area_number = YaleGlobal.Login.ForgetPassword.area_number  # 忘记密码页区号
    login_forget_next = YaleGlobal.Login.ForgetPassword.next  # 忘记密码页继续按键
    login_forget_invalid_phone = YaleGlobal.Login.ForgetPassword.invalid_phone  # 无效的号码
    login_forget_invalid_mail_text = YaleGlobal.Login.ForgetPassword.invalid_mail_text  # 无效的邮箱
    login_forget_invalid_code = YaleGlobal.Login.ForgetPassword.invalid_code  # 错误的验证码
    login_forget_phone_input = YaleGlobal.Login.ForgetPassword.phone_input  # 忘记密码页手机号输入框
    login_forget_mail_input = YaleGlobal.Login.ForgetPassword.mail_input  # 忘记密码页邮箱输入框
    login_forget_code_input = YaleGlobal.Login.ForgetPassword.code_input  # 忘记密码页验证码输入框
    login_forget_resend_code = YaleGlobal.Login.ForgetPassword.resend_code  # 验证码重新发送
    login_forget_code_sent_text = YaleGlobal.Login.ForgetPassword.code_sent_text  # 验证码已发送
    login_forget_continue_button = YaleGlobal.Login.ForgetPassword.continue_button  # 忘记密码验证码页继续按键
    login_forget_new_password_create_input = YaleGlobal.Login.ForgetPassword.new_password_create_input  # 创建新密码输入框
    login_forget_new_password_confirm_input = YaleGlobal.Login.ForgetPassword.new_password_confirm_input  # 确认新密码输入框
    login_forget_new_password_close = YaleGlobal.Login.ForgetPassword.new_password_close  # 修改密码关闭按键
    login_forget_new_password_save = YaleGlobal.Login.ForgetPassword.new_password_save  # 修改密码保存按键
    login_verify_code_input = YaleGlobal.Login.Verify.code_input  # 验证码
    login_verify_submit = YaleGlobal.Login.Verify.submit  # 提交
    login_verify_no_code = YaleGlobal.Login.Verify.no_code  # 未收到验证码
    login_verify_resend = YaleGlobal.Login.Verify.resend  # 重新发送
    homePage_homePage_main = YaleGlobal.HomePage.HomePage.main  # 主页
    homePage_homePage_no_rooms = YaleGlobal.HomePage.HomePage.no_rooms  # 暂未添加任何设备





    homePage_homePage_add_device = YaleGlobal.HomePage.HomePage.add_device  # add device
    homePage_else_set_device_image = YaleGlobal.HomePage.Else.set_device_image  # set device image
    homePage_else_enter_serial_number = YaleGlobal.HomePage.Else.enter_serial_number  # enter_serial_number
    homePage_else_lock_continue = YaleGlobal.HomePage.Else.lock_continue  # continue
    homePage_else_lock_start = YaleGlobal.HomePage.Else.lock_start  # start
    homePage_else_lock_skip = YaleGlobal.HomePage.Else.lock_skip  # skip
    homePage_else_choose_lock_name = YaleGlobal.HomePage.Else.choose_lock_name  # lock name
    homePage_else_add_new_house = YaleGlobal.HomePage.Else.add_new_house  # add new house
    homePage_else_instruction_link = YaleGlobal.HomePage.Else.instruction_link  # instruction link
    homePage_else_select_existing_house = YaleGlobal.HomePage.Else.select_existing_house  # select_existing_house
    homePage_else_my_house_name = YaleGlobal.HomePage.Else.my_house_name  # my house name
    homePage_else_install_skip = YaleGlobal.HomePage.Else.install_skip  # skip




    homePage_else_add_lock_text = YaleGlobal.HomePage.Else.add_lock_text  # 安装智能锁提示
    homePage_else_link_lock_text = YaleGlobal.HomePage.Else.link_lock_text  # 连接智能锁提示
    homePage_else_lock_install = YaleGlobal.HomePage.Else.lock_install  # 开始安装
    homePage_else_scan_smartlock_progress = YaleGlobal.HomePage.Else.scan_smartlock_progress  # 扫描智能锁进度
    homePage_else_scan_smartlock_close = YaleGlobal.HomePage.Else.scan_smartlock_close   # 选择锁关闭按钮
    # homePage_else_add_lock_continue = YaleGlobal.HomePage.Else.add_lock_continue  # 继续

    homePage_else_scan_smartlock_failure = YaleGlobal.HomePage.Else.scan_smartlock_failure  # 扫描失败
    homePage_else_reject_open_bt = YaleGlobal.HomePage.Else.reject_open_bt  # 拒绝打开蓝牙
    homePage_else_allow_open_bt = YaleGlobal.HomePage.Else.allow_open_bt  # 允许打开蓝牙


    # homePage_else_choose_name_radiobutton = YaleGlobal.HomePage.Else.choose_name_radiobutton    # 添加一个新住宅
    homePage_else_choose_house_continue = YaleGlobal.HomePage.Else.choose_house_continue   # 添加新住宅继续按钮
    homePage_else_house_new_name = YaleGlobal.HomePage.Else.house_new_name    # 住宅名字输入框
    homePage_else_house_new_image = YaleGlobal.HomePage.Else.house_new_image   # 上传住宅图片按钮
    homePage_else_choose_house_image = YaleGlobal.HomePage.Else.choose_house_image   # 选择住宅图片
    homePage_else_image_from_phone = YaleGlobal.HomePage.Else.image_from_phone  # 选择相册第一张图片
    homePage_else_house_image_view = YaleGlobal.HomePage.Else.house_image_view  # 显示图片
    homePage_else_take_house_photo = YaleGlobal.HomePage.Else.take_house_photo  # 拍一张照片
    homePage_else_default_house_name = YaleGlobal.HomePage.Else.default_house_name  # 默认住宅名Mi Casa
    homePage_else_default_lock_name = YaleGlobal.HomePage.Else.default_lock_name  # 锁默认命名前门
    homePage_else_all_house = YaleGlobal.HomePage.Else.all_house  # 已添加过的住宅

    homePage_else_compound_lock_name = YaleGlobal.HomePage.Else.compound_lock_name  # 锁的名称
    homePage_else_find_many_locks = YaleGlobal.HomePage.Else.find_many_locks  # 找到多把锁
    homePage_homePage_menu_in_home_page = YaleGlobal.HomePage.HomePage.menu_in_home_page  # 菜单键
    homePage_homePage_menu_in_other_page = YaleGlobal.HomePage.HomePage.menu_in_other_page  # 菜单键
    homePage_homePage_home_list = YaleGlobal.HomePage.HomePage.home_list  # 家的列表
    homePage_homePage_house_list = YaleGlobal.HomePage.HomePage.house_list  # 家的列表
    homePage_homePage_empty_device = YaleGlobal.HomePage.HomePage.empty_device  # 家的列表



    homePage_menu_user_icon = YaleGlobal.HomePage.Menu.user_icon  # 用户信息
    homePage_menu_user_name = YaleGlobal.HomePage.Menu.user_name  # 用户名称
    homePage_menu_home = YaleGlobal.HomePage.Menu.home  # 首页
    homePage_menu_help = YaleGlobal.HomePage.Menu.help  # 帮助
    homePage_menu_help_text = YaleGlobal.HomePage.Menu.help_text  # 联系我们
    homePage_menu_add_device = YaleGlobal.HomePage.Menu.add_device  # 添加设备
    homePage_user_name = YaleGlobal.HomePage.User.name  # 修改名字
    homePage_user_mail = YaleGlobal.HomePage.User.mail  # 用户邮箱
    homePage_user_phone = YaleGlobal.HomePage.User.phone  # 用户手机
    homePage_user_edit = YaleGlobal.HomePage.User.edit  # 编辑
    homePage_user_logout = YaleGlobal.HomePage.User.logout  # 登出
    homePage_user_icon = YaleGlobal.HomePage.User.icon  # 头像区域
    homePage_use_name_modify_title = YaleGlobal.HomePage.UserName.name_modify_title  # 你叫什么名字
    homePage_userName_first_name = YaleGlobal.HomePage.UserName.first_name  # 名输入框
    homePage_userName_last_name = YaleGlobal.HomePage.UserName.last_name  # 姓输入框
    homePage_userIcon_modify_icon = YaleGlobal.HomePage.UserIcon.modify_icon  # 头像
    homePage_userIcon_open_camera = YaleGlobal.HomePage.UserIcon.open_camera  # 拍一张照片
    homePage_userIcon_take_photo = YaleGlobal.HomePage.UserIcon.take_photo  # 拍摄
    homePage_userIcon_take_photo_done = YaleGlobal.HomePage.UserIcon.take_photo_done  # 完成
    homePage_userIcon_close_camera = YaleGlobal.HomePage.UserIcon.close_camera  # 关闭相机
    homePage_userIcon_select_from_album = YaleGlobal.HomePage.UserIcon.select_from_album  # 选择图片
    homePage_userIcon_select_first_photo = YaleGlobal.HomePage.UserIcon.select_first_photo  # 第一个图片
    homePage_userIcon_icon_updating = YaleGlobal.HomePage.UserIcon.icon_updating  # 上传图片中
    homePage_edit_account = YaleGlobal.HomePage.Edit.account  # 管理账户
    homePage_edit_change_password = YaleGlobal.HomePage.Edit.change_password  # 修改密码
    homePage_edit_change_password_close = YaleGlobal.HomePage.Edit.change_password_close  # 关闭
    homePage_edit_change_password_save = YaleGlobal.HomePage.Edit.change_password_save  # 保存
    homePage_edit_password_first = YaleGlobal.HomePage.Edit.password_first  # 第一个密码
    homePage_edit_password_second = YaleGlobal.HomePage.Edit.password_second  # 第二个密码
    homePage_else_add_device_text = YaleGlobal.HomePage.Else.add_device_text  # 智能设备
    homePage_else_cancel = YaleGlobal.HomePage.Else.cancel  # 取消
    homePage_else_sure_or_try = YaleGlobal.HomePage.Else.sure_or_try  # 确定/试试
    lock_main_lock = YaleGlobal.Lock.Main.lock  # 锁
    lock_main_record = YaleGlobal.Lock.Main.record  # 日志
    lock_main_user = YaleGlobal.Lock.Main.user  # 用户
    lock_main_setting = YaleGlobal.Lock.Main.setting  # 设定
    lock_main_lottie = YaleGlobal.Lock.Main.lottie  # 开关锁按键
    lock_main_locked = YaleGlobal.Lock.Main.locked  # 锁关闭
    lock_main_unlocked = YaleGlobal.Lock.Main.unlocked  # 锁开启
    lock_main_disconnection = YaleGlobal.Lock.Main.disconnection  # 锁失去连接
    lock_main_status_text = YaleGlobal.Lock.Main.status_text  # 锁状态的文案
    lock_main_house_name = YaleGlobal.Lock.Main.house_name  # 房间列表房间名称
    lock_main_feed_title = YaleGlobal.Lock.Main.feed_title  # 日志页活动标题
    lock_setting_house_setting = YaleGlobal.Lock.Setting.house_setting  # 房间设定
    lock_setting_lock_setting = YaleGlobal.Lock.Setting.lock_setting  # 锁的设置
    lock_user_invite = YaleGlobal.Lock.User.invite  # 邀请
    lock_user_invite_user = YaleGlobal.Lock.User.invite_user  # 邀请用户
    lock_user_invite_tmp = YaleGlobal.Lock.User.invite_tmp  # 访客
    lock_user_cancel = YaleGlobal.Lock.User.cancel  # 取消
    lock_lockSetting_loading = YaleGlobal.Lock.LockSetting.loading  # 刷新中
    lock_lockSetting_connection_error = YaleGlobal.Lock.LockSetting.connection_error  # 连接故障
    lock_lockSetting_cancel_connect = YaleGlobal.Lock.LockSetting.cancel_connect  # 取消连接
    lock_lockSetting_lock_name = YaleGlobal.Lock.LockSetting.lock_name  # 锁的名字
    lock_lockSetting_lock_name_edit = YaleGlobal.Lock.LockSetting.lock_name_edit  # 编辑锁的名字
    lock_lockSetting_lock_name_show = YaleGlobal.Lock.LockSetting.lock_name_show  # 显示锁的名字
    lock_lockSetting_alert_error = YaleGlobal.Lock.LockSetting.alert_error  # 错误提示
    lock_lockSetting_header_back_button = YaleGlobal.Lock.LockSetting.header_back_button  # 左上角返回按钮
    lock_lockSetting_auto_lock_status = YaleGlobal.Lock.LockSetting.auto_lock_status  # 自动上锁开关状态
    lock_lockSetting_wifi_lost = YaleGlobal.Lock.LockSetting.wifi_lost  # 似乎已断开与互联网的连接
    lock_lockSetting_module_serial = YaleGlobal.Lock.LockSetting.module_serial  # 模块序列号
    lock_lockSetting_module_version = YaleGlobal.Lock.LockSetting.module_version  # 模块版本
    lock_lockSetting_lock_model = YaleGlobal.Lock.LockSetting.lock_model  # 锁的型号
    lock_lockSetting_lock_firmware_version = YaleGlobal.Lock.LockSetting.lock_firmware_version  # 锁的固件版本
    lock_lockSetting_scroll_view = YaleGlobal.Lock.LockSetting.scroll_view  # 滑动设备信息
    lock_lockSetting_alarm = YaleGlobal.Lock.LockSetting.alarm  # smart alert
    lock_lockSetting_instructions = YaleGlobal.Lock.LockSetting.instructions   # install instructions
    lock_lockSetting_troubleshooting = YaleGlobal.Lock.LockSetting.troubleshooting  # install instructions


    lock_lockSetting_add_alert_button = YaleGlobal.Lock.LockSetting.add_alert_button  # 智能提醒+按钮
    lock_lockSetting_add_alert_back = YaleGlobal.Lock.LockSetting.add_alert_back  # 返回按钮
    lock_lockSetting_choose_event = YaleGlobal.Lock.LockSetting.choose_event  # 点击选择活动
    lock_lockSetting_alert_save_button = YaleGlobal.Lock.LockSetting.alert_save_button  # 保存按钮
    lock_lockSetting_choose_time = YaleGlobal.Lock.LockSetting.choose_time  # 选择时间
    lock_lockSetting_lock_by_special = YaleGlobal.Lock.LockSetting.lock_by_special  # 门由特定用户锁定或解锁
    lock_lockSetting_choose_alert_done = YaleGlobal.Lock.LockSetting.choose_alert_done  # 完成
    lock_lockSetting_visitor_not_show = YaleGlobal.Lock.LockSetting.visitor_not_show  # 访客
    lock_lockSetting_lock_by_someone = YaleGlobal.Lock.LockSetting.lock_by_someone  # 门由特定用户锁定或解锁
    lock_lockSetting_delete_button = YaleGlobal.Lock.LockSetting.delete_button  # 取消按钮
    lock_lockSetting_any_time = YaleGlobal.Lock.LockSetting.any_time  # 任何时候
    lock_lockSetting_fixed_time = YaleGlobal.Lock.LockSetting.fixed_time   # 在一个时间范围内
    lock_lockSetting_smart_alert_text = YaleGlobal.Lock.LockSetting.smart_alert_text  # 智能提醒内容
    lock_lockSetting_specific_smart_alert = YaleGlobal.Lock.LockSetting.specific_smart_alert  # 当{}锁门或将门解锁时提醒我
    lock_lockSetting_reset = YaleGlobal.Lock.LockSetting.reset  # 出厂设置
    lock_lockSetting_reset_button = YaleGlobal.Lock.LockSetting.reset_button  # 恢复出厂设置
    lock_lockSetting_reset_cancel = YaleGlobal.Lock.LockSetting.reset_cancel  # 取消
    lock_lockSetting_reset_confirm = YaleGlobal.Lock.LockSetting.reset_confirm  # 重置
    lock_lockSetting_reset_cleaning = YaleGlobal.Lock.LockSetting.reset_cleaning  # 清除数据
    lock_lockSetting_device_volume = YaleGlobal.Lock.LockSetting.device_volume  # 设备设置
    lock_lockSetting_volume_setting = YaleGlobal.Lock.LockSetting.volume_setting  # 音量设置
    lock_lockSetting_default_volume = YaleGlobal.Lock.LockSetting.default_volume  # 默认音量
    lock_lockSetting_volume_high = YaleGlobal.Lock.LockSetting.volume_high  # 高音量
    lock_lockSetting_volume_low = YaleGlobal.Lock.LockSetting.volume_low  # 低音量
    lock_lockSetting_volume_silent = YaleGlobal.Lock.LockSetting.volume_silent  # 静音
    lock_lockSetting_alert_update_volume = YaleGlobal.Lock.LockSetting.alert_update_volume  # 提示更新音量
    lock_lockSetting_device_info = YaleGlobal.Lock.LockSetting.device_info  # 设备信息
    lock_lockSetting_lock_icon = YaleGlobal.Lock.LockSetting.lock_icon  # 锁设置icon
    lock_lockSetting_assert_bt_not_open = YaleGlobal.Lock.LockSetting.assert_bt_not_open  # 蓝牙未打开
    lock_lockSetting_auto_lock = YaleGlobal.Lock.LockSetting.auto_lock  # 自动重新上锁
    lock_lockSetting_auto_lock_switch = YaleGlobal.Lock.LockSetting.auto_lock_switch  # 自动上锁开关
    lock_lockSetting_switch_save = YaleGlobal.Lock.LockSetting.switch_save  # 自动上锁开关保存
    lock_main_lock_manager = YaleGlobal.Lock.Main.lock_manager  # 锁按钮
    lock_main_lock_grey_status = YaleGlobal.Lock.Main.lock_grey_status  # 锁无法连接
    lock_main_lock_open_status = YaleGlobal.Lock.Main.lock_open_status  # 锁开启
    lock_main_lock_close_status = YaleGlobal.Lock.Main.lock_close_status  # 锁关闭
    lock_main_activity_show = YaleGlobal.Lock.Main.activity_show  # log show

    lock_main_user_lock_button = YaleGlobal.Lock.Main.user_lock_button  # 锁
    lock_main_lock_connect_status = YaleGlobal.Lock.Main.lock_connect_status  # 连接中
    lock_main_lock_unknown_status = YaleGlobal.Lock.Main.lock_unknown_status  # 未知
    lock_main_low_battery_warn = YaleGlobal.Lock.Main.low_battery_warn  # 低电量警告
    lock_main_my_lock_name = YaleGlobal.Lock.Main.my_lock_name  # 我的锁名称
    lock_main_lock_home_button = YaleGlobal.Lock.Main.lock_home_button  # 开关锁_home按钮
    lock_main_room_list = YaleGlobal.Lock.Main.room_list  # 房间列表
    lock_main_network_popup = YaleGlobal.Lock.Main.network_popup  # 网络自动连接弹窗
    lock_setting_setting_button = YaleGlobal.Lock.Setting.setting_button   # 设置按钮
    lock_setting_house_name_image = YaleGlobal.Lock.Setting.house_name_image  # 房屋名称和照片
    lock_setting_house_name = YaleGlobal.Lock.Setting.house_name  # 房屋名称
    lock_setting_house_image_button = YaleGlobal.Lock.Setting.house_image_button  # 房屋照片按钮
    lock_setting_back_button = YaleGlobal.Lock.Setting.back_button  # 返回按钮
    lock_setting_lock_name = YaleGlobal.Lock.Setting.lock_name  # 锁名字
    lock_setting_lock_icon = YaleGlobal.Lock.Setting.lock_icon  # 锁图标
    lock_setting_house_name_edit = YaleGlobal.Lock.Setting.house_name_edit  # 房屋名字输入框
    lock_setting_save_button = YaleGlobal.Lock.Setting.save_button  # 保存按钮
    lock_setting_cancel_button = YaleGlobal.Lock.Setting.cancel_button  # 取消按钮
    lock_setting_alert_house_name = YaleGlobal.Lock.Setting.alert_house_name  # 提示控件
    lock_setting_alert_name_notnull = YaleGlobal.Lock.Setting.alert_name_notnull  # 提示房间名字不能为空
    lock_setting_alert_wifi_close = YaleGlobal.Lock.Setting.alert_wifi_close   # 提示获取设置时出错
    lock_setting_save_name = YaleGlobal.Lock.Setting.save_name  # 保存修改
    lock_setting_upload_house_image = YaleGlobal.Lock.Setting.upload_house_image  # 上传图片中
    lock_setting_upload_image_failure = YaleGlobal.Lock.Setting.upload_image_failure  # 上传失败
    lock_setting_edit_house_owner = YaleGlobal.Lock.Setting.edit_house_owner  # 编辑房屋管理员
    lock_setting_show_house_owner = YaleGlobal.Lock.Setting.show_house_owner  # 显示房屋管理员
    lock_setting_user_list = YaleGlobal.Lock.Setting.user_list  # 显示房屋管理员和普通用户
    lock_setting_owner_back_button = YaleGlobal.Lock.Setting.owner_back_button   # 编辑房屋管理员返回键
    lock_setting_admin_permission_check = YaleGlobal.Lock.Setting.admin_permission_check   # 管理员权限勾选框
    lock_setting_admin_permission_name = YaleGlobal.Lock.Setting.admin_permission_name  # 登录的房屋管理员
    lock_setting_admin_scroll_house_owner = YaleGlobal.Lock.Setting.scroll_house_owner  # 滑动房屋管理员

    UserManagement_user_list = YaleGlobal.UserManagement.Main.user_list  # 我
    UserManagement_my_user_name = YaleGlobal.UserManagement.Main.my_user_name  # 用户列表
    UserManagement_admin_name = YaleGlobal.UserManagement.Main.admin_name  # 管理员列表名字
    lock_setting_another_phone_image = YaleGlobal.Lock.Setting.another_phone_image  # 选择相册第三张图片
    login_login_create_account = YaleGlobal.Login.Login.create_account  # 创建账号
    UserManagement_user_card = YaleGlobal.UserManagement.Main.user_card  # 门卡
    UserManagement_add_user_card = YaleGlobal.UserManagement.Main.add_user_card  # 点击添加门卡
    UserManagement_already_add = YaleGlobal.UserManagement.Main.already_add  # 门卡/指纹已添加
    UserManagement_user_fingerprint = YaleGlobal.UserManagement.Main.user_fingerprint  # 指纹
    UserManagement_add_user_fingerprint = YaleGlobal.UserManagement.Main.add_user_fingerprint  # 点击添加指纹
    UserManagement_back_bar_button = YaleGlobal.UserManagement.Main.back_bar_button  # 返回按钮
    UserManagement_pop_up = YaleGlobal.UserManagement.Main.pop_up  # prompt


    UserManagement_user_admin = YaleGlobal.UserManagement.Main.user_admin  # 管理员用户
    UserManagement_user_normal = YaleGlobal.UserManagement.Main.user_normal  # 普通用户
    UserManagement_user_guest = YaleGlobal.UserManagement.Main.user_guest  # 访客
    UserManagement_remove_card = YaleGlobal.UserManagement.Main.remove_card  # 删除门卡
    UserManagement_user_password = YaleGlobal.UserManagement.Main.user_password  # 密码
    UserManagement_password_input = YaleGlobal.UserManagement.Main.password_input  # 输入密码
    UserManagement_password_save = YaleGlobal.UserManagement.Main.password_save  # 保存密码
    UserManagement_save_update = YaleGlobal.UserManagement.Main.save_update  # 保存更改
    UserManagement_delete_user = YaleGlobal.UserManagement.Main.delete_user  # 删除用户
    UserManagement_user_invite = YaleGlobal.UserManagement.Main.user_invite  # 邀请用户
    userManagement_main_home_tab = YaleGlobal.UserManagement.Main.home_tab  # 开关锁tab
    userManagement_main_log_tab = YaleGlobal.UserManagement.Main.log_tab  # 日志tab
    userManagement_main_user_tab = YaleGlobal.UserManagement.Main.user_tab  # 用户管理tab
    userManagement_main_setting_tab = YaleGlobal.UserManagement.Main.setting_tab  # 设置tab
    userManagement_main_house_name = YaleGlobal.UserManagement.Main.house_name  # 首页房间列表命
    userManagement_invite_area_number = YaleGlobal.UserManagement.Invite.area_number  # 区号选择
    userManagement_invite_first_name = YaleGlobal.UserManagement.Invite.first_name  # 用户的名
    userManagement_invite_last_name = YaleGlobal.UserManagement.Invite.last_name  # 用户的姓
    userManagement_invite_back_button = YaleGlobal.UserManagement.Invite.back_button  # 邀请用户页返回按钮
    userManagement_invite_invite = YaleGlobal.UserManagement.Invite.invite  # invite user
    userManagement_invite_full_access = YaleGlobal.UserManagement.Invite.full_access  # invite user
    userManagement_invite_RFID_access = YaleGlobal.UserManagement.Invite.RFID_access  # RFID access
    userManagement_invite_PINCODE_access = YaleGlobal.UserManagement.Invite.PINCODE_access  # PINCODE access
    userManagement_invite_PINCODE_continue = YaleGlobal.UserManagement.Invite.PINCODE_continue  # PINCODE continue
    userManagement_invite_FP_access = YaleGlobal.UserManagement.Invite.FP_access  # FP_access
    userManagement_invite_start_FP_registration = YaleGlobal.UserManagement.Invite.start_FP_registration




    userManagement_invite_contact_continue = YaleGlobal.UserManagement.Invite.contact_continue  # contact continue
    userManagement_invite_lock_list = YaleGlobal.UserManagement.Invite.lock_list  # lock list
    userManagement_invite_lock_name = YaleGlobal.UserManagement.Invite.lock_name  # lock name
    userManagement_invite_lock_checked = YaleGlobal.UserManagement.Invite.lock_checked  # lock name
    userManagement_invite_lock_continue = YaleGlobal.UserManagement.Invite.lock_continue  # lock continue
    userManagement_invite_wait_connection = YaleGlobal.UserManagement.Invite.wait_connection  # wait connection
    userManagement_invite_card_continue = YaleGlobal.UserManagement.Invite.card_continue  # card continue
    userManagement_invite_start_RFID_registration = YaleGlobal.UserManagement.Invite.start_RFID_registration  # start_RFID_registration
    userManagement_invite_registration_timeout = YaleGlobal.UserManagement.Invite.registration_timeout  # registration timeout
    userManagement_invite_register_success = YaleGlobal.UserManagement.Invite.register_success  # register success
    userManagement_invite_register_sub_message = YaleGlobal.UserManagement.Invite.register_sub_message  # register sub message
    userManagement_invite_register_failure = YaleGlobal.UserManagement.Invite.register_failure  # register failure
    userManagement_invite_register_finish = YaleGlobal.UserManagement.Invite.register_finish  # register finish
    userManagement_invite_RFID_tab = YaleGlobal.UserManagement.Invite.RFID_tab  #  RFID tab
    userManagement_invite_lock_connection = YaleGlobal.UserManagement.Invite.lock_connection  # wait for lock connection
    # userManagement_invite_not_registered_status = YaleGlobal.UserManagement.Invite.not_registered_status  # not registered status
    userManagement_invite_RFID_registered_value = YaleGlobal.UserManagement.Invite.RFID_registered_value  # registered
    userManagement_invite_FP_registered_value = YaleGlobal.UserManagement.Invite.FP_registered_value  # registered

    userManagement_invite_access_always = YaleGlobal.UserManagement.Invite.access_always  # access always
    userManagement_invite_RFID_access_level = YaleGlobal.UserManagement.Invite.RFID_access_level  # RFID access level
    userManagement_invite_registered_info = YaleGlobal.UserManagement.Invite.registered_info  # registered info
    userManagement_invite_unregister_button = YaleGlobal.UserManagement.Invite.unregister_button  # unregister
    userManagement_invite_right_close = YaleGlobal.UserManagement.Invite.right_close  # right close
    userManagement_invite_RFID_first_name = YaleGlobal.UserManagement.Invite.RFID_first_name  # 用户的名
    userManagement_invite_RFID_last_name = YaleGlobal.UserManagement.Invite.RFID_last_name  # 用户的姓
    userManagement_invite_pincode_set = YaleGlobal.UserManagement.Invite.pincode_set  # set pincode





    userManagement_invite_country_search = YaleGlobal.UserManagement.Invite.country_search  # country search
    userManagement_invite_area_name = YaleGlobal.UserManagement.Invite.area_name    # country name
    userManagement_invite_phone_input = YaleGlobal.UserManagement.Invite.phone_input  # phone input
    userManagement_invite_add_yourself = YaleGlobal.UserManagement.Invite.add_yourself  # error message
    userManagement_invite_has_access = YaleGlobal.UserManagement.Invite.has_access   # has_access
    userManagement_invite_invite_cancel = YaleGlobal.UserManagement.Invite.invite_cancel  # cancel invite
    userManagement_invite_user_invited = YaleGlobal.UserManagement.Invite.user_invited  # user invited
    userManagement_invite_invited_contact = YaleGlobal.UserManagement.Invite.invited_contact  # invited contact
    userManagement_invite_not_registered = YaleGlobal.UserManagement.Invite.not_registered  # not registered contact
    userManagement_invite_access_set = YaleGlobal.UserManagement.Invite.access_set  # access set
    userManagement_invite_owner_access = YaleGlobal.UserManagement.Invite.owner_access  # owner access




    userManagement_invite_invite_user = YaleGlobal.UserManagement.Invite.invite_user  # 邀请管理员或普通用户
    userManagement_invite_invite_guest = YaleGlobal.UserManagement.Invite.invite_guest  # 邀请访客
    userManagement_invite_select_phone = YaleGlobal.UserManagement.Invite.select_phone  # 通过手机号邀请
    userManagement_invite_select_contact = YaleGlobal.UserManagement.Invite.select_contact  # 通过通讯录邀请
    userManagement_invite_send_button = YaleGlobal.UserManagement.Invite.send_button  # 确认按键
    userManagement_invite_user_access = YaleGlobal.UserManagement.Invite.user_access  # 用户权限等级
    # userManagement_invite_user_schedule = YaleGlobal.UserManagement.Invite.user_schedule  # 使用时间

    userManagement_invite_general_access = YaleGlobal.UserManagement.Invite.general_access  # 普通用户权限
    userManagement_invite_none_access = YaleGlobal.UserManagement.Invite.none_access  # 无权限
    userManagement_invite_schedule_always = YaleGlobal.UserManagement.Invite.schedule_always  # 永久使用
    userManagement_invite_schedule_recurring = YaleGlobal.UserManagement.Invite.schedule_recurring  # 多次使用
    userManagement_invite_schedule_temp = YaleGlobal.UserManagement.Invite.schedule_temp  # 临时使用
    userManagement_invite_error_prompt = YaleGlobal.UserManagement.Invite.error_prompt  # 底部弹出的错误提示框
    userManagement_invite_continue_button = YaleGlobal.UserManagement.Invite.continue_button  # 邀请访客页继续按键
    userManagement_invite_guest_first_name = YaleGlobal.UserManagement.Invite.guest_first_name  # 访客的名
    userManagement_invite_guest_last_name = YaleGlobal.UserManagement.Invite.guest_last_name  # 访客的姓
    # userManagement_invite_guest_contact = YaleGlobal.UserManagement.Invite.guest_contact  # 通过通讯录邀请访客
    userManagement_invite_save_pin = YaleGlobal.UserManagement.Invite.save_pin  # 保存访客进门密码
    userManagement_invite_pin_schedule = YaleGlobal.UserManagement.Invite.pin_schedule  # pin schedule
    # userManagement_invite_recurring_schedule = YaleGlobal.UserManagement.Invite.recurring_schedule   # recurring
    userManagement_invite_recur_sunday = YaleGlobal.UserManagement.Invite.recur_sunday
    userManagement_invite_recur_monday = YaleGlobal.UserManagement.Invite.recur_monday
    userManagement_invite_recur_tuesday = YaleGlobal.UserManagement.Invite.recur_tuesday
    userManagement_invite_recur_wednesday = YaleGlobal.UserManagement.Invite.recur_wednesday
    userManagement_invite_recur_thursday = YaleGlobal.UserManagement.Invite.recur_thursday
    userManagement_invite_recur_friday = YaleGlobal.UserManagement.Invite.recur_friday
    userManagement_invite_recur_saturday = YaleGlobal.UserManagement.Invite.recur_saturday
    userManagement_invite_only_user_schedule = YaleGlobal.UserManagement.Invite.only_user_schedule  # only user schedule
    userManagement_invite_pin_code = YaleGlobal.UserManagement.Invite.pin_code
    userManagement_invite_only_access_level = YaleGlobal.UserManagement.Invite.only_access_level







    userManagement_invite_create_pin = YaleGlobal.UserManagement.Invite.create_pin  # 点击设置进门密码
    # userManagement_invite_create_schedule = YaleGlobal.UserManagement.Invite.create_schedule  # 点击设置使用时间
    userManagement_invite_pin_input = YaleGlobal.UserManagement.Invite.pin_input  # 编辑密码框
    userManagement_invite_change_pin = YaleGlobal.UserManagement.Invite.change_pin  # 访客密码页保存更改按键
    userManagement_invite_guest_back_button = YaleGlobal.UserManagement.Invite.guest_back_button  # 邀请访客页返回按键
    userManagement_invite_cancel = YaleGlobal.UserManagement.Invite.cancel  # 取消
    userManagement_invite_confirm = YaleGlobal.UserManagement.Invite.confirm  # 确认
    userManagement_invite_invalid_contact = YaleGlobal.UserManagement.Invite.invalid_contact  # 非法手机号
    userManagement_invite_blank_contact = YaleGlobal.UserManagement.Invite.blank_contact  # 无手机号
    userManagement_invite_multiple_contact = YaleGlobal.UserManagement.Invite.multiple_contact  # 多手机号
    userManagement_invite_unregistered_contact = YaleGlobal.UserManagement.Invite.unregistered_contact  # 未注册手机号
    userManagement_invite_registered_contact = YaleGlobal.UserManagement.Invite.registered_contact  # 已注册手机号
    userManagement_invite_send_text = YaleGlobal.UserManagement.Invite.send_text  # 向访客发送短信
    userManagement_invite_popup_confirm = YaleGlobal.UserManagement.Invite.popup_confirm  # 弹框的确定按键
    userManagement_invite_popup_cancel = YaleGlobal.UserManagement.Invite.popup_cancel  # 弹框的取消按键
    userManagement_edit_pin_set = YaleGlobal.UserManagement.Edit.pin_set  # 我的权限页密码设置
    userManagement_edit_disable_pin = YaleGlobal.UserManagement.Edit.disable_pin  # 停用进门密码
    userManagement_edit_right_button = YaleGlobal.UserManagement.Edit.right_button  # 权限页右上角保存按键
    userManagement_edit_pin_input = YaleGlobal.UserManagement.Edit.pin_input  # 用户密码输入框
    userManagement_edit_save_pin = YaleGlobal.UserManagement.Edit.save_pin  # 用户密码保存按键

    userManagement_edit_rec_start_time = YaleGlobal.UserManagement.Edit.rec_start_time  # rec start time
    userManagement_edit_rec_end_time = YaleGlobal.UserManagement.Edit.rec_end_time  # rec end time














    userManagement_edit_schedule_set = YaleGlobal.UserManagement.Edit.schedule_set  # 使用时间设置
    userManagement_edit_card_set = YaleGlobal.UserManagement.Edit.card_set  # 门卡设置
    userManagement_edit_finger_set = YaleGlobal.UserManagement.Edit.finger_set  # 指纹设置
    userManagement_edit_back_button = YaleGlobal.UserManagement.Edit.back_button  # 返回按键
    userManagement_edit_delete_button = YaleGlobal.UserManagement.Edit.delete_button  # 删除用户按键
    userManagement_edit_manage_access_button = YaleGlobal.UserManagement.Edit.manage_access_button   # 保存权限修改
    userManagement_edit_guest_name = YaleGlobal.UserManagement.Edit.guest_name   # 访客姓名
    userManagement_edit_guest_first_name = YaleGlobal.UserManagement.Edit.guest_first_name   # 访客名
    userManagement_edit_guest_last_name = YaleGlobal.UserManagement.Edit.guest_last_name   # 访客姓
    notification_main_alert_open_bt = YaleGlobal.Notification.Main.alert_open_bt  # 蓝牙开门推送
    notification_main_alert_close_bt = YaleGlobal.Notification.Main.alert_close_bt  # 蓝牙开门推送
    notification_main_clear_button = YaleGlobal.Notification.Main.clear_button  # 清除所有通知
    notification_main_fix_time_mo = YaleGlobal.Notification.Main.fix_time_mo  # 周一
    notification_main_fix_time_tu = YaleGlobal.Notification.Main.fix_time_tu  # 周二
    notification_main_fix_time_we = YaleGlobal.Notification.Main.fix_time_we  # 周三
    notification_main_fix_time_th = YaleGlobal.Notification.Main.fix_time_th  # 周四
    notification_main_fix_time_fr = YaleGlobal.Notification.Main.fix_time_fr  # 周五
    notification_main_fix_time_sa = YaleGlobal.Notification.Main.fix_time_sa  # 周六
    notification_main_fix_time_su = YaleGlobal.Notification.Main.fix_time_su  # 周日
    notification_main_smartalert_start_time = YaleGlobal.Notification.Main.smartalert_start_time  # 开始时间
    notification_main_smartalert_end_time = YaleGlobal.Notification.Main.smartalert_end_time  # 开始时间
    notification_main_fix_time_1 = YaleGlobal.Notification.Main.fix_time_1  # 1点
    notification_main_fix_time_23 = YaleGlobal.Notification.Main.fix_time_23  # 24点
    notification_main_fix_time_55 = YaleGlobal.Notification.Main.fix_time_55  # 55分
    notification_main_confirm_time = YaleGlobal.Notification.Main.confirm_time  # 确定
    notification_main_notification_panel = YaleGlobal.Notification.Main.notification_panel  # 通知面板

else:
    pass
