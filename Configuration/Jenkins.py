import os
from urllib.request import urlopen
import requests
import subprocess

# 从jenkins自动下载最新版本apk并安装
# response = requests.get(url, headers=header, auth=HTTPBasicAuth('eco_system', 'ecosystem'))
# tree=jobs[name]
jenkins_path = os.environ['WORKSPACE']
def Download_Jenkins_Build():
    url = 'http://10.245.109.50:8080/api/python?pretty=true'
    jobs = eval(urlopen(url).read())['jobs']
    for i in jobs:
        for key, value in i.items():
            if value == 'Android-Panpan-Release':
                url = i['url']
                # print(i)
                lastSuccessfulBuild = eval(urlopen(url+'/api/python').read())['lastSuccessfulBuild']['url']
                # buildNumber = eval(urlopen(url+'/api/python').read())['builds'][0]['number']
                builds = eval(urlopen(url+'/api/python').read())['builds']
                build_type = eval(urlopen(lastSuccessfulBuild+'/api/python').read())['actions'][0]['parameters'][0]['value']
                if build_type == 'Debug':
                    relativePath = eval(urlopen(lastSuccessfulBuild + '/api/python').read())['artifacts'][0]['relativePath']
                    APKPath = lastSuccessfulBuild + 'artifact/' + relativePath
                else:
                    # raise Exception('This is a production release, no need to run automation.')
                    print('This is a production release, need back to latest debug build.')
                    for x in range(len(builds)):
                        index = builds[x]
                        buildURL = index['url']
                        build_type2 = eval(urlopen(buildURL+'/api/python').read())['actions'][0]['parameters'][0]['value']
                        build_result2 = eval(urlopen(buildURL+'/api/python').read())['result']
                        if build_type2 == 'Debug' and build_result2 == 'SUCCESS':
                            relativePath = eval(urlopen(buildURL + '/api/python').read())['artifacts'][0]['relativePath']
                            APKPath = buildURL + 'artifact/' + relativePath
                            break
                response = requests.get(APKPath)
                filename = os.path.basename(APKPath)
                os.chdir(jenkins_path + '\\APK')
                with open(filename, 'wb') as apk:
                    apk.write(response.content)
                apk.close()
                # installPath = os.path.join(os.getcwd(), filename)
                installPath = jenkins_path + '\\APK\\' + filename
                linuxPath = installPath.replace("\\", "/")
                print(linuxPath)
                # os.system('adb install -r ' + linuxPath)
                installResult = subprocess.Popen('adb install -r ' + linuxPath, shell=True, stdout=subprocess.PIPE)
                print(installResult.stdout.read().decode().strip())
                break



