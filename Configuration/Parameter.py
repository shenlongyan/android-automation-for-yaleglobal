import re
import subprocess
from Configuration.IDSelectLock import *
import os

jenkins_path = os.environ['WORKSPACE']
app_select = 'YaleGlobal'
version_yale_global = 'D:/jenkins/workspace/Android_Yale_Global_Regression/APK/app-yale-release0717.apk'

def get_apkname():
    global version_yale_global
    for root, dirs, files in os.walk(os.getcwd() + "\\APK\\"):
        # for name in files:
        apk_name = files[-1]
        windows_yale_global = jenkins_path + '\\APK\\' + apk_name
        version_yale_global = windows_yale_global .replace("\\", "/")
    return version_yale_global


class APPInfo:
    pass


class MobileInfo:
    __tmp_mobile_name = subprocess.Popen('adb shell getprop ro.product.vendor.model', stdout=subprocess.PIPE)
    mobile_name = __tmp_mobile_name.stdout.read().decode().strip()
    __tmp_serial_number = subprocess.Popen('adb shell getprop ro.boot.serialno', stdout=subprocess.PIPE)
    serial_number = __tmp_serial_number.stdout.read().decode().strip()
    __tmp_platform_name = subprocess.Popen('adb shell getprop net.bt.name', stdout=subprocess.PIPE)
    platform_name = __tmp_platform_name.stdout.read().decode().strip()
    __tmp_platform_version = subprocess.Popen('adb shell getprop ro.build.version.release', stdout=subprocess.PIPE)
    platform_version = __tmp_platform_version.stdout.read().decode().strip()


class APKInfo:
    __apk_name = get_apkname()
    print(__apk_name)

    __package_name = subprocess.Popen('aapt dump badging {}'.format(__apk_name), stdout=subprocess.PIPE)
    (__output_package_name, __err_package_name) = __package_name.communicate()
    __match_package_name = re.compile("package: name='(\S+)'").search(__output_package_name.decode())
    package_name = __match_package_name.group(1)

    __activity_name = subprocess.Popen('aapt dump badging {}'.format(__apk_name), stdout=subprocess.PIPE)
    (__output_activity_name, __err_activity_name) = __activity_name.communicate()
    __match_activity_name = re.compile("launchable-activity: name='(\S+)'").search(__output_activity_name.decode())
    activity_name = __match_activity_name.group(1)

    __version_name = subprocess.Popen('aapt dump badging {}'.format(__apk_name), stdout=subprocess.PIPE)
    (__output_version_name, __err_version_name) = __version_name.communicate()
    __match_version_name = re.compile("versionName='(\S+)'").search(__output_version_name.decode())
    version_name = __match_version_name.group(1)


class UserInfo:
    class First:
        name = 'Test 01'
        name_first = 'Test'
        name_last = '01'
        name_new_space = 'flying  fish '
        name_new_space_first = 'flying '
        name_new_space_last = 'fish '
        name_new_number = 'flying1 fish1'
        name_new_number_first = 'flying1'
        name_new_number_last = 'fish1'
        name_new_symbol = 'flying% fish#'
        name_new_symbol_first = 'flying%'
        name_new_symbol_last = 'fish#'
        name_new_all = 'fly ing fish#2'
        name_new_all_first = 'fly ing'
        name_new_all_last = 'fish#2'
        phone = '15000000001'
        phone_short = '185'
        phone_with_space = '185    '
        phone_with_symbol = '185#@$%^!'
        phone_with_chinese = '185中文'
        phone_with_english = '185En'
        phone_unregistered = '19013572468'  # 验证未注册过的手机号登录
        email = '15000000001@qq.com'
        email_unregistered = 'tr199407@gmail.com'  # 验证未注册过的邮箱登录
        password = 'Abcd1234'
        password_new = 'Abcd1234%'
        password_length = 'Ab1%'
        password_upper = 'ABCDEFGH'
        password_lower = 'abcdefgh'
        password_number = '12345678'
        password_english = 'ABCDefgh'
        password_symbol = '!@#$%^&*'
        password_wrong = 'Qwert1234'
        verify_code_wrong = '000000'
        pin = '000001'
        pin_new = '111000'
        finger_thumb = 'thumb'

    class Second:
        name = 'Test 02'
        name_first = 'Test'
        name_last = '02'
        phone = '15000000002'
        email = '15000000002@qq.com'
        password = 'Abcd1234'
        pin = '000002'
        pin_new = '876543'
        finger_thumb = 'thumb'

    class Third:
        name = '140222 20000'
        name_first = '140222'
        name_last = '20000'
        phone = '14022220000'
        email = '14022220000@qq.com'
        password = 'Abcd1234'
        pin = '456789'
        pin_new = '987654'
        finger_thumb = 'thumb'

    class Four:
        phone1 = '15000020004'
        password1 = 'Abc!1234'
        email1 = '15000020004@qq.com'
        name = 'admin shen'
        name_first = 'automation'
        name_last = 'shen'
        phone = '15000020000'
        phone_new = '15000010000'
        email = '58273333@qq.com'
        password = 'Abc!1234'
        pin = ''
        pin_new = ''
        finger_thumb = ''
        name_first_12 = 'joycejoycejoyce'
        name_last_12 = '沈沈沈沈沈沈沈沈沈沈沈沈'
        name_first_space = 'joyce shen'
        name_last_space = 'joyce shen'
        name_first_special = 'joyce!@#$%'
        name_last_special = 'joyce!@#$%'
        email_wrong = 'shenlongyan'
        area = 'Australia'
        phone_wrong = '1521676242'
        phone_wrong_15 = '152167624263333'
        phone_space = '  '
        password_short = 'Abc!123'
        password_wrong = 'ABC12345'
        password_20 = 'Abc!123Abc!123Abc!12'
        password_special = '_!#$*+-./:=?@[]^`|'

    class AdminJoyce:
        name = 'admin shen'
        email = '58273333@qq.com'
        phone = '15000020000'
        password = 'Abc!1234'
        name2 = 'admin2 shen'
        email2 = '123456@qq.com'
        phone2 = '15000030000'
        password2 = 'Abc!1234'

    class NormalJoyce:
        name = 'normal shen'
        phone = '15000010000'
        password = 'Abc!1234'
        email = 'shenlong@163.com'
        name2 = 'normal2 shen'
        phone2 = '15000040000'
        password2 = 'Abc!1234'
        email2 = 'longyan@163.com'

    class GuestJoyce:
        name2 = 'zhuguest'
        name = 'guest shen'
        first_name = 'guest'
        last_name = 'shen'
        phone = '15000050000'
        pin = '222555'

    class AdminPhilip:
        first_name = 'Admin'
        last_name = '01'
        name = 'Admin 01'
        phone = '15900000001'
        email = '15900000001@qq.com'
        password = 'Abcd1234'
        pin = '111222'

    class NormalPhilip:
        first_name = 'Normal'
        last_name = '01'
        name = 'Normal 01'
        phone = '15900000002'
        email = '15900000002@qq.com'
        password = 'Abcd1234'
        pin = '222333'
        pin_new = '333222'

    class GuestPhilip:
        first_name = 'Guest'
        new_name = 'NewGuest'
        name = 'Guest Guest'
        phone = '15900000003'
        email = '15900000003@qq.com'
        password = 'Abcd1234'
        pin = '333444'
        pin_new = '444333'

    class InvitedPhilip:
        first_name = 'Invited'
        name = 'Invited Invited'
        phone = '15937383827'  # 此号码是未注册号码
        password = 'Abcd1234'
        pin = '444555'
        pin_new = '555444'


class HouseInfo:
    name = 'Log'
#
#
# class GetLockInfo:
#
#     def __init__(self, lock_collection, lock_serial):
#         self._lock_collection = lock_collection
#         self._lock_serial = lock_serial
#
#     def lock_name(self):
#         lock_name = self._lock_serial + 'n'
#         return lock_name[-12:]
#
#     def lock_name_new(self):
#         lock_name_new = self._lock_serial + 'nn'
#         return lock_name_new[-12:]
#
#     def lock_id(self):
#         lock_id = self._lock_collection[self._lock_serial]['lock_id']
#         return lock_id
#
#     def lock_model(self):
#         lock_model = self._lock_collection[self._lock_serial]['lock_model']
#         return lock_model


class CaptureCompare:
    bt_icon = 'Capture/bt_icon.png'
    pin_on = 'Capture/pin_on.png'
    pin_off = 'Capture/pin_off.png'
    card_on = 'Capture/card_on.png'
    card_off = 'Capture/card_off.png'
    finger_on = 'Capture/finger_on.png'
    finger_off = 'Capture/finger_off.png'
    bt_icon_new = 'Capture/bt_icon_new.png'
    pin_on_new = 'Capture/pin_on_new.png'
    pin_off_new = 'Capture/pin_off_new.png'
    card_on_new = 'Capture/card_on_new.png'
    card_off_new = 'Capture/card_off_new.png'
    finger_on_new = 'Capture/finger_on_new.png'
    finger_off_new = 'Capture/finger_off_new.png'
