import os
import re
from Configuration import FolderList


def cancel_pass_link(report_file):
    with open(report_file, 'r', encoding='utf-8') as r:
        file_result = str(''.join(r.readlines()))
    r.close()
    res_t = re.compile(r'<!--css div popup start-->\n.*?'
                       r'<span class=\'status passCase\'>\n.*?'
                       r'<a.*?>\n.*?'
                       r'Pass</a></span>[\s\S]*?'
                       r'<!--css div popup end-->',
                       flags=re.MULTILINE)
    m_tr = re.sub(res_t, '<span class=\'status passCase\'>Pass</span>', file_result)
    result_html = open(report_file, 'w')
    result_html.write(m_tr)
    result_html.close()


def add_log_link(report_file, retry_times):
    address = FolderList.output_path
    href_link = "<div class='testcase'><a class='popup_link' onfocus='this.blur();' href='{}' target='_blank' >{}</a></div>"
    with open(report_file, 'r', encoding='gbk') as r:
        file_result = str(''.join(r.readlines()))
    res_t1 = re.compile(r"<div class='testcase'>.*?</div>", flags=re.MULTILINE)
    re_value1 = re.findall(res_t1, file_result)
    for i in range(0, len(re_value1)):
        file_result_new = file_result
        case_name = re_value1[i][22:-6]  # 获取case名称
        log_result = address + case_name + '.txt'
        if retry_times >= 1:
            for j in range(0, retry_times + 1):
                if j == 0:  # 多次retry时，第一次为正常处理
                    if os.path.exists(log_result):
                        res_t = re.compile(r"<div class='testcase'>{}</div>".format(case_name),
                                           flags=re.MULTILINE)
                        file_result_new = re.sub(res_t, href_link.format(case_name + '.txt', case_name), file_result_new)
                    else:
                        pass
                else:  # 失败重跑时的特殊处理
                    if os.path.exists(log_result.replace(': _retry:{}'.format(j), '')):
                        res_t = re.compile(r"<div class='testcase'>{}</div>".format(case_name),
                                           flags=re.MULTILINE)
                        link_name = case_name + '.txt'
                        file_result_new = re.sub(res_t, href_link.format(link_name.replace(': _retry:{}'.format(j), ''), case_name), file_result_new)
        else:  # retry不存在时的处理
            if os.path.exists(log_result):
                res_t = re.compile(r"<div class='testcase'>{}</div>".format(case_name),
                                   flags=re.MULTILINE)
                file_result_new = re.sub(res_t, href_link.format(case_name + '.txt', case_name), file_result_new)
        file_result = file_result_new
    with open(report_file, 'w') as html:
        html.write(file_result)
        html.close()
