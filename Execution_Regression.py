from Configuration.Jenkins import *
from Configuration.Parameter import *
import time
import unittest
from HTMLTestRunner_cn import HTMLTestRunner
from Configuration import FolderList
from Configuration.ReportModify import add_log_link, cancel_pass_link
from Configuration.testrail import *
from TestCase.test_base import TestBase
from TestCase.test_RFID import TestRFID
from TestCase.test_schedule import TestSchedule
# from TestCase.test_lock_toBind import TestAddDevice
# from TestCase.test_user_management import TestUserManagement
import os
import re
import shutil


# 1.从测试jenkins获取环境变量:jobPath,TestRunID,lockID,test_jig_ip,test_jig_lock_id
# print(os.environ)
jobPath = os.environ['WORKSPACE']
runID = os.environ['TestRunID']
# test_jig_lock_id = os.environ['test_jig_lock_id']  # test_jig的端口号
# test_jig_ip = os.environ['test_jig_ip']  # test_jig的ip地址
# selected_lock_id = os.environ['LockID']
print(jobPath, runID)


# 2.从开发jenkins自动下载最新版本apk并安装
DownloadAPK = os.environ['DownloadAPK']
if DownloadAPK is True:
    Download_Jenkins_Build()
else:
    pass


# 3.获取自动化脚本中所有case
# caseList1, caseList2, caseList3, caseList4, caseList5, caseList6, caseList7, caseInAutomation = [], [], [], [], [], [], [], []
caseList1 = TestBase().cases()
caseList2 = TestRFID().cases()
caseList3 = TestSchedule().cases()
# caseList3 = TestAddDevice().cases()
# caseList4 = TestSmartAlert().cases()
# caseList5 = TestLogin().cases()
# caseList6 = TestHomePage().cases()
# caseList7 = TestUserManagement().cases()
caseAutomation = caseList1 + caseList2 + caseList3
caseInAutomation = []
for i in range(len(caseAutomation)):
    if caseAutomation[i].startswith('test_C'):
        automation = caseAutomation[i].strip('test_')
    elif caseAutomation[i].startswith('test_D'):
        automation = caseAutomation[i].strip('test_D')
    else:
        print('case name error')
    caseInAutomation.append(automation)
# print(caseInAutomation)
# print(len(caseInAutomation))


# 4.根据testrunID加载测试用例
client = APIClient('https://assaabloyapac.testrail.io/')
client.user = 'Longyan.Shen@assaabloy.com'
client.password = 'eh2BLfThFs07D.ucWm3v-NBZtTlQ18nFFvzuVXyXS'
case = client.send_get('get_tests/' + str(runID))
caseList = []
caseIDList = []
isManualCase = []
isAutomationCase = []
for i in case:
    for key, value in i.items():
        # print(key, value)
        if key == 'case_id':
            caseList.append('C' + str(value))
            caseIDList.append(value)
suite = unittest.TestSuite()
flag1, flag2, flag3, flag4 = 1, 1, 1, 1
# 从testrail中筛选自动化case和手动case
for c in caseList:
    if c in caseInAutomation:
        isAutomationCase.append(c)
    else:
        isManualCase.append(c)
# print('all cases for automation test:' + str(isAutomationCase))
print('all cases for manual test:' + str(isManualCase))
for c in isAutomationCase:
    if 'C7114' <= c <= 'C7403':
        suite.addTest(TestBase('test_' + c))
    # elif 'C6925' <= c <= 'C7020':
    #     suite.addTest(TestRFID('test_' + c))
    # elif 'C6467' <= c <= 'C6604':
    #     suite.addTest(TestSchedule('test_' + c))
    # elif 'C529' <= c <= 'C573':
    #     suite.addTest(TestAddDevice('test_' + c))
    # elif 'C574' <= c <= 'C675':
    #     if flag1:
    #         suite.addTest(TestUserManagement('test_D000'))
    #         suite.addTest(TestLockSetting('test_C000'))
    #         flag1 = 0
    #     suite.addTest(TestLockSetting('test_' + c))
    # elif 'C826' <= c <= 'C904':
    #     if flag2:
    #         suite.addTest(TestLockSetting('test_D000'))
    #         suite.addTest(TestSmartAlert('test_C000'))
    #         flag2 = 0
    #     suite.addTest(TestSmartAlert('test_' + c))
    # elif 'C916' <= c <= 'C999':
    #     if flag3:
    #         suite.addTest(TestUserManagement('test_C000'))
    #         flag3 = 0
    #     suite.addTest(TestUserManagement('test_' + c))
    # elif 'C1000' <= c <= 'C1062':
    #     suite.addTest(TestUserManagement('test_D' + c))
    # else:
    #     # raise Exception('{} not exist'.format(c))
    #     print('{} not exist'.format(c))
# suite.addTest(TestBase('test_DC7400'))


# # 批量加载测试用例
# case_path = os.path.join(os.getcwd(), 'TestCase')
# case_path = 'D:\jenkins\workspace\joyce_test\TestCase'
# print('Check lock id & phone permission & wifi environment')
# print('Files_path:', os.getcwd())
# print('Cases_path:', case_path)
# discover = unittest.defaultTestLoader.discover(case_path, pattern="test_schedule.py", top_level_dir=None)


# 5.执行测试并生成html报告
if __name__ == '__main__':
    # reportPath = os.path.abspath(os.path.join(os.getcwd(), ".."))
    reportPath = jobPath + '\Report'
    os.chdir(reportPath)
    retry_times = 0
    report_name = FolderList.report_dir
    result_description = 'Mobile Version:{};\r\n' \
                         'OS Version:{};\r\n' \
                         'App Version:{};'.format(MobileInfo.mobile_name,
                                                  MobileInfo.platform_version,
                                                  APKInfo.version_name)
    with open(report_name, 'wb') as f:
        runner = HTMLTestRunner(stream=f, title='Test Result_Report', description=result_description,
                                verbosity=3, retry=retry_times, save_last_try=True)
        runner.run(suite)
    f.close()

#     # start_time_cancel_link = time.time()
#     # cancel_pass_link(report_name)
#     # print('Cancel link time spend:{}'.format(time.time() - start_time_cancel_link))
#     # start_time_add_log = time.time()
#     # add_log_link(report_name, retry_times)
#     # print('Add log time spend:{}'.format(time.time() - start_time_add_log))


# 6.把测试结果写回testrail
with open(report_name, 'r', encoding='utf-8') as r:
    file_result = r.read()
r.close()
res_pass = r'<td><div class=\'testcase\'>(.*?)</div></td>(.*?)<span class=\'status passCase\'>Pass</span></td>'
tc_pass = re.findall(res_pass, file_result, flags=re.M | re.S)
pass_cases = []
retest_cases = []
for i in tc_pass:
    if str(i[0]).startswith('test_C'):
        if str(i[0]) == 'test_C000':
            pass
        else:
            temp = str(i[0]).strip('test_C')
            passCaseID = int(temp)
            pass_cases.append(passCaseID)
    elif str(i[0]).startswith('test_D'):
        if str(i[0]) == 'test_D000':
            pass
        else:
            temp = str(i[0]).strip('test_DC')
            passCaseID = int(temp)
            pass_cases.append(passCaseID)
pass_list = []
retest_list = []
for case in caseIDList:
    if case in pass_cases:
        pass_list.append({'case_id': case, 'status_id': 1, 'comment': 'passed by Android automation scripts'})
    # else:
    #     retest_list.append({'case_id': case, 'status_id': 4, 'comment': 'need manual test'})
result_pass = client.send_post('add_results_for_cases/' + str(runID), data={'results': pass_list})
# result_retest = client.send_post('add_results_for_cases/' + str(runID), data={'results': retest_list})


# 7.移动testReport到workspace根目录下
# shutil.move(report_dir, jenkins_path)
shutil.copy(FolderList.report_dir, jobPath)
