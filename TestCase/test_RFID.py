import logging
import threading
import unittest
from Configuration.OperationMethod import *
from Configuration import StartUp
from Redis.VerificationCode import verification_code_phone, verification_code_email


class TestRFID(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = StartUp.driver
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H_%M_%S',
                            filename=FolderList.logging_name,
                            filemode='a')
        logging.getLogger()
        time.sleep(1)

    def add_img(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

    def setUp(self) -> None:
        os.system('adb shell svc wifi enable')
        bt_switch('on')
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    @adb_log
    def test_C6925(self):
        # inviting user/name input page/the function of button continue/the function of page redirect after typing user name
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        assert_result_displayed(userManagement_invite_lock_list, True)  # display lock list

    @adb_log
    def test_C6930(self):
        # inviting user/lock list selecting page/select function for single lock
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        assert_result_displayed(userManagement_invite_lock_checked, True)  # display lock checked

    @adb_log
    def test_C6935(self):
        # inviting user/lock list selecting page/"continue" function/Lock supporting RFID schedule
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        assert_result_displayed(userManagement_invite_wait_connection, True)  # display wait for lock connection

    @adb_log
    def test_C6936(self):
        # the page that waiting for connecting lock via BLE/"continue" function/lock without integrated key
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        # a = driver.find_element_by_id(userManagement_invite_card_continue).is_enabled()
        # print(a)
        wait_lock_connection()
        assert_result_displayed(userManagement_invite_start_RFID_registration, True)  # start_RFID_registration

    @adb_log
    def test_C6937(self):
        # start to register RFID page/connect lock via BLE sucessfully
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        assert_result_displayed(userManagement_invite_start_RFID_registration, True)  # start_RFID_registration
        assert_result_displayed(userManagement_invite_card_continue, True)  # continue

    @adb_log
    def test_C6941(self):
        # register RFID/failed from lock side/not register RFID in 20s
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout

    @adb_log
    def test_C6942(self):
        # register RFID/register RFID sucessfully from lock side/lock without integrated keypad
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # REGISTER RFID
        assert_result_displayed(userManagement_invite_register_success, True)  # register RFID success
        assert_result_displayed(userManagement_invite_register_sub_message.format('Front Door'), True)  # display lock name
        click_element(userManagement_invite_register_finish)  # click finish
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list
        click_element(UserManagement_user_admin.format('RFID ONLY'))  # click RFID ONLY
        assert_result_text(userManagement_invite_RFID_registered_value, 'REGISTERED')  # registered
        assert_result_displayed(userManagement_invite_access_always, True)  # always
        assert_result_displayed(userManagement_invite_RFID_access_level, True)  # CONTACTLESS KEY ONLY
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        assert_result_displayed(userManagement_invite_registered_info, True)  # registered

    @adb_log
    def test_C6943(self):
        # register RFID/failed from lock side/add duplicate RFID
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'FAILURE')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # add duplicate RFID
        assert_result_displayed(userManagement_invite_register_failure, True)  # register failure

    @adb_log
    def test_C6949(self):
        # RFID registeration success/check lock name
        # same with C6942
        pass

    @adb_log
    def test_C6950(self):
        # RFID registeration success/finish button/page change
        # same with C6942
        pass

    @adb_log
    def test_C6951(self):
        # register RFID/the page change on the user profile
        delete_RFID('RFID ONLY')
        click_element(userManagement_main_user_tab)  # invite
        # a = get_element(UserManagement_user_admin).text
        # print('a' + a + 'b')
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click philip
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        assert_result_displayed(userManagement_invite_lock_connection, True)  # display wait for lock connection

    @adb_log
    def test_C6952(self):
        # the page to wait for the lock connection/continue button/lock without integrated keypad
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click philip
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        assert_result_displayed(userManagement_invite_start_RFID_registration, True)  # start RFID registration

    @adb_log
    def test_C6953(self):
        # the page function of start RFID registeration/can connect lock via BLE
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click philip
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue

    @adb_log
    def test_C6957(self):
        # register RFID/Lock failed/not add card on lock till the 20s timeout
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click philip
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout

    @adb_log
    def test_C6959(self):
        # register RFID/Lock failed/add duplicated card
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Joyce Shen'))  # click joyce
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # add duplicate RFID
        assert_result_displayed(userManagement_invite_register_failure, True)  # register failure

    @adb_log
    def test_C6958(self):
        # register RFID/Lock add Successfully/lock without integrated keypad
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click philip
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # REGISTER RFID
        assert_result_displayed(userManagement_invite_register_success, True)  # register RFID success
        assert_result_displayed(userManagement_invite_register_sub_message.format('FRONT DOOR'), True)  # display lock name
        click_element(userManagement_invite_register_finish)  # click finish
        assert_result_displayed(userManagement_invite_RFID_tab, True)  # back to the user profile of owner B
        assert_result_text(userManagement_invite_RFID_registered_value, 'REGISTERED')  # registered
        assert_result_displayed(userManagement_invite_access_always, True)  # always
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        assert_result_displayed(userManagement_invite_registered_info, True)  # registered

    @adb_log
    def test_C6965(self):
        # RFID registeration success/check lock name
        # same with C6958
        pass

    @adb_log
    def test_C6966(self):
        # RFID registeration success/finish button/page change
        # same with C6958
        pass

    @adb_log
    def test_C6967(self):
        # register RFID/the page change on the user profile
        delete_RFID('Philadelphia She')
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Zhu Shen'))  # click Zhu Shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        assert_result_displayed(userManagement_invite_lock_connection, True)  # display wait for lock connection

    @adb_log
    def test_C6968(self):
        # the page to wait for the lock connection/continuebutton/lock without integrated keypad
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Zhu Shen'))  # click Zhu Shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        assert_result_displayed(userManagement_invite_start_RFID_registration, True)  # start RFID registration

    @adb_log
    def test_C6969(self):
        # the page function of start RFID registeration/can connect lock via BLE
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Zhu Shen'))  # click Zhu Shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue

    @adb_log
    def test_C6973(self):
        # register RFID/Lock failed/not add card on lock till the 20s timeout
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Zhu Shen'))  # click Zhu Shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout

    @adb_log
    def test_C6975(self):
        # register RFID/Lock failed/add duplicated card
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Alice Yuan'))  # click Alice
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # add duplicate RFID
        assert_result_displayed(userManagement_invite_register_failure, True)  # register failure

    @adb_log
    def test_C6974(self):
        # register RFID/Lock add Successfully/lock without integrated keypad
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Zhu Shen'))  # click Zhu Shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # REGISTER RFID
        assert_result_displayed(userManagement_invite_register_success, True)  # register RFID success
        assert_result_displayed(userManagement_invite_register_sub_message.format('FRONT DOOR'), True)  # display lock name
        click_element(userManagement_invite_register_finish)  # click finish
        assert_result_displayed(userManagement_invite_RFID_tab, True)  # back to the user profile of GUEST B
        assert_result_text(userManagement_invite_RFID_registered_value, 'REGISTERED')  # registered
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        assert_result_displayed(userManagement_invite_registered_info, True)  # registered

    @adb_log
    def test_C6981(self):
        # RFID registeration success/check Lock Name
        # same with C6975
        pass

    @adb_log
    def test_C6982(self):
        # RFID registeration success/finish button/page change
        # same with C6975
        pass

    @adb_log
    def test_C6983(self):
        # owner check the user profile of owner users/UI Test/owner didn't register contactless key
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Joyce Shen'))  # click Joyce Shen
        assert_result_text(userManagement_invite_RFID_registered_value, 'NOT REGISTERED')  # not registered

    @adb_log
    def test_C6984(self):
        # owner check the user profile of owner users/UI Test/owner register RFID for himself
        # test at C6958
        pass


    @adb_log
    def test_C6988(self):
        # owner check user details page of guest/UI test/guest didn't register contactless key
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Alice Yuan'))  # click a guest
        assert_result_text(userManagement_invite_RFID_registered_value, 'NOT REGISTERED')  # not registered

    @adb_log
    def test_C6989(self):
        # owner check user details page of guest/UI test/guest registered contactless key
        # test at C6974
        pass

    @adb_log
    def test_C6993(self):
        # owner check user details page of Contactless Key only/UI test/registered contactless key
        # test at C6942
        pass

    @adb_log
    def test_C6994(self):
        # owner check user details page of Contactless Key only/UI test/didn't register contactless key success
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('RFID FAILURE'))  # click RFID FAILURE
        assert_result_displayed(userManagement_invite_RFID_access_level, True)  # CONTACTLESS KEY ONLY
        assert_result_text(userManagement_invite_RFID_registered_value, 'NOT REGISTERED')  # not registered

    @adb_log
    def test_C7002(self):
        # owner check contactless key details page of owner/UI test/without integrated keypad
        # test at C6958
        pass

    @adb_log
    def test_C7004(self):
        # owner check contactless key details page of owner/unregister contactless key button/delete success
        # test at C6967
        pass

    @adb_log
    def test_C7005(self):
        # owner check contactless key details page of guest/UI test/without integrated keypad
        # test at C6974
        pass

    @adb_log
    def test_C7007(self):
        # owner check contactless key details page of guest/unregister contactless key button/delete success
        # test at C7014
        pass

    @adb_log
    def test_C7008(self):
        # owner check contactless key details page of Contactless Key only/UI test/without integrated keypad
        # test at C6942
        pass

    @adb_log
    def test_C7010(self):
        # owner check contactless key details page of Contactless Key only/unregister contactless key button/delete success
        # test at C6951
        pass
        # click_element(userManagement_main_user_tab)  # invite
        # click_element(UserManagement_user_admin.format('RFID ONLY'))  # click RFID ONLY
        # click_element(userManagement_invite_RFID_tab)  # click contactless key
        # click_element(userManagement_invite_unregister_button)  # click unregister button
        # click_element(userManagement_invite_confirm)  # click confirm
        # assert_result_displayed(userManagement_invite_invite, True)  # back to guest list
        # assert_result_displayed(UserManagement_user_admin.format('RFID ONLY'), False)  # not show in guest list

    @adb_log
    def test_C7014(self):
        # contactless key only user/failed to add contactless key when invitation, can restart to register
        delete_RFID('Zhu Shen')
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout
        click_element(userManagement_invite_card_continue)  # RESTART REGISTRATION
        click_element(userManagement_invite_card_continue)  # continue
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # REGISTER RFID
        assert_result_displayed(userManagement_invite_register_success, True)  # register RFID success
        click_element(userManagement_invite_register_finish)  # click finish
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list
        assert_result_displayed(UserManagement_user_admin.format('RFID ONLY'), True)  # show in guest list
        click_element(UserManagement_user_admin.format('RFID ONLY'))  # click RFID ONLY
        assert_result_text(userManagement_invite_RFID_registered_value, 'REGISTERED')  # registered

    @adb_log
    def test_C7015(self):
        # contactless key only user/"created" status contactless key can add contactless key again
        delete_RFID('RFID ONLY')     # delete RFID only user
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'CREATED')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout
        click_element(userManagement_invite_right_close)  # right close
        click_element(UserManagement_user_admin.format('RFID CREATED'))  # click RFID CREATED
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        click_element(userManagement_invite_lock_continue)  # continue
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # REGISTER RFID
        assert_result_displayed(userManagement_invite_register_success, True)  # register RFID success
        click_element(userManagement_invite_register_finish)  # click finish
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list
        assert_result_displayed(UserManagement_user_admin.format('RFID CREATED'), True)  # show in guest list
        click_element(UserManagement_user_admin.format('RFID CREATED'))  # click RFID CREATED
        assert_result_text(userManagement_invite_RFID_registered_value, 'REGISTERED')  # registered

    @adb_log
    def test_C7016(self):
        # guest user/add contactless key failed and restart to register
        delete_RFID('RFID CREATED')  # delete RFID user
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Zhu Shen'))  # click Zhu
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout
        click_element(userManagement_invite_card_continue)  # RESTART REGISTRATION
        click_element(userManagement_invite_card_continue)  # continue
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # REGISTER RFID
        assert_result_displayed(userManagement_invite_register_success, True)  # register RFID success
        click_element(userManagement_invite_register_finish)  # click finish
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list
        click_element(UserManagement_user_admin.format('Zhu Shen'))  # click Zhu Shen
        assert_result_text(userManagement_invite_RFID_registered_value, 'REGISTERED')  # registered

    @adb_log
    def test_C7017(self):
        # guest user/"created" status contactless key can add contactless key again
        delete_RFID('Zhu Shen')  # delete Zhu Shen
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Alice Yuan'))  # click Alice Yuan
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout
        click_element(userManagement_invite_right_close)  # right close
        click_element(UserManagement_user_admin.format('Alice Yuan'))  # click Alice Yuan
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        click_element(userManagement_invite_lock_continue)  # continue
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # REGISTER RFID
        assert_result_displayed(userManagement_invite_register_success, True)  # register RFID success
        click_element(userManagement_invite_register_finish)  # click finish
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list
        click_element(UserManagement_user_admin.format('Alice Yuan'))  # click Alice Yuan
        assert_result_text(userManagement_invite_RFID_registered_value, 'REGISTERED')  # registered

    @adb_log
    def test_C7018(self):
        # owner user/add contactless key failed and restart to register
        delete_RFID('Alice Yuan')
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # Philadelphia She
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout
        click_element(userManagement_invite_card_continue)  # RESTART REGISTRATION
        click_element(userManagement_invite_card_continue)  # continue
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # REGISTER RFID
        assert_result_displayed(userManagement_invite_register_success, True)  # register RFID success
        click_element(userManagement_invite_register_finish)  # click finish
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        assert_result_text(userManagement_invite_RFID_registered_value, 'REGISTERED')  # registered

    @adb_log
    def test_C7019(self):
        # owner user/"created" status contactless key can add contactless key again
        delete_RFID('Philadelphia She')
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Joyce Shen'))  # click Joyce
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout
        click_element(userManagement_invite_right_close)  # right close
        click_element(UserManagement_user_admin.format('Joyce Shen'))  # click Joyce Shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        click_element(userManagement_invite_lock_continue)  # continue
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # REGISTER RFID
        assert_result_displayed(userManagement_invite_register_success, True)  # register RFID success
        click_element(userManagement_invite_register_finish)  # click finish
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list
        click_element(UserManagement_user_admin.format('Joyce Shen'))  # click Joyce Shen
        assert_result_text(userManagement_invite_RFID_registered_value, 'REGISTERED')  # registered
        click_element(UserManagement_back_bar_button)  # BACK
        delete_RFID('Joyce Shen')

    @adb_log
    def test_C7020(self):
        # owner user/"created" status contactless key can add contactless key again
        # click_element(userManagement_main_home_tab)  # lock manage
        check_lock_connection()
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    def cases(self):
        return list(filter(lambda m: m.startswith("test_C") and callable(getattr(self, m)), dir(self)))
