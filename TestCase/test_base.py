import logging
import unittest
from Configuration.OperationMethod import *
from Configuration import StartUp
import random
import os

autoLock = os.environ['AutoLock']
print('autoLock=' + autoLock)

class TestBase(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = StartUp.driver
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H_%M_%S',
                            filename=FolderList.logging_name,
                            filemode='a')
        logging.getLogger()
        time.sleep(1)

    def add_img(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

    def setUp(self) -> None:
        os.system('adb shell svc wifi enable')
        bt_switch('on')
        # add_card_yale()
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    @adb_log
    def test_C7114(self):
        # Side Menu/Set Up An August Smart Lock/Set Up A Yale Smart Lock/Start
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        click_element(homePage_homePage_menu_in_home_page)  # click [HOME]
        click_element(homePage_homePage_add_device)  # click [set up new device]
        click_element(homePage_else_add_device_text)  # click [can't scan the code?]
        send_keys(homePage_else_enter_serial_number, selected_serial_number)  # Enter serial number
        click_element(homePage_else_lock_continue)  # click continue
        click_element(homePage_else_set_device_image)  # click set device image
        click_element(homePage_else_lock_start)  # click start
        assert_result_displayed(homePage_else_instruction_link, True)  # display installation instructions

    @adb_log
    def test_C7115(self):
        # Side Menu/Set Up a New Device/Set Up A Yale Smart Lock/Skip/Connect Smart Lock/Start
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        click_element(homePage_homePage_menu_in_home_page)  # click [HOME]
        click_element(homePage_homePage_add_device)  # click [set up new device]
        click_element(homePage_else_add_device_text)  # click [can't scan the code?]
        send_keys(homePage_else_enter_serial_number, selected_serial_number)  # Enter serial number
        click_element(homePage_else_lock_continue)  # click continue
        click_element(homePage_else_set_device_image)  # click set device image
        click_element(homePage_else_lock_start)  # click start
        click_element(homePage_else_lock_skip)  # click skip this step
        click_element(homePage_else_lock_start)  # click start
        assert_result_displayed(homePage_else_lock_install, True)  # display start setup

    @adb_log
    def test_C7116(self):
        # Lock Setup/Start Setup/Scanning
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        click_element(homePage_homePage_menu_in_home_page)  # click [HOME]
        click_element(homePage_homePage_add_device)  # click [set up new device]
        click_element(homePage_else_add_device_text)  # click [can't scan the code?]
        send_keys(homePage_else_enter_serial_number, selected_serial_number)  # Enter serial number
        click_element(homePage_else_lock_continue)  # click continue
        click_element(homePage_else_set_device_image)  # click set device image
        click_element(homePage_else_lock_start)  # click start
        click_element(homePage_else_lock_skip)  # click skip this step
        click_element(homePage_else_lock_start)  # click start
        click_element(homePage_else_lock_install)  # click start setup
        assert_result_displayed(homePage_else_add_new_house, True)  # display add a new house radio button

    @adb_log
    def test_C7121(self):
        # Lock Setup/House Association/Name Your Lock
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        click_element(homePage_homePage_menu_in_home_page)  # click [HOME]
        click_element(homePage_homePage_add_device)  # click [set up new device]
        click_element(homePage_else_add_device_text)  # click [can't scan the code?]
        send_keys(homePage_else_enter_serial_number, selected_serial_number)  # Enter serial number
        click_element(homePage_else_lock_continue)  # click continue
        click_element(homePage_else_set_device_image)  # click set device image
        click_element(homePage_else_lock_start)  # click start
        click_element(homePage_else_lock_skip)  # click skip this step
        click_element(homePage_else_lock_start)  # click start
        click_element(homePage_else_lock_install)  # click start setup
        send_keys(homePage_else_choose_lock_name, random.choice('abcdefghijklmnopqrstuvwxyz0123456789'))  # choose lock name

    @adb_log
    def test_C7122(self):
        # Lock Setup/House Association/Default Lock Name
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        click_element(homePage_homePage_menu_in_home_page)  # click [HOME]
        click_element(homePage_homePage_add_device)  # click [set up new device]
        click_element(homePage_else_add_device_text)  # click [can't scan the code?]
        send_keys(homePage_else_enter_serial_number, selected_serial_number)  # Enter serial number
        click_element(homePage_else_lock_continue)  # click continue
        click_element(homePage_else_set_device_image)  # click set device image
        click_element(homePage_else_lock_start)  # click start
        click_element(homePage_else_lock_skip)  # click skip this step
        click_element(homePage_else_lock_start)  # click start
        click_element(homePage_else_lock_install)  # click start setup
        assert_result_text(homePage_else_choose_lock_name, 'Front Door')  # default lock name

    @adb_log
    def test_C7123(self):
        # Lock Setup/House Association/Select Existing House
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        click_element(homePage_homePage_menu_in_home_page)  # click [HOME]
        click_element(homePage_homePage_add_device)  # click [set up new device]
        click_element(homePage_else_add_device_text)  # click [can't scan the code?]
        send_keys(homePage_else_enter_serial_number, selected_serial_number)  # Enter serial number
        click_element(homePage_else_lock_continue)  # click continue
        click_element(homePage_else_set_device_image)  # click set device image
        click_element(homePage_else_lock_start)  # click start
        click_element(homePage_else_lock_skip)  # click skip this step
        click_element(homePage_else_lock_start)  # click start
        click_element(homePage_else_lock_install, after_action_sleep=20)  # click start setup
        send_keys(homePage_else_choose_lock_name, random.choice('abcdefghijklmnopqrstuvwxyz0123456789'))  # lock name
        assert_result_displayed(homePage_else_select_existing_house, True)  # display existing house
        click_element(homePage_else_add_new_house)  # add a new house
        click_element(homePage_else_choose_house_continue)  # continue
        send_keys(homePage_else_house_new_name, 'yaleGlobal Automation')  # house name


    @adb_log
    def test_C7124(self):
        # Lock Setup/House Association/Create New House--add device successfully
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        click_element(homePage_homePage_menu_in_home_page)  # click [HOME]
        click_element(homePage_homePage_add_device)  # click [set up new device]
        click_element(homePage_else_add_device_text)  # click [can't scan the code?]
        send_keys(homePage_else_enter_serial_number, selected_serial_number)  # Enter serial number
        click_element(homePage_else_lock_continue)  # click continue
        click_element(homePage_else_set_device_image)  # click set device image
        click_element(homePage_else_lock_start)  # click start
        click_element(homePage_else_lock_skip)  # click skip this step
        click_element(homePage_else_lock_start)  # click start
        click_element(homePage_else_lock_install,  after_action_sleep=20)  # click start setup
        send_keys(homePage_else_choose_lock_name, selected_serial_number)  # lock name
        click_element(homePage_else_select_existing_house)   # click existing house
        click_element(homePage_else_choose_house_continue)  # continue
        time.sleep(60)
        click_element(homePage_else_install_skip)    # skip
        click_element(homePage_else_lock_start)  # try it out
        assert_result_text(lock_main_my_lock_name, selected_serial_number)  # display lock name
        assert_result_text(homePage_else_my_house_name, 'yaleGlobal Automation')  # display my house name

    @adb_log
    def test_C7236(self):
        # Lock screen/Name of the lock
        assert_result_displayed(homePage_else_my_house_name, True)  # display my house name
        assert_result_displayed(lock_main_my_lock_name, True)  # display lock name

    @adb_log
    def test_C7242(self):
        # Lock screen/Settings tab
        assert_result_displayed(userManagement_main_setting_tab, True)  # display settings tab

    @adb_log
    def test_C7243(self):
        # Lock screen/Activity Feed
        assert_result_displayed(userManagement_main_log_tab, True)  # display activity tab

    @adb_log
    def test_C7244(self):
        # Lock screen/Guest List
        assert_result_displayed(userManagement_main_user_tab, True)  # display guest list tab

    @adb_log
    def test_C7245(self):
        # BLE/Locked /red
        check_lock_connection()
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked

    # @adb_log
    # def test_C7246(self):
    #     # BLE/Unlocked/green
    #     if assert_result_displayed(lock_main_lock_close_status, True):
    #         pass
    #     else:
    #         click_element(lock_main_user_lock_button)  # lock
    #     click_element(lock_main_user_lock_button)  # lock
    #     assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
    #
    # @adb_log
    # def test_C7247(self):
    #     # BLE/Tap on red circle
    #     if assert_result_displayed(lock_main_lock_close_status, True):
    #         pass
    #     else:
    #         click_element(lock_main_user_lock_button)  # lock
    #     click_element(lock_main_user_lock_button)  # lock
    #     assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
    #
    # @adb_log
    # def test_C7248(self):
    #     # BLE/Tap on green circle
    #     click_element(lock_main_user_lock_button)  # lock
    #     assert_result_displayed(lock_main_lock_close_status, True)  # locked
    #
    # @adb_log
    # def test_C7249(self):
    #     # BLE/Tap on grey circle/Connecting
    #     check_lock_connection()
    #     time.sleep(30)  # 30s disconnect
    #     assert_result_displayed(lock_main_lock_grey_status, True)  # display COULD NOT CONNECT
    #     click_element(lock_main_user_lock_button)  # click lock
    #     assert_result_displayed(lock_main_lock_connect_status, True)  # display CONNECTING
    #
    # @adb_log
    # def test_C7250(self):
    #     # Lock operation/Unlock/Owner
    #     if assert_result_displayed(lock_main_lock_close_status, True):
    #         pass
    #     else:
    #         click_element(lock_main_user_lock_button)  # lock
    #         assert_result_displayed(lock_main_lock_close_status, True)
    #     click_element(lock_main_user_lock_button)  # lock
    #     assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
    #
    # @adb_log
    # def test_C7251(self):
    #     # Lock operation/Lock/Owner
    #     click_element(lock_main_user_lock_button)  # lock
    #     assert_result_displayed(lock_main_lock_close_status, True)  # locked
    #
    # @adb_log
    # def test_C7256(self):
    #     # Lock operation/Tap Activity feed/Go back /Unlock/
    #     click_element(userManagement_main_log_tab)  # click activity tab
    #     assert_result_displayed(lock_main_activity_show, True)  # display log
    #     click_element(lock_main_lock_manager)  # click lock manager
    #     assert_result_displayed(lock_main_lock_connect_status, True)  # display CONNECTING
    #
    # @adb_log
    # def test_C7257(self):
    #     # Lock operation/Tap User screen/ Go back/ Lock
    #     check_lock_connection()
    #     click_element(userManagement_main_user_tab)  # click user tab
    #     click_element(lock_main_lock_manager)  # click lock manager
    #     assert_result_displayed(lock_main_lock_connect_status, True)  # display CONNECTING
    #
    # @adb_log
    # def test_C7258(self):
    #     # Lock operation/Activity Feed Screen/ Connect/ Guest List Screen/ Connect/Keychain/Go back Connect/Unlock
    #     check_lock_connection()
    #     click_element(userManagement_main_log_tab)  # click activity tab
    #     time.sleep(10)
    #     click_element(lock_main_lock_manager)  # click lock manager
    #     assert_result_displayed(lock_main_lock_connect_status, True)  # display CONNECTING
    #     click_element(userManagement_main_user_tab)  # click user tab
    #     click_element(lock_main_lock_manager)  # click lock manager
    #     assert_result_displayed(lock_main_lock_connect_status, True)  # display CONNECTING
    #     driver.press_keycode(4)  # back
    #     click_element(userManagement_main_house_name)  # get into house
    #     assert_result_displayed(lock_main_lock_connect_status, True)  # display CONNECTING
    #
    # @adb_log
    # def test_C7261(self):
    #     # BLE/Scanning for lock
    #     pass
    #
    # @adb_log
    # def test_C7264(self):
    #     # BLE/Bluetooth OFF/Message
    #     bt_switch('off')
    #     assert_result_displayed(lock_lockSetting_assert_bt_not_open, True)  # BLUETOOTH DISABLED
    #     bt_switch('on')
    #
    # @adb_log
    # def test_C7267(self):
    #     # Lock state/Lock screen/Lock
    #     if assert_result_displayed(lock_main_lock_open_status, True):
    #         pass
    #     else:
    #         click_element(lock_main_user_lock_button)  # lock
    #         assert_result_displayed(lock_main_lock_open_status, True)
    #     click_element(lock_main_user_lock_button)  # lock
    #     assert_result_displayed(lock_main_lock_close_status, True)  # locked
    #
    # @adb_log
    # def test_C7268(self):
    #     # Lock state/Lock screen/Lock/Unlock
    #     if assert_result_displayed(lock_main_lock_close_status, True):
    #         pass
    #     else:
    #         click_element(lock_main_user_lock_button)  # lock
    #         assert_result_displayed(lock_main_lock_close_status, True)
    #     click_element(lock_main_user_lock_button)  # lock
    #     assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
    #
    # @adb_log
    # def test_C7386(self):
    #     # Lock Settings/Lock Name
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     click_element(lock_setting_lock_setting)  # click lock setting
    #     assert_result_displayed(lock_lockSetting_lock_name_show, True)  # display lock name
    #
    # @adb_log
    # def test_C7387(self):
    #     # Lock Settings/Smart Alerts
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     click_element(lock_setting_lock_setting)  # click lock setting
    #     assert_result_displayed(lock_lockSetting_alarm, True)  # display smart alert
    #
    # @adb_log
    # def test_C7389(self):
    #     # Lock Settings/Factory Reset
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     click_element(lock_setting_lock_setting)  # click lock setting
    #     assert_result_displayed(lock_lockSetting_reset, True)  # display factory reset
    #
    # @adb_log
    # def test_C7392(self):
    #     # Lock Settings/Installation Instructions
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     click_element(lock_setting_lock_setting)  # click lock setting
    #     assert_result_displayed(lock_lockSetting_instructions, True)  # display Installation Instructions
    #
    # @adb_log
    # def test_C7393(self):
    #     # Lock Settings/Troubleshooting Help
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     click_element(lock_setting_lock_setting)  # click lock setting
    #     assert_result_displayed(lock_lockSetting_troubleshooting, True)  # display Installation Instructions
    #
    # @adb_log
    # def test_C7394(self):
    #     # Lock Settings/Device Information screen/ Serial #
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     click_element(lock_setting_lock_setting)  # click lock setting
    #     click_element(lock_lockSetting_device_info)  # click device info
    #     time.sleep(20)
    #     check_lock_info(lock_lockSetting_module_serial)  # display module serial
    #
    # @adb_log
    # def test_C7395(self):
    #     # Lock Settings/Device Information screen/ Firmware Version
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     click_element(lock_setting_lock_setting)  # click lock setting
    #     click_element(lock_lockSetting_device_info)  # click device info
    #     time.sleep(20)
    #     check_lock_info(lock_lockSetting_module_version)  # display module version
    #
    # @adb_log
    # def test_C7398(self):
    #     # Lock Settings/No BLE Connection/Lock Connection Required
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     click_element(lock_setting_lock_setting)  # click lock setting
    #     click_element(lock_lockSetting_device_info)  # click device info
    #     time.sleep(20)
    #     check_lock_info(lock_lockSetting_module_version)  # display module version
    #
    # @adb_log
    # def test_C7399(self):
    #     # Lock Settings/No BLE Connection/Lock Connection Required/Ok
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     click_element(lock_setting_lock_setting)  # click lock setting
    #     click_element(lock_lockSetting_device_info)  # click device info
    #     time.sleep(20)
    #     check_lock_info(lock_lockSetting_module_version)  # display module version
    #
    # @adb_log
    # def test_C7381(self):
    #     # Settings Tab/House Name and Photo
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     click_element(lock_setting_house_setting)  # click house setting
    #     assert_result_displayed(lock_setting_house_name, True)  # display house name
    #     assert_result_displayed(lock_setting_house_image_button, True)  # display house_image
    #
    # @adb_log
    # def test_C7382(self):
    #     # Settings Tab/Edit House Owners
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     click_element(lock_setting_edit_house_owner)  # click edit house owner
    #     assert_result_displayed(lock_setting_admin_permission_check.format('Joyce Shen'), False)
    #
    # @adb_log
    # def test_C7383(self):
    #     # Settings Tab/Lock Settings
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     assert_result_displayed(lock_setting_lock_setting, True)  # display lock setting
    #
    # @adb_log
    # def test_C7402(self):
    #     # Lock Settings/Yale Lock Icon
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     assert_result_displayed(lock_setting_lock_icon, True)  # display lock icon
    #
    # @adb_log
    # def test_C7403(self):
    #     # Lock Settings/Information/Device Information
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     click_element(lock_setting_lock_setting)  # click lock setting
    #     scroll_and_click(lock_lockSetting_scroll_view, lock_lockSetting_device_info)  # 上滑点击设备信息
    #     check_lock_info(lock_lockSetting_module_serial)  # display module serial
    #     check_lock_info(lock_lockSetting_module_version)  # display module version
    #     check_lock_info(lock_lockSetting_lock_model)  # display lock model
    #     check_lock_info(lock_lockSetting_lock_firmware_version)  # display lock firmware version
    #
    # @adb_log
    # def test_C7299(self):
    #     # Invite / Self Invite
    #     # login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(userManagement_invite_invite)  # click invite
    #     click_element(userManagement_invite_full_access)  # click full access
    #     click_element(userManagement_invite_select_phone)  # invite via mobile
    #     click_element(userManagement_invite_area_number)  # click country code
    #     send_keys(userManagement_invite_country_search, 'China')  # enter China
    #     click_element(userManagement_invite_area_name)  # click China
    #     send_keys(userManagement_invite_phone_input, '15216762426')  # enter phone number
    #     click_element(userManagement_invite_phone_input)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     send_keys(userManagement_invite_first_name, 'a')  # input first name
    #     send_keys(userManagement_invite_last_name, 'b')  # input last name
    #     click_element(userManagement_invite_last_name)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     click_element(userManagement_invite_send_button)  # send
    #     assert_result_displayed(userManagement_invite_add_yourself, True)  # error prompt 'you can't add youself'
    #     click_element(userManagement_invite_invite_cancel)  # click ok on error popup
    #
    # @adb_log
    # def test_C7300(self):
    #     # Invite / Invitee already has access
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(userManagement_invite_invite)  # click invite
    #     click_element(userManagement_invite_full_access)  # click full access
    #     click_element(userManagement_invite_select_phone)  # invite via mobile
    #     click_element(userManagement_invite_area_number)  # click country code
    #     send_keys(userManagement_invite_country_search, 'China')  # enter China
    #     click_element(userManagement_invite_area_name)  # click China
    #     send_keys(userManagement_invite_phone_input, '18201823198')  # enter phone number
    #     click_element(userManagement_invite_phone_input)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     send_keys(userManagement_invite_first_name, 'a')  # input first name
    #     send_keys(userManagement_invite_last_name, 'b')  # input last name
    #     click_element(userManagement_invite_last_name)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     click_element(userManagement_invite_send_button)  # send
    #     assert_result_displayed(userManagement_invite_has_access, True)  # error prompt 'User already has access'
    #     click_element(userManagement_invite_invite_cancel)  # click ok on error popup
    #
    # @adb_log
    # def test_C7301(self):
    #     # Invite /Phone number/ Country code
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(userManagement_invite_invite)  # click invite
    #     click_element(userManagement_invite_full_access)  # click full access
    #     click_element(userManagement_invite_select_phone)  # invite via mobile
    #     click_element(userManagement_invite_area_number)  # click country code
    #     send_keys(userManagement_invite_country_search, 'China')  # enter China
    #     click_element(userManagement_invite_area_name)  # click China
    #     assert_result_text(userManagement_invite_area_number, '+86')  # check china code 86
    #
    # @adb_log
    # def test_C7302(self):
    #     # Invite / Phone number via keyboard
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(userManagement_invite_invite)  # click invite
    #     click_element(userManagement_invite_full_access)  # click full access
    #     click_element(userManagement_invite_select_phone)  # invite via mobile
    #     click_element(userManagement_invite_area_number)  # click country code
    #     send_keys(userManagement_invite_country_search, 'China')  # enter China
    #     click_element(userManagement_invite_area_name)  # click China
    #     send_keys(userManagement_invite_phone_input, '15000020000')  # enter phone number
    #     click_element(userManagement_invite_phone_input)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     send_keys(userManagement_invite_first_name, 'via')  # input first name
    #     send_keys(userManagement_invite_last_name, 'keyboard')  # input last name
    #     click_element(userManagement_invite_last_name)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     click_element(userManagement_invite_send_button)  # send
    #     click_element(userManagement_main_user_tab)  # guest list
    #     scroll_screen_up()
    #     assert_result_displayed(userManagement_invite_user_invited, True)  # display invited
    #
    # @adb_log
    # def test_C7303(self):
    #     # Invite / Phone number via contact list
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(userManagement_invite_invite)  # click invite
    #     click_element(userManagement_invite_full_access)  # click full access
    #     click_element(userManagement_invite_select_contact)  # invite via contact list
    #     click_element(userManagement_invite_multiple_contact)  # invite multiple contact
    #     click_element(userManagement_invite_send_button)  # send
    #     scroll_screen_up()
    #     assert_result_displayed(userManagement_invite_invited_contact, True)  # display multiple contact in guest list
    #
    # @adb_log
    # def test_C7304(self):
    #     # Invite / Already has an August account
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(userManagement_invite_invite)  # click invite
    #     click_element(userManagement_invite_full_access)  # click full access
    #     click_element(userManagement_invite_select_phone)  # invite via mobile
    #     click_element(userManagement_invite_area_number)  # click country code
    #     send_keys(userManagement_invite_country_search, 'China')  # enter China
    #     click_element(userManagement_invite_area_name)  # click China
    #     send_keys(userManagement_invite_phone_input, '18672787131')  # enter phone number
    #     click_element(userManagement_invite_phone_input)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     send_keys(userManagement_invite_first_name, 'a')  # input first name
    #     send_keys(userManagement_invite_last_name, 'b')  # input last name
    #     click_element(userManagement_invite_last_name)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     click_element(userManagement_invite_send_button)  # send
    #     scroll_screen_up()
    #     assert_result_displayed(userManagement_invite_registered_contact, True)  # display registered contact in guest list
    #
    # @adb_log
    # def test_C7305(self):
    #     # Invite / Invitee does not have an August account
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(userManagement_invite_invite)  # click invite
    #     click_element(userManagement_invite_full_access)  # click full access
    #     click_element(userManagement_invite_select_phone)  # invite via mobile
    #     click_element(userManagement_invite_area_number)  # click country code
    #     send_keys(userManagement_invite_country_search, 'China')  # enter China
    #     click_element(userManagement_invite_area_name)  # click China
    #     send_keys(userManagement_invite_phone_input, '15000030000')  # enter phone number
    #     click_element(userManagement_invite_phone_input)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     send_keys(userManagement_invite_first_name, 'not')  # input first name
    #     send_keys(userManagement_invite_last_name, 'registered')  # input last name
    #     click_element(userManagement_invite_last_name)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     click_element(userManagement_invite_send_button)  # send
    #     scroll_screen_up()
    #     assert_result_displayed(userManagement_invite_not_registered, True)  # display not registered contact in invited list
    #
    # @adb_log
    # def test_C7306(self):
    #     # Invite / Change access level for invitee from Guest to Owner
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(userManagement_invite_invite)  # click invite
    #     click_element(userManagement_invite_full_access)  # click full access
    #     click_element(userManagement_invite_select_phone)  # invite via mobile
    #     click_element(userManagement_invite_area_number)  # click country code
    #     send_keys(userManagement_invite_country_search, 'China')  # enter China
    #     click_element(userManagement_invite_area_name)  # click China
    #     send_keys(userManagement_invite_phone_input, '13761583904')  # enter phone number
    #     click_element(userManagement_invite_phone_input)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     send_keys(userManagement_invite_first_name, 'change')  # input first name
    #     send_keys(userManagement_invite_last_name, 'owner')  # input last name
    #     click_element(userManagement_invite_last_name)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     click_element(userManagement_invite_access_set)  # access set
    #     click_element(userManagement_invite_owner_access)  # guest change to owner
    #     click_element(userManagement_invite_send_button)  # send
    #     assert_result_text(userManagement_invite_access_set, 'OWNER')  # change to owner successfully
    #     click_element(userManagement_invite_send_button)  # send
    #
    # @adb_log
    # def test_C7311(self):
    #     # Invite / Cancel invite
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(userManagement_invite_invite)  # click invite
    #     click_element(userManagement_invite_full_access)  # click full access
    #     click_element(userManagement_invite_select_phone)  # invite via mobile
    #     click_element(userManagement_invite_area_number)  # click country code
    #     send_keys(userManagement_invite_country_search, 'China')  # enter China
    #     click_element(userManagement_invite_area_name)  # click China
    #     send_keys(userManagement_invite_phone_input, '13761583904')  # enter phone number
    #     click_element(userManagement_invite_phone_input)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     send_keys(userManagement_invite_first_name, 'change')  # input first name
    #     send_keys(userManagement_invite_last_name, 'owner')  # input last name
    #     click_element(userManagement_invite_last_name)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     click_element(userManagement_invite_back_button)  # back arrow
    #     click_element(userManagement_invite_back_button)  # back arrow
    #     click_element(userManagement_invite_back_button)  # back arrow
    #     assert_result_displayed(userManagement_invite_invite, True)  # back to guest list
    #
    # @adb_log
    # def test_C7314(self):
    #     # Invite / Delete Guest account
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(UserManagement_user_admin.format('Philadelphia She'))  # click philip
    #     click_element(UserManagement_delete_user)  # click delete
    #     click_element(userManagement_invite_confirm, after_action_sleep=8)  # confirm
    #     invite_user_global('18672787131')  # invite philip
    #
    # @adb_log
    # def test_C7320(self):
    #     # Recurring / Access Level and Schedule / Change Guest Access Level from Owner to Recurring
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(UserManagement_user_admin.format('Zhu Shen'))  # click Zhu
    #     click_element(userManagement_invite_access_set)  # access set
    #     click_element(userManagement_invite_general_access)  # owner change to guest
    #     click_element(userManagement_edit_right_button)  # save
    #     assert_result_text(userManagement_invite_access_set, 'GUEST')  # change to guest successfully
    #     click_element(userManagement_edit_schedule_set)  # schedule set
    #     click_element(userManagement_invite_schedule_recurring)  # set schedule recurring
    #     click_element(userManagement_edit_right_button)  # save
    #     # assert_result_text(userManagement_edit_schedule_set, 'M 8:00 AM - 8:00 PM')  # change to recurring successfully
    #     click_element(userManagement_edit_right_button)  # save update
    #
    # @adb_log
    # def test_C7321(self):
    #     # Recurring / Access Schedule / Change Guest Access Schedule from Temporary to Recurring
    #     click_element(userManagement_main_user_tab)  # user tab
    #     click_element(UserManagement_user_admin.format('Zhu Shen'))  # click Zhu
    #     click_element(userManagement_edit_schedule_set)  # schedule set
    #     click_element(userManagement_invite_schedule_temp)  # set schedule temporary
    #     click_element(userManagement_edit_right_button)  # save
    #     click_element(userManagement_edit_right_button)  # save update
    #
    # @adb_log
    # def test_C7322(self):
    #     # Recurring / Access Schedule / Change Guest Access Schedule from Always to Recurring
    #     click_element(userManagement_main_user_tab)  # user tab
    #     click_element(UserManagement_user_admin.format('Zhu Shen'))  # click Zhu
    #     click_element(userManagement_edit_schedule_set)  # schedule set
    #     click_element(userManagement_invite_schedule_always)  # set schedule always
    #     click_element(userManagement_edit_right_button)  # save
    #     click_element(userManagement_edit_right_button)  # save update
    #
    # @adb_log
    # def test_C7334(self):
    #     # Recurring / August account / Guest / Recurring / Check default
    #     click_element(userManagement_main_user_tab)  # user tab
    #     click_element(userManagement_invite_invite)  # click invite
    #     click_element(userManagement_invite_full_access)  # click full access
    #     click_element(userManagement_invite_select_phone)  # invite via mobile
    #     click_element(userManagement_invite_area_number)  # click country code
    #     send_keys(userManagement_invite_country_search, 'China')  # enter China
    #     click_element(userManagement_invite_area_name)  # click China
    #     send_keys(userManagement_invite_phone_input, '18672787131')  # enter phone number
    #     click_element(userManagement_invite_phone_input)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     send_keys(userManagement_invite_first_name, 'change')  # input first name
    #     send_keys(userManagement_invite_last_name, 'owner')  # input last name
    #     click_element(userManagement_invite_last_name)  # touch keyboard
    #     self.driver.keyevent(66)  # click enter keyboard
    #     click_element(userManagement_edit_schedule_set)  # schedule set
    #     click_element(userManagement_invite_schedule_recurring)  # set schedule recurring
    #     assert_result_text(userManagement_edit_rec_start_time, '8:00 AM')  # rec start time 8:00 AM
    #     assert_result_text(userManagement_edit_rec_end_time, '8:00 PM')  # rec end time 8:00 PM
    #
    # @adb_log
    # def test_C7343(self):
    #     # Temporary/Access Level and Schedule/Change Guest Access Level from Owner to Temporary
    #     click_element(userManagement_main_user_tab)  # user tab
    #     click_element(UserManagement_user_admin.format('Philadelphia She'))  # click philip
    #     click_element(userManagement_invite_access_set)  # access set
    #     click_element(userManagement_invite_general_access)  # owner change to guest
    #     click_element(userManagement_edit_right_button)  # save
    #     assert_result_text(userManagement_invite_access_set, 'GUEST')  # change to guest successfully
    #     click_element(userManagement_edit_schedule_set)  # schedule set
    #     click_element(userManagement_invite_schedule_temp)  # set schedule temporary
    #     click_element(userManagement_edit_right_button)  # save
    #     click_element(userManagement_edit_right_button)  # save update
    #
    # @adb_log
    # def test_C7344(self):
    #     # Temporary/Access Schedule/Change Guest Access Schedule from Always to Temporary
    #     click_element(userManagement_main_user_tab)  # user tab
    #     click_element(UserManagement_user_admin.format('Philadelphia She'))  # click philip
    #     click_element(userManagement_edit_schedule_set)  # schedule set
    #     click_element(userManagement_invite_schedule_always)  # set schedule always
    #     click_element(userManagement_edit_right_button)  # save
    #     assert_result_text(userManagement_edit_schedule_set, 'ALWAYS')  # change to ALWAYS successfully
    #     click_element(userManagement_edit_right_button)  # save update
    #
    # @adb_log
    # def test_C7345(self):
    #     # Temporary/Access Schedule/Change Guest Access Schedule from Recurring to Temporary
    #     click_element(userManagement_main_user_tab)  # user tab
    #     click_element(UserManagement_user_admin.format('Philadelphia She'))  # click philip
    #     click_element(userManagement_edit_schedule_set)  # schedule set
    #     click_element(userManagement_invite_schedule_recurring)  # set schedule recurring
    #     click_element(userManagement_edit_right_button)  # save
    #     click_element(userManagement_edit_right_button)  # save update
    #     time.sleep(10)
    #     click_element(UserManagement_user_admin.format('Philadelphia She'))  # click philip
    #     click_element(userManagement_edit_schedule_set)  # schedule set
    #     click_element(userManagement_invite_schedule_temp)  # set schedule temp
    #     click_element(userManagement_edit_right_button)  # save
    #     click_element(userManagement_edit_right_button)  # save update
    #
    # @adb_log
    # def test_C7356(self):
    #     # Access screen/Access Level from guest to owner
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(UserManagement_user_admin.format('Philadelphia She'))  # click philip
    #     click_element(userManagement_invite_access_set)  # access set
    #     click_element(userManagement_invite_owner_access)  # guest change to owner
    #     click_element(userManagement_edit_right_button)  # save
    #     assert_result_text(userManagement_invite_access_set, 'OWNER')  # change to owner successfully
    #     assert_result_text(userManagement_edit_schedule_set, 'ALWAYS')  # change to ALWAYS successfully
    #     click_element(userManagement_edit_right_button)  # save update
    #
    # @adb_log
    # def test_C7357(self):
    #     # Access screen/Access Level from owner to guest
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(UserManagement_user_admin.format('Philadelphia She'))  # click philip
    #     click_element(userManagement_invite_access_set)  # access set
    #     click_element(userManagement_invite_general_access)  # OWNER change to guest
    #     click_element(userManagement_edit_right_button)  # save
    #     assert_result_text(userManagement_invite_access_set, 'GUEST')  # change to GUEST successfully
    #     assert_result_text(userManagement_edit_schedule_set, 'ALWAYS')  # display ALWAYS
    #     click_element(userManagement_edit_right_button)  # save update
    #
    # @adb_log
    # def test_C7358(self):
    #     # Access screen/Owners have always access
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(UserManagement_user_admin.format('Joyce Shen'))  # click Joyce
    #     click_element(userManagement_edit_schedule_set)  # schedule set
    #     assert_result_text(userManagement_invite_access_set, 'OWNER')  # display OWNER
    #     assert_result_text(userManagement_edit_schedule_set, 'ALWAYS')  # display ALWAYS
    #
    # @adb_log
    # def test_C7359(self):
    #     # Access screen/access type for guest: always
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(UserManagement_user_admin.format('Zhu Shen'))  # click Zhu
    #     click_element(userManagement_edit_schedule_set)  # schedule set
    #     click_element(userManagement_invite_schedule_temp)  # set schedule temp
    #     click_element(userManagement_edit_right_button)  # save
    #     click_element(userManagement_edit_right_button)  # save update
    #     time.sleep(10)
    #     click_element(UserManagement_user_admin.format('Zhu Shen'))  # click Zhu
    #     click_element(userManagement_edit_schedule_set)  # schedule set
    #     click_element(userManagement_invite_schedule_always)  # set schedule always
    #     click_element(userManagement_edit_right_button)  # save
    #     click_element(userManagement_edit_right_button)  # save update
    #
    # @adb_log
    # def test_C7360(self):
    #     # Access screen/access type/change
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(UserManagement_user_admin.format('Zhu Shen'))  # click Zhu
    #     click_element(userManagement_edit_schedule_set)  # schedule set
    #     click_element(userManagement_invite_schedule_temp)  # set schedule temp
    #     click_element(userManagement_edit_right_button)  # save
    #     click_element(userManagement_edit_right_button)  # save update
    #
    # @adb_log
    # def test_DC7400(self):
    #     # Unity Factory Reset/Device Factory Reset
    #     click_element(userManagement_main_setting_tab)  # click setting tab
    #     click_element(lock_setting_lock_setting)  # click lock setting
    #     click_element(lock_lockSetting_reset)  # click factory reset
    #     click_element(lock_lockSetting_reset_button)  # click reset button
    #     click_element(lock_lockSetting_reset_confirm)  # click reset confirm button

    def cases(self):
        return list(filter(lambda m: m.startswith("test_C") and callable(getattr(self, m)), dir(self)))
