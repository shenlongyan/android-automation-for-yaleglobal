import logging
import unittest
from Configuration.OperationMethod import *
from Configuration import StartUp
import random
import os

autoLock = os.environ['AutoLock']
print('autoLock=' + autoLock)

class TestLockSetting(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = StartUp.driver
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H_%M_%S',
                            filename=FolderList.logging_name,
                            filemode='a')
        logging.getLogger()
        time.sleep(1)

    def add_img(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

    def setUp(self) -> None:
        os.system('adb shell svc wifi enable')
        bt_switch('on')
        # add_card_yale()
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    @adb_log
    def test_C5427(self):
        # 邀请列表显示
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        assert_result_displayed(userManagement_invite_full_access, True)  # display full access
        assert_result_displayed(userManagement_invite_PINCODE_access, True)  # display PINCODE access
        assert_result_displayed(userManagement_invite_RFID_access, True)  # display RFID access
        assert_result_displayed(userManagement_invite_FP_access, True)  # display fp access

    @adb_log
    def test_C5428(self):
        # 邀请Fingerprint Only按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        assert_result_displayed(userManagement_invite_RFID_first_name, True)  # display input name

    @adb_log
    def test_C5430(self):
        # 姓名输入页面返回按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        click_element(userManagement_invite_guest_back_button)  # back
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list

    @adb_log
    def test_C5431(self):
        # 姓名输入页面退出按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        click_element(userManagement_invite_right_close)  # close
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list

    @adb_log
    def test_C5432(self):
        # 用户姓名为空
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        assert_result_enable(userManagement_invite_contact_continue, False)    # continue button is grey

    @adb_log
    def test_C5433(self):
        # 用户姓名为空格
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, ' ')  # input space
        send_keys(userManagement_invite_RFID_last_name, ' ')  # input space
        assert_result_enable(userManagement_invite_contact_continue, True)  # continue button is highlight

    @adb_log
    def test_C5434(self):
        # 用户姓名为特殊字符
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, '!@#')  # input special character
        send_keys(userManagement_invite_RFID_last_name, '!@#')  # input special character
        assert_result_enable(userManagement_invite_contact_continue, True)  # continue button is highlight

    @adb_log
    def test_C5435(self):
        # 用户姓名为全数字
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, '123')  # input number
        send_keys(userManagement_invite_RFID_last_name, '456')  # input number
        assert_result_enable(userManagement_invite_contact_continue, True)  # continue button is highlight

    @adb_log
    def test_C5436(self):
        # 用户姓名为全英文
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'abc')  # input English
        send_keys(userManagement_invite_RFID_last_name, 'joyce')  # input English
        assert_result_enable(userManagement_invite_contact_continue, True)  # continue button is highlight

    @adb_log
    def test_C5437(self):
        # 用户姓名为全汉字
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, '龙燕')  # input character
        send_keys(userManagement_invite_RFID_last_name, '沈')  # input character
        assert_result_enable(userManagement_invite_contact_continue, True)  # continue button is highlight

    @adb_log
    def test_C5438(self):
        # 用户姓名为混合
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, '龙L123')  # input character
        send_keys(userManagement_invite_RFID_last_name, '沈s$')  # input character
        assert_result_enable(userManagement_invite_contact_continue, True)  # continue button is highlight

    @adb_log
    def test_C5440(self):
        # 姓名输入合法继续按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        assert_result_displayed(userManagement_invite_lock_list, True)  # display lock list

    @adb_log
    def test_C5442(self):
        # 选择锁页面返回按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_guest_back_button)  # back
        assert_result_displayed(userManagement_invite_RFID_first_name, True)  # display name input

    @adb_log
    def test_C5443(self):
        # 选择锁页面退出按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_right_close)  # close
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list

    @adb_log
    def test_C5449(self):
        # 单锁选择功能
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        assert_result_displayed(userManagement_invite_lock_checked, True)  # display lock checked
        assert_result_enable(userManagement_invite_lock_continue, True)  # continue button is highlight

    @adb_log
    def test_C5451(self):
        # 未选择锁继续按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        assert_result_enable(userManagement_invite_lock_continue, False)  # continue button is grey

    @adb_log
    def test_C5456(self):
        # 选择一把支持指纹schedule的锁继续按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        assert_result_displayed(userManagement_invite_wait_connection, True)  # display wait for lock connection

    @adb_log
    def test_C5458(self):
        # 蓝牙连锁页面关闭按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        click_element(userManagement_invite_right_close)  # close
        assert_result_displayed(UserManagement_pop_up, True)  # display pop_up window

    @adb_log
    def test_C5459(self):
        # 蓝牙连锁页面未连上锁继续按钮
        pass

    @adb_log
    def test_C5460(self):
        # 蓝牙连锁页面连上锁继续按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        click_element(userManagement_invite_card_continue)  # continue
        assert_result_displayed(userManagement_invite_start_FP_registration, True)  # start FP registration

    @adb_log
    def test_C5463(self):
        # 录入指纹引导页面关闭按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        click_element(userManagement_invite_card_continue)  # continue
        click_element(userManagement_invite_right_close)  # close
        assert_result_displayed(UserManagement_pop_up, True)  # display pop_up window

    @adb_log
    def test_C5464(self):
        # 录入指纹引导页面开始按钮
        pass

    @adb_log
    def test_C5466(self):
        # 录入指纹页面返回按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        click_element(userManagement_invite_card_continue)  # continue
        click_element(userManagement_invite_card_continue)  # continue
        driver.press_keycode(4)
        assert_result_displayed(UserManagement_pop_up, True)  # display pop_up window

    @adb_log
    def test_C5467(self):
        # 录入指纹页面关闭按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        click_element(userManagement_invite_card_continue)  # continue
        click_element(userManagement_invite_card_continue)  # continue
        click_element(userManagement_invite_right_close)  # close
        assert_result_displayed(UserManagement_pop_up, True)  # display pop_up window

    @adb_log
    def test_C5471(self):
        # 录入指纹成功页面完成按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().finger_register())  # REGISTER fingerprint
        assert_result_displayed(userManagement_invite_register_success, True)  # register FP success
        assert_result_displayed(userManagement_invite_register_sub_message.format('Front Door'), True)  # display lock name
        click_element(userManagement_invite_register_finish)  # click finish
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list
        click_element(UserManagement_user_admin.format('FP ONLY'))  # click FP ONLY
        assert_result_text(userManagement_invite_FP_registered_value, 'REGISTERED')  # registered
        assert_result_displayed(userManagement_invite_access_always, True)  # always
        # assert_result_displayed(userManagement_invite_RFID_access_level, True)  # CONTACTLESS KEY ONLY
        # click_element(userManagement_invite_RFID_tab)  # click contactless key
        # assert_result_displayed(userManagement_invite_registered_info, True)  # registered

    @adb_log
    def test_C5472(self):
        # 录入指纹成功页面锁名字
        pass

    @adb_log
    def test_C5473(self):
        # 录入指纹正常测试
        check_lock_connection()
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().finger_open())  # UNLOCK BY fingerprint
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C5477(self):
        # 添加指纹锁端超时
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout

    @adb_log
    def test_C5480(self):
        # 注册指纹超时页面关闭按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout
        click_element(userManagement_invite_right_close)  # close
        assert_result_text(userManagement_invite_FP_registered_value, 'NOT REGISTERED')  # NOT registered

    @adb_log
    def test_C5481(self):
        # 注册指纹超时页面重新注册按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout
        click_element(userManagement_invite_lock_continue)   # restart registration
        assert_result_displayed(userManagement_invite_wait_connection, True)  # display wait for lock connection

    @adb_log
    def test_C5481(self):
        # 注册指纹失败页面重新注册按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_FP_access)  # click fingerprint only
        send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        bt_switch('off')
        assert_result_displayed(userManagement_invite_register_failure, True)  # register failure
        click_element(userManagement_invite_lock_continue)  # restart registration
        assert_result_displayed(userManagement_invite_wait_connection, True)  # display wait for lock connection

    @adb_log
    def test_C5494(self):
        # Fingerprint Only用户详情页面用户默认schedule UI test at C5471
        pass

    @adb_log
    def test_C5495(self):
        # Fingerprint Only用户详情页面已注册指纹UI test at C5471
        pass

    @adb_log
    def test_C5496(self):
        # Fingerprint Only用户详情页面未注册指纹UI TEST AT C5480
        pass

    @adb_log
    def test_C5497(self):
        # Fingerprint Only用户详情页面保存按钮
        pass

    @adb_log
    def test_C5499(self):
        # Fingerprint Only用户详情页面删除指纹按钮
        pass

    @adb_log
    def test_C5500(self):
        # Fingerprint Only用户详情页面删除指纹确认按钮
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('FP ONLY'))  # click FP ONLY
        click_element(userManagement_invite_FP_registered_value)  # click fingerprint
        click_element(userManagement_invite_unregister_button)  # click unregister button
        click_element(userManagement_invite_confirm)  # click confirm
        assert_result_displayed(UserManagement_user_admin.format('FP ONLY'), False)  # FP only user deleted
        click_element(userManagement_main_home_tab)  # lock manage
        check_lock_connection()
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().finger_open())  # UNLOCK BY fingerprint
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C5502(self):
        # 锁支持指纹schedule_Fingerprint Only用户详情页面用户schedule为多次UI
        pass

    @adb_log
    def test_C5503(self):
        # 锁支持指纹schedule_Fingerprint Only用户详情页面用户schedule为临时UI
        pass

    @adb_log
    def test_C5504(self):
        # 锁支持指纹schedule_Fingerprint Only用户详情页面用户schedule为永久UI
        pass

    @adb_log
    def test_C5505(self):
        # 锁支持指纹schedule_Fingerprint Only用户永久改为多次
        invite_FP_only()        # invite_FP_only

    @adb_log
    def test_C5528(self):
        # 给管理员添加指纹
        register_FP(user_name)




















    def cases(self):
        return list(filter(lambda m: m.startswith("test_C") and callable(getattr(self, m)), dir(self)))
