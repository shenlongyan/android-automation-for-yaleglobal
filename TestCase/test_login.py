# import logging
# import unittest
# from Configuration.OperationMethod import *
# from Configuration import StartUp
# from Redis.VerificationCode import verification_code_phone, verification_code_email
#
#
# class TestLogin(unittest.TestCase):
#
#     @classmethod
#     def setUpClass(cls) -> None:
#         cls.driver = StartUp.driver
#         logging.basicConfig(level=logging.DEBUG,
#                             format='%(asctime)s %(levelname)s %(message)s',
#                             datefmt='%Y-%m-%d %H_%M_%S',
#                             filename=FolderList.logging_name,
#                             filemode='a')
#         logging.getLogger()
#         time.sleep(1)
#
#     def add_img(self):
#         # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
#         self.imgs.append(self.driver.get_screenshot_as_base64())
#         return True
#
#     def setUp(self) -> None:
#         os.system('adb shell svc wifi enable')
#         bt_switch('on')
#         self.imgs = []
#         self.addCleanup(self.cleanup)
#
#     def cleanup(self):
#         pass
#
#     # @unittest.skipIf(app_select == 'newpanpan', 'Depending on devices')  # 不同机型步骤不一致
#     # @adb_log
#     # def test_login_tc001(self):
#     #     if app_select == 'panpan':
#     #         click_element(register_button_login)  # 点击【登录】
#     #         assert_result_text(login_compound_login_method, login_assert_login_method_email)  # 显示【邮箱登录】
#     #         click_element(login_button_forget_password)  # 点击【忘记密码】
#     #         assert_result_text(login_assert_title, login_assert_forget_password_page_text)  # 显示忘记密码
#     #         click_element(register_button_back)  # 点击返回
#     #         click_element(login_button_lost_phone)  # 点击【遗失手机】
#     #         assert_result_displayed(login_assert_lost_phone, True)  # 显示遗失手机
#     #     elif app_select == 'newpanpan':
#     #         # 语言测试
#     #         # 需要去系统-设置模块，切换系统语言至英文，不同的机型操作步骤不一致
#     #         more_setting_cn = '/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[3]'
#     #         more_setting_en = '/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[4]'
#     #         language_setting = '/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[2]/android.widget.RelativeLayout'
#     #         language_setting_detail = '/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]'
#     #         language_select_cn = '/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]'
#     #         language_select_en = '/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[3]'
#     #         # 切换英语语言
#     #         os.system('adb shell am start com.android.settings/com.android.settings.Settings')
#     #         for i in range(0, 2):
#     #             start_x = driver.get_window_size()['width'] * 0.5
#     #             start_y = driver.get_window_size()['height'] * 0.7
#     #             end_x = start_x
#     #             end_y = driver.get_window_size()['height'] * 0.3
#     #             driver.swipe(int(start_x), int(start_y), int(end_x), int(end_y), 500)
#     #         click_element(more_setting_cn)
#     #         click_element(language_setting)
#     #         click_element(language_setting_detail)
#     #         click_element(language_select_en)
#     #         # 切换App
#     #         os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
#     #         click_element("//*[@text='Yale Access']")
#     #         assert_result_text(register_main_login, 'SIGN IN')  # 显示英语
#     #         # 切换中文语言
#     #         os.system('adb shell am start com.android.settings/com.android.settings.Settings')
#     #         click_element(more_setting_en)
#     #         click_element(language_setting)
#     #         click_element(language_setting_detail)
#     #         click_element(language_select_cn)
#     #         # 切换App
#     #         os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
#     #         click_element("//*[@text='Yale Access']")
#     #         assert_result_text(register_main_login, '登录')  # 显示中文
#     #     else:
#     #         assert False
#
#     @adb_log
#     def test_C491(self):
#         if app_select == 'panpan':
#             click_element(register_button_login)  # 点击【登录】
#             assert_result_text(login_compound_login_method, login_assert_login_method_email)  # 显示【邮箱登录】
#             click_element(login_compound_login_method)  # 点击【邮箱登录】
#             assert_result_text(login_compound_login_method, login_assert_login_method_phone)  # 显示【手机登录】
#             click_element(login_button_forget_password)  # 点击【忘记密码】
#             assert_result_text(login_assert_title, login_assert_forget_password_page_text)  # 显示忘记密码
#             click_element(register_button_back)  # 点击返回
#             click_element(login_button_lost_phone)  # 点击【遗失手机】
#             assert_result_displayed(login_assert_lost_phone, True)  # 显示遗失手机
#         elif app_select == 'newpanpan':
#             # 登录页返回按钮
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_back_button)  # 点击【返回按钮】
#             assert_result_displayed(register_main_register, True)  # 显示【注册】
#         else:
#             assert False
#
#     @adb_log
#     def test_C410(self):
#         if app_select == 'panpan':
#             click_element(register_button_login)  # 点击【登录】
#             click_element(login_compound_phone_area)  # 点击【手机区号】
#             click_element(register_button_phone_area_select)  # 选择【Afghanistan】
#             assert_result_displayed(register_button_phone_area_select, True)  # 手机区号显示【+93】
#         elif app_select == 'newpanpan':
#             # 手机登录页UI测试
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_type_switch)  # 点击【邮箱登录】
#             assert_result_text(login_login_type_switch, login_login_type_phone)  # 显示【手机号登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             assert_result_displayed(login_forget_password_switch_mode, True)  # 显示忘记密码页按键
#             # click_element(login_forget_back_button)  # 点击【返回】
#             self.driver.keyevent(4)  # 点击【返回】
#             click_element(login_login_lost_phone)  # 点击【遗失手机】
#             # assert_result_displayed(login_login_lost_phone_submit, True)  # 显示遗失手机页，如果是第一次跳转浏览器，部分机型会有弹框询问要打开的浏览器
#         else:
#             assert False
#
#     @adb_log
#     def test_C411(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             assert_result_text(homePage_homePage_main, homepage_assert_home_page_name.format(UserInfo.First.name))  # 显示xx的家
#             logout()
#         elif app_select == 'newpanpan':
#             # 手机邮箱登录切换
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_type_switch)  # 点击【邮箱登录】
#             assert_result_text(login_login_type_switch, login_login_type_phone)  # 显示【手机号登录】
#             click_element(login_login_type_switch)  # 点击【手机号登录】
#             assert_result_text(login_login_type_switch, login_login_type_mail)  # 显示【邮箱登录】
#         else:
#             assert False
#
#     @adb_log
#     def test_C412(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
#             assert_result_text(homePage_homePage_main, homepage_assert_home_page_name.format(UserInfo.First.name))  # 显示xx的家
#             logout()
#         elif app_select == 'newpanpan':
#             # 手机号地区选择
#             click_element(register_main_login)  # 点击【登录】
#             assert_result_text(login_login_area_number, login_login_area_number_china)  # 检查文本【+86】
#             click_element(login_login_area_number)  # 点击【区号】
#             send_keys(login_login_country_search, 'Hong Kong')  # 输入Hong Kong
#             click_element(login_login_area_name)  # 点击【Hong Kong】
#             assert_result_text(login_login_area_number, 'HK')  # 检查文本【HK】
#         else:
#             assert False
#
#     @adb_log
#     def test_C413(self):
#         if app_select == 'panpan':
#             user_info = phone_number_get_registered()
#             click_element(register_button_login)  # 点击【登录】
#             send_keys(login_button_login_phone, UserInfo.First.phone)  # 输入手机号
#             send_keys(login_button_login_password, UserInfo.First.password_new)  # 输入错误密码
#             click_element(login_button_sure)  # 点击【确定】
#             assert_result_text(register_assert_alert, login_assert_phone_or_password)  # 提示【手机号码或密码不正确】
#             clear_text_content(login_button_login_phone)  # 清除手机号输入
#             clear_text_content(login_button_login_password)  # 清除密码输入
#             send_keys(login_button_login_phone, user_info[0])  # 输入错误手机号
#             send_keys(login_button_login_password, UserInfo.First.password)  # 输入密码
#             click_element(login_button_sure)  # 点击【确定】
#             assert_result_text(register_assert_alert, login_assert_phone_or_password)  # 提示【手机号码或密码不正确】
#         elif app_select == 'newpanpan':
#             # 手机号为空
#             click_element(register_main_login)  # 点击欢迎页【登录】
#             send_keys(login_login_password_input, UserInfo.First.password)  # 不输入手机号，输入正确密码
#             click_element(login_login_submit)  # 点击登录页【登录】
#             assert_result_displayed(login_login_submit, True)  # 登录失败，登录按键置灰，留在登录页，显示登录按键
#         else:
#             assert False
#
#     @adb_log
#     def test_C414(self):
#         if app_select == 'panpan':
#             user_info = phone_number_get_registered()
#             click_element(register_button_login)  # 点击【登录】
#             click_element(login_compound_login_method)  # 点击【邮箱登录】
#             send_keys(login_button_login_email, UserInfo.First.email)  # 输入邮箱
#             send_keys(login_button_login_password, UserInfo.First.password_new)  # 输入错误密码
#             click_element(login_button_sure)  # 点击【确定】
#             assert_result_text(register_assert_alert, login_assert_email_or_password)  # 提示【邮箱或密码不正确】
#             clear_text_content(login_button_login_email)  # 清除邮箱号输入
#             clear_text_content(login_button_login_password)  # 清除密码输入
#             send_keys(login_button_login_email, user_info[2])  # 输入错误邮箱
#             send_keys(login_button_login_password, UserInfo.First.password)  # 输入密码
#             click_element(login_button_sure)  # 点击【确定】
#             assert_result_text(register_assert_alert, login_assert_email_or_password)  # 提示【邮箱或密码不正确】
#         elif app_select == 'newpanpan':
#             # 手机号小于11位
#             click_element(register_main_login)  # 点击欢迎页【登录】
#             send_keys(login_login_phone_input, UserInfo.First.phone_short)  # 输入小于11位手机号
#             send_keys(login_login_password_input, UserInfo.First.password)  # 输入正确密码
#             click_element(login_login_submit)  # 点击登录页【登录】
#             # image_compare()  # 登录失败，有对应提示
#             assert_result_displayed(login_login_submit, True)  # 仍然停留在登录页
#         else:
#             assert False
#
#     @adb_log
#     def test_C415(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
#             add_lock()
#             logout('house')
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             assert_result_displayed(homepage_compound_house_list, True)  # 查找房间名
#             click_element(homepage_button_select_room_or_user.format(HouseInfo.name))
#             factory_lock()
#             logout()
#             user_register()
#             assert_result_displayed(homePage_homePage_no_rooms, True)  # 没有房间名
#             logout()
#         elif app_select == 'newpanpan':
#             # 手机号大于11位
#             click_element(register_main_login)  # 点击欢迎页【登录】
#             send_keys(login_login_phone_input, UserInfo.First.phone + '1')  # 输入大于11位手机号
#             assert_result_text(login_login_phone_input, UserInfo.First.phone[:9])  # 显示9位手机号(Bug)，不可输入大于11位手机号
#         else:
#             assert False
#
#     @adb_log
#     def test_C416(self):
#         if app_select == 'panpan':
#             click_element(register_button_login)  # 点击【登录】
#             click_element(login_button_forget_password)  # 点击【忘记密码】
#             send_keys(login_button_forget_password_phone, UserInfo.First.phone)  # 输入手机号
#             click_element(login_button_forget_password_next)  # 点击【下一步】
#             assert_result_displayed(login_compound_resend_verify, True)  # 显示重新发送
#             send_keys(login_button_forget_password_verify_input, verification_code_phone(UserInfo.First.phone))  # 输入验证码
#             click_element(login_button_forget_password_sure)  # 点击【提交】
#             send_keys(login_button_forget_password_email, UserInfo.First.email)  # 输入邮箱
#             click_element(login_button_forget_password_next)  # 点击【下一步】
#             assert_result_displayed(login_compound_resend_verify, True)  # 显示重新发送
#             send_keys(login_button_forget_password_verify_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#             click_element(login_button_forget_password_sure)  # 点击【提交】
#             # 更新LastUser
#             file_name = 'Configuration/LastUser.txt'
#             file_name = open(file_name, 'w')
#             file_name.write('/')
#             file_name.close()
#             assert_result_displayed(login_compound_forget_password_finish, True)  # 显示【提交】
#         elif app_select == 'newpanpan':
#             # 手机号含空格
#             click_element(register_main_login)  # 点击欢迎页【登录】
#             send_keys(login_login_phone_input, UserInfo.First.phone_with_space)  # 输入带空格的手机号
#             send_keys(login_login_password_input, UserInfo.First.password)  # 输入正确密码
#             click_element(login_login_submit)  # 点击登录页【登录】
#             # image_compare()  # 登录失败，有对应提示
#             assert_result_displayed(login_login_submit, True)  # 仍然停留在登录页
#         else:
#             assert False
#
#     @adb_log
#     def test_C417(self):
#         if app_select == 'panpan':
#             click_element(register_button_login)  # 点击【登录】
#             click_element(login_button_forget_password)  # 点击【忘记密码】
#             send_keys(login_button_forget_password_phone, UserInfo.First.phone)  # 输入手机号
#             click_element(login_button_forget_password_next)  # 点击【下一步】
#             assert_result_displayed(login_compound_resend_verify, True)  # 显示重新发送
#             send_keys(login_button_forget_password_verify_input, verification_code_phone(UserInfo.First.phone))  # 输入验证码
#             click_element(login_button_forget_password_sure)  # 点击【提交】
#             send_keys(login_button_forget_password_email, UserInfo.First.email)  # 输入邮箱
#             click_element(login_button_forget_password_next)  # 点击【下一步】
#             assert_result_displayed(login_compound_resend_verify, True)  # 显示重新发送
#             send_keys(login_button_forget_password_verify_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#             click_element(login_button_forget_password_sure)  # 点击【提交】
#             # 更新LastUser
#             file_name = 'Configuration/LastUser.txt'
#             file_name = open(file_name, 'w')
#             file_name.write('/')
#             file_name.close()
#             assert_result_enable(login_compound_forget_password_finish, False)  # 不可点【提交】
#             send_keys(login_button_forget_password_new_1, 'abcd1234')  # 输入不符合要求的密码
#             send_keys(login_button_forget_password_new_2, 'abcd1234')  # 输入不符合要求的密码
#             assert_result_enable(login_compound_forget_password_finish, False)  # 不可点【提交】
#             clear_text_content(login_button_forget_password_new_1)  # 清除新密码
#             send_keys(login_button_forget_password_new_1, 'Abcd1234')  # 输入密码
#             assert_result_enable(login_compound_forget_password_finish, False)  # 不可点【提交】
#         elif app_select == 'newpanpan':
#             # 手机号含中文
#             click_element(register_main_login)  # 点击欢迎页【登录】
#             send_keys(login_login_phone_input, UserInfo.First.phone_with_chinese)  # 输入带中文的手机号
#             assert_result_text(login_login_phone_input, UserInfo.First.phone_short)  # 只显示数字，不可输入中文
#         else:
#             assert False
#
#     @adb_log
#     def test_C418(self):
#         if app_select == 'panpan':
#             click_element(register_button_login)  # 点击【登录】
#             click_element(login_button_forget_password)  # 点击【忘记密码】
#             send_keys(login_button_forget_password_phone, UserInfo.First.phone)  # 输入手机号
#             click_element(login_button_forget_password_next)  # 点击【下一步】
#             send_keys(login_button_forget_password_verify_input, verification_code_phone(UserInfo.First.phone))  # 输入验证码
#             click_element(login_button_forget_password_sure)  # 点击【提交】
#             send_keys(login_button_forget_password_email, UserInfo.First.email)  # 输入邮箱
#             click_element(login_button_forget_password_next)  # 点击【下一步】
#             send_keys(login_button_forget_password_verify_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#             click_element(login_button_forget_password_sure)  # 点击【提交】
#             # 更新LastUser
#             file_name = 'Configuration/LastUser.txt'
#             file_name = open(file_name, 'w')
#             file_name.write('/')
#             file_name.close()
#             assert_result_enable(login_compound_forget_password_finish, False)  # 不可点【提交】
#             send_keys(login_button_forget_password_new_1, 'Abcd1234')  # 输入密码
#             assert_result_enable(login_compound_forget_password_finish, False)  # 不可点【提交】
#             send_keys(login_button_forget_password_new_2, 'aBcd1234')  # 输入不同的密码
#             assert_result_enable(login_compound_forget_password_finish, False)  # 不可点【提交】
#             clear_text_content(login_button_forget_password_new_1)  # 清除新密码
#             assert_result_enable(login_compound_forget_password_finish, False)  # 不可点【提交】
#         elif app_select == 'newpanpan':
#             # 手机号含英文
#             click_element(register_main_login)  # 点击欢迎页【登录】
#             send_keys(login_login_phone_input, UserInfo.First.phone_with_english)  # 输入带英文的手机号
#             assert_result_text(login_login_phone_input, UserInfo.First.phone_short)  # 只显示数字，不可输入英文
#         else:
#             assert False
#
#     @adb_log
#     def test_C419(self):
#         if app_select == 'panpan':
#             click_element(register_button_login)  # 点击【登录】
#             click_element(login_button_forget_password)  # 点击【忘记密码】
#             send_keys(login_button_forget_password_phone, UserInfo.First.phone)  # 输入手机号
#             click_element(login_button_forget_password_next)  # 点击【下一步】
#             send_keys(login_button_forget_password_verify_input, verification_code_phone(UserInfo.First.phone))  # 输入验证码
#             click_element(login_button_forget_password_sure)  # 点击【提交】
#             send_keys(login_button_forget_password_email, UserInfo.First.email)  # 输入邮箱
#             click_element(login_button_forget_password_next)  # 点击【下一步】
#             send_keys(login_button_forget_password_verify_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#             click_element(login_button_forget_password_sure)  # 点击【提交】
#             # 更新LastUser
#             file_name = 'Configuration/LastUser.txt'
#             file_name = open(file_name, 'w')
#             file_name.write('/')
#             file_name.close()
#             send_keys(login_button_forget_password_new_1, UserInfo.First.password_new)  # 输入密码
#             send_keys(login_button_forget_password_new_2, UserInfo.First.password_new)  # 输入密码
#             click_element(login_compound_forget_password_finish)  # 点击【提交】
#             assert_result_displayed(register_button_privacy, True)  # 显示主页
#             # 将密码恢复成默认密码
#             click_element(register_button_login)  # 点击【登录】
#             click_element(login_button_forget_password)  # 点击【忘记密码】
#             send_keys(login_button_forget_password_phone, UserInfo.First.phone)  # 输入手机号
#             click_element(login_button_forget_password_next)  # 点击【下一步】
#             send_keys(login_button_forget_password_verify_input, verification_code_phone(UserInfo.First.phone))  # 输入验证码
#             click_element(login_button_forget_password_sure)  # 点击【提交】
#             send_keys(login_button_forget_password_email, UserInfo.First.email)  # 输入邮箱
#             click_element(login_button_forget_password_next)  # 点击【下一步】
#             send_keys(login_button_forget_password_verify_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#             click_element(login_button_forget_password_sure)  # 点击【提交】
#             # 更新LastUser
#             file_name = 'Configuration/LastUser.txt'
#             file_name = open(file_name, 'w')
#             file_name.write('/')
#             file_name.close()
#             send_keys(login_button_forget_password_new_1, UserInfo.First.password)  # 输入密码
#             send_keys(login_button_forget_password_new_2, UserInfo.First.password)  # 输入密码
#             click_element(login_compound_forget_password_finish)  # 点击【提交】
#         elif app_select == 'newpanpan':
#             # 手机号含特殊字符
#             click_element(register_main_login)  # 点击欢迎页【登录】
#             send_keys(login_login_phone_input, UserInfo.First.phone_with_symbol)  # 输入带特殊字符的手机号
#             assert_result_text(login_login_phone_input, UserInfo.First.phone_short)  # 只显示数字，不可输入特殊字符
#         else:
#             assert False
#
#     @adb_log
#     def test_C420(self):
#         if app_select == 'panpan':
#             click_element(register_button_login)  # 点击【登录】
#             click_element(login_button_forget_password)  # 点击【忘记密码】
#             send_keys(login_button_forget_password_phone, UserInfo.First.phone)  # 输入手机号
#             click_element(login_button_forget_password_next)  # 点击【下一步】
#             send_keys(login_button_forget_password_verify_input, verification_code_phone(UserInfo.First.phone))  # 输入验证码
#             click_element(login_button_forget_password_sure)  # 点击【提交】
#             send_keys(login_button_forget_password_email, UserInfo.First.email)  # 输入邮箱
#             click_element(login_button_forget_password_next)  # 点击【下一步】
#             send_keys(login_button_forget_password_verify_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#             click_element(login_button_forget_password_sure)  # 点击【提交】
#             # 更新LastUser
#             file_name = 'Configuration/LastUser.txt'
#             file_name = open(file_name, 'w')
#             file_name.write('/')
#             file_name.close()
#             send_keys(login_button_forget_password_new_1, UserInfo.First.password)  # 输入密码
#             send_keys(login_button_forget_password_new_2, UserInfo.First.password)  # 输入密码
#             click_element(login_compound_forget_password_finish)  # 点击【提交】
#             assert_result_displayed(register_button_privacy, True)  # 显示主页
#         elif app_select == 'newpanpan':
#             # 首次手机登录流程测试
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#         else:
#             assert False
#
#     @adb_log
#     def test_C421(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             assert_result_text(homePage_homePage_main, homepage_assert_home_page_name.format(UserInfo.First.name))  # 显示xx的家
#             logout()
#         elif app_select == 'newpanpan':
#             # 手机登录流程测试
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#         else:
#             assert False
#
#     @adb_log
#     def test_C422(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             add_lock()
#             logout('house')
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             assert_result_displayed(homepage_compound_house_list, True)  # 查找房间名
#             click_element(homepage_button_select_room_or_user.format(HouseInfo.name))
#             factory_lock()
#             logout()
#             user_register()
#             assert_result_displayed(homePage_homePage_no_rooms, True)  # 没有房间名
#             logout()
#         elif app_select == 'newpanpan':
#             # 未注册手机号登录流程防呆测试
#             click_element(register_main_login)  # 点击欢迎页【登录】
#             send_keys(login_login_phone_input, UserInfo.First.phone_unregistered)  # 输入未注册的手机号
#             send_keys(login_login_password_input, UserInfo.First.password)  # 输入密码
#             click_element(login_login_submit)  # 点击登录页【登录】
#             assert_result_text(login_login_input_or_password_error, login_login_phone_or_password_error_text)  # 手机号或密码错误
#         else:
#             assert False
#
#     @adb_log
#     def test_C423(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
#             add_lock()
#             logout('house')
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
#             assert_result_displayed(homepage_compound_house_list, True)  # 查找房间名
#             click_element(homepage_button_select_room_or_user.format(HouseInfo.name))
#             factory_lock()
#             logout()
#             user_register()
#             assert_result_displayed(homePage_homePage_no_rooms, True)  # 没有房间名
#             logout()
#         elif app_select == 'newpanpan':
#             # 手机登录验证码重发
#             click_element(register_main_login)  # 点击【登录】
#             send_keys(login_login_phone_input, UserInfo.Second.phone)  # 输入手机号
#             send_keys(login_login_password_input, UserInfo.Second.password)  # 输入密码
#             click_element(login_button_sure, after_action_sleep=30)  # 点击【确定】
#             click_element(login_verify_resend)  # 点击重发验证码
#             send_keys(login_verify_code_input, verification_code_phone(UserInfo.Second.phone), after_action_sleep=10)  # 输入手机验证码，自动下一步
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#             file_name = 'Configuration/LastUser.txt'
#             current_user_info = str(UserInfo.Second.phone) + '/' + str(UserInfo.Second.email)
#             file_name = open(file_name, 'w')
#             file_name.write(current_user_info)
#             file_name.close()
#         else:
#             assert False
#
#     @adb_log
#     def test_C424(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             add_lock()
#             wifi_switch('close')
#             driver.close_app()
#             driver.launch_app()
#             click_element(homepage_button_select_room_or_user.format(HouseInfo.name))  # 点击家
#             assert_result_text(register_assert_alert, login_assert_network_error)  # 提示【网络异常】
#             wifi_switch('open')
#             factory_lock()
#             logout()
#         elif app_select == 'newpanpan':
#             # 密码为空
#             click_element(register_main_login)  # 点击欢迎页【登录】
#             send_keys(login_login_phone_input, UserInfo.First.phone)  # 输入正确的手机号
#             send_keys(login_login_password_input, '   ')  # 输入空格
#             click_element(login_login_submit)  # 点击登录页【登录】
#             assert_result_displayed(login_login_input_or_password_error, True)  # 手机号或密码错误
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C425(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 手机号与密码不匹配
#             click_element(register_main_login)  # 点击欢迎页【登录】
#             send_keys(login_login_phone_input, UserInfo.First.phone)  # 输入正确的手机号
#             send_keys(login_login_password_input, UserInfo.First.password_wrong)  # 输入不匹配的密码
#             click_element(login_login_submit)  # 点击登录页【登录】
#             assert_result_displayed(login_login_input_or_password_error, True)  # 手机号或密码错误
#         else:
#             assert False
#
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Image compare')  # 图片对比
#     # @adb_log
#     # def test_login_tc019(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 密码显示或隐藏默认状态测试
#     #         click_element(register_main_login)  # 点击欢迎页【登录】
#     #         image_compare()  # 对比密码可见图标
#     #     else:
#     #         assert False
#
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Image compare')  # 图片对比
#     # @adb_log
#     # def test_login_tc020(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 密码显示或隐藏默认状态测试
#     #         click_element(register_main_login)  # 点击欢迎页【登录】
#     #         send_keys(login_login_password_input, UserInfo.First.password)  # 输入密码
#     #         click_element(login_login_password_show)  # 点击密码可见按键
#     #         image_compare()  # 对比密码可见图标
#     #     else:
#     #         assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C428(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 邮箱登录页UI测试
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_type_switch)  # 点击【邮箱登录】
#             assert_result_text(login_login_type_switch, login_login_type_phone)  # 显示【手机号登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             assert_result_displayed(login_forget_password_switch_mode, True)  # 显示忘记密码页按键
#             # click_element(login_forget_back_button)  # 点击【返回】
#             self.driver.keyevent(4)  # 点击【返回】
#             click_element(login_login_lost_phone)  # 点击【遗失手机】
#             # assert_result_displayed(login_login_lost_phone_submit, True)  # 显示遗失手机页
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C429(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 邮箱地址为空
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_type_switch)  # 点击【邮箱登录】
#             assert_result_text(login_login_type_switch, login_login_type_phone)  # 显示【手机号登录】
#             send_keys(login_login_mail_input, '   ')  # 输入空邮箱
#             send_keys(login_login_password_input, UserInfo.First.password)  # 输入正确密码
#             click_element(login_login_submit)  # 点击登录页【登录】
#             assert_result_displayed(login_login_mail_text_error, True)  # 提示请输入一个有效的邮箱地址
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C430(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 邮箱地址非法邮箱
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_type_switch)  # 点击【邮箱登录】
#             assert_result_text(login_login_type_switch, login_login_type_phone)  # 显示【手机号登录】
#             send_keys(login_login_mail_input, UserInfo.First.phone_short)  # 输入非法邮箱(仅数字)
#             send_keys(login_login_password_input, UserInfo.First.password)  # 输入正确密码
#             click_element(login_login_submit)  # 点击登录页【登录】
#             assert_result_displayed(login_login_mail_text_error, True)  # 提示请输入一个有效的邮箱地址
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C431(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 首次邮箱登录流程测试
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C432(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 邮箱登录流程测试
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C433(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 未注册邮箱登录流程防呆测试
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_type_switch)  # 点击【邮箱登录】
#             assert_result_text(login_login_type_switch, login_login_type_phone)  # 显示【手机号登录】
#             send_keys(login_login_mail_input, UserInfo.First.email_unregistered)  # 输入未注册邮箱
#             send_keys(login_login_password_input, UserInfo.First.password)  # 输入正确密码
#             click_element(login_login_submit)  # 点击登录页【登录】
#             assert_result_text(login_login_input_or_password_error, login_login_mail_or_password_error_text)  # 提示电子邮件或密码错误
#             clear_text_content(login_login_mail_input)  # 清空输入框
#             send_keys(login_login_mail_input, UserInfo.First.email)  # 输入正确邮箱
#             click_element(login_login_password_input)
#             clear_text_content(login_login_password_input)  # 清空输入框
#             send_keys(login_login_password_input, UserInfo.First.password_wrong)  # 输入错误密码
#             click_element(login_login_submit)  # 点击登录页【登录】
#             assert_result_text(login_login_input_or_password_error, login_login_mail_or_password_error_text)  # 提示电子邮件或密码错误
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C434(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 邮箱登录流程验证码重发
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_type_switch)  # 点击【邮箱登录】
#             send_keys(login_login_mail_input, UserInfo.Second.email)  # 输入邮箱
#             send_keys(login_login_password_input, UserInfo.Second.password)  # 输入密码
#             click_element(login_button_sure, after_action_sleep=30)  # 点击【确定】
#             click_element(login_verify_resend)  # 点击重发验证码
#             send_keys(login_verify_code_input, verification_code_email(UserInfo.Second.email), after_action_sleep=10)  # 输入验证码，自动下一步
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#             file_name = 'Configuration/LastUser.txt'
#             current_user_info = str(UserInfo.Second.phone) + '/' + str(UserInfo.Second.email)
#             file_name = open(file_name, 'w')
#             file_name.write(current_user_info)
#             file_name.close()
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C435(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 切换帐号
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#             login_yale(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'phone')
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C436(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 手机登录_验证码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#             login_yale(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'phone')
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C437(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 邮箱登录_验证码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#             login_yale(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'email')
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#         else:
#             assert False
#
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc031(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 免登录
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc032(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 超过免登录期重新登录
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc033(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 多设备登录
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc034(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 弱网络登录测试
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Not recommended')  # 不建议运行，影响稳定性
#     # @adb_log
#     # def test_login_tc035(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 无网络登录测试
#     #         click_element(register_main_login)  # 点击欢迎页【登录】
#     #         send_keys(login_login_phone_input, UserInfo.First.phone)  # 输入正确的手机号
#     #         send_keys(login_login_password_input, UserInfo.First.password)  # 输入正确的密码
#     #         os.system('adb shell svc wifi disable')  # 断开网络连接
#     #         click_element(login_login_submit)  # 点击登录页【登录】
#     #         assert_result_displayed(login_login_submit, True)  # 留在登录界面 登录异常的弹框无法捕捉
#     #         os.system('adb shell svc wifi enable')  # 恢复网络连接
#     #     else:
#     #         assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C443(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 忘记密码页UI测试
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             assert_result_displayed(login_forget_password_switch_mode, True)  # 显示切换验证方式按键
#             assert_result_displayed(login_forget_next, True)  # 显示下一步按键
#             assert_result_displayed(login_forget_phone_input, True)  # 显示手机号输入框
#             # click_element(login_forget_back_button)  # 点击【返回】
#             # assert_result_displayed(login_login_lost_phone, True)  # 返回登录页，显示遗失手机按键
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C444(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 手机号为空
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             click_element(login_forget_next)  # 点击【继续】
#             assert_result_displayed(login_forget_invalid_phone, True)  # 无效的手机号
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C445(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 手机号小于11位
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             send_keys(login_forget_phone_input, UserInfo.First.phone_short)  # 输入小于11位手机号
#             click_element(login_forget_next)  # 点击【继续】
#             assert_result_displayed(login_forget_invalid_phone, True)  # 无效的手机号
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C446(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 手机号大于11位
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             # send_keys(login_forget_phone_input, UserInfo.First.phone + '1')  # 输入大于11位手机号
#             send_keys(login_forget_phone_input, UserInfo.First.phone)  # 输入大于11位手机号
#             assert_result_text(login_forget_phone_input, UserInfo.First.phone)  # 不可输入大于11位手机号
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C447(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 未注册手机号
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             send_keys(login_forget_phone_input, UserInfo.First.phone_unregistered)  # 输入未注册手机号
#             click_element(login_forget_next)  # 点击继续
#             assert_result_displayed(login_forget_resend_code, True)  # 进入下一步
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C448(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 手机号含空格
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             send_keys(login_forget_phone_input, UserInfo.First.phone_with_space)  # 输入带空格手机号
#             click_element(login_forget_next)  # 点击继续
#             assert_result_displayed(login_forget_invalid_phone, True)  # 无效的手机号
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C449(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 手机号含中文
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             send_keys(login_forget_phone_input, UserInfo.First.phone_with_chinese)  # 输入带中文手机号
#             assert_result_text(login_forget_phone_input, UserInfo.First.phone_short)  # 不可输入中文
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C450(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 手机号含英文
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             send_keys(login_forget_phone_input, UserInfo.First.phone_with_english)  # 输入带英文手机号
#             assert_result_text(login_forget_phone_input, UserInfo.First.phone_short)  # 不可输入英文
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C451(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 手机号含特殊字符
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             send_keys(login_forget_phone_input, UserInfo.First.phone_with_symbol)  # 输入带符号手机号
#             assert_result_text(login_forget_phone_input, UserInfo.First.phone_short)  # 不可输入符号
#         else:
#             assert False
# #
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C452(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 手机验证码为空
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             send_keys(login_forget_phone_input, UserInfo.First.phone)  # 输入手机号
#             click_element(login_forget_next)  # 点击下一步
#             click_element(login_forget_continue_button)  # 点击继续
#             assert_result_displayed(login_forget_invalid_code, True)  # 验证码错误
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C453(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 手机验证码小于6位
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             send_keys(login_forget_phone_input, UserInfo.First.phone)  # 输入手机号
#             click_element(login_forget_next)  # 点击下一步
#             send_keys(login_forget_code_input, UserInfo.First.phone_short)  # 输入三位数验证码
#             click_element(login_forget_continue_button)  # 点击继续
#             assert_result_displayed(login_forget_invalid_code, True)  # 验证码错误
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C454(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 手机验证码错误
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             send_keys(login_forget_phone_input, UserInfo.First.phone)  # 输入手机号
#             click_element(login_forget_next)  # 点击下一步
#             send_keys(login_forget_code_input, UserInfo.First.verify_code_wrong)  # 输入错误的验证码
#             click_element(login_forget_continue_button)  # 点击继续
#             assert_result_displayed(login_forget_invalid_code, True)  # 验证码错误
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C455(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 手机验证码重发测试
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             send_keys(login_forget_phone_input, UserInfo.First.phone)  # 输入手机号
#             click_element(login_forget_next)  # 点击下一步
#             click_element(login_forget_resend_code)  # 点击重发
#             assert_result_text(login_forget_invalid_code, login_forget_code_sent_text)  # 验证码已发送
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C456(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 忘记密码邮箱地址防呆测试_非法
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             click_element(login_forget_password_switch_mode)  # 点击使用邮箱验证
#             send_keys(login_forget_mail_input, UserInfo.First.phone_short)  # 输入不合法的邮箱
#             click_element(login_forget_next)  # 点击继续
#             assert_result_text(login_forget_invalid_code, login_forget_invalid_mail_text)  # 无效的邮箱地址
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C457(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 忘记密码未注册邮箱地址防呆测试
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             click_element(login_forget_password_switch_mode)  # 点击使用邮箱验证
#             send_keys(login_forget_mail_input, UserInfo.First.email_unregistered)  # 输入未注册的邮箱
#             click_element(login_forget_next)  # 点击继续
#             assert_result_displayed(login_forget_resend_code, True)  # 进入下一步
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C458(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 邮箱验证码为空
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             click_element(login_forget_password_switch_mode)  # 点击使用邮箱验证
#             send_keys(login_forget_mail_input, UserInfo.First.email)  # 输入正确的邮箱
#             click_element(login_forget_next)  # 点击继续
#             click_element(login_forget_continue_button)  # 点击继续
#             assert_result_displayed(login_forget_invalid_code, True)  # 验证码错误
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C459(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 邮箱验证码小于6位
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             click_element(login_forget_password_switch_mode)  # 点击使用邮箱验证
#             send_keys(login_forget_mail_input, UserInfo.First.email)  # 输入正确的邮箱
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, UserInfo.First.phone_short)  # 输入三位验证码
#             click_element(login_forget_continue_button)  # 点击继续
#             assert_result_displayed(login_forget_invalid_code, True)  # 验证码错误
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C460(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 邮箱验证码错误
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             click_element(login_forget_password_switch_mode)  # 点击使用邮箱验证
#             send_keys(login_forget_mail_input, UserInfo.First.email)  # 输入正确的邮箱
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, UserInfo.First.verify_code_wrong)  # 输入错误的验证码
#             click_element(login_forget_continue_button)  # 点击继续
#             assert_result_displayed(login_forget_invalid_code, True)  # 验证码错误
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C461(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 邮箱验证码重发测试
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             click_element(login_forget_password_switch_mode)  # 点击使用邮箱验证
#             send_keys(login_forget_mail_input, UserInfo.First.email)  # 输入正确的邮箱
#             click_element(login_forget_next)  # 点击继续
#             click_element(login_forget_resend_code)  # 点击重发验证码
#             assert_result_text(login_forget_invalid_code, login_forget_code_sent_text)  # 验证码已发送
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C462(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 验证码有效期10分钟内
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             click_element(login_forget_password_switch_mode)  # 点击使用邮箱验证
#             send_keys(login_forget_mail_input, UserInfo.Second.email)  # 输入正确的邮箱
#             click_element(login_forget_next, after_action_sleep=5)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_email(UserInfo.Second.email))  # 输入验证码
#             click_element(login_forget_continue_button)  # 点击继续
#             assert_result_displayed(login_forget_phone_input, True)  # 进入下一页
#         else:
#             assert False
#
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Not recommended')  # 不建议运行，影响稳定性
#     # @adb_log
#     # def test_login_tc056(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 验证码有效期10分钟后
#     #         click_element(register_main_login)  # 点击【登录】
#     #         click_element(login_login_forget_password)  # 点击【忘记密码】
#     #         click_element(login_forget_password_switch_mode)  # 点击使用邮箱验证
#     #         send_keys(login_forget_mail_input, UserInfo.First.email)  # 输入正确的邮箱
#     #         click_element(login_forget_next)  # 点击继续
#     #         send_keys(login_forget_code_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#     #         time.sleep(660)  # 等待11分钟
#     #         click_element(login_forget_continue_button)  # 点击继续
#     #         assert_result_displayed(login_forget_invalid_code, True)  # 验证码失效
#     #     else:
#     #         assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C464(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 忘记密码正常流程测试
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             send_keys(login_forget_phone_input, UserInfo.First.phone)  # 输入正确的手机号
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_phone(UserInfo.First.phone))  # 输入验证码
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_mail_input, UserInfo.First.email)  # 输入正确的邮箱
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#             click_element(login_forget_next)  # 点击继续
#             assert_result_displayed(login_forget_new_password_confirm_input, True)  # 修改密码界面
#             self.driver.keyevent(4)
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C465(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 先邮箱后手机号验证
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             click_element(login_forget_password_switch_mode)  # 点击使用邮箱验证
#             send_keys(login_forget_mail_input, UserInfo.First.email)  # 输入正确的邮箱
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_phone_input, UserInfo.First.phone)  # 输入正确的手机号
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_phone(UserInfo.First.phone))  # 输入验证码
#             click_element(login_forget_next)  # 点击继续
#             assert_result_displayed(login_forget_new_password_confirm_input, True)  # 修改密码界面
#             self.driver.keyevent(4)
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C466(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 先手机号后邮箱验证
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             click_element(login_forget_password_switch_mode)  # 点击使用邮箱验证
#             click_element(login_forget_password_switch_mode)  # 点击使用手机号验证
#             send_keys(login_forget_phone_input, UserInfo.First.phone)  # 输入正确的手机号
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_phone(UserInfo.First.phone))  # 输入验证码
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_mail_input, UserInfo.First.email)  # 输入正确的邮箱
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#             click_element(login_forget_next)  # 点击继续
#             assert_result_displayed(login_forget_new_password_confirm_input, True)  # 修改密码界面
#             self.driver.keyevent(4)
#         else:
#             assert False
#
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc060(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 忘记密码弱网测试
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Not recommended')  # 不建议运行，影响稳定性
#     # @adb_log
#     # def test_login_tc061(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 忘记密码无网络测试
#     #         click_element(register_main_login)  # 点击欢迎页【登录】
#     #         click_element(login_login_forget_password)  # 点击【忘记密码】
#     #         send_keys(login_forget_phone_input, UserInfo.First.phone)  # 输入正确的手机号
#     #         os.system('adb shell svc wifi disable')  # 断开网络连接
#     #         click_element(login_forget_next)  # 点击继续
#     #         assert_result_displayed(login_forget_invalid_phone, True)  # 提升发送验证码出错
#     #         os.system('adb shell svc wifi enable')  # 恢复网络连接
#     #     else:
#     #         assert False
#
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @adb_log
#     # def test_login_tc062(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 修改密码UI测试
#     #         click_element(register_main_login)  # 点击【登录】
#     #         click_element(login_login_forget_password)  # 点击【忘记密码】
#     #         click_element(login_forget_password_switch_mode)  # 点击使用邮箱验证
#     #         click_element(login_forget_password_switch_mode)  # 点击使用手机号验证
#     #         send_keys(login_forget_phone_input, UserInfo.First.phone)  # 输入正确的手机号
#     #         click_element(login_forget_next)  # 点击继续
#     #         send_keys(login_forget_code_input, verification_code_phone(UserInfo.First.phone))  # 输入验证码
#     #         click_element(login_forget_next)  # 点击继续
#     #         send_keys(login_forget_mail_input, UserInfo.First.email)  # 输入正确的邮箱
#     #         click_element(login_forget_next)  # 点击继续
#     #         send_keys(login_forget_code_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#     #         click_element(login_forget_next)  # 点击继续
#     #         assert_result_displayed(login_forget_new_password_confirm_input, True)  # 确认新密码输入框
#     #         assert_result_displayed(login_forget_new_password_create_input, True)  # 创建新密码输入框
#     #         click_element(login_forget_new_password_close)  # 点击关闭按键
#     #         assert_result_displayed(login_login_lost_phone, True)  # 返回登录页
#     #     else:
#     #         assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C470(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 修改密码不符合规则
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             click_element(login_forget_password_switch_mode)  # 点击使用邮箱验证
#             click_element(login_forget_password_switch_mode)  # 点击使用手机号验证
#             send_keys(login_forget_phone_input, UserInfo.First.phone)  # 输入正确的手机号
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_phone(UserInfo.First.phone))  # 输入验证码
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_mail_input, UserInfo.First.email)  # 输入正确的邮箱
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_new_password_create_input, UserInfo.First.phone_short)  # 创建不符合规则的新密码
#             send_keys(login_forget_new_password_confirm_input, UserInfo.First.phone_short)  # 确认新密码
#             assert_result_displayed(login_forget_new_password_save, False)  # 无保存按键
#             self.driver.keyevent(4)
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C471(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 修改密码密码不一致
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             click_element(login_forget_password_switch_mode, after_action_sleep=5)  # 点击使用邮箱验证
#             click_element(login_forget_password_switch_mode, after_action_sleep=5)  # 点击使用手机号验证
#             send_keys(login_forget_phone_input, UserInfo.First.phone)  # 输入正确的手机号
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_phone(UserInfo.First.phone))  # 输入验证码
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_mail_input, UserInfo.First.email)  # 输入正确的邮箱
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_new_password_create_input, UserInfo.First.password)  # 创建新密码
#             send_keys(login_forget_new_password_confirm_input, UserInfo.First.password_wrong)  # 输入不一致的密码
#             assert_result_displayed(login_forget_new_password_save, False)  # 无保存按键
#             self.driver.keyevent(4)
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C472(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 修改密码和当前密码一致测试
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             click_element(login_forget_password_switch_mode)  # 点击使用邮箱验证
#             click_element(login_forget_password_switch_mode)  # 点击使用手机号验证
#             send_keys(login_forget_phone_input, UserInfo.First.phone)  # 输入正确的手机号
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_phone(UserInfo.First.phone))  # 输入验证码
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_mail_input, UserInfo.First.email)  # 输入正确的邮箱
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_new_password_create_input, UserInfo.First.password)  # 创建和之前一致的新密码
#             send_keys(login_forget_new_password_confirm_input, UserInfo.First.password)  # 确认密码
#             click_element(login_forget_new_password_save)  # 点击保存
#             click_element(homePage_homePage_menu_in_home_page)  # 跳转进入主页，点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C473(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 修改密码正常流程测试
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_forget_password)  # 点击【忘记密码】
#             send_keys(login_forget_phone_input, UserInfo.First.phone)  # 输入正确的手机号
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_phone(UserInfo.First.phone))  # 输入验证码
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_mail_input, UserInfo.First.email)  # 输入正确的邮箱
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_code_input, verification_code_email(UserInfo.First.email))  # 输入验证码
#             click_element(login_forget_next)  # 点击继续
#             send_keys(login_forget_new_password_create_input, UserInfo.First.password)  # 创建新密码
#             send_keys(login_forget_new_password_confirm_input, UserInfo.First.password)  # 确认密码
#             click_element(login_forget_new_password_save)  # 点击保存
#             click_element(homePage_homePage_menu_in_home_page)  # 跳转进入主页，点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C474(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 修改密码后邮箱登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C475(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 修改密码后手机登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#         else:
#             assert False
#
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc069(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 修改密码后另一台设备登录
#     #         pass
#     #     else:
#     #         assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C477(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 登出后手机重登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录A账号
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#             login_yale(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'phone')  # 登录B账号
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C478(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 登出后邮箱重登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')  # 登录A账号
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#             login_yale(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'email')  # 登录B账号
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
#             self.driver.keyevent(4)  # 返回首页
#             logout()
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     @adb_log
#     def test_C481(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 遗失手机页_UI测试
#             click_element(register_main_login)  # 点击【登录】
#             click_element(login_login_lost_phone)  # 点击【遗失手机】
#             # assert_result_displayed(login_login_lost_phone_submit, True)  # 遗失手机页正常展示
#         else:
#             assert False
#
#     def cases(self):
#         return list(filter(lambda m: m.startswith("test_C") and callable(getattr(self, m)), dir(self)))
#
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc073(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 邮箱地址为空
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc074(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 邮箱地址非法
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc075(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 邮箱地址不存在测试
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc076(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 邮箱验证码为空
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc077(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 邮箱验证码小于6位
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc078(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 邮箱验证码错误
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc079(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 邮箱验证码重发
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc080(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 正常流程测试
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc081(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 登录后断网
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'No case in panpan')
#     # @unittest.skipIf(app_select == 'newpanpan', 'Can not achieve')  # 暂未实现
#     # @adb_log
#     # def test_login_tc082(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # APP退出后断网重开
#     #         pass
#     #     else:
#     #         assert False