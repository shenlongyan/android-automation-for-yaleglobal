import logging
import unittest
from Configuration.OperationMethod import *
from Configuration import StartUp
import random


class TestSchedule(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = StartUp.driver
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H_%M_%S',
                            filename=FolderList.logging_name,
                            filemode='a')
        logging.getLogger()
        time.sleep(1)

    def add_img(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

    def setUp(self) -> None:
        os.system('adb shell svc wifi enable')
        bt_switch('on')
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    @adb_log
    def test_C6467(self):
        # owner user_no credentials
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Joyce Shen'))  # click Joyce Shen
        assert_result_text(userManagement_edit_schedule_set, 'ALWAYS')  # display ALWAYS
        click_element(userManagement_edit_schedule_set)  # schedule set
        assert_result_displayed(userManagement_edit_schedule_set, True)  # can not modify schedule
        click_element(UserManagement_back_bar_button)  # back
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        click_element(lock_main_user_lock_button)  # click lock
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6468(self):
        # owner user_register RFID
        register_RFID('Joyce Shen')    # add RFID
        click_element(userManagement_edit_schedule_set)  # schedule set
        assert_result_displayed(userManagement_edit_schedule_set, True)  # can not modify schedule
        click_element(UserManagement_back_bar_button)  # back
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6469(self):
        # owner user_register pincode
        delete_RFID('Joyce Shen')  # delete RFID
        pin = register_PINCODE('Joyce Shen')  # add PINCODE
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6470(self):
        # owner user_already have RFID_ register pincode
        register_RFID('Joyce Shen')  # register RFID
        click_element(userManagement_edit_schedule_set)  # schedule set
        assert_result_displayed(userManagement_edit_schedule_set, True)  # can not modify schedule
        click_element(UserManagement_back_bar_button)  # back
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6471(self):
        # owner user_already have RFID_ register pincode
        # test at C6470
        pass

    @adb_log
    def test_C6472(self):
        # owner user_modify access not saved_register RFID
        delete_RFID('Joyce Shen')  # delete RFID
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        click_element(userManagement_invite_access_set)  # access set
        click_element(userManagement_invite_general_access)  # owner change to guest
        click_element(userManagement_edit_right_button)  # save
        assert_result_text(userManagement_invite_access_set, 'GUEST')  # change to guest successfully
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # click continue
        requests.put(url=API().card_register())  # REGISTER RFID
        assert_result_displayed(userManagement_invite_register_success, True)  # register RFID success
        click_element(userManagement_invite_register_finish)  # click finish
        assert_result_text(userManagement_invite_RFID_registered_value, 'REGISTERED')  # registered
        click_element(UserManagement_back_bar_button)  # BACK
        delete_RFID('Philadelphia She')

    @adb_log
    def test_C6473(self):
        # owner user_modify access+schedule not saved_register RFID
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        click_element(userManagement_invite_access_set)  # access set
        click_element(userManagement_invite_general_access)  # owner change to guest
        click_element(userManagement_edit_right_button)  # save
        assert_result_text(userManagement_invite_access_set, 'GUEST')  # change to guest successfully
        click_element(userManagement_edit_schedule_set)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # set schedule recurring
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        assert_result_displayed(UserManagement_pop_up, True)  # prompt please save modify first

    @adb_log
    def test_C6491(self):
        # invite pincode only user_invite user_set schedule always
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_PINCODE_access)  # click pincode only
        send_keys(userManagement_invite_RFID_first_name, 'PINCODE')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'always')  # input last name
        click_element(userManagement_invite_PINCODE_continue)  # continue
        click_element(userManagement_invite_create_pin)  # create pin
        pincode1 = get_element(userManagement_invite_pin_input).text
        click_element(userManagement_invite_change_pin)  # save pin
        click_element(userManagement_invite_save_pin, after_action_sleep=10)  # save pin
        click_element(userManagement_main_home_tab)  # lock manager
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode1))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6492(self):
        # invite pincode only user_invite user_set schedule recurring
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_PINCODE_access)  # click pincode only
        send_keys(userManagement_invite_RFID_first_name, 'PINCODE')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'recur')  # input last name
        click_element(userManagement_invite_PINCODE_continue)  # continue
        click_element(userManagement_invite_create_pin)  # create pin
        pincode2 = get_element(userManagement_invite_pin_input).text
        click_element(userManagement_invite_change_pin)  # save pin
        click_element(userManagement_invite_pin_schedule)  # set schedule
        click_element(userManagement_invite_schedule_recurring)   # set recurring
        select_not_today()  # select not today time
        click_element(userManagement_invite_guest_back_button)
        click_element(userManagement_invite_save_pin, after_action_sleep=10)  # save pin
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode2))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C6493(self):
        # invite pincode only user_invite user_set schedule temporary
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_PINCODE_access)  # click pincode only
        send_keys(userManagement_invite_RFID_first_name, 'PINCODE')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'temp')  # input last name
        click_element(userManagement_invite_PINCODE_continue)  # continue
        click_element(userManagement_invite_create_pin)  # create pin
        pincode3 = get_element(userManagement_invite_pin_input).text
        click_element(userManagement_invite_change_pin)  # save pin
        click_element(userManagement_invite_pin_schedule)  # set schedule
        click_element(userManagement_invite_schedule_temp)  # set temp
        click_element(userManagement_invite_guest_back_button)
        click_element(userManagement_invite_save_pin, after_action_sleep=10)  # save pin
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode3))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6494(self):
        # invite contactless key only user_invite user_schedule default always
        invite_RFID_only()
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6498(self):
        # owner user_no credentials
        # test at C6467
        pass

    @adb_log
    def test_C6499(self):
        # owner user_registered RFID
        # test at C6468
        pass

    @adb_log
    def test_C6500(self):
        # owner user_registered pincode
        # test at C6469
        pass

    @adb_log
    def test_C6501(self):
        # owner user_registered pincode
        # test at C6470
        pass

    @adb_log
    def test_C6526(self):
        # contactless key only user_RFID status is loaded_schedule always_change to recurring
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('RFID ONLY'))  # click RFID ONLY
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # recurring
        select_not_today()  # select not today time
        click_element(userManagement_edit_right_button)
        click_element(userManagement_edit_right_button)
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C6529(self):
        # contactless key only user_RFID status is loaded_schedule always_change to temporary
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('RFID ONLY'))  # click RFID ONLY
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_temp)  # temp
        click_element(userManagement_edit_right_button)
        click_element(userManagement_edit_right_button)
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6530(self):
        # contactless key only user_RFID status is loaded_schedule temp_change to always
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('RFID ONLY'))  # click RFID ONLY
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_always)  # always
        click_element(userManagement_edit_right_button)
        click_element(userManagement_edit_right_button)
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        # delete_RFID('RFID ONLY')

    @adb_log
    def test_C6533(self):
        # contactless key only user_RFID status is created_schedule always_change to temporary
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'CREATED')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout
        click_element(userManagement_invite_right_close)  # right close
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('RFID CREATED'))  # click RFID CREATED
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_temp)  # temp
        click_element(userManagement_edit_right_button)
        click_element(userManagement_edit_right_button)
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list

    @adb_log
    def test_C6534(self):
        # contactless key only user_RFID status is created_schedule temporary_change to recurring
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('RFID CREATED'))  # click RFID CREATED
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # recurring
        click_element(userManagement_edit_right_button)
        click_element(userManagement_edit_right_button)
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list

    @adb_log
    def test_C6537(self):
        # contactless key only user_RFID status is created_schedule recurring_change to always
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('RFID CREATED'))  # click RFID CREATED
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_always)  # always
        click_element(userManagement_edit_right_button)
        click_element(userManagement_edit_right_button)
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list

    @adb_log
    def test_C6538(self):
        # pincode only user_registered pincode_schedule always_change to recurring
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('PINCODE always'))  # click PINCODE always
        click_element(userManagement_edit_schedule_set)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # recurring
        select_not_today()
        click_element(userManagement_edit_right_button)
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C6541(self):
        # pincode only user_registered pincode_schedule recurring_change to temporary
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('PINCODE always'))  # click PINCODE always
        click_element(userManagement_edit_schedule_set)  # schedule set
        click_element(userManagement_invite_schedule_temp)  # temp
        click_element(userManagement_edit_right_button)
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6543(self):
        # pincode only user_registered pincode_schedule temporary_change to recurring
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('PINCODE always'))  # click PINCODE always
        click_element(userManagement_edit_schedule_set)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # recurring
        select_not_today()
        click_element(userManagement_edit_right_button)
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C6552(self):
        # contactless key only user_registered RFID_cann't modify access
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('RFID ONLY'))  # click RFID ONLY
        click_element(userManagement_invite_only_access_level)   # click access level
        assert_result_displayed(userManagement_invite_RFID_tab, True)  # can't modify access

    @adb_log
    def test_C6553(self):
        # pincode only user_registered pincode_cann't modify access
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('PINCODE always'))  # click PINCODE always
        assert_result_displayed(userManagement_invite_only_access_level, False)  # can't modify access

    @adb_log
    def test_C6556(self):
        # owner user_registered RFID_change to guest recurring
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        click_element(userManagement_invite_access_set)  # access set
        click_element(userManagement_invite_owner_access)  # guest change to owner
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_edit_right_button)  # save
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # REGISTER RFID
        click_element(userManagement_invite_register_finish)  # click finish
        click_element(userManagement_invite_access_set)  # access set
        click_element(userManagement_invite_general_access)  # change to guest
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_edit_schedule_set)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # set schedule recurring
        select_not_today()
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        delete_RFID('Philadelphia She')

    @adb_log
    def test_C6558(self):
        # owner user_registered pincode_change to guest recurring
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        click_element(userManagement_invite_access_set)  # access set
        click_element(userManagement_invite_owner_access)  # guest change to owner
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_edit_right_button)  # save
        pincode = register_PINCODE('Philadelphia She')
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        click_element(userManagement_invite_access_set)  # access set
        click_element(userManagement_invite_general_access)  # change to guest
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_edit_schedule_set)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # set schedule recurring
        select_not_today()
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C6560(self):
        # owner user_registered RFID and pincode_change to guest recurring
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        click_element(userManagement_invite_access_set)  # access set
        click_element(userManagement_invite_owner_access)  # guest change to owner
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_edit_right_button)  # save
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # REGISTER RFID
        click_element(userManagement_invite_register_finish)  # click finish
        click_element(userManagement_invite_access_set)  # access set
        click_element(userManagement_invite_general_access)  # change to guest
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_edit_schedule_set)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # set schedule recurring
        select_not_today()
        click_element(userManagement_edit_right_button)  # save
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        delete_RFID('Philadelphia She')

    @adb_log
    def test_C6570(self):
        # owner user_disable pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        click_element(userManagement_invite_access_set)  # access set
        click_element(userManagement_invite_owner_access)  # guest change to owner
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_edit_right_button)  # save
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_edit_disable_pin)   # disable
        click_element(UserManagement_back_bar_button)
        assert_result_text(userManagement_invite_pin_code, '(DISABLED)')  # display disabled
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C6571(self):
        # owner user_enable pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        click_element(UserManagement_back_bar_button)
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6572(self):
        # owner user_modify pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        click_element(userManagement_invite_pincode_set)  # click pincode
        send_keys(userManagement_invite_pin_input, '111111')
        click_element(userManagement_invite_change_pin)  # modify and save
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6573(self):
        # owner user_modify and disable pincode
        assert False

    @adb_log
    def test_C6574(self):
        # owner user_modify and enable pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_edit_disable_pin)  # disable
        click_element(UserManagement_back_bar_button)
        click_element(userManagement_edit_right_button)  # save
        click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        send_keys(userManagement_invite_pin_input, '222222')
        click_element(userManagement_invite_change_pin)  # modify 
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6590(self):
        # pincode only user_schedule always_disable pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('PINCODE always'))  # click PINCODE always
        click_element(userManagement_edit_schedule_set)  # schedule set
        click_element(userManagement_invite_schedule_always)  # always
        click_element(userManagement_edit_right_button)
        click_element(userManagement_edit_right_button)  # save
        click_element(UserManagement_user_admin.format('PINCODE always'))  # click PINCODE always
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_edit_disable_pin)  # disable
        click_element(UserManagement_back_bar_button)
        assert_result_text(userManagement_invite_pin_code, '(DISABLED)')  # display disabled
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C6591(self):
        # pincode only user_schedule always_enable pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('PINCODE always'))  # click PINCODE always
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        click_element(UserManagement_back_bar_button)
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6592(self):
        # pincode only user_schedule always_modify pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('PINCODE always'))  # click PINCODE always
        click_element(userManagement_invite_pincode_set)  # click pincode
        send_keys(userManagement_invite_pin_input, '333333')
        click_element(userManagement_invite_change_pin)  # modify and save
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6593(self):
        # pincode only user_schedule always_modify and disable pincode
        assert False

    @adb_log
    def test_C6594(self):
        # pincode only user_schedule always_modify and enable pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('PINCODE always'))  # click PINCODE always
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_edit_disable_pin)  # disable
        click_element(UserManagement_back_bar_button)
        click_element(userManagement_edit_right_button)  # save
        click_element(UserManagement_user_admin.format('PINCODE always'))  # click PINCODE always
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        send_keys(userManagement_invite_pin_input, '444444')
        click_element(userManagement_invite_change_pin)  # modify
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6595(self):
        # pincode only user_schedule recurring_disable pincode
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('PINCODE recur'))  # click PINCODE recur
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_edit_disable_pin)  # disable
        click_element(UserManagement_back_bar_button)
        assert_result_text(userManagement_invite_pin_code, '(DISABLED)')  # display disabled
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C6596(self):
        # pincode only user_schedule recurring_enable pincode
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('PINCODE recur'))  # click PINCODE recur
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        click_element(UserManagement_back_bar_button)
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6597(self):
        # pincode only user_schedule recurring_modify pincode
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('PINCODE recur'))  # click PINCODE recur
        click_element(userManagement_invite_pincode_set)  # click pincode
        send_keys(userManagement_invite_pin_input, '555555')
        click_element(userManagement_invite_change_pin)  # modify and save
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6598(self):
        # pincode only user_schedule recurring_modify and disable pincode
        assert False

    @adb_log
    def test_C6599(self):
        # pincode only user_schedule recurring_modify and enable pincode
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('PINCODE recur'))  # click PINCODE always
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_edit_disable_pin)  # disable
        click_element(UserManagement_back_bar_button)
        click_element(userManagement_edit_right_button)  # save
        click_element(UserManagement_user_admin.format('PINCODE recur'))  # click PINCODE always
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        send_keys(userManagement_invite_pin_input, '666666')
        click_element(userManagement_invite_change_pin)  # modify
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6600(self):
        # pincode only user_schedule temporary_disable pincode
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('PINCODE temp'))  # click PINCODE temp
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_edit_disable_pin)  # disable
        click_element(UserManagement_back_bar_button)
        assert_result_text(userManagement_invite_pin_code, '(DISABLED)')  # display disabled
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C6601(self):
        # pincode only user_schedule temporary_enable pincode
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('PINCODE temp'))  # click PINCODE temp
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        click_element(UserManagement_back_bar_button)
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6602(self):
        # pincode only user_schedule temporary_modify pincode
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('PINCODE temp'))  # click PINCODE temp
        click_element(userManagement_invite_pincode_set)  # click pincode
        send_keys(userManagement_invite_pin_input, '777777')
        click_element(userManagement_invite_change_pin)  # modify and save
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6603(self):
        # pincode only user_schedule temporary_modify and disable pincode
        assert False

    @adb_log
    def test_C6604(self):
        # pincode only user_schedule temporary_modify and enable pincode
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('PINCODE temp'))  # click PINCODE temp
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_edit_disable_pin)  # disable
        click_element(UserManagement_back_bar_button)
        click_element(userManagement_edit_right_button)  # save
        click_element(UserManagement_user_admin.format('PINCODE temp'))  # click PINCODE temp
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        send_keys(userManagement_invite_pin_input, '888888')
        click_element(userManagement_invite_change_pin)  # modify
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6617(self):
        # pincode only user_registered pincode_schedule always_change schedule to recurring and disable pincode
        assert False

    @adb_log
    def test_C6618(self):
        # pincode only user_registered pincode_schedule always_change schedule to temporary and enable pincode
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('PINCODE always'))  # click PINCODE always
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_edit_disable_pin)  # disable
        click_element(UserManagement_back_bar_button)
        click_element(userManagement_edit_right_button)  # save
        click_element(UserManagement_user_admin.format('PINCODE always'))  # click PINCODE always
        click_element(userManagement_edit_schedule_set)  # schedule set
        click_element(userManagement_invite_schedule_temp)  # temp
        click_element(userManagement_edit_right_button)
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        click_element(UserManagement_back_bar_button)
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6619(self):
        # pincode only user_registered pincode_schedule recurring_change schedule to always and modify pincode
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('PINCODE recur'))  # click PINCODE recur
        click_element(userManagement_edit_schedule_set)  # schedule set
        click_element(userManagement_invite_schedule_always)  # always
        click_element(userManagement_edit_right_button)
        click_element(userManagement_invite_pincode_set)  # click pincode
        send_keys(userManagement_invite_pin_input, '999999')
        click_element(userManagement_invite_change_pin)  # modify and save
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6620(self):
        # pincode only user_registered pincode_schedule recurring_change schedule to temporary_modify and disable pincode
        assert False

    @adb_log
    def test_C6621(self):
        # pincode only user_registered pincode_schedule recurring_change schedule to temporary_modify and disable pincode
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('PINCODE temp'))  # click PINCODE temp
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_edit_disable_pin)  # disable
        click_element(UserManagement_back_bar_button)
        click_element(userManagement_edit_right_button)  # save
        click_element(UserManagement_user_admin.format('PINCODE temp'))  # click PINCODE temp
        click_element(userManagement_edit_schedule_set)  # schedule set
        click_element(userManagement_invite_schedule_always)  # always
        click_element(userManagement_edit_right_button)
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        send_keys(userManagement_invite_pin_input, '111222')
        click_element(userManagement_invite_change_pin)  # modify
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C6622(self):
        # pincode only user_registered pincode_schedule temporary_change schedule to recurring_modify pincode
        click_element(userManagement_main_user_tab)  # invite
        scroll_screen_up()
        click_element(UserManagement_user_admin.format('PINCODE always'))  # click PINCODE always
        click_element(userManagement_edit_schedule_set)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # recurring
        select_not_today()
        click_element(userManagement_edit_right_button)
        click_element(userManagement_invite_pincode_set)  # click pincode
        send_keys(userManagement_invite_pin_input, '222333')
        click_element(userManagement_invite_change_pin)  # modify and save
        pincode = get_element(userManagement_invite_pin_code).text
        click_element(userManagement_edit_right_button)  # save
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C6628(self):
        # owner user_schedule always_registered RFID and pincode_unregister RFID
        register_RFID('Joyce Shen')  # add RFID
        pin = get_element(userManagement_invite_pin_code).text
        click_element(UserManagement_back_bar_button)  # BACK
        delete_RFID('Joyce Shen')
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_close_status, True)   # locked
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # locked

    @adb_log
    def test_C6635(self):
        # contactless key only user_schedule always_registered RFID_unregister RFID
        pass

    @adb_log
    def test_C6636(self):
        # contactless key only user_schedule recurring_registered RFID_unregister RFID
        pass

    @adb_log
    def test_C6637(self):
        # contactless key only user_schedule temporary_registered RFID_unregister RFID
        pass

    # @adb_log
    # def test_C6639(self):
    #     # owner remove self_registered pincode
    #     click_element(userManagement_main_user_tab)  # invite
    #     click_element(UserManagement_user_admin.format('Philadelphia She'))  # click Philadelphia She
    #     pincode = get_element(userManagement_invite_pin_code).text
    #     click_element(userManagement_edit_delete_button)  # delete
    #     click_element(userManagement_invite_confirm)      # confirm
    #     click_element(userManagement_main_home_tab)  # lock manage
    #     if assert_result_displayed(lock_main_lock_close_status, True):
    #         pass
    #     else:
    #         click_element(lock_main_user_lock_button)  # lock
    #     requests.put(url=API().pin_open(pincode))  # UNLOCK BY pincode
    #     assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    def cases(self):
        return list(filter(lambda m: m.startswith("test_C") and callable(getattr(self, m)), dir(self)))






































