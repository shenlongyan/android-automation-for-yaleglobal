import logging
import unittest
from Configuration.OperationMethod import *
from Configuration import StartUp
import os

autoLock = os.environ['AutoLock']
print('autoLock=' + autoLock)

class TestUserManagement(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = StartUp.driver
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H_%M_%S',
                            filename=FolderList.logging_name.format('user_management'),
                            filemode='a')
        logging.getLogger()
        time.sleep(1)

    def add_img(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

    def setUp(self) -> None:
        os.system('adb shell svc wifi enable')
        bt_switch('on')
        os.popen('adb shell ime set com.sohu.inputmethod.sogou/.SogouIME')  # 切换搜狗输入法，部分输入法不支持enter键
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass
#
#     @adb_log
#     def test_C000(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 设置前置条件
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             try:
#                 if check_element(homePage_homePage_home_list, wait_time=5, check_type=False) is False:
#                     add_lock_yale()
#                     click_element(lock_main_lock_home_button)  # 点击home按钮
#             except NameError:
#                 pass
#             else:
#                 click_element(homePage_homePage_home_list)  # 点击住宅图片
#             logout('house')
#         else:
#             assert False
#
#     # @adb_log
#     # def test_C001(self):
#     #     if app_select == 'panpan':
#     #         login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#     #         click_element(userManagement_main_house_name)  # 进入房间
#     #         invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')  # 邀请用户
#     #         assert_result_displayed(user_compound_user_admin.format(UserInfo.First.name), True)  # 显示管理员darkbeing
#     #         assert_result_displayed(user_compound_user_admin.format(UserInfo.Second.name), True)  # 显示管理员ueiks
#     #         factory_lock()  # 恢复出厂设置
#     #         logout('house')
#     #     elif app_select == 'newpanpan':
#     #         # 用户列表页_UI
#     #         login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录主账号
#     #         try:
#     #             if check_element(homePage_homePage_home_list, wait_time=5, check_type=False) is False:
#     #                 add_lock_yale()
#     #                 click_element(lock_main_lock_home_button)  # 点击home按钮
#     #         except NameError:
#     #             pass
#     #         else:
#     #             click_element(homePage_homePage_home_list)  # 点击住宅图片
#     #         # TODO 定位权限询问处理
#     #         logout('house')  # 退出登录
#     #     else:
#     #         assert False
#
#     @adb_log
#     def test_C916(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(user_button_user)  # 点击【用户】
#             # 图片对比
#             driver.save_screenshot(CaptureCompare.bt_icon_new)
#             select_mark = "//*[@resource-id='com.aaecosys.apac_panpan:id/cell_guest_list_admin_pin_code']"
#             element = driver.find_element(By.XPATH, select_mark)
#             left = element.location['x']
#             top = element.location['y']
#             right = element.location['x'] + element.size['width']
#             bottom = element.location['y'] + element.size['height']
#             im = Image.open(CaptureCompare.bt_icon_new)
#             im = im.crop((left, top, right, bottom))
#             im.save(CaptureCompare.bt_icon_new)
#             image_1 = Image.open(CaptureCompare.bt_icon)
#             image_2 = Image.open(CaptureCompare.bt_icon_new)
#             try:
#                 ImageChops.difference(image_1, image_2)
#             except:
#                 assert False
#             else:
#                 pass
#             bt_switch('off')
#             compare_result(CaptureCompare.bt_icon, CaptureCompare.pin_on_new, user_assert_icon_bt.format(UserInfo.First.name))
#             bt_switch('on')
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户列表页_TAB
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_home_tab)  # 点击开关锁tab
#             assert_result_displayed(lock_main_lottie, True)  # 有开关锁按键
#             click_element(userManagement_main_log_tab)  # 点击日志tab
#             assert_result_displayed(lock_main_feed_title, True)  # 有活动标题
#             click_element(userManagement_main_setting_tab)  # 点击设置tab
#             assert_result_displayed(lock_setting_lock_setting, True)  # 有智能锁设置按键
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             assert_result_displayed(userManagement_invite_invite, True)  # 有邀请按键
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C917(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'alwasy')
#             add_pin(UserInfo.First.name, UserInfo.First.pin)
#             compare_result(CaptureCompare.pin_on, CaptureCompare.pin_on_new, user_assert_icon_pin.format(UserInfo.First.name))
#             compare_result(CaptureCompare.pin_off, CaptureCompare.pin_off_new, user_assert_icon_pin.format(UserInfo.Second.name))
#             click_element(user_button_user)  # 点击【用户】
#             click_element(user_compound_user_admin.format(UserInfo.First.name))  # 点击【管理员】
#             click_element(user_button_user_pin)  # 点击【密码】
#             click_element(user_button_user_pin_release)  # 点击【解除密码】
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
#             compare_result(CaptureCompare.pin_off, CaptureCompare.pin_off_new, user_assert_icon_pin.format(UserInfo.First.name))
#             compare_result(CaptureCompare.pin_on, CaptureCompare.pin_on_new, user_assert_icon_pin.format(UserInfo.Second.name))
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_邀请弹框UI
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             assert_result_displayed(userManagement_invite_invite_user, True)  # 显示邀请管理员或普通用户
#             assert_result_displayed(userManagement_invite_invite_guest, True)  # 显示邀请访客
#             assert_result_displayed(userManagement_invite_invite_cancel, True)  # 显示取消邀请
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C918(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'alwasy')
#             add_pin(UserInfo.First.name, UserInfo.First.pin)
#             add_card(UserInfo.First.name)
#             compare_result(CaptureCompare.card_on, CaptureCompare.card_on_new, user_assert_icon_card.format(UserInfo.First.name))
#             compare_result(CaptureCompare.card_off, CaptureCompare.card_off_new, user_assert_icon_card.format(UserInfo.Second.name))
#             click_element(user_button_user)  # 点击【用户】
#             click_element(user_compound_user_admin.format(UserInfo.First.name))  # 点击【管理员】
#             click_element(user_button_user_card)  # 点击【门卡管理】
#             click_element(user_button_user_finger_and_card_remove)  # 点击【删除】
#             click_element(homePage_else_sure_or_try)  # 点击【确定】
#             click_element(user_button_user_pin)  # 点击【密码】
#             click_element(user_button_user_pin_release)  # 点击【解除密码】
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
#             add_card(UserInfo.Second.name, 'normal')
#             compare_result(CaptureCompare.card_off, CaptureCompare.card_off_new, user_assert_icon_card.format(UserInfo.First.name))
#             compare_result(CaptureCompare.card_on, CaptureCompare.card_on_new, user_assert_icon_card.format(UserInfo.Second.name))
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_邀请弹框“取消”按钮
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_cancel)  # 点击取消邀请
#             assert_result_displayed(userManagement_invite_invite, True)  # 弹框消失
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C919(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'alwasy')
#             add_pin(UserInfo.First.name, UserInfo.First.pin)
#             add_finger(UserInfo.First.name, UserInfo.First.name, UserInfo.First.finger_thumb)
#             compare_result(CaptureCompare.finger_on, CaptureCompare.finger_on_new, user_assert_icon_finger.format(UserInfo.First.name))
#             compare_result(CaptureCompare.finger_off, CaptureCompare.finger_off_new, user_assert_icon_finger.format(UserInfo.Second.name))
#             click_element(user_button_user)  # 点击【用户】
#             click_element(user_compound_user_admin.format(UserInfo.First.name))  # 点击【管理员】
#             click_element(user_button_user_finger)  # 点击【指纹管理】
#             click_element(user_button_user_finger_and_card_remove)  # 点击【删除】
#             click_element(homePage_else_sure_or_try)  # 点击【确定】
#             click_element(user_button_user_pin)  # 点击【密码】
#             click_element(user_button_user_pin_release)  # 点击【解除密码】
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
#             add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb, 'normal')
#             compare_result(CaptureCompare.finger_off, CaptureCompare.finger_off_new, user_assert_icon_finger.format(UserInfo.First.name))
#             compare_result(CaptureCompare.finger_on, CaptureCompare.finger_on_new, user_assert_icon_finger.format(UserInfo.Second.name))
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号或通讯录_邀请选择页UI
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             assert_result_displayed(userManagement_invite_select_phone, True)  # 通过手机号邀请
#             assert_result_displayed(userManagement_invite_select_contact, True)  # 通过通讯录邀请
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C920(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号或通讯录_邀请选择页UI
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             click_element(userManagement_invite_back_button)  # 点击返回按键
#             assert_result_displayed(userManagement_invite_invite, True)  # 返回用户列表页
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C921(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(user_button_user)  # 点击【用户】
#             click_element(user_button_invite_open)  # 打开邀请用户
#             click_element(user_button_invite_close)  # 关闭邀请用户
#             click_element(user_button_invite_open)  # 打开邀请用户
#             click_element(user_button_invite_admin)  # 点击【管理员邀请】
#             assert_result_text(login_assert_title, user_assert_invite_user)  # 邀请朋友
#             click_element(homepage_button_detail_back)  # 点击返回
#             click_element(user_button_invite_open)  # 打开邀请用户
#             click_element(user_button_invite_normal)  # 点击【普通用户邀请】
#             assert_result_text(login_assert_title, user_assert_invite_user)  # 邀请朋友
#             click_element(homepage_button_detail_back)  # 点击返回
#             click_element(user_button_invite_open)  # 打开邀请用户
#             click_element(user_button_invite_guest)  # 点击【访客邀请】
#             assert_result_text(login_assert_title, user_assert_invite_user)  # 邀请朋友
#             click_element(homepage_button_detail_back)  # 点击返回
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_邀请设定页UI
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             click_element(userManagement_invite_select_phone)  # 点击手机号邀请
#             send_keys(userManagement_invite_phone_input, UserInfo.Second.phone)  # 输入邀请的手机号
#             click_element(userManagement_invite_phone_input)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             send_keys(userManagement_invite_first_name, UserInfo.Second.name_first)  # 输入名
#             send_keys(userManagement_invite_last_name, UserInfo.Second.name_last)  # 输入姓
#             click_element(userManagement_invite_last_name)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             assert_result_displayed(userManagement_invite_user_access, True)  # 显示权限等级
#             assert_result_displayed(userManagement_invite_user_schedule, True)  # 显示使用时间
#             self.driver.keyevent(4)  # 返回
#             self.driver.keyevent(4)  # 返回
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C922(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_邀请设定页“<-”按钮
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             click_element(userManagement_invite_select_phone)  # 点击手机号邀请
#             send_keys(userManagement_invite_phone_input, UserInfo.Second.phone)  # 输入邀请的手机号
#             click_element(userManagement_invite_phone_input)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             send_keys(userManagement_invite_first_name, UserInfo.Second.name_first)  # 输入名
#             send_keys(userManagement_invite_last_name, UserInfo.Second.name_last)  # 输入姓
#             click_element(userManagement_invite_last_name)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             assert_result_displayed(userManagement_invite_user_access, True)  # 显示权限等级
#             assert_result_displayed(userManagement_invite_user_schedule, True)  # 显示使用时间
#             click_element(userManagement_invite_back_button)  # 点击返回按键
#             assert_result_displayed(userManagement_invite_phone_input, True)  # 回到上一页
#             self.driver.keyevent(4)  # 返回
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C923(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_管理员_未注册
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             time.sleep(5)  # 等待loading
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             invite_user_yale('admin', user_b[0], user_b[1])  # 邀请管理员
#             logout('house')  # 退出登录
#             user_register_yale()  # 注册账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(userManagement_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, True)  # 验证管理员，显示日志Tab
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout()  # 退出登录 用户删除自己 此时已经回到了主页
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C924(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_管理员_已注册
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             invite_user_yale('admin', user_b[0], user_b[1])  # 邀请管理员
#             logout('house')  # 退出登录
#             login_yale(user_b[0], user_b[2], user_b[3], 'phone')  # 登录账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, True)  # 验证管理员，显示日志Tab
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout()  # 退出登录 用户删除自己 此时已经回到了主页
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C925(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_普通用户_未注册_永久
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             invite_user_yale('normal', user_b[0], user_b[1], 'always')  # 邀请普通用户
#             logout('house')  # 退出登录
#             user_register_yale()  # 注册账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, False, wait_time=1)  # 验证普通用户，不显示日志Tab
#             check_lock_connection()  # 等待锁连接
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C926(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_普通用户_未注册_重复
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             invite_user_yale('normal', user_b[0], user_b[1], 'recur')  # 邀请普通用户
#             logout('house')  # 退出登录
#             user_register_yale()  # 注册账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, False, wait_time=1)  # 验证普通用户，不显示日志Tab
#             check_lock_connection()  # 等待锁连接
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C927(self):
#         if app_select == 'panpan':
#             user_info = phone_number_get_not_registered()
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(user_button_user)  # 点击【用户】
#             click_element(user_button_invite_open)  # 打开邀请用户
#             click_element(user_button_invite_admin)  # 点击【管理员邀请】
#             send_keys(register_button_register_phone, user_info[0])  # 输入手机号
#             send_keys(register_button_register_name, user_info[1])  # 输入用户名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(user_assert_lock_name, 'selected_lock_name')  # 显示锁名
#             click_element(homepage_button_detail_back)  # 点击返回
#             click_element(user_button_invite_open)  # 打开邀请用户
#             click_element(user_button_invite_normal)  # 点击【普通用户邀请】
#             send_keys(register_button_register_phone, user_info[0])  # 输入手机号
#             send_keys(register_button_register_name, user_info[1])  # 输入用户名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(user_assert_lock_name, 'selected_lock_name')  # 显示锁名
#             click_element(homepage_button_detail_back)  # 点击返回
#             click_element(user_button_invite_open)  # 打开邀请用户
#             click_element(user_button_invite_guest)  # 点击【访客邀请】
#             send_keys(register_button_register_phone, user_info[0])  # 输入手机号
#             send_keys(register_button_register_name, user_info[1])  # 输入用户名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(user_assert_lock_name, 'selected_lock_name')  # 显示锁名
#             click_element(homepage_button_detail_back)  # 点击返回
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_普通用户_未注册_临时
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             invite_user_yale('normal', user_b[0], user_b[1], 'temp')  # 邀请普通用户
#             logout('house')  # 退出登录
#             user_register_yale()  # 注册账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, False, wait_time=1)  # 验证普通用户，不显示日志Tab
#             check_lock_connection()  # 等待锁连接
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C928(self):
#         if app_select == 'panpan':
#             user_info = phone_number_get_registered()
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(user_button_user)  # 点击【用户】
#             click_element(user_button_invite_open)  # 打开邀请用户
#             click_element(user_button_invite_admin)  # 点击【管理员邀请】
#             click_element(login_compound_login_method)  # 点击【从通讯录邀请】
#             click_element(user_button_cancel_contact)  # 点击取消
#             send_keys(register_button_register_phone, user_info[0])  # 输入手机号
#             send_keys(register_button_register_name, user_info[1])  # 输入用户名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(user_assert_has_account_notice, user_assert_has_account_notice_text)  # 提示已有账号
#             click_element(homepage_button_detail_back)  # 点击返回
#             click_element(user_button_invite_open)  # 打开邀请用户
#             click_element(user_button_invite_normal)  # 点击【普通用户邀请】
#             click_element(login_compound_login_method)  # 点击【从通讯录邀请】
#             click_element(user_button_cancel_contact)  # 点击取消
#             send_keys(register_button_register_phone, user_info[0])  # 输入手机号
#             send_keys(register_button_register_name, user_info[1])  # 输入用户名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(user_assert_has_account_notice, user_assert_has_account_notice_text)  # 提示已有账号
#             click_element(homepage_button_detail_back)  # 点击返回
#             click_element(user_button_invite_open)  # 打开邀请用户
#             click_element(user_button_invite_guest)  # 点击【访客邀请】
#             click_element(login_compound_login_method)  # 点击【从通讯录邀请】
#             click_element(user_button_cancel_contact)  # 点击取消
#             send_keys(register_button_register_phone, user_info[0])  # 输入手机号
#             send_keys(register_button_register_name, user_info[1])  # 输入用户名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(user_assert_has_account_notice, user_assert_has_account_notice_text)  # 提示已有账号
#             click_element(homepage_button_detail_back)  # 点击返回
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_普通用户_已注册_永久
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             invite_user_yale('normal', user_b[0], user_b[1], 'always')  # 邀请普通用户
#             logout('house')  # 退出登录
#             login_yale(user_b[0], user_b[2], user_b[3], 'phone')  # 登录账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, False, wait_time=1)  # 验证普通用户，不显示日志Tab
#             check_lock_connection()  # 等待锁连接
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C929(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('admin', UserInfo.Second.phone, 'fish', True, 'always')  # 邀请用户
#             logout('house')
#             login_yale(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'email')
#             click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
#             assert_result_text(homePage_menu_user_name, UserInfo.Second.name)  # 显示用户名称
#             click_element(homePage_menu_home)  # 点击【首页】
#             logout('house')
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(homepage_button_select_room_or_user.format(HouseInfo.name))  # 点击房间
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_普通用户_已注册_重复
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             invite_user_yale('normal', user_b[0], user_b[1], 'recur')  # 邀请普通用户
#             logout('house')  # 退出登录
#             login_yale(user_b[0], user_b[2], user_b[3], 'phone')  # 登录账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, False, wait_time=1)  # 验证普通用户，不显示日志Tab
#             check_lock_connection()  # 等待锁连接
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C930(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(user_button_user)  # 点击【用户】
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_normal)  # 点击【普通用户邀请】
#             send_keys(register_button_register_phone, UserInfo.Second.phone)  # 输入手机号
#             send_keys(register_button_register_name, UserInfo.Second.name)  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             click_element(register_button_sure)  # 点击【下一步】
#             click_element(user_button_invite_sure)  # 点击【确定】
#             click_element(user_button_invite_schedule_recur)  # 点击【多次使用】
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_普通用户_已注册_临时
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             invite_user_yale('normal', user_b[0], user_b[1], 'temp')  # 邀请普通用户
#             logout('house')  # 退出登录
#             login_yale(user_b[0], user_b[2], user_b[3], 'phone')  # 登录账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, False, wait_time=1)  # 验证普通用户，不显示日志Tab
#             check_lock_connection()  # 等待锁连接
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C931(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_输入手机号页UI
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             click_element(userManagement_invite_select_phone)  # 点击手机号邀请
#             assert_result_displayed(userManagement_invite_phone_input, True)  # 显示手机号输入框
#             self.driver.keyevent(4)  # 返回
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C932(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_输入手机号页“<-”按钮
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             click_element(userManagement_invite_select_phone)  # 点击手机号邀请
#             click_element(userManagement_invite_back_button)  # 点击返回按键
#             assert_result_displayed(userManagement_invite_select_phone, True)  # 显示通过手机号邀请
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C933(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(user_button_user)  # 点击【用户】
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_admin)  # 点击【管理员邀请】
#             send_keys(register_button_register_phone, UserInfo.Second.phone)  # 输入手机号
#             send_keys(register_button_register_name, UserInfo.Second.name)  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             click_element(register_button_sure)  # 点击【下一步】
#             click_element(user_button_invite_sure)  # 点击【确定】
#             click_element(user_button_invite_schedule_temp)  # 点击【临时使用】
#             assert_result_text(register_assert_alert, user_assert_invite_admin_alert)  # 提示【管理员任何时候都能访问】
#             click_element(user_button_invite_schedule_recur)  # 点击【多次使用】
#             assert_result_text(register_assert_alert, user_assert_invite_admin_alert)  # 提示【管理员任何时候都能访问】
#             click_element(user_button_invite_sure)  # 点击【确定】
#             click_element(user_button_message_back)  # 点击【返回】
#             assert_result_displayed(user_compound_user_admin.format(UserInfo.Second.name), True)  # 显示客户名称
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_输入手机号页国家代号选择
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             click_element(userManagement_invite_select_phone)  # 点击手机号邀请
#             click_element(userManagement_invite_area_number)  # 点击区号选择
#             send_keys(login_login_country_search, 'Hong Kong')  # 输入Hong Kong
#             click_element(login_login_area_name)  # 点击【Hong Kong】
#             assert_result_text(userManagement_invite_area_number, '+852')  # 检查文本【+852】
#             self.driver.keyevent(4)  # 返回
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C934(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(user_button_user)  # 点击【用户】
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_normal)  # 点击【普通用户邀请】
#             user_info = phone_number_get_not_registered()
#             send_keys(register_button_register_phone, user_info[0])  # 输入手机号
#             send_keys(register_button_register_name, user_info[1])  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             click_element(user_button_invite_sure)  # 点击【确定】
#             click_element(user_button_invite_schedule_temp)  # 点击【临时使用】
#             assert_result_displayed(register_assert_alert, False, 5)  # 没有弹窗信息显示
#             click_element(user_button_invite_schedule_always)  # 点击【永久使用】
#             assert_result_displayed(register_assert_alert, False, 5)  # 没有弹窗信息显示
#             click_element(user_button_invite_schedule_recur)  # 点击【多次使用】
#             assert_result_displayed(register_assert_alert, False, 5)  # 没有弹窗信息显示
#             click_element(user_button_invite_sure)  # 点击【确定】
#             click_element(user_button_message_back)  # 点击【返回】
#             assert_result_displayed(user_compound_user_invite.format(user_info[1]), True)  # 显示客户名称
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_输入手机号页_手机号为空
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             click_element(userManagement_invite_select_phone)  # 点击手机号邀请
#             click_element(userManagement_invite_phone_input)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             assert_result_text(userManagement_invite_error_prompt, '请录入有效的手机号码')  # 错误提示
#             self.driver.keyevent(4)  # 返回
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C935(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(user_button_user)  # 点击【用户】
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_guest)  # 点击【访客邀请】
#             user_info = phone_number_get_not_registered()
#             send_keys(register_button_register_phone, user_info[0])  # 输入手机号
#             send_keys(register_button_register_name, user_info[1])  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             click_element(user_button_invite_sure)  # 点击【确定】
#             click_element(user_button_invite_schedule_always)  # 点击【永久使用】
#             assert_result_displayed(register_assert_alert, False, 5)  # 没有弹窗信息显示
#             click_element(user_button_invite_schedule_recur)  # 点击【多次使用】
#             assert_result_displayed(register_assert_alert, False, 5)  # 没有弹窗信息显示
#             click_element(user_button_invite_schedule_temp)  # 点击【临时使用】
#             assert_result_displayed(register_assert_alert, False, 5)  # 没有弹窗信息显示
#             click_element(user_button_invite_sure)  # 点击【确定】
#             click_element(user_button_message_back)  # 点击【返回】
#             assert_result_displayed(user_compound_user_guest.format(user_info[1]), True)  # 显示客户名称
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_输入手机号页_手机号位数不为11位
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             click_element(userManagement_invite_select_phone)  # 点击手机号邀请
#             send_keys(userManagement_invite_phone_input, UserInfo.First.phone_short)  # 非11位手机号
#             click_element(userManagement_invite_phone_input)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             assert_result_text(userManagement_invite_error_prompt, '请录入有效的手机号码')  # 错误提示
#             self.driver.keyevent(4)  # 返回
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C936(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_info = phone_number_get_not_registered()
#             invite_user('admin', user_info[0], user_info[1], False, 'always')
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_admin)  # 点击【管理员邀请】
#             send_keys(register_button_register_phone, user_info[0])  # 输入手机号
#             send_keys(register_button_register_name, user_info[1])  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(register_assert_alert, user_already_get_lock)  # 提示用户已拥有该房间所有的锁
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_输入手机号页_手机号位数为非数字
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             click_element(userManagement_invite_select_phone)  # 点击手机号邀请
#             send_keys(userManagement_invite_phone_input, UserInfo.First.phone_with_english)  # 非数字
#             click_element(userManagement_invite_phone_input)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             assert_result_text(userManagement_invite_error_prompt, '请录入有效的手机号码')  # 错误提示
#             self.driver.keyevent(4)  # 返回
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C937(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_输入用户名页UI
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             click_element(userManagement_invite_select_phone)  # 点击手机号邀请
#             send_keys(userManagement_invite_phone_input, UserInfo.Second.phone)  # 正常手机号
#             click_element(userManagement_invite_phone_input)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             assert_result_displayed(userManagement_invite_first_name, True)  # 进入下一页
#             self.driver.keyevent(4)  # 返回
#             self.driver.keyevent(4)  # 返回
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C938(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_输入用户名页“<-”按钮
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             click_element(userManagement_invite_select_phone)  # 点击手机号邀请
#             send_keys(userManagement_invite_phone_input, UserInfo.Second.phone)  # 正常手机号
#             click_element(userManagement_invite_phone_input)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             click_element(userManagement_invite_back_button)  # 点击返回按键
#             assert_result_displayed(userManagement_invite_select_phone, True)  # 返回上一页
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C939(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_admin)  # 点击【管理员邀请】
#             send_keys(register_button_register_phone, UserInfo.Second.phone)  # 输入手机号
#             send_keys(register_button_register_name, UserInfo.Second.name)  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(register_assert_alert, user_already_get_lock)  # 提示用户已拥有该房间所有的锁
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_输入用户名页_姓为空
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             click_element(userManagement_invite_select_phone)  # 点击手机号邀请
#             send_keys(userManagement_invite_phone_input, UserInfo.Second.phone)  # 正常手机号
#             click_element(userManagement_invite_phone_input)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             send_keys(userManagement_invite_first_name, UserInfo.Second.name_first)  # 输入名
#             click_element(userManagement_invite_last_name)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             assert_result_text(userManagement_invite_error_prompt, '请输入用户名')  # 错误提示
#             self.driver.keyevent(4)  # 返回
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C940(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_normal)  # 点击【普通用户邀请】
#             send_keys(register_button_register_phone, UserInfo.Second.phone)  # 输入手机号
#             send_keys(register_button_register_name, UserInfo.Second.name)  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(register_assert_alert, user_already_get_lock)  # 提示用户已拥有该房间所有的锁
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_输入用户名页_名为空
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             click_element(userManagement_invite_select_phone)  # 点击手机号邀请
#             send_keys(userManagement_invite_phone_input, UserInfo.Second.phone)  # 正常手机号
#             click_element(userManagement_invite_phone_input)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             send_keys(userManagement_invite_last_name, UserInfo.Second.name_last)  # 输入姓
#             click_element(userManagement_invite_last_name)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             assert_result_text(userManagement_invite_error_prompt, '请输入用户名')  # 错误提示
#             self.driver.keyevent(4)  # 返回
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C941(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_guest)  # 点击【访客邀请】
#             send_keys(register_button_register_phone, UserInfo.Second.phone)  # 输入手机号
#             send_keys(register_button_register_name, UserInfo.Second.name)  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(register_assert_alert, user_already_get_lock)  # 提示用户已拥有该房间所有的锁
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_手机号邀请_输入用户名页_姓/名都为空
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_user)  # 点击邀请管理员或普通用户
#             click_element(userManagement_invite_select_phone)  # 点击手机号邀请
#             send_keys(userManagement_invite_phone_input, UserInfo.Second.phone)  # 正常手机号
#             click_element(userManagement_invite_phone_input)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             click_element(userManagement_invite_last_name)  # 调起键盘
#             self.driver.keyevent(66)  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
#             assert_result_text(userManagement_invite_error_prompt, '请输入用户名')  # 错误提示
#             self.driver.keyevent(4)  # 返回
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C942(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_admin)  # 点击【管理员邀请】
#             send_keys(register_button_register_phone, UserInfo.Second.phone)  # 输入手机号
#             send_keys(register_button_register_name, UserInfo.Second.name)  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(register_assert_alert, user_already_get_lock)  # 提示用户已拥有该房间所有的锁
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_管理员_未注册
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             invite_user_yale('admin', user_b[0], user_b[1])  # 邀请管理员
#             logout('house')  # 退出登录
#             user_register_yale()  # 注册账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, True)  # 验证管理员，显示日志Tab
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout()  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C943(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_normal)  # 点击【普通用户邀请】
#             send_keys(register_button_register_phone, UserInfo.Second.phone)  # 输入手机号
#             send_keys(register_button_register_name, UserInfo.Second.name)  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(register_assert_alert, user_already_get_lock)  # 提示用户已拥有该房间所有的锁
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_管理员_已注册
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             invite_user_yale('admin', user_b[0], user_b[1])  # 邀请管理员
#             logout('house')  # 退出登录
#             login_yale(user_b[0], user_b[2], user_b[3], 'phone')  # 登录账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, True)  # 验证管理员，显示日志Tab
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout()  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C944(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_guest)  # 点击【访客邀请】
#             send_keys(register_button_register_phone, UserInfo.Second.phone)  # 输入手机号
#             send_keys(register_button_register_name, UserInfo.Second.name)  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(register_assert_alert, user_already_get_lock)  # 提示用户已拥有该房间所有的锁
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_普通用户_未注册_永久
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             invite_user_yale('normal', user_b[0], user_b[1], 'always')  # 邀请普通用户
#             logout('house')  # 退出登录
#             user_register_yale()  # 注册账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, False, wait_time=1)  # 验证普通用户，不显示日志Tab
#             check_lock_connection()  # 等待锁连接
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C945(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('guest', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_admin)  # 点击【管理员邀请】
#             send_keys(register_button_register_phone, UserInfo.Second.phone)  # 输入手机号
#             send_keys(register_button_register_name, UserInfo.Second.name)  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(register_assert_alert, user_already_get_lock)  # 提示用户已拥有该房间所有的锁
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_普通用户_未注册_重复
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             invite_user_yale('normal', user_b[0], user_b[1], 'recur')  # 邀请普通用户
#             logout('house')  # 退出登录
#             user_register_yale()  # 注册账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, False, wait_time=1)  # 验证普通用户，不显示日志Tab
#             check_lock_connection()  # 等待锁连接
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C946(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('guest', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_normal)  # 点击【普通用户邀请】
#             send_keys(register_button_register_phone, UserInfo.Second.phone)  # 输入手机号
#             send_keys(register_button_register_name, UserInfo.Second.name)  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(register_assert_alert, user_already_get_lock)  # 提示用户已拥有该房间所有的锁
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_普通用户_未注册_临时
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             invite_user_yale('normal', user_b[0], user_b[1], 'temp')  # 邀请普通用户
#             logout('house')  # 退出登录
#             user_register_yale()  # 注册账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, False, wait_time=1)  # 验证普通用户，不显示日志Tab
#             check_lock_connection()  # 等待锁连接
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C947(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('guest', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_guest)  # 点击【访客邀请】
#             send_keys(register_button_register_phone, UserInfo.Second.phone)  # 输入手机号
#             send_keys(register_button_register_name, UserInfo.Second.name)  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             assert_result_text(register_assert_alert, user_already_get_lock)  # 提示用户已拥有该房间所有的锁
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_普通用户_已注册_永久
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             invite_user_yale('normal', user_b[0], user_b[1], 'always')  # 邀请普通用户
#             logout('house')  # 退出登录
#             login_yale(user_b[0], user_b[2], user_b[3], 'phone')  # 登录账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, False, wait_time=1)  # 验证普通用户，不显示日志Tab
#             check_lock_connection()  # 等待锁连接
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C948(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_普通用户_已注册_重复
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             invite_user_yale('normal', user_b[0], user_b[1], 'recur')  # 邀请普通用户
#             logout('house')  # 退出登录
#             login_yale(user_b[0], user_b[2], user_b[3], 'phone')  # 登录账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, False, wait_time=1)  # 验证普通用户，不显示日志Tab
#             check_lock_connection()  # 等待锁连接
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C949(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_普通用户_已注册_临时
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             invite_user_yale('normal', user_b[0], user_b[1], 'temp')  # 邀请普通用户
#             logout('house')  # 退出登录
#             login_yale(user_b[0], user_b[2], user_b[3], 'phone')  # 登录账号b
#             time.sleep(5)  # 等待loading
#             assert_result_displayed(userManagement_main_house_name, True)  # 验证已被邀请
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间，等待锁连接
#             assert_result_displayed(userManagement_main_log_tab, False, wait_time=1)  # 验证普通用户，不显示日志Tab
#             check_lock_connection()  # 等待锁连接
#             click_element(lock_main_lottie)  # 连接成功，点击开锁
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(lock_main_house_name, after_action_sleep=5)  # 点击进入房间
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C950(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_未注册_永久_默认开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             pin = invite_user_yale('guest', user_b[0], user_b[1], 'always', 'default')  # 邀请访客
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C951(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_未注册_重复_默认开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             pin = invite_user_yale('guest', user_b[0], user_b[1], 'recur', 'default')  # 邀请访客
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C952(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin)
#             add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb)
#             add_card(UserInfo.Second.name)
#             click_element(user_compound_user_admin.format(UserInfo.Second.name))  # 选择管理员
#             click_element(user_button_remove_user)  # 点击【删除用户】
#             click_element(user_button_factory_reset_and_delete_yes)  # 点击【删除】
#             assert_result_text(register_assert_alert, user_assert_user_remove_has_pin)  # 提示先删除密码
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_未注册_临时_默认开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             pin = invite_user_yale('guest', user_b[0], user_b[1], 'temp', 'default')  # 邀请访客
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C953(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
#             add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb, 'normal')
#             add_card(UserInfo.Second.name, 'normal')
#             click_element(user_compound_user_normal.format(UserInfo.Second.name))  # 选择普通用户
#             click_element(user_button_remove_user)  # 点击【删除用户】
#             click_element(user_button_factory_reset_and_delete_yes)  # 点击【删除】
#             assert_result_text(register_assert_alert, user_assert_user_remove_has_pin)  # 提示先删除密码
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_已注册_永久_默认开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             pin = invite_user_yale('guest', user_b[0], user_b[1], 'always', 'default')  # 邀请访客
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C954(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('guest', UserInfo.Second.phone, UserInfo.Second.name, True, 'temp')
#             click_element(user_compound_user_guest.format(UserInfo.Second.name))  # 点击访客
#             click_element(user_button_remove_user)  # 点击【删除用户】
#             click_element(user_button_factory_reset_and_delete_yes)  # 点击【删除】
#             assert_result_text(register_assert_alert, user_assert_user_remove_has_pin)  # 提示需要先删除密码
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_已注册_重复_默认开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             pin = invite_user_yale('guest', user_b[0], user_b[1], 'recur', 'default')  # 邀请访客
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C955(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'recur')
#             click_element(user_compound_user_normal.format(UserInfo.Second.name))  # 点击普通用户
#             click_element(user_button_user_level)  # 点击【权限等级】
#             click_element(user_button_user_level_admin)  # 点击【管理员】
#             click_element(user_button_user_level_save)  # 点击【保存】
#             assert_result_text(user_assert_dialog, user_assert_dialog_from_normal_to_admin)  # 提示先修改使用时间
#             click_element(homePage_else_sure_or_try)  # 点击【好的】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_已注册_临时_默认开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 未注册账号
#             pin = invite_user_yale('guest', user_b[0], user_b[1], 'temp', 'default')  # 邀请访客
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C956(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_未注册_永久_非默认开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             pin = user_b[0][-6:]  # 密码为手机号后六位
#             invite_user_yale('guest', user_b[0], user_b[1], 'always', pin)  # 邀请访客
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C957(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             click_element(user_compound_user_admin.format(UserInfo.Second.name))  # 点击管理员
#             assert_result_displayed(user_assert_user_level_more, True)  # 权限等级可编辑
#             assert_result_displayed(user_assert_user_schedule_more, False, 5)  # 使用时间不可编辑
#             assert_result_displayed(user_assert_user_pin_more, True)  # 密码可编辑
#             assert_result_displayed(user_assert_user_finger_more, True)  # 指纹可编辑
#             assert_result_displayed(user_assert_user_card_more, True)  # 门卡可编辑
#             click_element(homepage_button_detail_back)  # 点击返回
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_未注册_重复_非默认开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             pin = user_b[0][-6:]  # 密码为手机号后六位
#             invite_user_yale('guest', user_b[0], user_b[1], 'recur', pin)  # 邀请访客
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C958(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'recur')
#             click_element(user_compound_user_normal.format(UserInfo.Second.name))  # 点击普通用户
#             assert_result_displayed(user_assert_user_level_more, True)  # 权限等级可编辑
#             assert_result_displayed(user_assert_user_schedule_more, True)  # 使用时间可编辑
#             assert_result_displayed(user_assert_user_pin_more, True)  # 密码可编辑
#             assert_result_displayed(user_assert_user_finger_more, True)  # 指纹可编辑
#             assert_result_displayed(user_assert_user_card_more, True)  # 门卡可编辑
#             click_element(homepage_button_detail_back)  # 点击返回
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_未注册_临时_非默认开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             pin = user_b[0][-6:]  # 密码为手机号后六位
#             invite_user_yale('guest', user_b[0], user_b[1], 'temp', pin)  # 邀请访客
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C959(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('guest', UserInfo.Second.phone, UserInfo.Second.name, True, 'temp')
#             click_element(user_compound_user_guest.format(UserInfo.Second.name))  # 点击访客
#             assert_result_displayed(user_assert_user_level_more, False, 5)  # 权限等级不可编辑
#             assert_result_displayed(user_assert_user_schedule_more, True)  # 使用时间可编辑
#             assert_result_displayed(user_assert_user_pin_more, True)  # 密码可编辑
#             click_element(homepage_button_detail_back)  # 点击返回
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_已注册_永久_非默认开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 未注册账号
#             pin = user_b[0][-6:]  # 密码为手机号后六位
#             invite_user_yale('guest', user_b[0], user_b[1], 'always', pin)  # 邀请访客
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C960(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_已注册_重复_非默认开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 未注册账号
#             pin = user_b[0][-6:]  # 密码为手机号后六位
#             invite_user_yale('guest', user_b[0], user_b[1], 'recur', pin)  # 邀请访客
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C961(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_已注册_临时_非默认开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             pin = user_b[0][-6:]  # 密码为手机号后六位
#             invite_user_yale('guest', user_b[0], user_b[1], 'temp', pin)  # 邀请访客
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             delete_user_yale(user_b[1] + ' ' + user_b[1])  # 删除用户
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C962(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_未注册_永久_已存在已启用开门密码
#             # 此处添加两个管理员的密码，First为enable，Second为disable
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.First.name, UserInfo.First.phone)  # 设置管理员密码
#             invite_user_yale('admin', UserInfo.Second.phone, UserInfo.Second.name)  # 邀请第二个管理员
#             password_register(UserInfo.Second.name, UserInfo.Second.phone)  # 设置管理员密码
#             disable_pin_yale(UserInfo.Second.name)  # 禁用密码
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             invite_user_yale('guest', user_b[0], user_b[1], 'always', UserInfo.First.pin)  # 邀请访客
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 邀请失败
#             # click_element(userManagement_invite_popup_confirm)  # 点击确定
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C963(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_未注册_永久_已存在未启用开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             invite_user_yale('guest', user_b[0], user_b[1], 'always', UserInfo.Second.pin)  # 邀请访客
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 邀请失败
#             # click_element(userManagement_invite_popup_confirm)  # 点击确定
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C964(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_未注册_重复_已存在开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             invite_user_yale('guest', user_b[0], user_b[1], 'recur', UserInfo.First.pin)  # 邀请访客
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 邀请失败
#             # click_element(userManagement_invite_popup_confirm)  # 点击确定
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C965(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_未注册_临时_已存在开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_not_registered()  # 未注册账号
#             invite_user_yale('guest', user_b[0], user_b[1], 'temp', UserInfo.First.pin)  # 邀请访客
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 邀请失败
#             # click_element(userManagement_invite_popup_confirm)  # 点击确定
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C966(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_已注册_永久_已存在已启用开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             invite_user_yale('guest', user_b[0], user_b[1], 'always', UserInfo.First.pin)  # 邀请访客
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 邀请失败
#             # click_element(userManagement_invite_popup_confirm)  # 点击确定
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C967(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_已注册_永久_已存在未启用开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             invite_user_yale('guest', user_b[0], user_b[1], 'always', UserInfo.Second.pin)  # 邀请访客
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 邀请失败
#             # click_element(userManagement_invite_popup_confirm)  # 点击确定
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C968(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_已注册_重复_已存在开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             invite_user_yale('guest', user_b[0], user_b[1], 'recur', UserInfo.First.pin)  # 邀请访客
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 邀请失败
#             # click_element(userManagement_invite_popup_confirm)  # 点击确定
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C969(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_已注册_临时_已存在开门密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             user_b = phone_number_get_registered()  # 已注册账号
#             invite_user_yale('guest', user_b[0], user_b[1], 'temp', UserInfo.First.pin)  # 邀请访客
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 邀请失败
#             # click_element(userManagement_invite_popup_confirm)  # 点击确定
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # click_element(userManagement_invite_guest_back_button)  # 返回
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C970(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_邀请页UI
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             assert_result_displayed(userManagement_invite_guest_first_name, True)  # 展示页面元素
#             assert_result_displayed(userManagement_invite_guest_last_name, True)  # 展示页面元素
#             assert_result_displayed(userManagement_invite_guest_contact, True)  # 展示页面元素
#             assert_result_displayed(userManagement_invite_continue_button, True)  # 展示页面元素
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C971(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_邀请页“<-”按钮
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             click_element(userManagement_invite_guest_back_button)  # 点击返回按键
#             assert_result_displayed(userManagement_invite_invite, True)  # 返回用户管理页
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C972(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_邀请页物理退出
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             self.driver.keyevent(4)  # 返回
#             click_element(userManagement_invite_cancel)  # 点击取消
#             self.driver.keyevent(4)  # 返回
#             click_element(userManagement_invite_confirm)  # 点击离开
#             assert_result_displayed(userManagement_invite_invite, True)  # 返回用户管理页
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     # @adb_log
#     # def test_user_management_tc059(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 用户邀请_联系人邀请_访客_邀请页_已注册_昵称保持
#     #         login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#     #         click_element(userManagement_main_house_name)  # 进入房间
#     #         click_element(userManagement_main_user_tab)  # 点击用户管理tab
#     #         click_element(userManagement_invite_invite)  # 点击邀请用户
#     #         click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#     #         # TODO 通讯录邀请
#     #         logout('house')  # 退出登录
#     #     else:
#     #         assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C974(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_邀请页_姓为空
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             send_keys(userManagement_invite_guest_first_name, UserInfo.Second.name_first)  # 输入名
#             click_element(userManagement_invite_continue_button)  # 点击继续
#             assert_result_text(userManagement_invite_error_prompt, '你必须输入名字和姓氏')  # 错误提示
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C975(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_邀请页_名为空
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             send_keys(userManagement_invite_guest_last_name, UserInfo.Second.name_last)  # 输入姓
#             click_element(userManagement_invite_continue_button)  # 点击继续
#             assert_result_text(userManagement_invite_error_prompt, '你必须输入名字和姓氏')  # 错误提示
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C976(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_邀请页_姓/名都为空
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             click_element(userManagement_invite_continue_button)  # 点击继续
#             assert_result_text(userManagement_invite_error_prompt, '你必须输入名字和姓氏')  # 错误提示
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C977(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_非法手机号
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             click_element(userManagement_invite_guest_contact)  # 点击通讯录邀请
#             click_element(userManagement_invite_invalid_contact)  # 选择非法手机号
#             assert_result_text(userManagement_invite_error_prompt, '这个联系人的号码无效，请重新选择')  # 错误提示
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C978(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_无手机号
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             click_element(userManagement_invite_guest_contact)  # 点击通讯录邀请
#             # click_element(userManagement_invite_blank_contact)  # 选择无手机号
#             # assert_result_text(userManagement_invite_error_prompt, '名字为空或者号码无效')  # 错误提示
#             assert_result_displayed(userManagement_invite_blank_contact, False, wait_time=2)  # 不显示空手机号
#             self.driver.keyevent(4)
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C979(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_多手机号
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             click_element(userManagement_invite_guest_contact)  # 点击通讯录邀请
#             click_element(userManagement_invite_multiple_contact)  # 选择多个手机号
#             # assert_result_text(userManagement_invite_error_prompt, '名字为空或者号码无效')  # 错误提示
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C980(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_保存密码页UI
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             send_keys(userManagement_invite_guest_first_name, UserInfo.Second.name_first)  # 输入名
#             send_keys(userManagement_invite_guest_last_name, UserInfo.Second.name_last)  # 输入姓
#             click_element(userManagement_invite_continue_button)  # 继续
#             assert_result_displayed(userManagement_invite_create_schedule, True)  # 设置使用时间
#             assert_result_displayed(userManagement_invite_create_pin, True)  # 设置进门密码
#             assert_result_displayed(userManagement_invite_save_pin, True)  # 保存进门密码按键
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C981(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_保存密码页“<-”按钮
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             send_keys(userManagement_invite_guest_first_name, UserInfo.Second.name_first)  # 输入名
#             send_keys(userManagement_invite_guest_last_name, UserInfo.Second.name_last)  # 输入姓
#             click_element(userManagement_invite_continue_button)  # 继续
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             assert_result_displayed(userManagement_invite_guest_first_name, True)  # 返回上一页
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C982(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_保存密码页_向访客发送短信_默认未启用
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             send_keys(userManagement_invite_guest_first_name, UserInfo.Second.name_first)  # 输入名
#             send_keys(userManagement_invite_guest_last_name, UserInfo.Second.name_last)  # 输入姓
#             click_element(userManagement_invite_continue_button)  # 继续
#             assert_result_text(userManagement_invite_send_text, '向访客发送短信 关闭')  # 发送短信默认未启用
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C983(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_保存密码页_向访客发送短信
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             send_keys(userManagement_invite_guest_first_name, UserInfo.Second.name_first)  # 输入名
#             send_keys(userManagement_invite_guest_last_name, UserInfo.Second.name_last)  # 输入姓
#             click_element(userManagement_invite_continue_button)  # 继续
#             assert_result_displayed(userManagement_invite_send_text, True)  # 向访客发送短信
#             click_element(userManagement_invite_send_text)  # 点击发送短信
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C984(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_保存密码页_未选联系人_向访客发送短信
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             send_keys(userManagement_invite_guest_first_name, UserInfo.Second.name_first)  # 输入名
#             send_keys(userManagement_invite_guest_last_name, UserInfo.Second.name_last)  # 输入姓
#             click_element(userManagement_invite_continue_button)  # 继续
#             assert_result_displayed(userManagement_invite_send_text, True)  # 向访客发送短信
#             click_element(userManagement_invite_send_text)  # 点击发送短信
#             assert_result_text(userManagement_invite_send_text, '向访客发送短信 开启')  # 验证已开启
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C985(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_设置密码页UI
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             send_keys(userManagement_invite_guest_first_name, UserInfo.Second.name_first)  # 输入名
#             send_keys(userManagement_invite_guest_last_name, UserInfo.Second.name_last)  # 输入姓
#             click_element(userManagement_invite_continue_button)  # 继续
#             click_element(userManagement_invite_create_pin)  # 点击进门密码
#             assert_result_displayed(userManagement_invite_pin_input, True)  # 显示密码输入框
#             assert_result_displayed(userManagement_invite_change_pin, True)  # 显示保存更改
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C986(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_设置密码页“<-”按钮
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             send_keys(userManagement_invite_guest_first_name, UserInfo.Second.name_first)  # 输入名
#             send_keys(userManagement_invite_guest_last_name, UserInfo.Second.name_last)  # 输入姓
#             click_element(userManagement_invite_continue_button)  # 继续
#             click_element(userManagement_invite_create_pin)  # 点击进门密码
#             assert_result_displayed(userManagement_invite_pin_input, True)  # 显示密码输入框
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             assert_result_displayed(userManagement_invite_create_pin, True)  # 显示保存更改
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C987(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_设置密码页_空密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             send_keys(userManagement_invite_guest_first_name, UserInfo.Second.name_first)  # 输入名
#             send_keys(userManagement_invite_guest_last_name, UserInfo.Second.name_last)  # 输入姓
#             click_element(userManagement_invite_continue_button)  # 继续
#             click_element(userManagement_invite_create_pin)  # 点击进门密码
#             clear_text_content(userManagement_invite_pin_input)  # 清空默认密码
#             send_keys(userManagement_invite_pin_input, ' ')  # 输入密码
#             click_element(userManagement_invite_change_pin)  # 点击保存更改
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码无效
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             assert_result_displayed(userManagement_invite_change_pin, True)  # 点击“确定”后仍停在密码设置页
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C988(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户邀请_联系人邀请_访客_设置密码页_非六位数字密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击用户管理tab
#             click_element(userManagement_invite_invite)  # 点击邀请用户
#             click_element(userManagement_invite_invite_guest)  # 点击邀请访客
#             send_keys(userManagement_invite_guest_first_name, UserInfo.Second.name_first)  # 输入名
#             send_keys(userManagement_invite_guest_last_name, UserInfo.Second.name_last)  # 输入姓
#             click_element(userManagement_invite_continue_button)  # 继续
#             click_element(userManagement_invite_create_pin)  # 点击进门密码
#             clear_text_content(userManagement_invite_pin_input)  # 清空默认密码
#             send_keys(userManagement_invite_pin_input, '123')  # 输入密码
#             click_element(userManagement_invite_change_pin)  # 点击保存更改
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码无效
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             assert_result_displayed(userManagement_invite_change_pin, True)  # 点击“确定”后仍停在密码设置页
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             click_element(userManagement_invite_guest_back_button)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C989(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_PINCode_添加
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             # password_register(UserInfo.First.name, UserInfo.First.phone)  # 管理员已有密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.First.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C990(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_PINCode_禁用
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             # password_register(UserInfo.Second.name, UserInfo.Second.phone)  # 管理员已有禁用密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.Second.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示门关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no pin code keyboard')
#     @adb_log
#     def test_C991(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(homepage_button_select_room_or_user.format(HouseInfo.name))
#             click_element(userManagement_main_house_name)  # 进入房间
#             auto_lock('on')
#             click_element(user_button_user)  # 点击【用户】
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_normal)  # 点击【普通用户邀请】
#             send_keys(register_button_register_phone, UserInfo.Second.phone)  # 输入手机号
#             send_keys(register_button_register_name, UserInfo.Second.name)  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             click_element(register_button_sure)  # 点击【下一步】
#             click_element(user_button_invite_sure)  # 点击【确定】
#             click_element(user_button_invite_schedule_temp)  # 点击【临时使用】
#             click_element(time_button_invite_temp_end_date)  # 点击【结束日期】
#             click_element(time_button_invite_temp_prev_month)  # 点击【上个月】
#             click_element(time_button_invite_temp_date_select.format(1))  # 选择第一天
#             click_element(time_button_invite_temp_save)  # 点击【确定】
#             assert_result_text(register_assert_alert, time_assert_early_then_current)  # 提示结束时间不能早于当前时间
#             click_element(time_button_invite_temp_end_date)  # 点击【结束日期】
#             click_element(time_button_invite_temp_date_select.format(get_today()))  # 选择今天
#             click_element(time_button_invite_temp_save)  # 点击【确定】
#             click_element(time_button_invite_temp_end_time)  # 点击【结束时间】
#             click_element(time_button_invite_keyboard)  # 点击【键盘】
#             target_time = get_time(need_minute=10)  # 获取时间
#             click_element(time_button_invite_input_hour)  # 点击【小时】
#             clear_text_content(time_button_invite_input_hour)  # 清除输入
#             send_keys(time_button_invite_input_hour, target_time[0], hide_keyboard=False)  # 输入小时
#             click_element(time_button_invite_input_minute)  # 点击【分钟】
#             clear_text_content(time_button_invite_input_minute)  # 清除输入
#             send_keys(time_button_invite_input_minute, target_time[1], hide_keyboard=False)  # 输入分钟
#             click_element(time_button_invite_temp_save)  # 点击【确定】
#             click_element(user_button_invite_sure)  # 点击【确定】
#             start_time = time.time()  # 获取开始时间
#             click_element(user_button_message_back)  # 点击【返回】
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
#             add_card(UserInfo.Second.name, 'normal')
#             add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb, 'normal')
#             click_element(user_button_lock)  # 点击【锁】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             click_element(lock_button_open_lock)  # 点击【开锁】
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             # Todo 密码开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             requests.put(url=API.request_api_card_open)  # 门卡开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             requests.put(url=API.request_api_finger_open.format(UserInfo.Second.name, UserInfo.Second.finger_thumb))  # 指纹开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             logout('house')
#             end_time = time.time()  # 获取结束时间
#             # 提醒时间超范围外
#             wait_time = int(target_time[2])*60 - int(end_time - start_time) + 60  # 总的等待时间，减去已经花费的时间，再多等一分钟
#             if wait_time <= 0:
#                 pass
#             else:
#                 time.sleep(int(target_time[2])*60 - int(end_time - start_time))
#             login_yale(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'phone')
#             click_element(add_assert_select_house.format(HouseInfo.name))  # 选择家
#             assert_result_displayed(time_assert_expired, True)
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             # Todo 密码开门
#             assert_result_displayed(lock_assert_lock_status_opening, wait_time=5, assert_type=False)  # 不显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             requests.put(url=API.request_api_card_open)  # 门卡开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             requests.put(url=API.request_api_finger_open.format(UserInfo.Second.name, UserInfo.Second.finger_thumb))  # 指纹开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             logout('house')
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_PINCode_激活
#             # 初始条件:First密码pin,激活；Second密码pin,激活
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             enable_pin_yale(UserInfo.Second.name)  # 激活密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.Second.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no pin code keyboard')
#     @adb_log
#     def test_C992(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(homepage_button_select_room_or_user.format(HouseInfo.name))
#             click_element(userManagement_main_house_name)  # 进入房间
#             auto_lock('on')
#             click_element(user_button_user)  # 点击【用户】
#             click_element(user_button_invite_open)  # 打开邀请
#             click_element(user_button_invite_normal)  # 点击【普通用户邀请】
#             send_keys(register_button_register_phone, UserInfo.Second.phone)  # 输入手机号
#             send_keys(register_button_register_name, UserInfo.Second.name)  # 输入姓名
#             click_element(register_button_sure)  # 点击【下一步】
#             click_element(register_button_sure)  # 点击【下一步】
#             click_element(user_button_invite_sure)  # 点击【确定】
#             click_element(user_button_invite_schedule_recur)  # 点击【多次使用】
#             click_element(time_button_invite_recur_end_time)  # 点击【结束时间】
#             click_element(time_button_invite_keyboard)  # 点击【键盘】
#             target_time = get_time(need_minute=10)  # 获取时间
#             click_element(time_button_invite_input_hour)  # 点击【小时】
#             clear_text_content(time_button_invite_input_hour)  # 清除输入
#             send_keys(time_button_invite_input_hour, target_time[0], hide_keyboard=False)  # 输入小时
#             click_element(time_button_invite_input_minute)  # 点击【分钟】
#             clear_text_content(time_button_invite_input_minute)  # 清除输入
#             send_keys(time_button_invite_input_minute, target_time[1], hide_keyboard=False)  # 输入分钟
#             click_element(time_button_invite_temp_save)  # 点击【确定】
#             start_time = time.time()  # 获取开始时间
#             click_element(user_button_invite_sure)  # 点击【确定】
#             click_element(user_button_message_back)  # 点击【返回】
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
#             add_card(UserInfo.Second.name, 'normal')
#             add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb, 'normal')
#             click_element(user_button_lock)  # 点击【锁】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             click_element(lock_button_open_lock)  # 点击【开锁】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             # Todo 密码开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             requests.put(url=API.request_api_card_open)  # 门卡开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             requests.put(url=API.request_api_finger_open.format(UserInfo.Second.name, UserInfo.Second.finger_thumb))  # 指纹开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             logout('house')
#             end_time = time.time()  # 获取结束时间
#             # 提醒时间超范围外
#             wait_time = int(target_time[2])*60 - int(end_time - start_time) + 60  # 总的等待时间，减去已经花费的时间，再多等一分钟
#             if wait_time <= 0:
#                 pass
#             else:
#                 time.sleep(int(target_time[2])*60 - int(end_time - start_time))
#             login_yale(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'phone')
#             click_element(add_assert_select_house.format(HouseInfo.name))  # 选择家
#             assert_result_displayed(time_assert_expired, True)
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             # Todo 密码开门
#             assert_result_displayed(lock_assert_lock_status_opening, wait_time=5, assert_type=False)  # 不显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             requests.put(url=API.request_api_card_open)  # 门卡开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             requests.put(url=API.request_api_finger_open.format(UserInfo.Second.name, UserInfo.Second.finger_thumb))  # 指纹开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             logout('house')
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_PINCode_修改
#             # 初始条件:First密码pin_new,激活；Second密码pin,激活
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.First.name, UserInfo.First.pin_new)  # 修改密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.First.pin_new))  # 新密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no pin code keyboard')
#     @adb_log
#     def test_C993(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(homepage_button_select_room_or_user.format(HouseInfo.name))
#             click_element(userManagement_main_house_name)  # 进入房间
#             auto_lock('on')
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
#             add_card(UserInfo.Second.name, 'normal')
#             add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb, 'normal')
#             click_element(user_button_lock)  # 点击【锁】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             click_element(lock_button_open_lock)  # 点击【开锁】
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             # Todo 密码开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             requests.put(url=API.request_api_card_open)  # 门卡开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             requests.put(url=API.request_api_finger_open.format(UserInfo.Second.name, UserInfo.Second.finger_thumb))  # 指纹开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_PINCode_添加为_已有已启用密码
#             # 初始条件:First密码pin_new,激活；Second密码pin,激活
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.First.name, UserInfo.Second.pin)  # 修改为已启用密码
#             time.sleep(5)
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C994(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_PINCode_添加为_已有未启用密码
#             # 初始条件:First密码pin_new,激活；Second密码pin,激活
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             disable_pin_yale(UserInfo.Second.name)  # 禁用Second密码
#             password_register(UserInfo.First.name, UserInfo.Second.pin)  # 设置First密码为禁用密码
#             time.sleep(5)
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C995(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_PINCode_激活为_已有密码
#             # 初始条件:First密码随机,未启用；Second密码pin,禁用
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.First.name, UserInfo.First.pin)  # 设置First密码
#             click_element(UserManagement_user_admin.format(UserInfo.Second.name))  # 点击管理员
#             click_element(UserManagement_user_password)  # 点击【密码】
#             click_element(UserManagement_password_save)  # 点击激活
#             clear_text_content(UserManagement_password_input)  # 清除密码
#             send_keys(UserManagement_password_input, UserInfo.First.pin)  # 激活为已有密码
#             click_element(UserManagement_password_save)  # 保存密码
#             # click_element(UserManagement_save_update)  # 保存修改
#             time.sleep(5)
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C996(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_PINCode_修改为_已有密码
#             # 初始条件:First密码pin,启用；Second密码随机,未启用
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.Second.name, UserInfo.Second.pin)  # 启用Second密码
#             password_register(UserInfo.Second.name, UserInfo.First.pin)  # 设置为已有密码
#             time.sleep(5)
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # password_register(UserInfo.Second.name, UserInfo.Second.pin)  # 设置密码
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_C997(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_RFID_添加
#             # 初始条件:First密码pin,启用；Second密码pin,启用
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             add_card_yale(UserInfo.First.name)  # 添加门卡
#             # check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().card_open())  # 门卡开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C998(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(homepage_button_select_room_or_user.format(HouseInfo.name))
#             click_element(userManagement_main_house_name)  # 进入房间
#             auto_lock('on')
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
#             add_card(UserInfo.Second.name, 'normal')
#             add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb, 'normal')
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin_new, 'normal')
#             click_element(user_button_lock)  # 点击【锁】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             # Todo 旧密码开门
#             assert_result_displayed(lock_assert_lock_status_opening, wait_time=5, assert_type=False)  # 不显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             # Todo 新密码开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_RFID_删除
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             delete_card_yale(UserInfo.First.name)  # 删除门卡
#             click_element(lock_main_lock_manager)  # 点击锁按钮
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().card_open())  # 门卡开门
#             assert_result_displayed(lock_main_lock_close_status, True, wait_time=2)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_C999(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             click_element(user_compound_user_admin.format(UserInfo.Second.name))  # 点击管理员
#             click_element(user_button_user_pin)  # 点击【密码】
#             click_element(user_button_user_pin_input)  # 点击密码框
#             clear_text_content(user_button_user_pin_input)  # 清除输入
#             send_keys(user_button_user_pin_input, UserInfo.Second.pin)  # 输入pin
#             click_element(user_button_user_pin_save)  # 点击保存
#             invite_user('admin', UserInfo.Third.phone, UserInfo.Third.name, True, 'always')
#             click_element(user_compound_user_admin.format(UserInfo.Third.name))  # 点击管理员
#             click_element(user_button_user_pin)  # 点击【密码】
#             click_element(user_button_user_pin_input)  # 点击密码框
#             clear_text_content(user_button_user_pin_input)  # 清除输入
#             send_keys(user_button_user_pin_input, UserInfo.Second.pin)  # 输入pin
#             assert_result_text(register_assert_alert, user_assert_pin_used)  # 提示密码已被使用
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_RFID_重复添加
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             add_card_yale(UserInfo.First.name)  # 添加门卡
#             click_element(UserManagement_user_list)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.Second.name))  # 点击管理员
#             click_element(UserManagement_user_card)  # 点击【门卡】
#             click_element(UserManagement_add_user_card)  # 点击【添加门卡】
#             click_element(UserManagement_add_user_card)  # 点击【下一步】
#             click_element(UserManagement_add_user_card)  # 点击【下一步】
#             time.sleep(10)
#             requests.put(url=API().card_register())  # 调用添加门卡接口
#             time.sleep(5)
#             assert_result_displayed(userManagement_invite_popup_cancel, True)  # 添加失败弹框
#             click_element(userManagement_invite_popup_cancel)  # 点击取消
#             self.driver.keyevent(4)  # 返回上一页
#             logout('house')  # 退出登录
#         else:
#             assert False
#
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc086(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_管理员_RFID_APP端添加超时
# #             pass
# #         else:
# #             assert False
# #
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1001(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_RFID_锁端添加超时
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(UserManagement_user_list)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.Second.name))  # 点击管理员
#             click_element(UserManagement_user_card)  # 点击【门卡】
#             click_element(UserManagement_add_user_card)  # 点击【添加门卡】
#             click_element(UserManagement_add_user_card)  # 点击【下一步】
#             click_element(UserManagement_add_user_card)  # 点击【下一步】
#             time.sleep(30)  # 等待超时
#             time.sleep(30)  # 等待超时
#             assert_result_displayed(userManagement_invite_popup_cancel, True)  # 添加失败弹框
#             click_element(userManagement_invite_popup_cancel)  # 点击取消
#             self.driver.keyevent(4)  # 返回上一页
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_DC1002(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             invite_user('normal', UserInfo.Third.phone, UserInfo.Third.name, True, 'always')
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin)
#             click_element(user_compound_user_normal.format(UserInfo.Second.name))  # 选择普通用户
#             click_element(user_button_user_pin)  # 点击【密码】
#             click_element(user_button_user_pin_release)  # 点击【解除密码】
#             click_element(user_button_lock)  # 点击【锁】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             # Todo 旧密码开门
#             assert_result_displayed(lock_assert_lock_status_opening, wait_time=5, assert_type=False)  # 不显示【门锁打开】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             click_element(user_compound_user_admin.format(UserInfo.Third.name))  # 点击普通用户
#             click_element(user_button_user_pin)  # 点击【密码】
#             click_element(user_button_user_pin_input)  # 点击密码框
#             clear_text_content(user_button_user_pin_input)  # 清除输入
#             send_keys(user_button_user_pin_input, UserInfo.Second.pin)  # 输入pin
#             assert_result_text(register_assert_alert, user_assert_pin_used)  # 提示密码已被使用
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin)
#             click_element(user_button_lock)  # 点击【锁】
#             assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
#             # Todo 旧密码开门
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_FP_添加
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             add_finger_yale(UserInfo.First.name)  # 添加指纹
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().finger_open())  # 指纹开门
#             time.sleep(2)
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_DC1003(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
#             add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb, 'normal')
#             click_element(user_button_lock)  # 点击【锁】
#             requests.put(url=API.request_api_finger_open.format(UserInfo.Second.name, UserInfo.Second.finger_thumb))  # 指纹开锁
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_FP_删除
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             delete_finger_yale(UserInfo.First.name)  # 删除指纹
#             click_element(lock_main_lock_manager)  # 点击锁按钮
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().finger_open())  # 指纹开门
#             time.sleep(2)
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1004(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_FP_重复添加
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             add_finger_yale(UserInfo.First.name)  # 添加指纹
#             click_element(UserManagement_user_list)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.Second.name))  # 点击管理员
#             click_element(UserManagement_user_fingerprint)  # 点击【指纹】
#             click_element(UserManagement_add_user_fingerprint)  # 点击【添加指纹】
#             click_element(UserManagement_add_user_fingerprint)  # 点击【下一步】
#             requests.put(url=API().finger_register())  # 调用添加指纹接口
#             click_element(UserManagement_add_user_fingerprint)  # 点击【下一步】
#             time.sleep(10)
#             assert_result_displayed(UserManagement_add_user_card, True)  # 目前可重复添加指纹
#             click_element(UserManagement_add_user_card)  # 点击【完成】
#             driver.press_keycode(4)
#             delete_finger_yale(UserInfo.Second.name)  # 删除Second的指纹
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     # @adb_log
#     # def test_user_management_tc091(self):
#     #     if app_select == 'panpan':
#     #         login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
#     #         click_element(userManagement_main_house_name)  # 进入房间
#     #         invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#     #         add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
#     #         add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb, 'normal')
#     #         click_element(user_compound_user_normal.format(UserInfo.Second.name))  # 选择普通用户
#     #         click_element(user_button_user_finger)  # 点击【指纹管理】
#     #         click_element(user_button_user_finger_and_card_remove)  # 点击【删除】
#     #         click_element(homePage_else_sure_or_try)  # 点击【确定】
#     #         click_element(homepage_button_detail_back)  # 点击【返回】
#     #         click_element(user_button_lock)  # 点击【锁】
#     #         requests.put(url=API.request_api_finger_open.format(UserInfo.Second.name, UserInfo.Second.finger_thumb))  # 指纹开锁
#     #         assert_result_displayed(lock_assert_lock_status_opening, False, wait_time=10)  # 不显示【门锁打开】
#     #         factory_lock()
#     #         logout('house')
#     #     elif app_select == 'newpanpan':
#     #         # 用户属性设定_管理员_FP_APP端添加超时
#     #         pass
#     #     else:
#     #         assert False
#
#     @adb_log
#     def test_DC1006(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
#             add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb, 'normal')
#             invite_user('normal', UserInfo.Third.phone, UserInfo.Third.name, True, 'always')
#             add_pin(UserInfo.Third.name, UserInfo.Third.pin, 'normal')
#             add_finger(UserInfo.Third.name, UserInfo.Second.name, UserInfo.Second.finger_thumb, 'normal')  # 添加重复指纹
#             # Todo 该锁没有重复验证
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_FP_锁端添加超时
#             """
#             管理员First: pin启用，门卡启用，指纹启用
#             管理员Second: pin启用，门卡无，指纹无
#             """
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             check_lock_connection()  # 等待锁连接
#             requests.put(url=API().finger_open())  # 调用指纹开门接口，注册指纹后指纹会一直生效，所以要指纹开门一次
#             click_element(UserManagement_user_list)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.Second.name))  # 点击管理员
#             click_element(UserManagement_user_fingerprint)  # 点击【指纹】
#             click_element(UserManagement_add_user_fingerprint)  # 点击【添加指纹】
#             click_element(UserManagement_add_user_fingerprint)  # 点击【下一步】
#             click_element(UserManagement_add_user_fingerprint)  # 点击【下一步】
#             time.sleep(60)  # 等待超时
#             time.sleep(60)  # 等待超时
#             assert_result_displayed(userManagement_invite_popup_cancel, True)  # 添加失败弹框
#             click_element(userManagement_invite_popup_cancel)  # 点击取消
#             self.driver.keyevent(4)  # 返回主页
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1007(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_属性页UI
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.First.name))  # 点击管理员
#             assert_result_displayed(userManagement_edit_access_set, True)  # 用户权限设置
#             assert_result_displayed(userManagement_edit_schedule_set, True)  # 使用时间设置
#             assert_result_displayed(userManagement_edit_card_set, True)  # 门卡设置
#             assert_result_displayed(userManagement_edit_finger_set, True)  # 指纹设置
#             assert_result_displayed(userManagement_edit_back_button, True)  # 返回按键
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1008(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_属性页_返回按钮
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.First.name))  # 点击管理员
#             assert_result_displayed(userManagement_edit_access_set, True)  # 用户权限设置
#             click_element(userManagement_edit_back_button)  # 点击返回
#             assert_result_displayed(userManagement_main_user_tab, True)  # 返回成功
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_DC1010(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
#             add_card(UserInfo.Second.name, 'normal')
#             click_element(user_button_lock)  # 点击【锁】
#             requests.put(url=API.request_api_card_open)  # 门卡开锁
#             assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_属性页_取消删除用户
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.Second.name))  # 点击管理员
#             click_element(UserManagement_delete_user)  # 点击删除用户
#             assert_result_displayed(userManagement_invite_popup_cancel, True)
#             click_element(userManagement_invite_popup_cancel)  # 点击取消
#             self.driver.keyevent(4)  # 返回
#             assert_result_displayed(UserManagement_user_admin.format(UserInfo.Second.name), True)  # 用户存在
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1009(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_属性页_删除用户
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             delete_user_yale(UserInfo.Second.name)  # 删除用户
#             assert_result_displayed(UserManagement_user_admin.format(UserInfo.Second.name), False)  # 用户不存在
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_DC1011(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
#             add_card(UserInfo.Second.name, 'normal')
#             click_element(user_compound_user_normal.format(UserInfo.Second.name))  # 选择普通用户
#             click_element(user_button_user_card)  # 点击【门卡管理】
#             click_element(user_button_user_finger_and_card_remove)  # 点击【删除】
#             click_element(homePage_else_sure_or_try)  # 点击【确定】
#             click_element(homepage_button_detail_back)  # 点击【返回】
#             click_element(user_button_lock)  # 点击【锁】
#             requests.put(url=API.request_api_card_open)  # 门卡开锁
#             assert_result_displayed(lock_assert_lock_status_opening, False, wait_time=10)  # 不显示【门锁打开】
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_属性页_删除自己
#             # 风险大，不做删除，只做取消删除处理
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.First.name))  # 点击管理员
#             # click_element(UserManagement_delete_user)  # 点击删除用户
#             # assert_result_displayed(userManagement_invite_popup_cancel, True)
#             # click_element(userManagement_invite_popup_cancel)  # 点击取消
#             assert_result_displayed(userManagement_invite_popup_cancel, False, wait_time=3)  # 不显示删除button
#             self.driver.keyevent(4)  # 返回
#             assert_result_displayed(UserManagement_user_admin.format(UserInfo.First.name), True)  # 用户存在
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_DC1012(self):
#         if app_select == 'panpan':
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
#             add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
#             add_card(UserInfo.Second.name, 'normal')
#             invite_user('normal', UserInfo.Third.phone, UserInfo.Third.name, True, 'always')
#             add_pin(UserInfo.Third.name, UserInfo.Third.pin, 'normal')
#             add_card(UserInfo.Third.name, 'normal')  # 添加重复门卡
#             # Todo 该锁没有重复验证
#             factory_lock()
#             logout('house')
#         elif app_select == 'newpanpan':
#             # 用户属性设定_管理员_属性页_删除最后一个管理员
#             # 风险大，不做删除，只做取消删除处理
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.First.name))  # 点击管理员
#             # click_element(UserManagement_delete_user)  # 点击删除用户
#             # assert_result_displayed(userManagement_invite_popup_cancel, True)
#             # click_element(userManagement_invite_popup_cancel)  # 点击取消
#             assert_result_displayed(userManagement_invite_popup_cancel, False, wait_time=3)  # 不显示删除button
#             self.driver.keyevent(4)  # 返回
#             assert_result_displayed(UserManagement_user_admin.format(UserInfo.First.name), True)  # 用户存在
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1013(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_永久_添加
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user_yale('normal', UserInfo.NormalPhilip.phone, UserInfo.NormalPhilip.name, 'always')  # 邀请普通用户
#             password_register(UserInfo.NormalPhilip.name, UserInfo.NormalPhilip.pin)  # 设置密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.NormalPhilip.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1014(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_永久_禁用
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             disable_pin_yale(UserInfo.NormalPhilip.name)  # 禁用密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.NormalPhilip.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_close_status, True, wait_time=2)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1015(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_永久_激活
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             enable_pin_yale(UserInfo.NormalPhilip.name)  # 激活密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.NormalPhilip.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1016(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_永久_修改
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.NormalPhilip.name, UserInfo.NormalPhilip.pin_new)  # 修改密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.NormalPhilip.pin_new))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1017(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_永久_添加为_已有已启用密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.NormalPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1018(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_永久_添加为_已有未启用密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.NormalPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1019(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_永久_激活为_已有密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.NormalPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1020(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_永久_修改为_已有密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.NormalPhilip.name, UserInfo.NormalPhilip.pin)  # 设置密码
#             password_register(UserInfo.NormalPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1021(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_重复_添加
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             delete_user_yale(UserInfo.NormalPhilip.name)  # 删除之前的普通用户
#             invite_user_yale('normal', UserInfo.NormalPhilip.phone, UserInfo.NormalPhilip.name, 'recur')  # 邀请普通用户
#             password_register(UserInfo.NormalPhilip.name, UserInfo.NormalPhilip.pin)  # 设置密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.NormalPhilip.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1022(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_重复_禁用
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             disable_pin_yale(UserInfo.NormalPhilip.name)  # 禁用密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.NormalPhilip.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1023(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_重复_激活
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             enable_pin_yale(UserInfo.NormalPhilip.name)  # 激活密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.NormalPhilip.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1024(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_重复_修改
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.NormalPhilip.name, UserInfo.NormalPhilip.pin_new)  # 修改密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.NormalPhilip.pin_new))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1025(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_重复_添加为_已有密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.NormalPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1026(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_重复_激活为_已有密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.NormalPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1027(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_重复_修改为_已有密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.NormalPhilip.name, UserInfo.NormalPhilip.pin)  # 设置密码
#             password_register(UserInfo.NormalPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1028(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_临时_添加
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             delete_user_yale(UserInfo.NormalPhilip.name)  # 删除之前的普通用户
#             invite_user_yale('normal', UserInfo.NormalPhilip.phone, UserInfo.NormalPhilip.name, 'temp')  # 邀请普通用户
#             password_register(UserInfo.NormalPhilip.name, UserInfo.NormalPhilip.pin)  # 设置密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.NormalPhilip.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1029(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_临时_禁用
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             disable_pin_yale(UserInfo.NormalPhilip.name)  # 禁用密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.NormalPhilip.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1030(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_临时_激活
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             enable_pin_yale(UserInfo.NormalPhilip.name)  # 激活密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.NormalPhilip.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1031(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_临时_修改
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.NormalPhilip.name, UserInfo.NormalPhilip.pin_new)  # 修改密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.NormalPhilip.pin_new))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1032(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_临时_添加为_已有密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.NormalPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1033(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_临时_激活为_已有密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.NormalPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1034(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_PINCode_临时_修改为_已有密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.NormalPhilip.name, UserInfo.NormalPhilip.pin)  # 设置密码
#             password_register(UserInfo.NormalPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1035(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_RFID_添加
#             # 初始条件：管理员First密码pin启用，门卡启用，指纹启用；普通用户Normal密码未启用，门卡无，指纹无
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             delete_card_yale(UserInfo.First.name)  # 删除门卡
#             add_card_yale(UserInfo.NormalPhilip.name)  # 添加门卡
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().card_open())  # 门卡开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1036(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_RFID_删除
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             delete_card_yale(UserInfo.NormalPhilip.name)  # 删除门卡
#             click_element(lock_main_lock_manager)  # 点击锁按钮
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().card_open())  # 门卡开门
#             assert_result_displayed(lock_main_lock_close_status, True, wait_time=2)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1037(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_RFID_重复添加
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             add_card_yale(UserInfo.First.name)  # 添加门卡
#             click_element(UserManagement_user_list)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.NormalPhilip.name))  # 点击普通用户
#             click_element(UserManagement_user_card)  # 点击【门卡】
#             click_element(UserManagement_add_user_card)  # 点击【添加门卡】
#             click_element(UserManagement_add_user_card)  # 点击【下一步】
#             click_element(UserManagement_add_user_card)  # 点击【下一步】
#             time.sleep(10)
#             requests.put(url=API().card_register())  # 调用添加门卡接口
#             time.sleep(5)
#             assert_result_displayed(userManagement_invite_popup_cancel, True)  # 添加失败弹框
#             click_element(userManagement_invite_popup_cancel)  # 点击取消
#             self.driver.keyevent(4)  # 返回上一页
#             logout('house')  # 退出登录
#         else:
#             assert False
#
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc124(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_普通用户_RFID_APP端添加超时
# #             pass
# #         else:
# #             assert False
# #
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1039(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_RFID_锁端添加超时
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(UserManagement_user_list)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.NormalPhilip.name))  # 点击普通用户
#             click_element(UserManagement_user_card)  # 点击【门卡】
#             click_element(UserManagement_add_user_card)  # 点击【添加门卡】
#             click_element(UserManagement_add_user_card)  # 点击【下一步】
#             click_element(UserManagement_add_user_card)  # 点击【下一步】
#             time.sleep(30)  # 等待超时
#             assert_result_displayed(userManagement_invite_popup_cancel, True)  # 添加失败弹框
#             click_element(userManagement_invite_popup_cancel)  # 点击取消
#             self.driver.keyevent(4)  # 返回上一页
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1040(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_FP_添加
#             # 初始条件：管理员First密码pin启用，门卡启用，指纹启用；普通用户Normal密码未启用，门卡无，指纹无
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             add_finger_yale(UserInfo.NormalPhilip.name, '2', '2')  # 添加指纹
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().finger_open('2', '2'))  # 指纹开门
#             time.sleep(2)
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示门开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1041(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_FP_删除
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             delete_finger_yale(UserInfo.NormalPhilip.name)  # 删除指纹
#             click_element(lock_main_lock_manager)  # 点击锁按钮
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().finger_open('2', '2'))  # 指纹开门
#             time.sleep(2)
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1042(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_FP_重复添加
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(UserManagement_user_list)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.NormalPhilip.name))  # 点击普通用户
#             click_element(UserManagement_user_fingerprint)  # 点击【指纹】
#             click_element(UserManagement_add_user_fingerprint)  # 点击【添加指纹】
#             click_element(UserManagement_add_user_fingerprint)  # 点击【下一步】
#             requests.put(url=API().finger_register())  # 调用添加指纹接口
#             click_element(UserManagement_add_user_fingerprint)  # 点击【下一步】
#             time.sleep(10)
#             assert_result_displayed(UserManagement_add_user_card, True)  # 目前可重复添加指纹
#             click_element(UserManagement_add_user_card)  # 点击【完成】
#             driver.press_keycode(4)
#             delete_finger_yale(UserInfo.NormalPhilip.name)  # 删除Second的指纹
#             logout('house')  # 退出登录
#         else:
#             assert False
#
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc129(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_普通用户_FP_APP端添加超时
# #             pass
# #         else:
# #             assert False
# #
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1044(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_FP_锁端添加超时
#             # 初始条件：管理员First密码pin启用，门卡启用，指纹启用；普通用户Normal密码未启用，门卡无，指纹无
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             check_lock_connection()  # 等待锁连接
#             requests.put(url=API().finger_open())  # 调用指纹开门接口，注册指纹后指纹会一直生效，所以要指纹开门一次
#             click_element(UserManagement_user_list)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.NormalPhilip.name))  # 点击普通用户
#             click_element(UserManagement_user_fingerprint)  # 点击【指纹】
#             click_element(UserManagement_add_user_fingerprint)  # 点击【添加指纹】
#             click_element(UserManagement_add_user_fingerprint)  # 点击【下一步】
#             click_element(UserManagement_add_user_fingerprint)  # 点击【下一步】
#             time.sleep(60)  # 等待超时
#             time.sleep(60)  # 等待超时
#             assert_result_displayed(userManagement_invite_popup_cancel, True)  # 添加失败弹框
#             click_element(userManagement_invite_popup_cancel)  # 点击取消
#             self.driver.keyevent(4)  # 返回主页
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1045(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_属性页UI
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.NormalPhilip.name))  # 点击普通用户
#             assert_result_displayed(userManagement_edit_access_set, True)  # 用户权限设置
#             assert_result_displayed(userManagement_edit_schedule_set, True)  # 使用时间设置
#             assert_result_displayed(userManagement_edit_card_set, True)  # 门卡设置
#             assert_result_displayed(userManagement_edit_finger_set, True)  # 指纹设置
#             assert_result_displayed(userManagement_edit_back_button, True)  # 返回按键
#             self.driver.keyevent(4)  # 返回
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1046(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_属性页_返回按钮
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.NormalPhilip.name))  # 点击普通用户
#             assert_result_displayed(userManagement_edit_access_set, True)  # 用户权限设置
#             click_element(userManagement_edit_back_button)  # 点击返回
#             assert_result_displayed(userManagement_main_user_tab, True)  # 返回成功
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1048(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_属性页_取消删除用户
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             click_element(userManagement_main_user_tab)  # 点击【用户列表】
#             click_element(UserManagement_user_admin.format(UserInfo.NormalPhilip.name))  # 点击普通用户
#             click_element(UserManagement_delete_user)  # 点击删除用户
#             assert_result_displayed(userManagement_invite_popup_cancel, True)
#             click_element(userManagement_invite_popup_cancel)  # 点击取消
#             self.driver.keyevent(4)  # 返回
#             assert_result_displayed(UserManagement_user_admin.format(UserInfo.NormalPhilip.name), True)  # 用户存在
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1047(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_普通用户_属性页_删除用户
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             delete_user_yale(UserInfo.NormalPhilip.name)  # 删除用户
#             assert_result_displayed(UserManagement_user_admin.format(UserInfo.NormalPhilip.name), False, wait_time=3)  # 用户不存在
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1049(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_访客_PINCode_永久_禁用
#             # 初始条件：管理员First密码pin启用，门卡启用，指纹启用
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             invite_user_yale('guest', UserInfo.GuestPhilip.phone, UserInfo.GuestPhilip.first_name, 'always',
#                              UserInfo.GuestPhilip.pin)
#             disable_pin_yale(UserInfo.GuestPhilip.name)  # 禁用密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.GuestPhilip.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1050(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_访客_PINCode_永久_激活
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             enable_pin_yale(UserInfo.GuestPhilip.name)  # 激活密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.GuestPhilip.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1051(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_访客_PINCode_永久_修改
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.GuestPhilip.name, UserInfo.GuestPhilip.pin_new)  # 修改密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.GuestPhilip.pin_new))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1052(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_访客_PINCode_永久_激活为_已有已启用密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             disable_pin_yale(UserInfo.GuestPhilip.name)  # 禁用访客密码
#             password_register(UserInfo.GuestPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1053(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_访客_PINCode_永久_激活为_已有未启用密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             disable_pin_yale(UserInfo.First.name)  # 禁用管理员密码
#             password_register(UserInfo.GuestPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1055(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_访客_PINCode_永久_修改为_已有未启用密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.GuestPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1054(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_访客_PINCode_永久_修改为_已有已启用密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             enable_pin_yale(UserInfo.First.name)  # 激活管理员密码
#             password_register(UserInfo.GuestPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1056(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_访客_PINCode_重复_禁用
#             # 初始条件：管理员First密码pin启用，门卡启用，指纹启用
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             delete_user_yale(UserInfo.GuestPhilip.name)  # 删除永久访客
#             invite_user_yale('guest', UserInfo.GuestPhilip.phone, UserInfo.GuestPhilip.first_name, 'recur',
#                              UserInfo.GuestPhilip.pin)
#             disable_pin_yale(UserInfo.GuestPhilip.name)  # 禁用密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.GuestPhilip.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1057(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_访客_PINCode_重复_激活
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             enable_pin_yale(UserInfo.GuestPhilip.name)  # 激活密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.GuestPhilip.pin))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1058(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_访客_PINCode_重复_修改
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.GuestPhilip.name, UserInfo.GuestPhilip.pin_new)  # 修改密码
#             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
#             check_lock_connection()  # 等待锁连接
#             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             requests.put(url=API().pin_open(UserInfo.GuestPhilip.pin_new))  # 密码开门
#             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
#             if autoLock is True:
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             else:
#                 click_element(lock_main_lottie)  # 连接成功，点击开锁
#                 assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
#             logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1059(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_访客_PINCode_重复_激活为_已有已启用密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             disable_pin_yale(UserInfo.GuestPhilip.name)  # 禁用访客密码
#             password_register(UserInfo.GuestPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1060(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_访客_PINCode_重复_激活为_已有未启用密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             disable_pin_yale(UserInfo.First.name)  # 禁用管理员密码
#             password_register(UserInfo.GuestPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1062(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_访客_PINCode_重复_修改为_已有未启用密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             password_register(UserInfo.GuestPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     @adb_log
#     def test_DC1061(self):
#         if app_select == 'panpan':
#             pass
#         elif app_select == 'newpanpan':
#             # 用户属性设定_访客_PINCode_重复_修改为_已有已启用密码
#             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#             click_element(userManagement_main_house_name)  # 进入房间
#             enable_pin_yale(UserInfo.First.name)  # 激活管理员密码
#             password_register(UserInfo.GuestPhilip.name, UserInfo.First.pin)  # 修改已存在密码
#             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
#             click_element(userManagement_invite_popup_confirm)  # 点击好的
#             # logout('house')  # 退出登录
#         else:
#             assert False
#
#     @adb_log
#     def test_D000(self):
#         # 恢复出厂设置
#         login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
#         click_element(userManagement_main_house_name)  # 进入房间
#         factory_lock_yale()
#         logout()
#
#     def cases(self):
#         return list(filter(lambda m: m.startswith("test_C") or m.startswith("test_DC") and callable(getattr(self, m)), dir(self)))
#
#     # @adb_log
#     # def test_C001(self):
#     #     # 恢复出厂设置
#     #     login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
#     #     click_element(homePage_homePage_home_list)  # 点击住宅图片
#     #     factory_lock_yale()
#     #     logout()
#
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc149(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_访客_PINCode_临时_禁用
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             invite_user_yale('guest', UserInfo.GuestPhilip.phone, UserInfo.GuestPhilip.first_name, 'temp',
# #                              UserInfo.GuestPhilip.pin)  # 此处应该邀请一个新的访客
# #             disable_pin_yale(UserInfo.GuestPhilip.name)  # 禁用密码
# #             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
# #             check_lock_connection()  # 等待锁连接
# #             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
# #             requests.put(url=API().pin_open(UserInfo.GuestPhilip.pin))  # 密码开门
# #             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc150(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_访客_PINCode_临时_激活
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             enable_pin_yale(UserInfo.GuestPhilip.name)  # 激活密码
# #             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
# #             check_lock_connection()  # 等待锁连接
# #             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
# #             requests.put(url=API().pin_open(UserInfo.GuestPhilip.pin))  # 密码开门
# #             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc151(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_访客_PINCode_临时_修改
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             password_register(UserInfo.GuestPhilip.name, UserInfo.GuestPhilip.pin_new)  # 修改密码
# #             click_element(userManagement_main_home_tab, after_action_sleep=5)  # 点击房间主页，等待锁连接
# #             check_lock_connection()  # 等待锁连接
# #             assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
# #             requests.put(url=API().pin_open(UserInfo.GuestPhilip.pin))  # 密码开门
# #             assert_result_displayed(lock_main_lock_open_status, True)  # 显示锁开启
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc152(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_访客_PINCode_临时_激活为_已有已启用密码
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             disable_pin_yale(UserInfo.GuestPhilip.name)  # 禁用访客密码
# #             password_register(UserInfo.GuestPhilip.name, UserInfo.First.pin)  # 修改已存在密码
# #             time.sleep(5)
# #             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
# #             click_element(userManagement_invite_popup_confirm)  # 点击好的
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc153(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_访客_PINCode_临时_激活为_已有未启用密码
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             disable_pin_yale(UserInfo.First.name)  # 禁用管理员密码
# #             password_register(UserInfo.GuestPhilip.name, UserInfo.First.pin)  # 修改已存在密码
# #             time.sleep(5)
# #             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
# #             click_element(userManagement_invite_popup_confirm)  # 点击好的
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc154(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_访客_PINCode_临时_修改为_已有未启用密码
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             password_register(UserInfo.GuestPhilip.name, UserInfo.First.pin)  # 修改已存在密码
# #             time.sleep(5)
# #             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
# #             click_element(userManagement_invite_popup_confirm)  # 点击好的
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc155(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_访客_PINCode_临时_修改为_已有已启用密码
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             enable_pin_yale(UserInfo.First.name)  # 激活管理员密码
# #             password_register(UserInfo.GuestPhilip.name, UserInfo.First.pin)  # 修改已存在密码
# #             time.sleep(5)
# #             assert_result_displayed(userManagement_invite_popup_confirm, True)  # 提示密码已存在
# #             click_element(userManagement_invite_popup_confirm)  # 点击好的
# #             delete_card_yale(UserInfo.GuestPhilip.name)  # 删除用户
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc156(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_访客_属性页UI
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             click_element(userManagement_main_user_tab)  # 点击【用户列表】
# #             click_element(UserManagement_user_admin.format(UserInfo.GuestPhilip.name))  # 点击访客
# #             assert_result_displayed(userManagement_edit_schedule_set, True)  # 使用时间设置
# #             assert_result_displayed(userManagement_edit_pin_set, True)  # 密码设置
# #             assert_result_displayed(userManagement_edit_back_button, True)  # 返回按键
# #             self.driver.keyevent(4)  # 返回
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc157(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_访客_属性页_姓名修改
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             click_element(userManagement_main_user_tab)  # 点击【用户列表】
# #             click_element(UserManagement_user_admin.format(UserInfo.GuestPhilip.name))  # 点击访客
# #             click_element(userManagement_edit_guest_name)  # 点击姓名
# #             send_keys(userManagement_edit_guest_first_name, UserInfo.GuestPhilip.new_name)  # 输入名
# #             send_keys(userManagement_edit_guest_last_name, UserInfo.GuestPhilip.new_name)  # 输入姓
# #             click_element(userManagement_invite_popup_confirm)  # 确定
# #             self.driver.keyevent(4)  # 返回
# #             assert_result_displayed(UserManagement_user_admin.format
# #                                     (UserInfo.GuestPhilip.new_name + ' ' + UserInfo.GuestPhilip.new_name), True)
# #             click_element(UserManagement_user_admin.format
# #                           (UserInfo.GuestPhilip.new_name + ' ' + UserInfo.GuestPhilip.new_name))  # 点击访客
# #             click_element(userManagement_edit_guest_name)  # 点击姓名
# #             send_keys(userManagement_edit_guest_first_name, UserInfo.GuestPhilip.first_name)  # 输入名
# #             send_keys(userManagement_edit_guest_last_name, UserInfo.GuestPhilip.first_name)  # 输入姓
# #             click_element(userManagement_invite_popup_confirm)  # 确定
# #             self.driver.keyevent(4)  # 返回
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc158(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_访客_属性页_取消姓名修改
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             click_element(userManagement_main_user_tab)  # 点击【用户列表】
# #             click_element(UserManagement_user_admin.format(UserInfo.GuestPhilip.name))  # 点击访客
# #             click_element(userManagement_edit_guest_name)  # 点击编辑访客姓名
# #             click_element(userManagement_invite_popup_cancel)  # 取消
# #             self.driver.keyevent(4)  # 返回
# #             assert_result_displayed(UserManagement_user_admin.format(UserInfo.GuestPhilip.name), True)  # 未修改
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc159(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_访客_属性页_姓名为空
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             click_element(userManagement_main_user_tab)  # 点击【用户列表】
# #             click_element(UserManagement_user_admin.format(UserInfo.GuestPhilip.name))  # 点击访客
# #             click_element(userManagement_edit_guest_name)  # 点击编辑访客姓名
# #             click_element(userManagement_invite_popup_confirm)  # 点击确定
# #             self.driver.keyevent(4)  # 返回
# #             assert_result_displayed(UserManagement_user_admin.format(UserInfo.GuestPhilip.name), True)  # 未修改
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc160(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_访客_属性页_返回按钮
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             click_element(userManagement_main_user_tab)  # 点击【用户列表】
# #             click_element(UserManagement_user_admin.format(UserInfo.GuestPhilip.name))  # 点击访客
# #             click_element(userManagement_edit_back_button)  # 点击返回
# #             assert_result_displayed(userManagement_main_user_tab, True)  # 显示用户列表
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc161(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_访客_属性页_取消删除用户
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             click_element(userManagement_main_user_tab)  # 点击【用户列表】
# #             click_element(UserManagement_user_admin.format(UserInfo.GuestPhilip.name))  # 点击访客
# #             click_element(userManagement_edit_delete_button)  # 点击删除
# #             click_element(userManagement_invite_popup_cancel)  # 点击取消
# #             self.driver.keyevent(4)  # 返回
# #             assert_result_displayed(UserManagement_user_admin.format(UserInfo.GuestPhilip.name), True)  # 访客未删除
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc162(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_访客_属性页_删除用户
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             click_element(userManagement_main_user_tab)  # 点击【用户列表】
# #             click_element(UserManagement_user_admin.format(UserInfo.GuestPhilip.name))  # 点击访客
# #             click_element(userManagement_edit_delete_button)  # 点击删除
# #             click_element(userManagement_invite_popup_confirm)  # 点击确定
# #             assert_result_displayed(UserManagement_user_admin.format(UserInfo.GuestPhilip.name),
# #                                     False, wait_time=2)  # 访客已删除
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc163(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_邀请的人_属性页UI
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             click_element(userManagement_main_user_tab)  # 点击【用户列表】
# #             invite_user_yale('admin', UserInfo.InvitedPhilip.phone, UserInfo.InvitedPhilip.first_name)  # 邀请的人
# #             click_element(UserManagement_user_admin.format(UserInfo.InvitedPhilip.name))  # 点击邀请的人
# #             assert_result_displayed(userManagement_edit_access_set, True)  # 权限设置
# #             assert_result_displayed(userManagement_edit_schedule_set, True)  # 时间设置
# #             assert_result_displayed(userManagement_edit_back_button, True)  # 返回按键
# #             self.driver.keyevent(4)  # 返回
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc164(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_邀请的人_属性页_返回按钮
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             click_element(userManagement_main_user_tab)  # 点击【用户列表】
# #             click_element(UserManagement_user_admin.format(UserInfo.InvitedPhilip.name))  # 点击邀请的人
# #             click_element(userManagement_edit_back_button)  # 点击返回
# #             assert_result_displayed(userManagement_main_user_tab, True)  # 显示用户列表
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc165(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_邀请的人_取消删除用户
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             click_element(userManagement_main_user_tab)  # 点击【用户列表】
# #             click_element(UserManagement_user_admin.format(UserInfo.InvitedPhilip.name))  # 点击邀请的人
# #             click_element(userManagement_edit_delete_button)  # 点击删除
# #             click_element(userManagement_invite_popup_cancel)  # 点击取消
# #             self.driver.keyevent(4)  # 返回
# #             assert_result_displayed(UserManagement_user_admin.format(UserInfo.InvitedPhilip.name), True)  # 未删除
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
# #     @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
# #     @adb_log
# #     def test_user_management_tc166(self):
# #         if app_select == 'panpan':
# #             pass
# #         elif app_select == 'newpanpan':
# #             # 用户属性设定_邀请的人_删除用户
# #             login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')  # 登录
# #             click_element(userManagement_main_house_name)  # 进入房间
# #             click_element(userManagement_main_user_tab)  # 点击【用户列表】
# #             click_element(UserManagement_user_admin.format(UserInfo.InvitedPhilip.name))  # 点击邀请的人
# #             click_element(userManagement_edit_delete_button)  # 点击删除
# #             click_element(userManagement_invite_popup_confirm)  # 点击确定
# #             assert_result_displayed(UserManagement_user_admin.format(UserInfo.InvitedPhilip.name),
# #                                     False, wait_time=2)  # 已删除
# #             logout('house')  # 退出登录
# #         else:
# #             assert False
# #
#     # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     # @adb_log
#     # def test_user_management_tc167(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 用户属性设定_管理员_门卡图标显示
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     # @adb_log
#     # def test_user_management_tc168(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 用户属性设定_管理员_指纹图标显示
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     # @adb_log
#     # def test_user_management_tc169(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 用户属性设定_管理员_密码图标显示
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     # @adb_log
#     # def test_user_management_tc170(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 用户属性设定_普通用户_门卡图标显示
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     # @adb_log
#     # def test_user_management_tc171(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 用户属性设定_普通用户_指纹图标显示
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     # @adb_log
#     # def test_user_management_tc172(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 用户属性设定_普通用户_密码图标显示
#     #         pass
#     #     else:
#     #         assert False
#     #
#     # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
#     # @adb_log
#     # def test_user_management_tc173(self):
#     #     if app_select == 'panpan':
#     #         pass
#     #     elif app_select == 'newpanpan':
#     #         # 用户属性设定_访客_密码图标显示
#     #         pass
#     #     else:
#     #         assert False