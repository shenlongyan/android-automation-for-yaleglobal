# from Configuration.Parameter import GetLockInfo
import os

app_select = 'yale'

lock_info = {'M20006A': {'lock_id': 'A187240BC7311043BDDEB680A0B19DA7', 'lock_model': 'FC5-A'},
             'M210000084': {'lock_id': 'D413B5824CCBF242957E3FD33F44B231', 'lock_model': ''},
             'M21000006K': {'lock_id': 'CD2FAD7977FCB045B717890C8A83F48D', 'lock_model': ''},
             'L81000010C': {'lock_id': 'D7D3C43889991E4EB61CB1B539736AAD', 'lock_model': ''},
             'M2I000006A': {'lock_id': 'A187240BC7311043BDDEB680A0B19DA7', 'lock_model': 'YALE YSJ741', 'module_version': '1.0.3', 'lock_firmware_version': ''},
             'M2100005Y8': {'lock_id': '479F502F50F3C8469E4A69408BE782D1', 'lock_model': 'YDSK170', 'module_version': '', 'lock_firmware_version': ''},
             'M2I00005X9': {'lock_id': '7D10762C6F7FA3409B1C2E34EEACA89F', 'lock_model': 'YALE YSJ731', 'module_version': '1.0.5', 'lock_firmware_version': '1.6.0'},
             'M2I0000081': {'lock_id': '26BB9ADC0D79964284756F90F323B0B7', 'lock_model': 'DOORMAX CLC170', 'module_version': '1.0.5', 'lock_firmware_version': ''}
             }


class GetLockInfo:

    def __init__(self, lock_collection, lock_serial):
        self._lock_collection = lock_collection
        self._lock_serial = lock_serial

    def lock_name(self):
        lock_name = self._lock_serial + 'n'
        return lock_name[-12:]

    def lock_name_new(self):
        lock_name_new = self._lock_serial + 'nn'
        return lock_name_new[-12:]

    def lock_id(self):
        lock_id = self._lock_collection[self._lock_serial]['lock_id']
        return lock_id

    def lock_model(self):
        lock_model = self._lock_collection[self._lock_serial]['lock_model']
        return lock_model

    def module_version(self):
        module_version = self._lock_collection[self._lock_serial]['module_version']
        return module_version

    def lock_firmware_version(self):
        lock_firmware_version = self._lock_collection[self._lock_serial]['lock_firmware_version']
        return lock_firmware_version

    def module_serial(self):
        module_serial = self._lock_serial
        return module_serial

# test_jig_lock_id = '1'  # test_jig的端口号
# test_jig_ip = 'http://192.168.8.10'  # test_jig的ip地址
# lock_result = GetLockInfo(lock_info, 'M2I0000081')
# selected_lock_name = lock_result.lock_name()
# selected_lock_name_new = lock_result.lock_name_new()
# selected_lock_id = lock_result.lock_id()
# selected_lock_model = lock_result.lock_model()
# selected_module_version = lock_result.module_version()
# selected_lock_firmware_version = lock_result.lock_firmware_version()
# selected_module_serial = lock_result.module_serial()

RunOnWhere = 'Server'
if RunOnWhere == 'Server':
    # 第二处修改 # run on server
    test_jig_lock_id = os.environ['test_jig_lock_id']  # test_jig的端口号
    test_jig_ip = os.environ['test_jig_ip']  # test_jig的ip地址
    selected_lock_id = os.environ['LockID']
    selected_serial_number = os.environ['SerialNumber']
    print(selected_lock_id, test_jig_ip, test_jig_lock_id, selected_serial_number)
    # pass
elif RunOnWhere == 'local':
    # run on local
    test_jig_lock_id = '3'
    test_jig_ip = 'http://192.168.8.10'
    selected_lock_id = 'A187240BC7311043BDDEB680A0B19DA7'         #'05BCFECFCB0AE542924C624852582D28'
    selected_serial_number = 'M2I000006A'                               #'M2I00000Q5'
else:
    raise ReferenceError


class API:
    if app_select == 'panpan':
        __http = 'http://10.245.110.127:5000'
        request_api_card_register = '{}/{}/register/rfcard'.format(__http, selected_lock_id)
        request_api_card_open = '{}/{}/unlock/rfcard'.format(__http, selected_lock_id)
        request_api_finger_open = '{}/{}/{}/unlock/fingerprint/{}'.format(__http, selected_lock_id, {}, {})
        request_api_finger_register = '{}/{}/{}/enroll/fingerprint/{}'.format(__http, selected_lock_id, {}, {})
    elif app_select == 'yale':
        def card_open(self):
            __http = test_jig_ip
            lock_id = test_jig_lock_id  # 有对应的端口
            request_api_card_open = '{}/{}/unlock/rfcard'.format(__http, lock_id)
            return request_api_card_open

        def card_register(self):
            __http = test_jig_ip
            lock_id = test_jig_lock_id  # 有对应的端口
            request_api_card_register = '{}/{}/register/rfcard'.format(__http, lock_id)
            return request_api_card_register

        def pin_open(self, pin):
            __http = test_jig_ip
            lock_id = test_jig_lock_id  # 有对应的端口
            full_pin = '*' + str(pin) + '*'  # 前后加*号
            request_api_pin_open = '{}/{}/keypadOperator/{}'.format(__http, lock_id, full_pin)
            return request_api_pin_open

        def finger_open(self, user_id='1', finger_id='1'):
            __http = test_jig_ip
            lock_id = test_jig_lock_id  # 有对应的端口
            request_api_finger_open = '{}/{}/{}/unlock/fingerprint/{}'.format(__http, lock_id, user_id, finger_id)
            return request_api_finger_open

        def finger_register(self, user_id='1', finger_id='1'):
            __http = test_jig_ip
            lock_id = test_jig_lock_id  # 有对应的端口
            request_api_finger_register = '{}/{}/{}/enroll/fingerprint/{}'.format(__http, lock_id, user_id, finger_id)
            return request_api_finger_register

        def password_open(self, user_phone):
            __http = test_jig_ip
            lock_id = test_jig_lock_id  # 有对应的端口
            password = user_phone[-6:]  # 密码取手机号后六位
            full_password = '%2A' + str(password) + '%2A'
            request_api_password_open = '{}/{}/keypadOperator/{}'.format(__http, lock_id, full_password)
            return request_api_password_open

    else:
        raise ReferenceError
