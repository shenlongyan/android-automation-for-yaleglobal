import cv2
import datetime
import requests
import time
from appium.webdriver.common.mobileby import By
from Configuration import FolderList
from Configuration.IDSelectApp import *
from Configuration.IDSelectLock import *
from Configuration.Parameter import *
from Configuration.StartUp import driver
from functools import wraps
from PIL import Image
from PIL import ImageChops
from Redis.VerificationCode import verification_code_phone, verification_code_email
import os
import random
from unittest import TestResult, TestCase


RunOnWhere = 'Server'
if RunOnWhere == 'Server':
    # 第五处修改 # run on server
    jobPath = os.environ['WORKSPACE']
elif RunOnWhere == 'local':
    # run on local
    jobPath = 'C:\\Users\\aa\\androidregression0228'
else:
    raise ReferenceError


def adb_log(func):
    @wraps(func)
    def wrap_the_func(*args, **kwargs):
        global log
        output_path = FolderList.output_path
        log_name = func.__name__
        log_result = output_path + log_name + '.txt'
        output_file = open(log_result, 'wt')
        driver.close_app()
        driver.launch_app()
        log = subprocess.Popen('adb logcat -v time', stdout=output_file)
        # 防止错误退出时，再次启动时无法继续运行
        start_time = time.time()
        while 1:
            if time.time() - start_time >= 30:
                log.terminate()
                assert False
            else:
                if app_select == 'panpan':
                    break
                elif app_select == 'yale':
                    try:  # 当前为登录页
                        __check_element(register_main_login, wait_time=1)
                    except:
                        pass
                    else:
                        login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
                        break
                    try:  # 当前登录账号有绑锁
                        __check_element(homePage_homePage_house_list, wait_time=1)
                    except:
                        pass
                    else:
                        click_element(homePage_homePage_home_list)  # 点击住宅图片
                        break
                    try:  # 当前登录账号无绑锁
                        __check_element(homePage_homePage_no_rooms, wait_time=1)
                    except:
                        pass
                    else:
                        # logout()
                        break
                    try:  # 当前页面为网络自动连接弹窗
                        __check_element(lock_main_network_popup, wait_time=1)
                    except:
                        pass
                    else:
                        click_element('android:id/button2')
                        os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
                        click_element("//*[@text='Yale Access']")
                        logout()
                        break
                    try:  # 当前为通知面板
                        __check_element(notification_main_notification_panel, wait_time=1)
                    except:
                        pass
                    else:
                        os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
                        click_element("//*[@text='Yale Access']")
                        click_element(homePage_homePage_home_list)  # 点击住宅图片
                        # logout()
                        break
                else:
                    assert False
        func(*args, **kwargs)
        log.terminate()

    return wrap_the_func


def depend_on(CaseName):
    def decorator(func):
        def wrap_the_func(self):
            # pendingList = []
            pendingCase = func.__name__
            print(pendingCase + ' depend on ' + CaseName)
            # pendingList.append(pendingCase)
        return wrap_the_func
    return decorator


def get_today():
    """邀请用户时的有效日期"""
    now = datetime.datetime.now()
    today = now.strftime('%d')
    return today


def select_not_today():
    click_element(userManagement_invite_recur_sunday)
    click_element(userManagement_invite_recur_monday)
    click_element(userManagement_invite_recur_tuesday)
    click_element(userManagement_invite_recur_wednesday)
    click_element(userManagement_invite_recur_thursday)
    click_element(userManagement_invite_recur_friday)
    click_element(userManagement_invite_recur_saturday)
    # click_element(userManagement_invite_recur_start_time)
    # click_element(userManagement_invite_schedule_am)  # 上午
    # click_element(userManagement_invite_schedule_0)  # 12点
    # click_element(notification_main_confirm_time)
    # click_element(userManagement_invite_recur_end_time)
    # click_element(userManagement_invite_schedule_pm)  # 下午
    # click_element(userManagement_invite_schedule_23)  # 23点
    # click_element(notification_main_confirm_time)
    click_element(userManagement_edit_manage_access_button)  # 保存
    click_element(userManagement_edit_manage_access_button)  # 保存




def get_time(need_minute):
    """邀请用户时的有效时间"""
    now = datetime.datetime.now()
    target_time = now + datetime.timedelta(minutes=need_minute)
    hour = target_time.strftime('%H')
    minute = target_time.strftime('%M')
    return hour, minute, need_minute


def get_weekday(select_type=True):
    """新增智能提醒时的有效日期
    select_type=True:当前日期未选中，则选择该日期，否则不动作
    select_type=False:当前日期已选中，则取消该日期，否则不动作"""
    today = datetime.datetime.now().weekday()
    weekday_list = {0: time_button_alert_monday,
                    1: time_button_alert_tuesday,
                    2: time_button_alert_wednesday,
                    3: time_button_alert_thursday,
                    4: time_button_alert_friday,
                    5: time_button_alert_saturday,
                    6: time_button_alert_sunday}
    today_selected = weekday_list[today]
    check_type = __check_element(today_selected, wait_time=30)
    if check_type == 'id':
        check_result = driver.find_element_by_id(today_selected).get_attribute('selected')
    elif check_type == 'xpath':
        check_result = driver.find_element_by_xpath(today_selected).get_attribute('selected')
    else:
        log.terminate()
        assert False
    if select_type is True:
        if check_result == 'true':
            pass
        else:
            click_element(today_selected)  # 点击今天的日期
    elif select_type is False:
        if check_result == 'true':
            click_element(today_selected)  # 点击今天的日期
        else:
            pass
    else:
        raise Exception('select_type error:{}'.format(select_type))


# TODO 此处需要加判断，若文件不存在则先本地生成一个新文件
def phone_number_get_not_registered():
    file_name = jobPath + '\\Configuration\\PhoneList.txt'
    with open(file_name, 'r') as f:
        lines = f.readlines()
        last_line = lines[-1].strip('\n')
    user_phone = last_line
    user_name = last_line
    user_email = str(last_line) + '@qq.com'
    # user_password = 'Abc!1234'
    user_password = 'Abcd1234'
    return user_phone, user_name, user_email, user_password


def phone_number_get_registered():
    file_name = jobPath + '\\Configuration\\PhoneList.txt'
    with open(file_name, 'r') as f:
        lines = f.readlines()
        last_line = lines[-1].strip('\n')
    user_phone = str(int(last_line) - 1)
    user_name = str(int(last_line) - 1)
    user_email = str(int(last_line) - 1) + '@qq.com'
    # user_password = 'Abcd1234_!#$*+-./:=?@[]^`|'
    user_password = 'Abcd1234'
    return user_phone, user_name, user_email, user_password


def phone_number_create():
    file_name = jobPath + '\\Configuration\\PhoneList.txt'
    with open(file_name, 'r') as f:
        lines = f.readlines()
        last_line = lines[-1].strip('\n')
    new_number = str(int(last_line) + 1)
    file_name = open(file_name, 'a')
    file_name.write('\n' + str(new_number))
    file_name.close()
    user_phone = last_line
    user_name = last_line
    user_email = str(last_line) + '@qq.com'
    # user_password = 'Abcd1234_!#$*+-./:=?@[]^`|'
    user_password = 'Abcd1234'
    return user_phone, user_name, user_email, user_password


def user_register():
    user_info = phone_number_create()
    click_element(register_main_register)  # 点击【注册】
    send_keys(register_button_register_phone, user_info[0])  # 输入手机号
    send_keys(register_button_register_name, user_info[1])  # 输入昵称
    send_keys(register_button_register_email, user_info[2])  # 输入邮箱
    send_keys(register_button_register_password, "Abcd1234_!#$*+-./:=?@[]^`|")  # 输入密码
    click_element(register_button_sure)  # 点击确定
    send_keys(register_button_verify_input, verification_code_phone(user_info[0]))  # 输入手机验证码
    click_element(register_button_sure)  # 点击提交
    send_keys(register_button_verify_input, verification_code_email(user_info[2]))  # 输入邮箱验证码
    click_element(register_button_sure)  # 点击提交
    user_phone = user_info[0]
    user_name = user_info[1]
    user_email = user_info[2]
    user_password = user_info[3]
    return user_phone, user_name, user_email, user_password


def user_register_yale():
    user_info = phone_number_create()
    click_element(register_main_register)  # 点击【注册】
    click_element(register_register_start)  # 点击【开始】
    click_element(register_policy_agree)  # 点击【我同意】
    send_keys(register_register_first_name, user_info[1])  # 输入名
    send_keys(register_register_last_name, user_info[1])  # 输入姓
    send_keys(register_register_mail, user_info[2])  # 输入邮箱
    send_keys(register_register_phone, user_info[0])  # 输入手机号
    send_keys(register_register_password, user_info[3])  # 输入密码
    click_element(register_register_next, after_action_sleep=5)  # 点击下一步
    click_element(register_userIcon_skip, after_action_sleep=5)  # 点击跳过这步
    send_keys(register_verifyCode_code_input, verification_code_phone(user_info[0]))  # 输入正确手机验证码
    send_keys(register_verifyCode_code_input, verification_code_email(user_info[2]))  # 输入正确邮箱验证码
    time.sleep(5)
    assert_result_displayed(homePage_homePage_menu_in_home_page, True)  # 注册成功，显示菜单按键
    # assert_result_displayed(homePage_homePage_add_device, True)  # 注册成功进入首页显示添加设备按钮
    user_phone = user_info[0]
    user_name = user_info[1]
    user_email = user_info[2]
    user_password = user_info[3]
    return user_phone, user_name, user_email, user_password


def __check_element(content_text, wait_time=30, check_type=True):
    start_time = time.time()
    while 1:
        if time.time() - start_time >= wait_time:
            if check_type is True:
                log.terminate()
                assert False
            else:
                return False
        else:
            try:
                driver.find_element_by_id(content_text)
            except:
                pass
            else:
                return 'id'
            try:
                driver.find_element_by_xpath(content_text)
            except:
                pass
            else:
                return 'xpath'


def click_element(content_text, wait_time=30, after_action_sleep=1):
    check_type = __check_element(content_text, wait_time=wait_time)
    if check_type == 'id':
        try:
            driver.find_element_by_id(content_text).click()
        except:
            log.terminate()
            assert False
        else:
            pass
    elif check_type == 'xpath':
        try:
            driver.find_element_by_xpath(content_text).click()
        except:
            log.terminate()
            assert False
        else:
            pass
    else:
        pass
    time.sleep(after_action_sleep)


def send_keys(content_text, input_text, wait_time=30, after_action_sleep=1, hide_keyboard=True):
    """hide_keyboard:不隐藏键盘，可能会挡住部分按键"""
    check_type = __check_element(content_text, wait_time=wait_time)
    if check_type == 'id':
        try:
            driver.find_element_by_id(content_text).send_keys(input_text)
        except:
            log.terminate()
            assert False
        else:
            pass
    elif check_type == 'xpath':
        try:
            driver.find_element_by_xpath(content_text).send_keys(input_text)
        except:
            log.terminate()
            assert False
        else:
            pass
    else:
        pass
    if hide_keyboard is True:
        driver.hide_keyboard()
    else:
        pass
    time.sleep(after_action_sleep)


def scroll_and_click(content_text, target_content, wait_time=30):
    # 确认整体元素已显示
    __check_element(content_text, wait_time=wait_time)
    # 对具体的元素进行操作
    count = 0
    while 1:
        # 对目标元素进行点击，找不到则滚屏；若超过9次则认为没有定位到该元素
        try:
            driver.find_element_by_xpath(target_content)
        except:
            count += 1
            if count > 9:
                raise Exception('can not find lock')
            else:
                start_x = driver.get_window_size()['width'] * 0.5
                start_y = driver.get_window_size()['height'] * 0.7
                end_x = start_x
                end_y = driver.get_window_size()['height'] * 0.3
                driver.swipe(int(start_x), int(start_y), int(end_x), int(end_y), 500)
        else:
            driver.find_element_by_xpath(target_content).click()
            break


def clear_text_content(content_text):
    content = ''
    check_type = __check_element(content_text)
    if check_type == 'id':
        content = driver.find_elements_by_id(content_text)
    elif check_type == 'xpath':
        content = driver.find_element_by_xpath(content_text)
    else:
        pass
    text = ''
    for i in content:
        text += i.get_attribute('text')
    driver.keyevent(123)  # 表示光标移动到最后
    for i in range(0, len(text)):
        driver.press_keycode(67)


def assert_result_enable(content_text, assert_type, wait_time=30):
    check_type = __check_element(content_text, check_type=assert_type, wait_time=wait_time)
    if check_type == 'id':
        try:
            assert bool(driver.find_element_by_id(content_text).is_enabled()) is assert_type
        except:
            log.terminate()
            assert False
        else:
            assert True
    elif check_type == 'xpath':
        try:
            assert bool(driver.find_element_by_xpath(content_text).is_enabled()) is assert_type
        except:
            log.terminate()
            assert False
        else:
            assert True
    else:
        return assert_type


def assert_result_displayed(content_text, assert_type, wait_time=30):
    check_type = __check_element(content_text, check_type=assert_type, wait_time=wait_time)
    if check_type == 'id':
        try:
            assert bool(driver.find_element_by_id(content_text).is_displayed()) is assert_type
        except:
            log.terminate()
            assert False
        else:
            assert True
    elif check_type == 'xpath':
        try:
            assert bool(driver.find_element_by_xpath(content_text).is_displayed()) is assert_type
        except:
            log.terminate()
            assert False
        else:
            assert True
    else:
        return assert_type


def assert_result_checked(content_text, assert_type, wait_time=30):
    check_type = __check_element(content_text, check_type=assert_type, wait_time=wait_time)
    if check_type == 'id':
        try:
            assert bool(driver.find_element_by_id(content_text).get_attribute('checked')) is assert_type
        except:
            log.terminate()
            assert False
        else:
            assert True
    elif check_type == 'xpath':
        try:
            assert bool(driver.find_element_by_xpath(content_text).get_attribute('checked')) is assert_type
        except:
            log.terminate()
            assert False
        else:
            assert True
    else:
        return assert_type


def assert_result_text(content_text, assert_text):
    content_text_result = ''
    check_type = __check_element(content_text)
    assert_text_result = str(assert_text).replace(" ", "").strip()
    if check_type == 'id':
        content_text_result = str(driver.find_element_by_id(content_text).text).replace(" ", "").strip()
    elif check_type == 'xpath':
        content_text_result = str(driver.find_element_by_xpath(content_text).text).replace(" ", "").strip()
    else:
        pass
    try:
        assert content_text_result == assert_text_result
    except:
        log.terminate()
        assert False
    else:
        assert True


def login_yale(user_phone, user_email, user_password, login_method):
    """登录
    login_method==phone:手机号登录
    login_method==email:邮箱登录"""
    # 登录
    click_element(register_main_login)  # 点击【登录】
    if login_method == 'phone':
        send_keys(login_login_phone_input, user_phone)  # 输入手机号
        send_keys(login_login_password_input, user_password)  # 输入密码
    elif login_method == 'email':
        click_element(login_login_type_switch)  # 点击【邮箱登录】
        send_keys(login_login_mail_input, user_email)  # 输入邮箱
        send_keys(login_login_password_input, user_password)  # 输入密码
    else:
        raise Exception('login_method error:{}'.format(login_method))
    click_element(login_button_sure, after_action_sleep=5)  # 点击【确定】
    try:
        if check_element(login_verify_code_input, wait_time=1):
            if login_method == 'phone':
                send_keys(login_verify_code_input, verification_code_phone(user_phone),
                          after_action_sleep=5)  # 输入手机验证码，自动下一步
            elif login_method == 'email':
                send_keys(login_verify_code_input, verification_code_email(user_email),
                          after_action_sleep=5)  # 输入邮箱验证码，自动下一步
        else:
            raise Exception
    except Exception as e:
        pass


def login(user_phone, user_email, user_password, login_method):
    """登录
    login_method==phone:手机号登录
    login_method==email:邮箱登录"""
    # 登录
    click_element(register_main_login)  # 点击【登录】
    if login_method == 'phone':
        send_keys(login_login_phone_input, user_phone)  # 输入手机号
        send_keys(login_login_password_input, user_password)  # 输入密码
    elif login_method == 'email':
        click_element(login_login_type_switch)  # 点击【邮箱登录】
        send_keys(login_login_mail_input, user_email)  # 输入邮箱
        send_keys(login_login_password_input, user_password)  # 输入密码
    else:
        raise Exception('login_method error:{}'.format(login_method))
    click_element(login_button_sure, after_action_sleep=5)  # 点击【确定】
    try:
        if check_element(login_verify_code_input, wait_time=1):
            if login_method == 'phone':
                send_keys(login_verify_code_input, verification_code_phone(user_phone),
                          after_action_sleep=5)  # 输入手机验证码，自动下一步
            elif login_method == 'email':
                send_keys(login_verify_code_input, verification_code_email(user_email),
                          after_action_sleep=5)  # 输入邮箱验证码，自动下一步
        else:
            raise Exception
    except Exception as e:
        pass


def login_panpan(user_phone, user_email, user_password, login_method):
    """登录
    login_method==phone:手机号登录
    login_method==email:邮箱登录"""
    file_name = 'Configuration/LastUser.txt'
    with open(file_name, 'r') as f:
        lines = f.readlines()
        last_line = lines[-1].strip('\n')
    # TODO 此处需要加判断，若文件不存在则先本地生成一个新文件
    current_user_info = str(user_phone) + '/' + str(user_email)
    if current_user_info == last_line:
        last_user = 'same'
    else:
        last_user = 'different'
    # 登录
    if app_select == 'panpan':
        click_element(register_button_login)  # 点击【登录】
        if login_method == 'phone':
            send_keys(login_button_login_phone, user_phone)  # 输入手机号
            send_keys(login_button_login_password, user_password)  # 输入密码
        elif login_method == 'email':
            click_element(login_compound_login_method)  # 点击【邮箱登录】
            send_keys(login_button_login_email, user_email)  # 输入邮箱
            send_keys(login_button_login_password, user_password)  # 输入密码
        else:
            raise Exception('login_method error:{}'.format(login_method))
        click_element(login_button_sure, after_action_sleep=5)  # 点击【确定】
        if last_user == 'same':
            pass
        else:
            if login_method == 'phone':
                send_keys(register_button_verify_input, verification_code_phone(user_phone))  # 输入手机验证码
            elif login_method == 'email':
                send_keys(register_button_verify_input, verification_code_email(user_email))  # 输入邮箱验证码
            click_element(register_button_sure)  # 点击【提交】
            file_name = open(file_name, 'w')
            file_name.write(current_user_info)
            file_name.close()
    elif app_select == 'yale':
        click_element(register_main_login)  # 点击【登录】
        if login_method == 'phone':
            send_keys(login_login_phone_input, user_phone)  # 输入手机号
            send_keys(login_login_password_input, user_password)  # 输入密码
        elif login_method == 'email':
            click_element(login_login_type_switch)  # 点击【邮箱登录】
            send_keys(login_login_mail_input, user_email)  # 输入邮箱
            send_keys(login_login_password_input, user_password)  # 输入密码
        else:
            raise Exception('login_method error:{}'.format(login_method))
        click_element(login_button_sure, after_action_sleep=5)  # 点击【确定】
        if last_user == 'same':
            pass
        else:
            if login_method == 'phone':
                send_keys(login_verify_code_input, verification_code_phone(user_phone),
                          after_action_sleep=10)  # 输入手机验证码，自动下一步
            elif login_method == 'email':
                send_keys(login_verify_code_input, verification_code_email(user_email),
                          after_action_sleep=10)  # 输入邮箱验证码，自动下一步
            # click_element(login_verify_submit)  # 点击【提交】
            file_name = open(file_name, 'w')
            file_name.write(current_user_info)
            file_name.close()
    else:
        assert False


def logout(logout_type='home', wait_time=30):
    """退出当前登录
    logout_type==home:登录后页面
    logout_type==house:登录后点击房间的页面
    logout_type==other:其他页面"""
    start_time = time.time()
    while 1:
        if time.time() - start_time >= wait_time:
            log.terminate()
            assert False
        else:
            if app_select == 'panpan':
                if logout_type == 'home':
                    click_element(homePage_homePage_menu_in_home_page)
                    break
                elif logout_type == 'house':
                    click_element(add_button_menu_in_house)
                    break
                elif logout_type == 'other':
                    click_element(homePage_homePage_menu_in_other_page)
                    break
                else:
                    pass
            elif app_select == 'yale':
                if logout_type == 'home':
                    click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
                    break
                elif logout_type == 'house':
                    click_element(homePage_homePage_menu_in_other_page)  # 点击菜单
                    break
                else:
                    raise ValueError('input error:{}'.format(logout_type))
            else:
                assert False
    click_element(homePage_menu_user_icon)  # 点击用户头像
    click_element(homePage_user_logout)  # 点击【退出】
    click_element(homePage_else_sure_or_try)  # 点击【确定】


def invite_user(invite_type, phone, name, registered, frequency):
    """邀请用户
    invite_type:邀请类型；admin/normal/guest
    registered:是否已注册；True/False
    frequency:使用频次；always/recur/temp"""
    click_element(user_button_user)  # 点击【用户】
    click_element(user_button_invite_open)  # 打开邀请
    if invite_type == 'admin':
        click_element(user_button_invite_admin)  # 点击【管理员邀请】
    elif invite_type == 'normal':
        click_element(user_button_invite_normal)  # 点击【普通用户邀请】
    elif invite_type == 'guest':
        click_element(user_button_invite_guest)  # 点击【访客邀请】
    else:
        raise Exception('invite_type error:{}'.format(invite_type))
    send_keys(register_button_register_phone, phone)  # 输入手机号
    send_keys(register_button_register_name, name)  # 输入姓名
    click_element(register_button_sure)  # 点击【下一步】
    if registered is True:
        click_element(register_button_sure)  # 点击【下一步】
    else:
        pass
    click_element(user_button_invite_sure)  # 点击【确定】
    if invite_type == 'admin':
        click_element(user_button_invite_schedule_always)  # 点击【永久使用】
    else:
        if frequency == 'always':
            click_element(user_button_invite_schedule_always)  # 点击【永久使用】
        elif frequency == 'recur':
            click_element(user_button_invite_schedule_recur)  # 点击【多次使用】
        elif frequency == 'temp':
            click_element(user_button_invite_schedule_temp)  # 点击【临时使用】
        else:
            raise Exception('frequency error:{}'.format(frequency))
    click_element(user_button_invite_sure)  # 点击【确定】
    click_element(user_button_message_back)  # 点击【返回】


def invite_user_yale(invite_type, phone, name, frequency='always', password=None):
    """Yale China 邀请用户
    :param invite_type: 邀请类型；admin/normal/guest
    :param phone: 手机号/contact(手机联系人)
    :param name: 姓名
    :param frequency: 使用频次；always/recur/temp
    :param password: 访客密码：default/******/None
    :return:None
    """
    click_element(userManagement_main_user_tab)  # 点击【用户】
    click_element(userManagement_invite_invite)  # 点击邀请用户
    try:
        click_element(userManagement_invite_invite, wait_time=1)  # 概率性点不到邀请用户
    except:
        pass
    default_pin = None
    if invite_type == 'guest':
        click_element(userManagement_invite_invite_guest)  # 点击【访客邀请】
        if phone == 'unregistered_contact':
            click_element(userManagement_invite_guest_contact)  # 点击联系人
            click_element(userManagement_invite_unregistered_contact)  # 未注册联系人
        elif phone == 'registered_contact':
            click_element(userManagement_invite_guest_contact)  # 点击联系人
            click_element(userManagement_invite_registered_contact)  # 已注册联系人
        else:
            send_keys(userManagement_invite_guest_first_name, name)  # 输入名
            send_keys(userManagement_invite_guest_last_name, name)  # 输入姓
            click_element(userManagement_invite_continue_button)  # 继续
        if password is None:  # 不设置密码
            pass
        elif password == 'default':  # 默认密码
            click_element(userManagement_invite_create_pin)  # 点击设置密码
            default_pin = get_element(userManagement_invite_pin_input).text  # 获取默认密码
            click_element(userManagement_invite_change_pin, after_action_sleep=5)  # 点击保存更改
        else:  # 指定密码
            click_element(userManagement_invite_create_pin)  # 点击设置密码
            clear_text_content(userManagement_invite_pin_input)  # 清空默认密码
            send_keys(userManagement_invite_pin_input, password)  # 输入密码
            click_element(userManagement_invite_change_pin, after_action_sleep=5)  # 点击保存更改
        if frequency == 'always':
            pass
        elif frequency == 'recur':
            try:
                click_element(userManagement_invite_create_schedule, wait_time=5)  # 创建一个schedule
                click_element(userManagement_invite_schedule_recurring)  # 重复
                click_element(userManagement_invite_guest_back_button)  # 返回
            except:
                pass
        elif frequency == 'temp':
            try:
                click_element(userManagement_invite_create_schedule, wait_time=5)  # 创建一个schedule
                click_element(userManagement_invite_schedule_temp)  # 临时
                click_element(userManagement_invite_guest_back_button)  # 返回
            except:
                pass
        try:
            click_element(userManagement_invite_save_pin, wait_time=5, after_action_sleep=15)  # 保存密码
        except:
            pass
        return default_pin

    elif (invite_type == 'admin') or (invite_type == 'normal'):
        click_element(userManagement_invite_invite_user)  # 点击【管理员和普通用户】
        click_element(userManagement_invite_select_phone)  # 点击手机号邀请
        send_keys(userManagement_invite_phone_input, phone)  # 11位手机号
        click_element(userManagement_invite_phone_input)  # 调起键盘
        os.system('adb shell input keyevent KEYCODE_ENTER')  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
        send_keys(userManagement_invite_first_name, name)
        send_keys(userManagement_invite_last_name, name)
        click_element(userManagement_invite_last_name)  # 调起键盘
        os.system('adb shell input keyevent KEYCODE_ENTER')  # 键盘回车键，此处只能用搜狗输入法，谷歌等输入法不能响应enter键
        if invite_type == 'admin':
            click_element(userManagement_invite_user_access)  # 设置权限
            click_element(userManagement_invite_owner_access)  # 管理员
            click_element(userManagement_invite_send_button)  # 点击确定
        elif invite_type == 'normal':
            #  默认就是普通用户权限
            if frequency == 'always':
                pass  # 默认永久
            elif frequency == 'recur':
                click_element(userManagement_invite_user_schedule)  # 设置schedule
                click_element(userManagement_invite_schedule_recurring)  # 重复
                click_element(userManagement_invite_send_button)  # 确定
            elif frequency == 'temp':
                click_element(userManagement_invite_user_schedule)  # 设置schedule
                click_element(userManagement_invite_schedule_temp)  # 临时
                click_element(userManagement_invite_send_button)  # 确定
            else:
                raise Exception('frequency error:{}'.format(frequency))
        click_element(userManagement_invite_send_button, after_action_sleep=15)  # 邀请后等待十五秒
    else:
        raise Exception('invite_type error:{}'.format(invite_type))


def delete_user_yale(user_name):
    click_element(UserManagement_user_list)  # 点击【用户列表】
    if check_element(UserManagement_user_normal.format(user_name), wait_time=5, check_type=False) is False:
       pass
    else:
        click_element(UserManagement_user_admin.format(user_name))  # 点击用户名
        click_element(UserManagement_delete_user)  # 点击删除用户
        click_element(lock_setting_save_button, after_action_sleep=15)  # 确认删除


def disable_pin_yale(user_name):
    click_element(UserManagement_user_list)  # 点击【用户列表】
    time.sleep(1)
    click_element(UserManagement_user_admin.format(user_name))  # 点击用户名
    click_element(userManagement_edit_pin_set)  # 点击进入密码页
    click_element(userManagement_edit_disable_pin)  # 点击停用密码
    click_element(userManagement_edit_right_button, after_action_sleep=10)  # 右上角保存按键


def enable_pin_yale(user_name):
    click_element(UserManagement_user_list)  # 点击【用户列表】
    time.sleep(1)
    click_element(UserManagement_user_admin.format(user_name))  # 点击用户名
    click_element(userManagement_edit_pin_set)  # 点击进入密码页
    click_element(UserManagement_password_save)  # 点击激活密码
    click_element(UserManagement_password_save)  # 保存密码
    click_element(userManagement_edit_right_button, after_action_sleep=10)  # 右上角保存按键


def add_lock():
    click_element(homePage_homePage_menu_in_home_page)  # 点击菜单
    click_element(homePage_menu_add_device)  # 点击【添加设备】
    click_element(add_button_device_1)  # 选择图片
    click_element(add_compound_device_1)  # 选择产品
    click_element(add_button_installed)  # 点击【已安装】
    click_element(add_button_start_connect)  # 点击【开始连接】
    click_element(add_compound_start_setup)  # 点击【开始设置】
    # 初次安装时有此验证，请求定位权限
    if __check_element(soft_button_location_permission, wait_time=2, check_type=False) is False:
        pass
    else:
        click_element(soft_button_location_permission)
        click_element(add_button_double_confirm)  # 请求定位权限
        click_element(soft_button_location_permission)
    scroll_and_click(add_assert_select_lock_or_photo, add_compound_room_or_lock_name.format(selected_lock_id))  # 选择锁
    send_keys(add_button_lock_name_input_with_house, 'selected_lock_name')  # 输入锁名
    click_element(add_button_create_lock_name)  # 点击【下一步】
    click_element(add_compound_room_or_lock_name.format(HouseInfo.name))  # 选择房间
    click_element(homePage_else_sure_or_try)  # 点击【试试】


def add_lock_yale():
    click_element(homePage_homePage_add_device)  # 点击【添加设备】
    click_element(homePage_else_add_device_text)  # 点击设置智能门锁
    click_element(homePage_else_manual_scan)  # 手动选择我的设备
    click_element(homePage_else_set_device_text)  # 点击选择智能门锁
    click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
    click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
    click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
    click_element(homePage_else_lock_install)  # 点击开始安装按钮
    scroll_and_click(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
    click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
    time.sleep(60)
    click_element(homePage_else_lock_start)  # 点击【试试】按钮(盼盼才有)
    assert_result_displayed(homePage_else_my_house_name, True)  # 显示我的住宅名


def add_lock_yale_new():
    click_element(homePage_homePage_add_device)  # click [set up new device]
    click_element(homePage_else_add_device_text)  # click [can't scan the code?]
    assert_result_displayed(homePage_else_enter_serial_number, True)  # Enter the serial number page
    send_keys(homePage_else_enter_serial_number, selected_serial_number)  # Enter serial number
    click_element(homePage_else_lock_continue)  # click continue
    # click_element(homePage_else_set_device_image)  # click set device image
    click_element(homePage_else_lock_start)  # click start
    click_element(homePage_else_lock_start)  # click start
    click_element(homePage_else_lock_install)  # 开始安装
    send_keys(homePage_else_choose_lock_name, selected_serial_number)  # 锁命名
    click_element(homePage_else_choose_name_radiobutton)  # 添加新住宅
    click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
    send_keys(homePage_else_house_new_name, selected_serial_number + ' ' + random.choice('abcdefghijklmnopqrstuvwxyz0123456789'))  # 住宅命名
    click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
    time.sleep(60)
    click_element(homePage_else_lock_start)  # 点击【试试】按钮
    assert_result_text(lock_main_my_lock_name, selected_serial_number)  # display lock name
    assert_result_displayed(homePage_else_my_house_name, True)  # 显示我的住宅名


def auto_lock(setting_value):
    """自动上锁的开关"""
    click_element(user_button_setting)  # 点击【设定】
    click_element(user_button_setting_lock, after_action_sleep=3)  # 点击【锁的设置】
    while 1:
        if __check_element(lock_assert_lock_setting_connecting_status, wait_time=5, check_type=False):  # 正在连接锁
            pass
        else:
            break
    if setting_value == 'on' and __check_element(lock_compound_lock_setting_auto) == 'id':
        assert_text = driver.find_element_by_id(lock_compound_lock_setting_auto).text
        if assert_text == lock_compound_lock_setting_auto_open:
            pass
        else:
            click_element(lock_compound_lock_setting_auto)
    elif setting_value == 'on' and __check_element(lock_compound_lock_setting_auto) == 'xpath':
        assert_text = driver.find_element_by_xpath(lock_compound_lock_setting_auto).text
        if assert_text == lock_compound_lock_setting_auto_open:
            pass
        else:
            click_element(lock_compound_lock_setting_auto)
    elif setting_value == 'off' and __check_element(lock_compound_lock_setting_auto) == 'id':
        assert_text = driver.find_element_by_id(lock_compound_lock_setting_auto).text
        if assert_text == lock_compound_lock_setting_auto_close:
            pass
        else:
            click_element(lock_compound_lock_setting_auto)
    elif setting_value == 'off' and __check_element(lock_compound_lock_setting_auto) == 'xpath':
        assert_text = driver.find_element_by_xpath(lock_compound_lock_setting_auto).text
        if assert_text == lock_compound_lock_setting_auto_close:
            pass
        else:
            click_element(lock_compound_lock_setting_auto)
    else:
        raise Exception('setting_value error:{}'.format(setting_value))
    click_element(homepage_button_detail_back)  # 点击【返回】


def factory_lock():
    """对锁进行解绑"""
    if app_select == 'panpan':
        # 初次安装时有此验证，请求定位权限
        if __check_element(soft_button_location_permission, wait_time=2, check_type=False) is False:
            pass
        else:
            click_element(soft_button_location_permission)
            click_element(add_button_double_confirm)  # 请求定位权限
            click_element(soft_button_location_permission)
        # # Todo 硬件经常故障，暂时注释
        # # 门锁不会自动上锁，若之前case报错，则会影响后续，加此验证
        # click_element(user_button_lock)  # 点击【锁】
        # start_time_lock_status = time.time()
        # while 1:
        #     if time.time() -start_time_lock_status >= 30:
        #         log.terminate()
        #         assert False
        #     else:
        #         try:
        #             __check_element(lock_assert_lock_status_opening, wait_time=1) in ['id', 'xpath']  # 未显示【门锁打开】
        #         except:
        #             pass
        #         else:
        #             time.sleep(10)
        #             click_element(lock_button_open_lock)  # 点击【关锁】
        #             assert_result_displayed(lock_assert_lock_status_closing, True)
        #             break
        #         try:
        #             __check_element(lock_assert_lock_status_closing, wait_time=1) in ['id', 'xpath']  # 显示【门锁关闭】
        #         except:
        #             pass
        #         else:
        #             break
        click_element(user_button_setting)  # 点击【设定】
        click_element(user_button_setting_lock)  # 点击【锁的设置】
        start_time_lock_connect = time.time()
        while 1:
            if time.time() - start_time_lock_connect >= 30:
                log.terminate()
                assert False
            else:
                try:
                    __check_element(lock_assert_lock_setting_connecting_status, wait_time=2, check_type=False)  # 正在连接锁
                except:
                    pass
                else:
                    break
        click_element(user_button_setting_lock_factory)  # 点击【恢复出厂设置】
        click_element(user_button_factory_reset)  # 点击【恢复出厂设置】
        click_element(user_button_factory_reset_and_delete_yes)  # 点击【重置】
        click_element(homePage_else_sure_or_try)  # 点击【确定】
    elif app_select == 'yale':
        click_element(lock_main_setting)  # 点击【设定】
        click_element(lock_setting_lock_setting)  # 点击【锁的设置】
        start_time_lock_connect = time.time()
        while 1:
            if time.time() - start_time_lock_connect >= 30:
                log.terminate()
                assert False
            else:
                try:
                    __check_element(lock_lockSetting_loading, wait_time=2, check_type=False)  # 正在连接锁
                except:
                    pass
                else:
                    break
        click_element(lock_lockSetting_reset)  # 点击【出厂设置】
        click_element(lock_lockSetting_reset_button)  # 点击【恢复出厂设置】
        click_element(lock_lockSetting_reset_confirm)  # 点击【重置】


def factory_lock_yale():
    """对锁进行解绑"""
    check_lock_connection()  # 检查锁的连接
    # assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
    click_element(lock_setting_setting_button)  # 点击设置按钮
    click_element(lock_setting_lock_setting)  # 点击锁设置按钮
    start_time_lock_connect = time.time()
    while 1:
        if time.time() - start_time_lock_connect >= 30:
            log.terminate()
            assert False
        else:
            try:
                __check_element(lock_lockSetting_loading, wait_time=2, check_type=False)  # 刷新中不存在
            except:
                pass
            else:
                break
    click_element(lock_lockSetting_reset)  # 点击出厂设置
    click_element(lock_lockSetting_reset_button)    # 点击恢复出厂设置按钮
    click_element(lock_lockSetting_reset_confirm)  # 点击重置
    if __check_element(lock_lockSetting_reset_confirm, wait_time=20, check_type=False) is False:
        pass
    else:
        click_element(lock_lockSetting_reset_confirm)  # 点击好的(盼盼)
        click_element(lock_lockSetting_reset_button)  # 点击恢复出厂设置按钮(盼盼)
        click_element(lock_lockSetting_reset_confirm)  # 点击重置


def wifi_switch(switch_code):
    if switch_code == 'close':
        os.system('adb shell svc wifi disable')
    elif switch_code == 'open':
        os.system('adb shell svc wifi enable')
        # click_element("//*[@resource-id='android:id/button2']")
    else:
        raise Exception('switch_code error:{}'.format(switch_code))


def switch_wifi_status():
    os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
    os.system('adb shell am start -a android.settings.WIFI_SETTINGS')
    click_element("//*[@resource-id='android:id/checkbox']")
    driver.press_keycode(3)
    click_element("//*[@text='Yale Access']")
    time.sleep(2)


def bt_switch(switch_code):
    current_status_temp = subprocess.Popen('adb shell settings get global bluetooth_on', stdout=subprocess.PIPE)
    current_status = str(current_status_temp.stdout.read(), encoding='utf-8').strip('\r\n')
    if current_status == str(0):  # 获取当前的蓝牙状态
        current_status_code = 'off'
    elif current_status == str(1):
        current_status_code = 'on'
    else:
        raise Exception('current_status error:{}'.format(current_status))
    if current_status_code == switch_code:
        pass
    else:
        os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
        if __check_element("//*[@text='蓝牙']", wait_time=3, check_type=False):
            click_element("//*[@text='蓝牙']")
        else:
            os.system('adb shell am start -a android.settings.BLUETOOTH_SETTINGS')
        click_element("//*[@resource-id='android:id/checkbox']")
        # driver.press_keycode(3)
        os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
        click_element("//*[@text='Yale Access']")
        time.sleep(5)


def add_alert():
    """添加智能提醒"""
    click_element(user_button_setting)  # 点击【锁】
    click_element(user_button_setting_lock)  # 点击【锁的设置】
    click_element(lock_button_setting_smart_alert)  # 点击【智能提醒设置】
    click_element(lock_button_setting_add_smart_alert)  # 点击【新增智能提醒】
    click_element(lock_button_lock_setting_smart_event_choose)  # 点击【选择事件】
    click_element(lock_button_lock_setting_smart_event_user)  # 点击【用户】
    click_element(lock_button_lock_setting_smart_event_user_all)  # 点击【任意用户】
    click_element(lock_button_lock_setting_smart_event_user_save)  # 点击【完成】
    click_element(lock_button_lock_setting_smart_event_save)  # 点击【完成】
    click_element(lock_button_lock_setting_smart_time_choose)  # 点击【选择时间】
    click_element(lock_button_lock_setting_smart_time_always)  # 点击【任何时间】
    click_element(lock_button_lock_setting_smart_time_save)  # 点击【保存】
    click_element(lock_compound_lock_setting_smart_save)  # 点击【保存】
    click_element(homepage_button_detail_back)  # 点击【返回】
    click_element(homepage_button_detail_back)  # 点击【返回】


def add_pin(user_name, user_pin, user_level='admin'):
    """添加密码
    user_level:用户等级；admin/normal/guest"""
    click_element(user_button_user)  # 点击【用户】
    if user_level == 'admin':
        click_element(user_compound_user_admin.format(user_name))  # 点击管理员
    elif user_level == 'normal':
        click_element(user_compound_user_normal.format(user_name))  # 点击普通用户
    elif user_level == 'guest':
        click_element(user_compound_user_guest.format(user_name))  # 点击访客
    else:
        raise Exception('user_level error:{}'.format(user_level))
    click_element(user_button_user_pin)  # 点击【密码】
    click_element(user_button_user_pin_input)  # 点击密码框
    clear_text_content(user_button_user_pin_input)  # 清除输入
    send_keys(user_button_user_pin_input, user_pin)  # 输入密码
    click_element(user_button_user_pin_save)  # 点击【完成】


def add_finger(user_name, user_id, finger_id, user_level='admin'):
    """添加指纹
    user_level:用户等级；admin/normal/guest"""
    click_element(user_button_user)  # 点击【用户】
    if user_level == 'admin':
        click_element(user_compound_user_admin.format(user_name))  # 点击管理员
    elif user_level == 'normal':
        click_element(user_compound_user_normal.format(user_name))  # 点击普通用户
    else:
        raise Exception('user_level error:{}'.format(user_level))
    click_element(user_button_user_finger)  # 点击【指纹管理】
    requests.put(url=API.request_api_finger_register.format(user_id, finger_id))  # 添加指纹
    click_element(user_button_user_finger_and_card_add)  # 点击【添加指纹】
    click_element(user_button_user_finger_and_card_add)  # 点击【添加指纹】
    click_element(user_button_user_finger_and_card_add)  # 点击【添加指纹】
    click_element(user_button_user_finger_and_card_add, wait_time=60)  # 点击【完成】
    click_element(homepage_button_detail_back)  # 点击【返回】


def add_card(user_name, user_level='admin'):
    """添加门卡
    user_level:用户等级；admin/normal/guest"""
    click_element(user_button_user)  # 点击【用户】
    if user_level == 'admin':
        click_element(user_compound_user_admin.format(user_name))  # 点击管理员
    elif user_level == 'normal':
        click_element(user_compound_user_normal.format(user_name))  # 点击普通用户
    else:
        raise Exception('user_level error:{}'.format(user_level))
    click_element(user_button_user_card)  # 点击【门卡管理】
    click_element(user_button_user_finger_and_card_add)  # 点击【添加门卡】
    click_element(user_button_user_finger_and_card_add)  # 点击【添加门卡】
    click_element(user_button_user_finger_and_card_add)  # 点击【添加门卡】
    while 1:
        if __check_element(register_assert_alert, wait_time=3, check_type=False):
            requests.put(url=API.request_api_card_register)  # 添加门卡
            time.sleep(2)
        else:
            break
    click_element(user_button_user_finger_and_card_add)  # 点击【完成】
    click_element(homepage_button_detail_back)  # 点击【返回】


def add_card_yale(user_name, user_level='admin'):
    # 添加门卡
    click_element(UserManagement_user_list)  # 点击【用户列表】
    time.sleep(1)
    if user_level == 'admin':
        click_element(UserManagement_user_admin.format(user_name))  # 点击管理员
    elif user_level == 'normal':
        click_element(UserManagement_user_normal.format(user_name))  # 点击普通用户
    else:
        raise Exception('user_level error:{}'.format(user_level))
    click_element(UserManagement_user_card)  # 点击【门卡】
    try:
        if __check_element(UserManagement_already_add, wait_time=3, check_type=False) is False:
            pass
        else:
            click_element(UserManagement_remove_card)  # 删除门卡
            time.sleep(8)
            click_element(UserManagement_user_card)  # 点击【门卡】
    except NameError:
        pass
    else:
        click_element(UserManagement_add_user_card)  # 点击【添加门卡】
        click_element(UserManagement_add_user_card)  # 点击【下一步】
        click_element(UserManagement_add_user_card)  # 点击【下一步】
        time.sleep(8)
        requests.put(url=API().card_register())  # 调用添加门卡接口
        click_element(UserManagement_add_user_card)  # 点击【完成】
        driver.press_keycode(4)
        click_element(lock_main_lock_manager)  # 点击锁按钮
        check_lock_connection()


def delete_card_yale(user_name):
    # 删除门卡
    click_element(UserManagement_user_list)  # 点击【用户列表】
    time.sleep(1)
    click_element(UserManagement_user_admin.format(user_name))  # 点击管理员
    click_element(UserManagement_user_card)  # 点击【门卡】
    click_element(UserManagement_remove_card)  # 删除门卡
    click_element(UserManagement_back_bar_button)  # 返回

def register_RFID(user_name):
    # register RFID
    click_element(userManagement_main_user_tab)  # invite
    click_element(UserManagement_user_admin.format(user_name))  # click user name
    click_element(userManagement_invite_RFID_tab)  # click contactless key
    wait_lock_connection()
    click_element(userManagement_invite_card_continue)  # continue
    try:
        requests.put(url=API().card_register())  # REGISTER RFID, run on jig
    except:
        pass   # local手动
    click_element(userManagement_invite_register_finish)  # click finish


def register_FP(user_name, user_id, finger_id):
    # register FP
    click_element(userManagement_main_user_tab)  # invite
    click_element(UserManagement_user_admin.format(user_name))  # click user name
    click_element(userManagement_invite_FP_tab)  # click FP
    wait_lock_connection()
    click_element(userManagement_invite_card_continue)  # continue
    try:
        requests.put(url=API().finger_register(user_id, finger_id))  # REGISTER FP, run on jig
        time.sleep(10)
    except:
        pass       # local手动
    click_element(userManagement_invite_register_finish)  # click finish


def wait_lock_connection():
    # wait lock connection
    wait_start_time = time.time()
    while 1:
        if time.time() - wait_start_time >= 60:
            log.terminate()
            assert False
        else:
            try:
                assert_result_enable(userManagement_invite_card_continue, True)
            except:
                pass
            else:
                click_element(userManagement_invite_card_continue)  # continue
                break


def delete_RFID(user_name, user_type='owner or guest'):
    # DELETE RFID
    click_element(userManagement_main_user_tab)  # invite
    click_element(UserManagement_user_admin.format(user_name))  # click user_name
    if get_element(userManagement_invite_RFID_registered_value).text == '已注册':
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        click_element(userManagement_invite_unregister_button)  # click unregister button
        click_element(userManagement_invite_confirm, after_action_sleep=15)  # click confirm
        if user_type == 'owner or guest':
            assert_result_text(userManagement_invite_RFID_registered_value, '未注册')  # 未注册
            click_element(UserManagement_back_bar_button)  # BACK
        else:
            pass
    else:
        click_element(UserManagement_back_bar_button)  # BACK


def delete_FP(user_name, user_type='owner or guest'):
    # DELETE FP
    click_element(userManagement_main_user_tab)  # invite
    click_element(UserManagement_user_admin.format(user_name))  # click user_name
    # assert_result_text(userManagement_invite_FP_registered_value, '已注册')
    if get_element(userManagement_invite_FP_registered_value).text == '已注册':
        click_element(userManagement_invite_FP_registered_value)
        click_element(userManagement_invite_unregister_button)  # click unregister button
        click_element(userManagement_invite_confirm, after_action_sleep=15)  # click confirm
        if user_type == 'owner or guest':
            assert_result_text(userManagement_invite_FP_registered_value, '未注册')  # 未注册
            click_element(UserManagement_back_bar_button)  # BACK
        else:
            pass
    else:
        click_element(UserManagement_back_bar_button)  # BACK


def register_PINCODE(user_name):
    # register PINCODE
    click_element(userManagement_main_user_tab)  # invite
    click_element(UserManagement_user_admin.format(user_name))  # click user name
    click_element(userManagement_invite_pincode_set)  # click entry code
    pincode = get_element(userManagement_invite_pin_input).text
    click_element(userManagement_invite_change_pin)  # save pin
    click_element(userManagement_edit_right_button)   # save
    return pincode


def invite_RFID_only():
    # invite RFID only user
    click_element(userManagement_main_user_tab)  # invite
    click_element(userManagement_invite_invite)  # click invite
    click_element(userManagement_invite_RFID_access)  # click contactless key only
    send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
    send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input last name
    click_element(userManagement_invite_contact_continue)  # continue
    click_element(userManagement_invite_lock_name)  # select lock
    click_element(userManagement_invite_lock_continue)  # continue
    wait_lock_connection()
    click_element(userManagement_invite_card_continue)  # continue
    try:
        requests.put(url=API().card_register())  # REGISTER RFID, run on jig
    except:
        pass       # local 手动加
    click_element(userManagement_invite_register_finish)  # click finish


def invite_PINCODE_only(frequency='always'):
    # invite pincode only user
    click_element(userManagement_main_user_tab)  # invite
    click_element(userManagement_invite_invite)  # click invite
    click_element(userManagement_invite_PINCODE_access)  # click pincode only
    send_keys(userManagement_invite_RFID_first_name, 'PINCODE')  # input first name
    send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input last name
    click_element(userManagement_invite_PINCODE_continue)  # continue
    click_element(userManagement_invite_create_pin)  # create pin
    pincode = get_element(userManagement_invite_pin_input).text
    click_element(userManagement_invite_change_pin)  # save pin
    if frequency == 'always':
        pass  # 默认永久
    elif frequency == 'recur':
        click_element(userManagement_invite_pin_schedule)  # 设置schedule
        click_element(userManagement_invite_schedule_recurring)  # 重复
        click_element(userManagement_invite_guest_back_button)  # 返回
    elif frequency == 'temp':
        click_element(userManagement_edit_schedule_set)  # 设置schedule
        click_element(userManagement_invite_schedule_temp)  # 临时
        click_element(userManagement_invite_guest_back_button)  # 返回
    else:
        raise Exception('frequency error:{}'.format(frequency))
    click_element(userManagement_invite_save_pin, after_action_sleep=10)  # save pin


def invite_FP_only(user_id, finger_id):
    # invite FP only user
    click_element(userManagement_main_user_tab)  # invite
    click_element(userManagement_invite_invite)  # click invite
    click_element(userManagement_invite_FP_access)  # click fingerprint only
    send_keys(userManagement_invite_RFID_first_name, 'FP')  # input FIRST NAME
    send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input LAST NAME
    click_element(userManagement_invite_contact_continue)  # continue
    click_element(userManagement_invite_lock_name)  # select lock
    click_element(userManagement_invite_lock_continue)  # continue
    wait_lock_connection()
    click_element(userManagement_invite_card_continue)  # continue
    try:
        requests.put(url=API().finger_register(user_id, finger_id))  # REGISTER fingerprint run on jig
        time.sleep(10)
    except:
        pass   # local手动
    click_element(userManagement_invite_register_finish)  # click finish


def password_register(user_name, user_phone, user_level ='admin'):
    # 设置密码
    click_element(UserManagement_user_list)  # 点击【用户列表】
    time.sleep(1)
    if user_level == 'admin':
        click_element(UserManagement_user_admin.format(user_name))  # 点击管理员
    elif user_level == 'normal':
        click_element(UserManagement_user_normal.format(user_name))  # 点击普通用户
    else:
        raise Exception('user_level error:{}'.format(user_level))
    try:
        click_element(UserManagement_user_admin.format(user_name), wait_time=1)  # 上一步有概率点不到
    except:
        pass
    click_element(UserManagement_user_password)  # 点击【密码】
    clear_text_content(UserManagement_password_input)   # 清除密码
    send_keys(UserManagement_password_input, user_phone[-6:])  # 手机号后6位
    time.sleep(1)
    click_element(UserManagement_password_save)  # 保存密码
    try:
        click_element(UserManagement_password_save, wait_time=5)  # 保存密码
    except:
        pass
    try:
        click_element(UserManagement_save_update, wait_time=10, after_action_sleep=10)  # 保存修改
    except:
        pass


def add_finger_yale(user_name, user_id='1', finger_id='1', user_level='admin'):
    # 添加指纹
    click_element(UserManagement_user_list)  # 点击【用户列表】
    time.sleep(1)
    if user_level == 'admin':
        click_element(UserManagement_user_admin.format(user_name))  # 点击管理员
    elif user_level == 'normal':
        click_element(UserManagement_user_normal.format(user_name))  # 点击普通用户
    else:
        raise Exception('user_level error:{}'.format(user_level))
    click_element(UserManagement_user_fingerprint)  # 点击【指纹】
    try:
        if __check_element(UserManagement_already_add, wait_time=3, check_type=False) is False:
            pass
        else:
            click_element(UserManagement_remove_card)  # 删除指纹
            time.sleep(8)
            click_element(UserManagement_user_fingerprint)  # 点击【指纹】
    except NameError:
        pass
    else:
        click_element(UserManagement_add_user_fingerprint)  # 点击【添加指纹】
        click_element(UserManagement_add_user_fingerprint)  # 点击【下一步】
        requests.put(url=API().finger_register(user_id, finger_id))  # 调用添加指纹接口
        click_element(UserManagement_add_user_fingerprint)  # 点击【下一步】
        # time.sleep(10)
        time.sleep(20)
        click_element(UserManagement_add_user_card)  # 点击【完成】
        driver.press_keycode(4)
        click_element(lock_main_lock_manager)  # 点击锁按钮
        check_lock_connection()


def delete_finger_yale(user_name):
    click_element(UserManagement_user_list)  # 点击【用户列表】
    time.sleep(1)
    click_element(UserManagement_user_admin.format(user_name))  # 点击管理员
    click_element(UserManagement_user_fingerprint)  # 点击【指纹】
    click_element(UserManagement_remove_card)  # 点击删除【指纹】
    time.sleep(10)
    os.system('adb shell input keyevent KEYCODE_BACK')  # 返回上一页


def log_assert_text(assert_text, list_location=0, wait_time=30, check_type=True, need_refresh=True):
    """锁的操作日志内容断言
    assert_text:显示的文言
    list_location:日志列表中的行数
    need_refresh:在日志页是否需要刷新；避免对同一日志断言时反复刷新"""
    start_time = time.time()
    while 1:
        click_element(user_button_record)  # 点击【操作记录】
        content_text = log_assert_field
        if need_refresh is True:
            start_x = driver.get_window_size()['width'] * 0.5
            start_y = driver.get_window_size()['height'] * 0.3
            end_x = start_x
            end_y = driver.get_window_size()['height'] * 0.7
            driver.swipe(int(start_x), int(start_y), int(end_x), int(end_y), 500)
        elif need_refresh is False:
            pass
        else:
            raise Exception('need_refresh error:{}'.format(need_refresh))
        try:
            __check_element(content_text, wait_time=2, check_type=check_type)
        except:
            pass
        else:
            check_type = __check_element(content_text, wait_time=2, check_type=check_type)
            if time.time() - start_time >= wait_time:
                log.terminate()
                assert False
            else:
                if check_type == 'id':
                    list_collection = driver.find_elements_by_id(content_text)
                    target_text = list_collection[list_location].get_attribute('content-desc')
                    break
                elif check_type == 'xpath':
                    list_collection = driver.find_elements_by_xpath(content_text)
                    target_text = list_collection[list_location].get_attribute('content-desc')
                    break
                else:
                    pass
    if assert_text in target_text:
        assert True
    else:
        log.terminate()
        assert False


def log_assert_time(list_location=0, wait_time=30, check_type=True, need_refresh=True):
    """锁的操作日志时间断言
    start_time:日志的时间；发起请求后在当前基础上增加相应的分钟，日志时间在此范围内则断言正确
    list_location:日志列表中的行数
    need_refresh:在日志页是否需要刷新；避免对同一日志断言时反复刷新"""
    click_element(user_button_record)  # 点击【操作记录】
    content_text = log_assert_field
    check_type = __check_element(content_text, wait_time=wait_time, check_type=check_type)
    start_time = time.time()
    while 1:
        if need_refresh is True:
            start_x = driver.get_window_size()['width'] * 0.5
            start_y = driver.get_window_size()['height'] * 0.3
            end_x = start_x
            end_y = driver.get_window_size()['height'] * 0.7
            driver.swipe(int(start_x), int(start_y), int(end_x), int(end_y), 500)
        elif need_refresh is False:
            pass
        else:
            raise Exception('need_refresh error:{}'.format(need_refresh))
        if time.time() - start_time >= 60:  # 因允许1分钟时间差，故此处允许1分钟等待
            log.terminate()
            assert False
        else:
            if check_type == 'id':
                list_collection = driver.find_elements_by_id(content_text)
                target_text = list_collection[list_location].get_attribute('content-desc')
                break
            elif check_type == 'xpath':
                list_collection = driver.find_elements_by_xpath(content_text)
                target_text = list_collection[list_location].get_attribute('content-desc')
                break
            else:
                click_element(user_button_lock, after_action_sleep=5)  # 点击【锁】
                click_element(user_button_record)  # 点击【操作记录】
    content_time = re.split(r'TimeStep:', target_text)[1]  # 只获取时间部分
    content_time_format = datetime.datetime.strptime(content_time, '%H:%M')
    current_time = str(datetime.datetime.now())[11:16]
    current_time_get = ''.join(current_time)
    current_time_format = datetime.datetime.strptime(current_time_get, '%H:%M')
    max_difference_time = datetime.timedelta(seconds=60)
    result = current_time_format - content_time_format
    if datetime.timedelta(seconds=0) <= result <= max_difference_time:
        assert True
    else:
        log.terminate()
        assert False


def check_lock_info(content_text, wait_time=30, check_type=True):
    """固件信息判断，长度不为0表示信息不为空"""
    content = ''
    check_type = __check_element(content_text, wait_time=wait_time, check_type=check_type)
    if check_type == 'id':
        content = driver.find_elements_by_id(content_text)
    elif check_type == 'xpath':
        content = driver.find_elements_by_xpath(content_text)
    else:
        pass
    version_result = content[0].get_attribute('text')
    if len(version_result) > 0:
        pass
    else:
        log.terminate()
        assert False


def get_element(content_text, wait_time=30):
    check_type = __check_element(content_text, wait_time=wait_time)
    if check_type == 'id':
        element = driver.find_element(By.ID, content_text)
        return element
    elif check_type == 'xpath':
        element = driver.find_element(By.XPATH, content_text)
        return element
    else:
        log.terminate()
        assert False


# 计算单通道的直方图的相似值
def calculate(image1, image2):
    hist1 = cv2.calcHist([image1], [0], None, [256], [0.0, 255.0])
    hist2 = cv2.calcHist([image2], [0], None, [256], [0.0, 255.0])
    # 计算直方图的重合度
    degree = 0
    for i in range(len(hist1)):
        if hist1[i] != hist2[i]:
            degree = degree + (1 - abs(hist1[i] - hist2[i]) / max(hist1[i], hist2[i]))
        else:
            degree = degree + 1
    degree = degree / len(hist1)
    return degree


# 通过得到每个通道的直方图来计算相似度
def classify_hist_with_split(image1, image2, size=(256, 256)):
    # 将图像resize后，分离为三个通道，再计算每个通道的相似值
    image1 = cv2.resize(image1, size)
    image2 = cv2.resize(image2, size)
    sub_image1 = cv2.split(image1)
    sub_image2 = cv2.split(image2)
    sub_data = 0
    for im1, im2 in zip(sub_image1, sub_image2):
        sub_data += calculate(im1, im2)
    sub_data = sub_data / 3
    return sub_data


def compare_result(image1, image2, content_text):
    driver.save_screenshot(image2)
    element = get_element(content_text)
    left = element.location['x']
    top = element.location['y']
    right = element.location['x'] + element.size['width']
    bottom = element.location['y'] + element.size['height']
    im = Image.open(image1)
    im = im.crop((left, top, right, bottom))
    im.save(image2)
    img1 = cv2.imread(image1)
    cv2.imshow('img1', img1)
    img2 = cv2.imread(image2)
    cv2.imshow('img2', img2)
    degree = classify_hist_with_split(img1, img2)
    if int(degree) == 1:
        assert True
    else:
        log.terminate()
        assert False


def image_compare(source_file, content_text):
    file_name = 'Capture/result.png'
    driver.save_screenshot(file_name)
    element = get_element(content_text)
    left = element.location['x']
    top = element.location['y']
    right = element.location['x'] + element.size['width']
    bottom = element.location['y'] + element.size['height']
    im = Image.open(file_name)
    im = im.crop((left, top, right, bottom))
    im.save(file_name)
    pic_1 = Image.open(source_file)
    pic_2 = Image.open(file_name)
    try:
        deff = ImageChops.difference(pic_1, pic_2)
        if deff.getbbox() is None:
            print(1)
        else:
            print(deff)
    except ValueError as e:
        pass


def bt_open_and_notification(user1_phone, user1_email, user1_password,
                             user2_phone, user2_email, user2_password,
                             login_type, notification_result, home_name):
    # 蓝牙开锁
    login_yale(user1_phone, user1_email, user1_password, login_type)
    click_element(homepage_button_select_room_or_user.format(home_name))  # 选择家
    assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
    click_element(lock_button_open_lock)  # 点击开锁
    assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
    logout('house')
    login_yale(user2_phone, user2_email, user2_password, login_type)
    click_element(homepage_button_select_room_or_user.format(home_name), after_action_sleep=60)  # 选择家
    driver.open_notifications()  # 打开消息推送
    assert_result_displayed(notification_assert_open_bt, notification_result)  # 显示蓝牙开门
    click_element(notification_button_clear)  # 清除所有通知


def bt_close_and_notification(user1_phone, user1_email, user1_password,
                              user2_phone, user2_email, user2_password,
                              login_type, notification_result, home_name):
    # 蓝牙关锁
    login_yale(user1_phone, user1_email, user1_password, login_type)
    click_element(homepage_button_select_room_or_user.format(home_name))  # 选择家
    assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
    click_element(lock_button_open_lock)  # 点击关锁
    assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
    logout('house')
    login_yale(user2_phone, user2_email, user2_password, login_type)
    click_element(homepage_button_select_room_or_user.format(home_name), after_action_sleep=60)  # 选择家
    driver.open_notifications()  # 打开消息推送
    assert_result_displayed(notification_assert_close_bt, notification_result)  # 显示蓝牙关门
    click_element(notification_button_clear)  # 清除所有通知


def card_open_and_notification(user1_phone, user1_email, user1_password,
                               user2_phone, user2_email, user2_password,
                               login_type, notification_result, home_name):
    # 门卡开锁
    login_yale(user1_phone, user1_email, user1_password, login_type)
    click_element(homepage_button_select_room_or_user.format(home_name))  # 选择家
    assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
    requests.put(url=API.request_api_card_open)  # 门卡开门
    assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
    logout('house')
    login_yale(user2_phone, user2_email, user2_password, login_type)
    click_element(homepage_button_select_room_or_user.format(home_name), after_action_sleep=60)  # 选择家
    driver.open_notifications()  # 打开消息推送
    assert_result_displayed(notification_assert_open_card, notification_result)  # 显示门卡开门
    click_element(notification_button_clear)  # 清除所有通知


def finger_open_and_notification(user1_phone, user1_email, user1_password, user1_name, user1_id,
                                 user2_phone, user2_email, user2_password,
                                 login_type, notification_result, home_name):
    # 指纹开锁
    login_yale(user1_phone, user1_email, user1_password, login_type)
    click_element(homepage_button_select_room_or_user.format(home_name))  # 选择家
    assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
    requests.put(url=API.request_api_finger_open.format(user1_name, user1_id))  # 指纹开门
    assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
    logout('house')
    login_yale(user2_phone, user2_email, user2_password, login_type)
    click_element(homepage_button_select_room_or_user.format(home_name), after_action_sleep=60)  # 选择家
    driver.open_notifications()  # 打开消息推送
    assert_result_displayed(notification_assert_open_finger, notification_result)  # 显示指纹开门
    click_element(notification_button_clear)  # 清除所有通知


def pin_open_and_notification(user1_phone, user1_email, user1_password,
                              user2_phone, user2_email, user2_password,
                              login_type, notification_result, home_name):
    # 密码开锁
    login_yale(user1_phone, user1_email, user1_password, login_type)
    click_element(homepage_button_select_room_or_user.format(home_name))  # 选择家
    assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
    # Todo 密码开门
    assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
    logout('house')
    login_yale(user2_phone, user2_email, user2_password, login_type)
    click_element(homepage_button_select_room_or_user.format(home_name), after_action_sleep=60)  # 选择家
    driver.open_notifications()  # 打开消息推送
    # Todo 密码开门
    click_element(notification_button_clear)  # 清除所有通知


def auto_lock_switch(status):
    click_element(lock_setting_setting_button)  # 点击设置按钮
    click_element(lock_setting_lock_setting)  # 点击锁设置按钮
    time.sleep(60)
    if get_element(lock_lockSetting_auto_lock_status).text == status:
        pass
    else:
        click_element(lock_lockSetting_auto_lock)  # 自动重新上锁
        click_element(lock_lockSetting_auto_lock_switch)  # 点击自动上锁开关
        click_element(lock_lockSetting_switch_save)  # 自动上锁开关保存
    driver.press_keycode(4)
    click_element(lock_main_lock_manager)  # 点击锁按钮
    check_lock_connection()


def delete_guest(user_name):
    # 删除访客
    click_element(UserManagement_user_list)  # 点击【用户列表】
    time.sleep(1)
    click_element(UserManagement_user_admin.format(user_name))  # 点击访客
    click_element(UserManagement_delete_user)  # 点击删除用户
    click_element(userManagement_invite_confirm)  # 确认删除


def check_lock_connection2():
    # 检查锁的连接
    start_time_lock_connect = time.time()
    while 1:
        if time.time() - start_time_lock_connect >= 30:
            log.terminate()
            assert False
        else:
            try:
                if (__check_element(lock_main_lock_grey_status, wait_time=2, check_type=False) is False) and (__check_element(lock_main_lock_connect_status, wait_time=2, check_type=False) is False):  # 无法连接
                    break
                else:
                    click_element(lock_main_user_lock_button)  # 无法连接时点击锁
                    time.sleep(5)
            except:
                pass


def scroll_screen_up():
    start_x = driver.get_window_size()['width'] * 0.5
    start_y = driver.get_window_size()['height'] * 0.7
    end_x = start_x
    end_y = driver.get_window_size()['height'] * 0.3
    driver.swipe(int(start_x), int(start_y), int(end_x), int(end_y), 500)


def check_element(content_text, wait_time=30, check_type=True):
    start_time = time.time()
    while 1:
        if time.time() - start_time >= wait_time:
            if check_type is True:
                log.terminate()
                assert False
            else:
                return False
        else:
            try:
                driver.find_element_by_id(content_text)
            except:
                pass
            else:
                return 'id'
            try:
                driver.find_element_by_xpath(content_text)
            except:
                pass
            else:
                return 'xpath'


def add_smart_alert(username, time_type='anytime', what_time='default'):
    click_element(lock_setting_setting_button)  # 点击设置按钮
    click_element(lock_setting_lock_setting, after_action_sleep=15)  # 点击锁设置
    click_element(lock_lockSetting_alarm)  # 点击智能提醒
    click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
    click_element(lock_lockSetting_choose_event)  # 点击选择活动
    click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
    click_element(UserManagement_user_admin.format(username))  # 点击登录账号
    click_element(lock_lockSetting_choose_time)  # 点击选择时间
    if time_type == 'anytime':
        click_element(lock_lockSetting_any_time)  # 选择任何时候
    elif time_type == 'fixedtime':
        click_element(lock_lockSetting_fixed_time)  # 选择固定时间
        if what_time =='default':
            click_element(notification_main_fix_time_sa)
            click_element(notification_main_smartalert_start_time)   # 点击开始时间
            click_element(notification_main_fix_time_1)  # 点击1点
            click_element(notification_main_confirm_time)  # 点击确定
            click_element(notification_main_smartalert_end_time)  # 点击结束时间
            click_element(notification_main_fix_time_23)  # 点击23点
            click_element(notification_main_fix_time_55)  # 点击55分
            click_element(notification_main_confirm_time)  # 点击确定
        elif what_time =='future':
            click_element(notification_main_fix_time_su)
            click_element(notification_main_fix_time_mo)
            click_element(notification_main_fix_time_tu)
            click_element(notification_main_fix_time_we)
            click_element(notification_main_fix_time_th)
            click_element(notification_main_fix_time_fr)
        else:
            raise Exception('what_time error:{}'.format(what_time))
    else:
        raise Exception('time_type error:{}'.format(time_type))
    click_element(lock_lockSetting_alert_save_button)  # 保存
    click_element(lock_lockSetting_add_alert_back, after_action_sleep=15)  # 返回
    click_element(login_login_back_button)  # 返回
    click_element(lock_main_lock_manager)  # 点击锁按钮
    check_lock_connection()


def check_lock_connection():
    # 检查锁的连接
    start_time_lock_connect = time.time()
    while 1:
        if time.time() - start_time_lock_connect >= 60:
            log.terminate()
            assert False
        else:
            try:
                if (__check_element(lock_main_lock_grey_status, wait_time=3, check_type=False) is False) \
                   and (__check_element(lock_main_lock_connect_status, wait_time=3, check_type=False) is False)\
                   and (__check_element(homePage_else_find_many_locks, wait_time=3, check_type=False) is False)\
                   and (__check_element(lock_main_lock_unknown_status, wait_time=3, check_type=False) is False):
                    break
                else:
                    if __check_element(homePage_else_find_many_locks, wait_time=1, check_type=False) is False:
                        click_element(lock_main_user_lock_button)  # 无法连接时点击锁
                        time.sleep(5)
                    else:
                        if __check_element(lock_main_low_battery_warn, wait_time=1, check_type=False) is False:
                            click_element(userManagement_invite_popup_confirm)   # 点击尝试连接
                            click_element(userManagement_invite_popup_confirm)  # 点击站在门旁边
                            click_element(userManagement_invite_popup_cancel)  # 取消修复连接
                            click_element(lock_main_user_lock_button)  # 无法连接时点击锁
                            time.sleep(5)
                        else:
                            click_element(userManagement_invite_popup_cancel)  # 我已经换了电池
                            click_element(lock_main_user_lock_button)  # 无法连接时点击锁
                            time.sleep(5)
            except:
                pass


def scroll_and_rescan(content_text, target_content, wait_time=30):
    # 确认整体元素已显示
    __check_element(content_text, wait_time=wait_time)
    # 对具体的元素进行操作
    count = 0
    while 1:
        # 对目标元素进行点击，找不到则滚屏；若超过9次则认为没有定位到该元素
        try:
            driver.find_element_by_xpath(target_content)
        except:
            count += 1
            if count > 9:
                click_element(userManagement_invite_popup_confirm)  # 重新扫描
                start_time_rescan = time.time()
                while 1:
                    if time.time() - start_time_rescan >= 60:
                        log.terminate()
                        assert False
                    else:
                        try:
                            driver.find_element_by_xpath(target_content)
                        except:
                            scroll_screen_up()  # 上滑
                        else:
                            driver.find_element_by_xpath(target_content).click()
                            break
                break
            else:
                start_x = driver.get_window_size()['width'] * 0.5
                start_y = driver.get_window_size()['height'] * 0.7
                end_x = start_x
                end_y = driver.get_window_size()['height'] * 0.3
                driver.swipe(int(start_x), int(start_y), int(end_x), int(end_y), 500)
        else:
            driver.find_element_by_xpath(target_content).click()
            break
