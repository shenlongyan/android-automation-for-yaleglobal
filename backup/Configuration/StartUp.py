from appium import webdriver
from Configuration.Parameter import MobileInfo, APKInfo


def start_up():
    desired_caps = {'deviceName': MobileInfo.serial_number,
                    'platformName': MobileInfo.platform_name,
                    'platformVersion': MobileInfo.platform_version,
                    'appPackage': APKInfo.package_name,
                    'appActivity': APKInfo.activity_name,
                    'dontStopAppOnReset': True,
                    'noReset': True,
                    'newCommandTimeout': 36000,
                    'adbExecTimeout': 100000}
    # print(desired_caps)
    driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
    return driver


driver = start_up()
