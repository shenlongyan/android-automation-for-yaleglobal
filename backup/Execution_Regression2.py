from Configuration.Jenkins import *
from Configuration.Parameter import *
import time
import unittest
from HTMLTestRunner_cn import HTMLTestRunner
from Configuration import FolderList
from Configuration.ReportModify import add_log_link, cancel_pass_link
from Configuration.testrail import *
from TestCase.test_lock_register import TestRegister
from TestCase.test_lock_setting import TestLockSetting
from TestCase.test_lock_toBind import TestAddDevice
from TestCase.test_lock_toPush import TestSmartAlert
from TestCase.test_login import TestLogin
from TestCase.test_page_home import TestHomePage
from TestCase.test_user_management import TestUserManagement
# from TestCase.test_bridge import TestBridge
from TestCase.test_base_setup import TestSetup
from TestCase.test_contactless_key import TestRFID
from TestCase.test_credential_fingerprint import TestFP
from TestCase.test_credential_schedule import TestSchedule
import os
import re
import shutil
from Configuration.OperationMethod import depend_on

# # 0.多个锁连续跑，获取jenkins中所有锁的参数
# runIDList1 = os.environ['TestRunID']
# runIDList = runIDList1.split(',')
#
# test_jig_lock_id_List1 = os.environ['test_jig_lock_id']  # test_jig的端口号
# test_jig_lock_id_List = test_jig_lock_id_List1.split(',')
#
# test_jig_ip_List1 = os.environ['test_jig_ip']  # test_jig的ip地址
# test_jig_ip_List = test_jig_ip_List1.split(',')
#
# selected_lock_id_List1 = os.environ['LockID']
# selected_lock_id_List = selected_lock_id_List1.split(',')
#
# selected_serial_number_List1 = os.environ['SerialNumber']
# selected_serial_number_List = selected_serial_number_List1.split(',')

# print(runIDList[1], test_jig_lock_id_List[2], test_jig_ip_List[1], selected_lock_id_List[1], selected_serial_number_List[1])

RunOnWhere = 'Server'
# 1.从测试jenkins获取环境变量:jobPath,TestRunID,lockID,test_jig_ip,test_jig_lock_id
# print(os.environ)
if RunOnWhere == 'Server':
    # 第七处修改 # run on server
    jobPath = os.environ['WORKSPACE']
    # for i in runIDList:
    #     runID = i
    #     print('当前testrun的ID是' + runID)
    runID = os.environ['TestRunID']
elif RunOnWhere == 'local':
    # run on local
    jobPath = 'C:\\Users\\aa\\androidregression0228'
    runID = '877'     # '81'
else:
    raise ReferenceError


# 2.从开发jenkins自动下载最新版本apk并安装
if RunOnWhere == 'Server':
    # 第八处修改 # run on server
    DownloadAPK = os.environ['DownloadAPK']
elif RunOnWhere == 'local':
    # run on local
    DownloadAPK = False
else:
    raise ReferenceError
if DownloadAPK is True:
    Download_Jenkins_Build()
else:
    pass


# 3.获取自动化脚本中所有case
# caseList1, caseList2, caseList3, caseList4, caseList5, caseList6, caseList7, caseInAutomation = [], [], [], [], [], [], [], []
# caseList1 = TestRegister().cases()
caseList2 = TestLockSetting().cases()
# caseList3 = TestAddDevice().cases()
# caseList4 = TestSmartAlert().cases()
# caseList5 = TestLogin().cases()
# caseList6 = TestHomePage().cases()
# caseList7 = TestUserManagement().cases()
# caseList8 = TestBridge().cases()
caseList9 = TestSetup().cases()
caseList10 = TestRFID().cases()
caseList11 = TestFP().cases()
caseList12 = TestSchedule().cases()
# caseAutomation = caseList2 + caseList3 + caseList4 + caseList7
caseAutomation = caseList2 + caseList9 + caseList10 + caseList11 + caseList12
caseInAutomation = []
for i in range(len(caseAutomation)):
    if caseAutomation[i].startswith('test_C'):
        automation = caseAutomation[i].strip('test_')
    elif caseAutomation[i].startswith('test_D'):
        automation = caseAutomation[i].strip('test_D')
    else:
        print('case name error')
    caseInAutomation.append(automation)
# print(caseInAutomation)
# print(len(caseInAutomation))


# 4.根据testrunID加载测试用例
client = APIClient('https://assaabloyapac.testrail.io/')
client.user = 'Longyan.Shen@assaabloy.com'
client.password = 'eh2BLfThFs07D.ucWm3v-NBZtTlQ18nFFvzuVXyXS'
# runID = 82
# for i in range(len(runIDList)):
#     runID = runIDList[i]
#     test_jig_lock_id = test_jig_lock_id_List[i]
#     test_jig_ip = test_jig_ip_List[i]
#     selected_lock_id = selected_lock_id_List[i]
#     selected_serial_number = selected_serial_number_List[i]
#     print(runID, selected_lock_id, test_jig_ip, test_jig_lock_id, selected_serial_number)
case = client.send_get('get_tests/' + str(runID))
caseList = []
caseIDList = []
isManualCase = []
isAutomationCase = []
for i in case:
    for key, value in i.items():
        # print(key, value)
        if key == 'case_id':
            caseList.append('C' + str(value))
            caseIDList.append(value)
suite = unittest.TestSuite()
flag1, flag2, flag3, flag4 = 1, 1, 1, 1
# 从testrail中筛选自动化case和手动case
for c in caseList:
    if c in caseInAutomation:
        isAutomationCase.append(c)
    else:
        isManualCase.append(c)
# print('all cases for automation test:' + str(isAutomationCase))
print('all cases for manual test:' + str(isManualCase))
for c in isAutomationCase:
    # if 'C228' <= c <= 'C360':
    #     suite.addTest(TestRegister('test_' + c))
    # elif 'C410' <= c <= 'C491':
    #     suite.addTest(TestLogin('test_' + c))
    # elif 'C492' <= c <= 'C526':
    #     suite.addTest(TestHomePage('test_' + c))
    # if 'C529' <= c <= 'C573':
    #     suite.addTest(TestAddDevice('test_' + c))
    if len(c) == 4 and 'C574' <= c <= 'C675':
        if flag1:
            # suite.addTest(TestUserManagement('test_D000'))
            suite.addTest(TestLockSetting('test_C000'))
            flag1 = 0
        suite.addTest(TestLockSetting('test_' + c))
    # elif 'C826' <= c <= 'C904':
    #     if flag2:
    #         # suite.addTest(TestLockSetting('test_D000'))
    #         suite.addTest(TestSmartAlert('test_C000'))
    #         flag2 = 0
    #     suite.addTest(TestSmartAlert('test_' + c))
    # elif 'C916' <= c <= 'C999':
    #     if flag3:
    #         suite.addTest(TestUserManagement('test_C000'))
    #         flag3 = 0
    #     suite.addTest(TestUserManagement('test_' + c))
    # elif 'C1000' <= c <= 'C1062':
    #     suite.addTest(TestUserManagement('test_D' + c))
        # elif 'C7413' <= c <= 'C7536':
        #     suite.addTest(TestBridge('test_' + c))
    elif 'C56562' <= c <= 'C56580':
        suite.addTest(TestSetup('test_' + c))
    elif 'C57850' <= c <= 'C57941':
        suite.addTest(TestRFID('test_' + c))
    elif 'C58019' <= c <= 'C58118':
        suite.addTest(TestFP('test_' + c))
    elif 'C58140' <= c <= 'C58409':
        suite.addTest(TestSchedule('test_' + c))
    else:
        # raise Exception('{} not exist'.format(c))
        print('{} not exist'.format(c))
    # suite.addTest(TestSmartAlert('test_D000'))


    # # 批量加载测试用例
    # case_path = os.path.join(os.getcwd(), 'TestCase')
    # case_path = 'D:\jenkins\workspace\joyce_test\TestCase'
    # print('Check lock id & phone permission & wifi environment')
    # print('Files_path:', os.getcwd())
    # print('Cases_path:', case_path)
    # discover = unittest.defaultTestLoader.discover(case_path, pattern="test_lock_toBind.py", top_level_dir=None)


# 5.执行测试并生成html报告
if __name__ == '__main__':
    # reportPath = os.path.abspath(os.path.join(os.getcwd(), ".."))
    reportPath = jobPath + '\Report'
    os.chdir(reportPath)
    retry_times = 0
    report_name = FolderList.report_dir
    result_description = 'Mobile Version:{};\r\n' \
                        'OS Version:{};\r\n' \
                        'App Version:{};'.format(MobileInfo.mobile_name,
                                                 MobileInfo.platform_version,
                                                 APKInfo.version_name)
    with open(report_name, 'wb') as f:
        runner = HTMLTestRunner(stream=f, title='Test Result_Report', description=result_description,
                                verbosity=3, retry=retry_times, save_last_try=True)
        # runner.run(suite)
        # global result
        result = runner.run(suite)
        print('error用例数:' + str(len(result.errors)))
        print('failure用例数:' + str(len(result.failures)))
        # print(result.failures[0][0])
        # print(result.wasSuccessful())
    f.close()

    # def depend_on(CaseName):
    #     def decorator(func):
    #         def wrap_the_func():
    #             if CaseName in result.failures[0] or result.errors[0]:
    #                 assert False
    #             else:
    #                 pass
    #         return wrap_the_func
    #     return decorator

    #     # start_time_cancel_link = time.time()
    #     # cancel_pass_link(report_name)
    #     # print('Cancel link time spend:{}'.format(time.time() - start_time_cancel_link))
    #     # start_time_add_log = time.time()
    #     # add_log_link(report_name, retry_times)
    #     # print('Add log time spend:{}'.format(time.time() - start_time_add_log))


# 6.把测试结果写回testrail
with open(report_name, 'r', encoding='utf-8') as r:
    file_result = r.read()
r.close()
res_pass = r'<td><div class=\'testcase\'>(.*?)</div></td>(.*?)<span class=\'status passCase\'>Pass</span></td>'
tc_pass = re.findall(res_pass, file_result, flags=re.M | re.S)
pass_cases = []
retest_cases = []
for i in tc_pass:
    if str(i[0]).startswith('test_C'):
        if str(i[0]) == 'test_C000':
            pass
        else:
            temp = str(i[0]).strip('test_C')
            passCaseID = int(temp)
            pass_cases.append(passCaseID)
    elif str(i[0]).startswith('test_D'):
        if str(i[0]) == 'test_D000':
            pass
        else:
            temp = str(i[0]).strip('test_DC')
            passCaseID = int(temp)
            pass_cases.append(passCaseID)
pass_list = []
# retest_list = []
# 新增pendinglist 2020.11.19
pending_cases1 = [57881, 57924]  # depend on 57873
pending_cases2 = [56566, 56575]  # depend on 56562
pending_cases3 = [57915, 57941]  # depend on 57896
pending_cases4 = [57920]  # depend on 57912
pending_cases5 = [58286]  # depend on 58238
pending_cases6 = [58287]  # depend on 58252
if 57873 in pass_cases:
    pass_cases.extend(pending_cases1)
elif 56562 in pass_cases:
    pass_cases.extend(pending_cases2)
elif 57896 in pass_cases:
    pass_cases.extend(pending_cases3)
elif 57912 in pass_cases:
    pass_cases.extend(pending_cases4)
elif 58238 in pass_cases:
    pass_cases.extend(pending_cases5)
elif 58252 in pass_cases:
    pass_cases.extend(pending_cases6)
else:
    pass

for case in caseIDList:
    if case in pass_cases:
        pass_list.append({'case_id': case, 'status_id': 1, 'comment': 'passed by Android automation scripts on App Version ' + '2.10.0' + ' Build ' + '146'})
    # else:
    #     retest_list.append({'case_id': case, 'status_id': 4, 'comment': 'need manual test on App Version ' + APKInfo.version_name + ' Build ' + APKInfo.build_number})
result_pass = client.send_post('add_results_for_cases/' + str(runID), data={'results': pass_list})
# # result_retest = client.send_post('add_results_for_cases/' + str(runID), data={'results': retest_list})


# 7.移动testReport到workspace根目录下
# shutil.move(report_dir, jenkins_path)
shutil.copy(FolderList.report_dir, jobPath)
