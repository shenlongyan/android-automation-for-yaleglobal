import json
import redis
import time


def verification_code_phone(phone_number):
    pool = redis.ConnectionPool(host='localhost', port=16379, decode_responses=True)
    red = redis.Redis(connection_pool=pool)
    start_time = time.time()
    verification_code = ''
    search_phone = 'aep:phone:+86{}'.format(phone_number)
    while 1:
        try:
            json.loads(red.get(search_phone))['code']
        except:
            if time.time() - start_time >= 60:
                print('get phone verification code time out')
                break
            else:
                pass
        else:
            verification_code = json.loads(red.get(search_phone))['code']
            break
    red.close()
    return verification_code


def verification_code_email(email_address):
    pool = redis.ConnectionPool(host='localhost', port=16379, decode_responses=True)
    red = redis.Redis(connection_pool=pool)
    start_time = time.time()
    verification_code = ''
    search_email = 'aep:email:{}'.format(email_address)
    while 1:
        try:
            json.loads(red.get(search_email))['code']
        except:
            if time.time() - start_time >= 60:
                print('get email verification code time out')
                break
            else:
                pass
        else:
            verification_code = json.loads(red.get(search_email))['code']
            break
    red.close()
    return verification_code
