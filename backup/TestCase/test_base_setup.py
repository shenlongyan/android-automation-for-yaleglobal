import logging
import unittest
from Configuration.OperationMethod import *
from Configuration import StartUp
import random
import os


RunOnWhere = 'Server'
if RunOnWhere == 'Server':
    # 第六处修改 # run on server
    autoLock = os.environ['AutoLock']
    print('autoLock=' + autoLock)
elif RunOnWhere == 'local':
    # run on local
    autoLock = True
else:
    raise ValueError


class TestSetup(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = StartUp.driver
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H_%M_%S',
                            filename=FolderList.logging_name,
                            filemode='a')
        logging.getLogger()
        time.sleep(1)

    def add_img(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

    def setUp(self) -> None:
        os.system('adb shell svc wifi enable')
        bt_switch('on')
        # add_card_yale()
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    @adb_log
    def test_C56562(self):
        pass
        # CAN'T SCAN THE CODE? Button
        # # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # # click_element(homePage_homePage_menu_in_home_page)  # click [HOME]
        # click_element(homePage_homePage_add_device)  # click [set up new device]
        # click_element(homePage_else_add_device_text)  # click [can't scan the code?]
        # assert_result_displayed(homePage_else_enter_serial_number, True)  # Enter the serial number page
        # send_keys(homePage_else_enter_serial_number, selected_serial_number)  # Enter serial number
        # click_element(homePage_else_lock_continue)  # click continue
        # # click_element(homePage_else_set_device_image)  # click set device image
        # click_element(homePage_else_lock_start)  # click start
        # click_element(homePage_else_lock_start)  # click start
        # click_element(homePage_else_lock_install)  # 开始安装
        # send_keys(homePage_else_choose_lock_name, selected_serial_number)  # 锁命名
        # click_element(homePage_else_choose_name_radiobutton)  # 添加新住宅
        # click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
        # send_keys(homePage_else_house_new_name, selected_serial_number + ' ' + random.choice('abcdefghijklmnopqrstuvwxyz0123456789')) # 住宅命名
        # click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
        # time.sleep(60)
        # click_element(homePage_else_lock_start)  # 点击【试试】按钮
        # assert_result_text(lock_main_my_lock_name, selected_serial_number)  # display lock name
        # assert_result_displayed(homePage_else_my_house_name, True)  # 显示我的住宅名

    @depend_on('test_C56562')
    def test_C56566(self):
        # Enter the correct serial Number
        pass

    @depend_on('test_C56562')
    def test_C56575(self):
        # Continue button
        pass

    @adb_log
    def test_C56580(self):
        # Select lock type
        click_element(homePage_homePage_menu_in_other_page)  # click [HOME]
        click_element(homePage_menu_add_device)  # click [set up new device]
        click_element(homePage_else_add_device_text)  # click [can't scan the code?]
        click_element(homePage_else_manual_scan)  # 手动选择我的设备
        click_element(homePage_else_set_device_text)  # 点击选择智能门锁
        click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
        click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
        click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
        click_element(homePage_else_lock_install)  # 点击开始安装按钮
        assert_result_displayed(homePage_else_scan_smartlock_progress, True)  # 显示扫描智能锁进度
        assert_result_displayed(homePage_else_find_many_locks, True)  # 显示找到多把锁弹窗


    def cases(self):
        return list(filter(lambda m: m.startswith("test_C") and callable(getattr(self, m)), dir(self)))
