import logging
import unittest
from Configuration.OperationMethod import *
from Configuration import StartUp


class TestBridge(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = StartUp.driver
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H_%M_%S',
                            filename=FolderList.logging_name,
                            filemode='a')
        logging.getLogger()
        time.sleep(1)

    def add_img(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

    def setUp(self) -> None:
        os.system('adb shell svc wifi enable')
        bt_switch('on')
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    @adb_log
    def test_C7413(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 绑定bridge
            login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 菜单
            click_element(homePage_menu_add_device)  # 添加设备
            click_element(homePage_else_add_bridge_text)  # 添加智能附件
            click_element(homePage_else_venus_setup)  # 设置网关
            click_element(homePage_else_venus_setup)  # 网关已经插好了
            click_element(homePage_else_venus_setup)  # 灯是绿色的
            try:
                click_element(homePage_else_venus_serial)  # 选择网关C5WB20000J
            except:
                pass
            click_element(homePage_else_venus_setup)  # 找到了网关
            click_element(homePage_else_venus_house)  # 选择房间和锁
            click_element(homePage_else_select_network)  # 选择网络
            click_element(homePage_else_network_continue)  # 继续
            send_keys(homePage_else_input_password, 'assa2020')  # 输入密码
            click_element(userManagement_invite_confirm)  # 完成
            click_element(homePage_else_allow_open_bt)  # 允许打开网络
            click_element(homePage_else_allow_open_bt)  # 允许打开网络
            try:
                click_element(homePage_else_allow_open_bt)  # 允许打开网络
            except:
                pass
            # try:
            #     send_keys(homePage_else_input_password, 'assa2020')  # 输入密码
            #     click_element(userManagement_invite_confirm)  # 完成
            # except:
            #     pass
            time.sleep(30)
            click_element(homePage_else_venus_finish)  # 关闭
            click_element(userManagement_invite_confirm)  # 确认退出
            try:
                click_element(homePage_else_allow_open_bt)  # 允许打开网络
            except:
                pass
            # logout('home')
        else:
            assert False

    @adb_log
    def test_C7536(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 开关锁_bridge
            login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            bt_switch('off')  # 关闭蓝牙
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(homePage_else_venus_icon)  # 点击网关图标
                click_element(userManagement_invite_popup_cancel)  # 明白了
                time.sleep(10)
            click_element(lock_main_user_lock_button)  # 点击锁
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            bt_switch('on')  # 打开蓝牙
        else:
            assert False

    def cases(self):
        return list(filter(lambda m: m.startswith("test_C") and callable(getattr(self, m)), dir(self)))

    @adb_log
    def test_log_tc001(self):
        login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        click_element(homePage_homePage_home_list)  # 点击住宅图片
        invite_user_yale('admin', UserInfo.AdminJoyce.phone2, UserInfo.AdminJoyce.name2)  # 邀请管理员B
        auto_lock('off')
        click_element(user_button_lock)  # 点击【锁】
        assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
        click_element(lock_button_open_lock, after_action_sleep=10)  # 点击【开锁】
        click_element(lock_button_open_lock)  # 点击【关锁】
        assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
        log_assert_time()
        log_assert_text(log_assert_manual_lock_close.format(selected_lock_name), need_refresh=False)
        log_assert_text(UserInfo.First.name, need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc002(self):
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        auto_lock('off')
        click_element(user_button_lock)  # 点击【锁】
        assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
        click_element(lock_button_open_lock)  # 点击【关锁】
        log_assert_time()
        log_assert_text(log_assert_manual_lock_open.format(selected_lock_name), need_refresh=False)
        log_assert_text(UserInfo.First.name, need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc003(self):
        # 管理员
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        auto_lock('off')
        logout('house')
        # 普通用户
        login(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'phone')
        click_element(homepage_button_select_room_or_user.format(HouseInfo.name))  # 选择家
        click_element(user_button_lock)  # 点击【锁】
        assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
        click_element(lock_button_open_lock, after_action_sleep=10)  # 点击【开锁】
        click_element(lock_button_open_lock)  # 点击【关锁】
        logout('house')
        # 管理员
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        click_element(homepage_button_select_room_or_user.format(HouseInfo.name))  # 选择家
        log_assert_time()
        log_assert_text(log_assert_manual_lock_close.format(selected_lock_name), need_refresh=False)
        log_assert_text(UserInfo.Second.name, need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc004(self):
        # 管理员
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        auto_lock('off')
        logout('house')
        # 普通用户
        login(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'phone')
        click_element(homepage_button_select_room_or_user.format(HouseInfo.name))  # 选择家
        click_element(user_button_lock)  # 点击【锁】
        assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
        click_element(lock_button_open_lock)  # 点击【开锁】
        logout('house')
        # 管理员
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        click_element(homepage_button_select_room_or_user.format(HouseInfo.name))  # 选择家
        log_assert_time()
        log_assert_text(log_assert_manual_lock_open.format(selected_lock_name), need_refresh=False)
        log_assert_text(UserInfo.Second.name, need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc005(self):
        # 管理员
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        auto_lock('on')
        # 管理员
        invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        add_pin(UserInfo.Second.name, UserInfo.Second.pin)
        add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb)
        click_element(user_button_lock)  # 点击【锁】
        assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
        requests.put(url=API.request_api_finger_open.format(UserInfo.Second.name, UserInfo.Second.finger_thumb))  # 指纹开门
        assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
        log_assert_time()
        log_assert_text(log_assert_manual_lock_open.format(selected_lock_name), wait_time=60, need_refresh=False)
        log_assert_text(UserInfo.Second.name, wait_time=60, need_refresh=False)
        factory_lock()
        # 普通用户
        add_lock()
        invite_user('normal', UserInfo.Third.phone, UserInfo.Third.name, True, 'always')
        add_pin(UserInfo.Third.name, UserInfo.Third.pin, 'normal')
        add_finger(UserInfo.Third.name, UserInfo.Third.name, UserInfo.Third.finger_thumb, 'normal')
        click_element(user_button_lock)  # 点击【锁】
        assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁关闭】
        requests.put(url=API.request_api_finger_open.format(UserInfo.Third.name, UserInfo.Third.finger_thumb))  # 指纹开门
        assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
        log_assert_time()
        log_assert_text(UserInfo.Third.name, 1, wait_time=60, need_refresh=False)
        log_assert_text(log_assert_manual_lock_open.format(selected_lock_name), wait_time=60, need_refresh=False)
        factory_lock()
        logout()

    @unittest.skip(reason='no pin code keyboard')
    @adb_log
    def test_log_tc006(self):
        # 管理员
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        auto_lock('on')
        # 管理员
        invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        add_pin(UserInfo.Second.name, UserInfo.Second.pin)
        add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb)
        click_element(user_button_lock)  # 点击【锁】
        assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
        # Todo 密码开门
        assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
        log_assert_time()  # Todo 更新断言
        log_assert_text(log_assert_manual_lock_open.format(selected_lock_name), wait_time=60, need_refresh=False)
        log_assert_text(UserInfo.Second.name, wait_time=60, need_refresh=False)
        factory_lock()
        # 普通用户
        add_lock()
        invite_user('normal', UserInfo.Third.phone, UserInfo.Third.name, True, 'always')
        add_pin(UserInfo.Third.name, UserInfo.Third.pin, 'normal')
        add_finger(UserInfo.Third.name, UserInfo.Third.name, UserInfo.Third.finger_thumb, 'normal')
        click_element(user_button_lock)  # 点击【锁】
        assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁关闭】
        # Todo 密码开门
        assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
        log_assert_time()  # Todo 更新断言
        log_assert_text(UserInfo.Third.name, 1, wait_time=60, need_refresh=False)
        log_assert_text(log_assert_manual_lock_open.format(selected_lock_name), wait_time=60, need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc007(self):
        # 管理员
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        auto_lock('on')
        # 管理员
        invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        add_pin(UserInfo.Second.name, UserInfo.Second.pin)
        add_card(UserInfo.Second.name)
        click_element(user_button_lock)  # 点击【锁】
        assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
        requests.put(url=API.request_api_card_open.format)  # 门卡开门
        assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
        log_assert_time()
        log_assert_text(log_assert_manual_lock_open.format(selected_lock_name), wait_time=60, need_refresh=False)
        log_assert_text(UserInfo.Second.name, wait_time=60, need_refresh=False)
        factory_lock()
        # 普通用户
        add_lock()
        invite_user('normal', UserInfo.Third.phone, UserInfo.Third.name, True, 'always')
        add_pin(UserInfo.Third.name, UserInfo.Third.pin, 'normal')
        add_card(UserInfo.Third.name, 'normal')
        click_element(user_button_lock)  # 点击【锁】
        assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁关闭】
        requests.put(url=API.request_api_card_open)  # 门卡开门
        assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
        log_assert_time()
        log_assert_text(UserInfo.Third.name, wait_time=60, need_refresh=False)
        log_assert_text(log_assert_manual_lock_open.format(selected_lock_name), wait_time=60, need_refresh=False)
        factory_lock()
        logout()

    @unittest.skip(reason='no pin code keyboard')
    @adb_log
    def test_log_tc011(self):
        # 管理员
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        auto_lock('on')
        # 管理员
        invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        add_pin(UserInfo.Second.name, UserInfo.Second.pin)
        add_card(UserInfo.Second.name)
        click_element(user_button_lock)  # 点击【锁】
        assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
        # Todo 密码开门
        assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
        log_assert_time()  # Todo 更新断言
        log_assert_text(log_assert_manual_lock_open.format(selected_lock_name), wait_time=60, need_refresh=False)
        log_assert_text(UserInfo.Second.name, wait_time=60, need_refresh=False)
        factory_lock()
        # 访客
        add_lock()
        invite_user('guest', UserInfo.Third.phone, UserInfo.Third.name, True, 'always')
        add_pin(UserInfo.Third.name, UserInfo.Third.pin, 'guest')
        add_card(UserInfo.Third.name, 'guest')
        click_element(user_button_lock)  # 点击【锁】
        assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁关闭】
        # Todo 密码开门
        assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
        log_assert_time()  # Todo 更新断言
        log_assert_text(UserInfo.Third.name, wait_time=60, need_refresh=False)
        log_assert_text(log_assert_manual_lock_open.format(selected_lock_name), wait_time=60, need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc014(self):
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        click_element(user_compound_user_normal.format(UserInfo.Second.name))  # 点击普通用户
        click_element(user_assert_user_level_more)  # 点击【权限等级】
        click_element(user_button_user_level_admin)  # 点击【管理员】
        click_element(user_button_user_level_save)  # 点击【保存】
        log_assert_time()
        log_assert_text(log_assert_level_change_permission_add.format(selected_lock_name, UserInfo.Second.name), need_refresh=False)
        log_assert_text(UserInfo.First.name, need_refresh=False)
        click_element(user_button_user)  # 点击【用户】
        click_element(user_compound_user_admin.format(UserInfo.Second.name))  # 点击管理员
        click_element(user_assert_user_level_more)  # 点击【权限等级】
        click_element(user_button_user_level_normal)  # 点击【普通用户】
        click_element(user_button_user_level_save)  # 点击【保存】
        log_assert_text(log_assert_level_change_permission_delete.format(selected_lock_name, UserInfo.Second.name), 1)  # 此处放在第一条，表示有新增记录
        log_assert_time(need_refresh=False)
        log_assert_text(log_assert_level_change_permission_add.format(selected_lock_name, UserInfo.Second.name), need_refresh=False)
        log_assert_text(UserInfo.First.name, need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc015(self):
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
        log_assert_time()
        log_assert_text(log_assert_pin_or_user_add.format(selected_lock_name, UserInfo.Second.name), need_refresh=False)
        log_assert_text(UserInfo.First.name, need_refresh=False)
        add_pin(UserInfo.Second.name, UserInfo.Second.pin_new, 'normal')  # 修改密码
        log_assert_text(log_assert_pin_delete.format(selected_lock_name, UserInfo.Second.name), 1)  # 此处放在第一条，表示有新增记录
        log_assert_time(need_refresh=False)
        log_assert_text(log_assert_pin_or_user_add.format(selected_lock_name, UserInfo.Second.name), need_refresh=False)
        log_assert_text(UserInfo.First.name, need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc016(self):
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
        click_element(user_compound_user_normal.format(UserInfo.Second.name))  # 选择普通用户
        click_element(user_button_user_pin)  # 点击【密码】
        click_element(user_button_user_pin_release)  # 点击【解除密码】
        click_element(user_compound_user_normal.format(UserInfo.Second.name))  # 选择普通用户
        click_element(user_button_user_pin)  # 点击【密码】
        click_element(user_button_user_pin_save)  # 点击【激活密码】
        log_assert_time()
        log_assert_text(log_assert_pin_activity.format(selected_lock_name, UserInfo.Second.name), need_refresh=False)
        log_assert_text(UserInfo.First.name, need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc017(self):
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
        click_element(user_compound_user_normal.format(UserInfo.Second.name))  # 选择普通用户
        click_element(user_button_user_pin)  # 点击【密码】
        click_element(user_button_user_pin_release)  # 点击【解除密码】
        log_assert_time()
        log_assert_text(log_assert_pin_release.format(selected_lock_name, UserInfo.Second.name), need_refresh=False)
        log_assert_text(UserInfo.First.name, need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc020(self):
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
        add_card(UserInfo.Second.name, 'normal')
        log_assert_time()
        log_assert_text(log_assert_card_add.format(selected_lock_name, UserInfo.Second.name), need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc021(self):
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
        add_card(UserInfo.Second.name, 'normal')
        click_element(user_button_user)  # 点击【用户】
        click_element(user_compound_user_normal.format(UserInfo.Second.name))  # 选择普通用户
        click_element(user_button_user_card)  # 点击【门卡管理】
        click_element(user_button_user_finger_and_card_remove)  # 点击【删除】
        click_element(homePage_else_sure_or_try)  # 点击【确定】
        click_element(homepage_button_detail_back)  # 点击【返回】
        log_assert_time()
        log_assert_text(log_assert_card_delete.format(selected_lock_name, UserInfo.Second.name), need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc022(self):
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        invite_user('guest', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        log_assert_time()
        log_assert_text(log_assert_pin_or_user_add.format(selected_lock_name, UserInfo.Second.name), need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc023(self):
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
        add_lock()
        invite_user('guest', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        click_element(user_compound_user_guest.format(UserInfo.Second.name))  # 选择访客
        click_element(user_button_user_pin)  # 点击【密码】
        click_element(user_button_user_pin_release)  # 点击【解除密码】
        click_element(user_compound_user_guest.format(UserInfo.Second.name))  # 选择访客
        click_element(user_button_remove_user)  # 点击【移除用户】
        click_element(user_button_factory_reset_and_delete_yes)  # 点击【删除】
        log_assert_time()
        log_assert_text(log_assert_pin_delete.format(selected_lock_name, UserInfo.Second.name), need_refresh=False)
        log_assert_text(UserInfo.First.name, need_refresh=False)
        log_assert_text(log_assert_level_change_permission_delete.format(selected_lock_name, UserInfo.Second.name), 1, need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc024(self):
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
        add_lock()
        invite_user('guest', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'guest')
        log_assert_text(log_assert_pin_or_user_add.format(selected_lock_name, UserInfo.Second.name))
        log_assert_text(UserInfo.First.name, need_refresh=False)
        add_pin(UserInfo.Second.name, UserInfo.Second.pin_new, 'guest')
        add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'guest')
        log_assert_time()
        log_assert_text(log_assert_pin_or_user_add.format(selected_lock_name, UserInfo.Second.name), need_refresh=False)
        log_assert_text(UserInfo.First.name, need_refresh=False)
        log_assert_text(log_assert_pin_delete.format(selected_lock_name, UserInfo.Second.name), 1, need_refresh=False)
        factory_lock()
        logout()

    @adb_log
    def test_log_tc025(self):
        login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
        add_lock()
        invite_user('guest', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
        add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'guest')
        click_element(user_compound_user_guest.format(UserInfo.Second.name))  # 选择访客
        click_element(user_button_user_pin)  # 点击【密码】
        click_element(user_button_user_pin_release)  # 点击【解除密码】
        log_assert_time()
        log_assert_text(log_assert_pin_release.format(selected_lock_name, UserInfo.Second.name), need_refresh=False)
        log_assert_text(UserInfo.First.name, need_refresh=False)
        factory_lock()
        logout()
