import logging
import threading
import unittest
from Configuration.OperationMethod import *
from Configuration import StartUp
from Redis.VerificationCode import verification_code_phone, verification_code_email


class TestRFID(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = StartUp.driver
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H_%M_%S',
                            filename=FolderList.logging_name,
                            filemode='a')
        logging.getLogger()
        time.sleep(1)

    def add_img(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

    def setUp(self) -> None:
        os.system('adb shell svc wifi enable')
        bt_switch('on')
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    @adb_log
    def test_C57850(self):
        # inviting user/name input page/the function of button continue/the function of page redirect after typing user name
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        assert_result_displayed(userManagement_invite_RFID_first_name, True)  # display first name input
        assert_result_displayed(userManagement_invite_RFID_last_name, True)  # display last name input
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        assert_result_displayed(userManagement_invite_lock_list, True)  # display lock list

    @adb_log
    def test_C57872(self):
        # register RFID/failed from lock side/not register RFID in 20s
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'TIMEOUT')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout

    @adb_log
    def test_C57880(self):
        # register RFID/failed from lock side/add duplicate RFID
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'FAILURE')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # add duplicate RFID  # run on server, local 手动加
        assert_result_displayed(userManagement_invite_register_failure, True)  # register failure

    @adb_log
    def test_C57873(self):
        # RFID registeration success/check lock name
        click_element(userManagement_main_user_tab)  # invite
        click_element(userManagement_invite_invite)  # click invite
        click_element(userManagement_invite_RFID_access)  # click contactless key only
        send_keys(userManagement_invite_RFID_first_name, 'RFID')  # input first name
        send_keys(userManagement_invite_RFID_last_name, 'ONLY')  # input last name
        click_element(userManagement_invite_contact_continue)  # continue
        click_element(userManagement_invite_lock_name)  # select lock
        click_element(userManagement_invite_lock_continue)  # continue
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # REGISTER RFID   # run on server, local 手动加
        assert_result_displayed(userManagement_invite_register_success, True)  # register RFID success
        assert_result_displayed(userManagement_invite_register_sub_message.format(selected_serial_number), True)  # display lock name
        click_element(userManagement_invite_register_finish)  # click finish
        assert_result_displayed(userManagement_invite_invite, True)  # back to guest list
        click_element(UserManagement_user_admin.format('RFID ONLY'))  # click RFID ONLY
        assert_result_displayed(userManagement_invite_RFID_access_level, True)  # CONTACTLESS KEY ONLY
        assert_result_displayed(userManagement_invite_access_always, True)  # always
        assert_result_text(userManagement_invite_RFID_registered_value, '已注册')  # registered
        # click_element(userManagement_invite_RFID_tab)  # click contactless key
        # assert_result_displayed(userManagement_invite_registered_info, True)  # registered

    @depend_on('test_C57873')
    def test_C57881(self):
        # RFID registeration success/finish button/page change
        pass

    @adb_log
    def test_C57888(self):
        # owner: register RFID/Lock failed/not add card on lock till the 20s timeout
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Hehe Shen'))  # click Hehe Shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout

    @adb_log
    def test_C57889(self):
        # register RFID/Lock failed/add duplicated card
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Hehe Shen'))  # click Hehe Shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # add duplicate RFID   # run on server, local 手动加
        assert_result_displayed(userManagement_invite_register_failure, True)  # register failure

    @adb_log
    def test_C57893(self):
        # register RFID card failed/failed to register RFID/RESTART REGISTRATION button
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Hehe Shen'))  # click Hehe Shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # add duplicate RFID  # run on server, local 手动加
        assert_result_displayed(userManagement_invite_register_failure, True)  # register failure
        click_element(userManagement_invite_card_continue)  # RESTART REGISTRATION
        assert_result_displayed(userManagement_invite_wait_connection, True)  # 正在等待锁的连接

    @adb_log
    def test_C57896(self):
        # RFID registeration success/check lock name
        delete_RFID('RFID ONLY', 'RFID only user')
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Hehe Shen'))  # click Hehe Shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # REGISTER RFID  # run on server, local 手动加
        assert_result_displayed(userManagement_invite_register_success, True)  # register RFID success
        assert_result_displayed(userManagement_invite_register_sub_message.format(selected_serial_number), True)  # display lock name
        click_element(userManagement_invite_register_finish)  # click finish
        assert_result_displayed(userManagement_invite_RFID_tab, True)  # back to the user profile of owner B
        assert_result_text(userManagement_invite_RFID_registered_value, '已注册')  # registered
        assert_result_displayed(userManagement_invite_access_always, True)  # always
        # click_element(userManagement_invite_RFID_tab)  # click contactless key
        # assert_result_displayed(userManagement_invite_registered_info, True)  # registered

    @adb_log
    def test_C57904(self):
        # guest: register RFID/Lock failed/not add card on lock till the 20s timeout
        click_element(userManagement_main_user_tab)  # invite
        try:
            if check_element(UserManagement_user_normal.format(UserInfo.NormalJoyce.name), wait_time=5, check_type=False) is False:
                invite_user_yale('normal', UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.name)  # 邀请普通用户A
        except NameError:
            pass
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout

    @adb_log
    def test_C57905(self):
        # register RFID/Lock failed/add duplicated card
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # add duplicate RFID   # run on server, local 手动加
        assert_result_displayed(userManagement_invite_register_failure, True)  # register failure

    @adb_log
    def test_C57907(self):
        # register RFID card failed/register RFID timeout on the lock side/RESTART REGISTRATION button
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        time.sleep(20)
        assert_result_displayed(userManagement_invite_registration_timeout, True)  # display registration timeout
        click_element(userManagement_invite_card_continue)  # RESTART REGISTRATION
        assert_result_displayed(userManagement_invite_wait_connection, True)  # 正在等待锁的连接

    @adb_log
    def test_C57912(self):
        # RFID registeration success/check lock name
        delete_RFID('Hehe Shen')
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        wait_lock_connection()
        click_element(userManagement_invite_card_continue)  # continue
        requests.put(url=API().card_register())  # REGISTER RFID   # run on server, local 手动加
        assert_result_displayed(userManagement_invite_register_success, True)  # register RFID success
        assert_result_displayed(userManagement_invite_register_sub_message.format(selected_serial_number), True)  # display lock name
        click_element(userManagement_invite_register_finish)  # click finish
        assert_result_displayed(userManagement_invite_RFID_tab, True)  # back to the user profile of guest
        assert_result_text(userManagement_invite_RFID_registered_value, '已注册')  # registered
        assert_result_displayed(userManagement_invite_access_always, True)  # always
        # click_element(userManagement_invite_RFID_tab)  # click contactless key
        # assert_result_displayed(userManagement_invite_registered_info, True)  # registered

    @depend_on('test_C57896')
    def test_C57915(self):
        # owner check the user profile of owner users/UI Test/owner register RFID for himself
        pass

    @depend_on('test_C57912')
    def test_C57920(self):
        # owner check user details page of guest/UI test/guest registered contactless key
        pass

    @depend_on('test_C57873')
    def test_C57924(self):
        # owner check user details page of Contactless Key only/UI test/registered contactless key
        pass

    @adb_log
    def test_C57935(self):
        # owner check user details page of Contactless Key only/REMOVE USER button
        invite_RFID_only()
        click_element(userManagement_main_home_tab)  # lock manage
        check_lock_connection()
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
            assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().card_open())  # UNLOCK BY RFID    # run on server, local 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('RFID ONLY'))  # click RFID ONLY
        click_element(userManagement_edit_delete_button)    # delete user
        click_element(userManagement_invite_confirm)  # click confirm
        assert_result_displayed(UserManagement_user_admin.format('RFID ONLY'), False)  # remove user success
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
            assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().card_open())  # UNLOCK BY RFID     # run on server, local 手动
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked

    @adb_log
    def test_C57927(self):
        # owner check contactless key details page of guest/unregister contactless key button/delete success
        check_lock_connection()
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
            assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().card_open())  # UNLOCK BY RFID    # run on server, local 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        click_element(userManagement_invite_RFID_tab)  # click contactless key
        click_element(userManagement_invite_unregister_button)  # 注销门卡
        click_element(userManagement_invite_confirm)  # click confirm
        assert_result_text(userManagement_invite_RFID_registered_value, '未注册')  # not registered
        click_element(UserManagement_back_bar_button)  # BACK
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
            assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().card_open())  # UNLOCK BY RFID     # run on server, local 手动
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked

    @depend_on('test_C57896')
    def test_C57941(self):
        # owner check contactless key details page of Contactless Key only/unregister contactless key button/delete success
        pass


    def cases(self):
        return list(filter(lambda m: m.startswith("test_C") and callable(getattr(self, m)), dir(self)))