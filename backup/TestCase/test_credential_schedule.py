import logging
import unittest
from Configuration.OperationMethod import *
from Configuration import StartUp
import random


class TestSchedule(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = StartUp.driver
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H_%M_%S',
                            filename=FolderList.logging_name,
                            filemode='a')
        logging.getLogger()
        time.sleep(1)

    def add_img(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

    def setUp(self) -> None:
        os.system('adb shell svc wifi enable')
        bt_switch('on')
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    @adb_log
    def test_C58140(self):
        # owner user_already have pincode and RFID_ register FP
        delete_FP('龙燕 沈')
        pin = register_PINCODE('龙燕 沈')  # add PINCODE
        register_RFID('龙燕 沈')
        click_element(UserManagement_back_bar_button)  # BACK
        register_FP('龙燕 沈', 'owner', 'owner')
        assert_result_displayed(userManagement_invite_access_always, True)  # always
        click_element(UserManagement_back_bar_button)  # BACK
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('owner', 'owner'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58152(self):
        # guest user_schedule always_already have pincode and RFID_ register FP
        delete_RFID('龙燕 沈')
        # delete_FP('normal shen')
        pin = register_PINCODE('normal shen')  # add PINCODE
        register_RFID('normal shen')
        # register_FP('normal shen', 'guest', 'guest')
        assert_result_displayed(userManagement_invite_access_always, True)  # always
        click_element(UserManagement_back_bar_button)  # BACK
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().finger_open(4, 4))  # UNLOCK BY FP
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        try:
            requests.put(url=API().card_open())  # UNLOCK BY RFID
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58160(self):
        # guest user_schedule recurring_already have pincode and RFID_ register FP
        delete_user_yale('normal shen')
        invite_user_yale('normal', UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.name, 'recur')  # 邀请普通用户设置schedule为recur
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        click_element(userManagement_edit_schedule_set)  # schedule set
        select_not_today()
        pin = register_PINCODE('normal shen')  # add PINCODE
        register_RFID('normal shen')
        click_element(UserManagement_back_bar_button)  # BACK
        register_FP('normal shen', 'guest', 'guest')
        click_element(UserManagement_back_bar_button)  # BACK
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('guest', 'guest'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        time.sleep(5)
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        time.sleep(5)
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C58172(self):
        # guest user_schedule temporary_already have pincode and RFID_ register FP
        # delete_user_yale('normal shen')
        # invite_user_yale('normal', UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.name, 'temp')  # 邀请guest设置schedule为temp
        pin = register_PINCODE('normal shen')  # add PINCODE
        register_RFID('normal shen')
        click_element(UserManagement_back_bar_button)  # BACK
        register_FP('normal shen', 'guest', 'guest')
        click_element(UserManagement_back_bar_button)  # BACK
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('guest', 'guest'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58168(self):
        # guest user_modify access+schedule not saved_register FP
        delete_user_yale('normal shen')
        invite_user_yale('normal', UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.name, 'temp')  # 邀请guest设置schedule为temp
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_owner_access)
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_invite_FP_tab)  # click FP
        assert_result_displayed(userManagement_invite_modification_not_saved, True)  # 提示修改未保存

    @adb_log
    def test_C58189(self):
        # owner user_registered RFID、FP and pincode: schedule can't modify
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('Hehe Shen'))  # click Hehe Shen
        click_element(userManagement_edit_schedule_set)
        assert_result_text(userManagement_invite_error_prompt, '管理员总是可以访问')
        assert_result_displayed(userManagement_invite_schedule_temp, False)   # 未出现临时使用

    @adb_log
    def test_C58194(self):
        # guest user_schedule always_registered pincode_pincode state "loaded"_change to recurring
        delete_user_yale('normal shen')
        invite_user_yale('normal', UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.name)  # 邀请guest
        pin = register_PINCODE('normal shen')  # add PINCODE
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        click_element(userManagement_edit_schedule_set)
        click_element(userManagement_invite_schedule_recurring)  # 改为多次
        select_not_today()
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode on server
        except:
            pass      # 手动
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C58196(self):
        # guest user_schedule always_registered pincode_pincode state "disabled"_change to recurring
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_edit_schedule_set)
        click_element(userManagement_invite_schedule_always)  # 改为永久
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        click_element(userManagement_edit_pin_set)
        click_element(userManagement_edit_disable_pin)
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        click_element(userManagement_edit_schedule_set)
        click_element(userManagement_invite_schedule_recurring)  # 改为多次
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        if assert_result_displayed(lock_main_lock_close_status, True):
            pass
        else:
            click_element(lock_main_user_lock_button)  # lock
            assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass      # 手动
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C58202(self):
        # guest user_schedule always_registered RFID、FP and pincode_pincode state "loaded"_change to recurring
        delete_user_yale('normal shen')
        invite_user_yale('normal', UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.name)  # 邀请guest
        pin = register_PINCODE('normal shen')  # add PINCODE
        register_RFID('normal shen')
        click_element(UserManagement_back_bar_button)  # BACK
        register_FP('normal shen', 5, 5)
        click_element(userManagement_edit_schedule_set)
        click_element(userManagement_invite_schedule_recurring)  # 改为多次
        select_not_today()
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().finger_open(5, 5))  # UNLOCK BY FP
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        time.sleep(5)
        try:
            requests.put(url=API().card_open())  # UNLOCK BY RFID
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        time.sleep(5)
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C58203(self):
        # guest user_schedule always_registered RFID、FP and pincode_pincode state "loaded"_change to temporary
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_edit_schedule_set)
        click_element(userManagement_invite_schedule_temp)  # 改为临时
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().finger_open(5, 5))  # UNLOCK BY FP
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        try:
            requests.put(url=API().card_open())  # UNLOCK BY RFID
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58204(self):
        # guest user_schedule always_registered RFID、FP and pincode_pincode state "disabled"_change to recurring
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_edit_schedule_set)
        click_element(userManagement_invite_schedule_always)  # 改为永久
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        click_element(userManagement_edit_pin_set)
        click_element(userManagement_edit_disable_pin)
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('normal shen'))  # click normal shen
        click_element(userManagement_edit_schedule_set)
        click_element(userManagement_invite_schedule_recurring)  # 改为多次
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().finger_open(5, 5))  # UNLOCK BY FP
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        try:
            requests.put(url=API().card_open())  # UNLOCK BY RFID
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C58238(self):
        # contactless key only user_RFID status is loaded_schedule always_change to recurring
        click_element(userManagement_main_user_tab)  # invite
        while 1:
            try:
                if check_element(UserManagement_user_normal.format('RFID ONLY'), wait_time=5, check_type=False) is False:
                    break
                else:
                    click_element(UserManagement_user_admin.format('RFID ONLY'))  # click RFID ONLY
                    click_element(userManagement_edit_delete_button)  # delete user
                    click_element(userManagement_invite_confirm)  # click confirm
            except NameError:
                pass
        delete_RFID('normal shen')
        invite_RFID_only()
        click_element(UserManagement_user_admin.format('RFID ONLY'))  # click RFID ONLY
        click_element(userManagement_invite_only_access_level)
        assert_result_displayed(userManagement_invite_only_access_level, True)  # access cann't modify
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # recurring
        select_not_today()  # select not today time
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        # if assert_result_displayed(lock_main_lock_close_status, True):
        #     pass
        # else:
        #     click_element(lock_main_user_lock_button)  # lock
        #     assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        try:
            requests.put(url=API().card_open())  # UNLOCK BY RFID
        except:
            pass   # 手动
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C58240(self):
        # contactless key only user_RFID status is loaded_schedule recurring_change to always
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('RFID ONLY'))  # click RFID ONLY
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_always)  # set always
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().card_open())  # UNLOCK BY RFID
        except:
            pass   # 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58243(self):
        # contactless key only user_RFID status is loaded_schedule temporary_change to recurring
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('RFID ONLY'))  # click RFID ONLY
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_temp)  # set temporary
        click_element(userManagement_invite_temp_start_date)  # 点击临时开始日期
        click_element(userManagement_invite_temp_future_time)  # 选择以后的时间，每个月最后一天
        click_element(notification_main_confirm_time)  # 确定
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().card_open())  # UNLOCK BY RFID
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('RFID ONLY'))  # click RFID ONLY
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # set recurring
        click_element(userManagement_invite_recur_start_time)
        click_element(userManagement_invite_schedule_am)  # 上午
        click_element(userManagement_invite_schedule_0)  # 12点
        click_element(notification_main_confirm_time)
        click_element(userManagement_invite_recur_end_time)
        click_element(userManagement_invite_schedule_pm)  # 下午
        click_element(userManagement_invite_schedule_23)  # 23点
        click_element(notification_main_confirm_time)
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().card_open())  # UNLOCK BY RFID
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58252(self):
        # pincode only user_registered pincode_pincode state "loaded"_schedule recurring_change to always
        invite_PINCODE_only('recur')
        click_element(UserManagement_user_admin.format('PINCODE ONLY'))  # click PINCODE ONLY
        assert_result_displayed(userManagement_invite_only_access_level, False)  # access cann't modify
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_always)  # always
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58255(self):
        # pincode only user_registered pincode_pincode state "loaded"_schedule temporary_change to recurring
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('PINCODE ONLY'))  # click PINCODE ONLY
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_temp)  # set temporary
        click_element(userManagement_invite_temp_start_date)  # 点击临时开始日期
        click_element(userManagement_invite_temp_future_time)  # 选择以后的时间，每个月最后一天
        click_element(notification_main_confirm_time)  # 确定
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('PINCODE ONLY'))  # click PINCODE ONLY
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # set recurring
        click_element(userManagement_invite_recur_start_time)
        click_element(userManagement_invite_schedule_am)  # 上午
        click_element(userManagement_invite_schedule_0)  # 12点
        click_element(notification_main_confirm_time)
        click_element(userManagement_invite_recur_end_time)
        click_element(userManagement_invite_schedule_pm)  # 下午
        click_element(userManagement_invite_schedule_23)  # 23点
        click_element(notification_main_confirm_time)
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58262(self):
        # FP only user_FP status is loaded_schedule always_change to recurring
        pass

    @adb_log
    def test_C58263(self):
        # FP only user_FP status is loaded_schedule always_change to temporary
        pass

    @adb_log
    def test_C58264(self):
        # FP only user_FP status is loaded_schedule recurring_change to always
        pass

    @adb_log
    def test_C58265(self):
        # FP only user_FP status is loaded_schedule recurring_change to temporary
        pass

    @adb_log
    def test_C58266(self):
        # FP only user_FP status is loaded_schedule temporary_change to always
        pass

    @adb_log
    def test_C58267(self):
        # FP only user_FP status is loaded_schedule temporary_change to recurring
        pass

    @adb_log
    def test_C58279(self):
        # owner user_change to guest always_registered RFID、FP and pincode
        delete_RFID('RFID ONLY', 'RFID only user')
        register_RFID('龙燕 沈')
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_general_access)  # 改为普通用户
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('owner', 'owner'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58285(self):
        # guest user_schedule always_change to owner_registered RFID、FP and pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('龙燕 沈'))  # click 龙燕 沈
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_owner_access)  # 改为管理员
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('owner', 'owner'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @depend_on('test_C58238')
    def test_C58286(self):
        # contactless key only user_registered RFID_can't modify access, same with C58238
        pass

    @depend_on('test_C58252')
    def test_C58287(self):
        # pincode only user_registered pincode_can't modify access, same with C58252
        pass

    @adb_log
    def test_C58288(self):
        # FP only user_registered FP_can't modify access
        invite_FP_only('only', 'only')
        click_element(UserManagement_user_admin.format('FP ONLY'))  # click FP ONLY
        click_element(userManagement_invite_only_access_level)
        assert_result_displayed(userManagement_invite_only_access_level, True)  # access cann't modify

    @adb_log
    def test_C58293(self):
        # owner user_registered pincode_pincode state "loaded"_change to guest recurring
        invite_user_yale('admin', UserInfo.AdminJoyce.phone2, UserInfo.AdminJoyce.name2)  # 邀请管理员B
        pin = register_PINCODE('admin2 shen')  # add PINCODE
        click_element(UserManagement_user_admin.format('admin2 shen'))  # click admin2 shen
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_general_access)  # 改为普通用户
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # recurring
        select_not_today()  # select not today time
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass   # 手动
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C58294(self):
        # owner user_registered pincode_pincode state "loaded"_change to guest temporary
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('admin2 shen'))  # click admin2 shen
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_owner_access)  # 改为管理员
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('admin2 shen'))  # click admin2 shen
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_general_access)  # 改为普通用户
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_temp)  # temp
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass  # 手动
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58301(self):
        # owner user_registered RFID、FP and pincode_pincode state "loaded"_change to guest recurring
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('龙燕 沈'))  # click 龙燕 沈
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_general_access)  # 改为普通用户
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # recurring
        select_not_today()  # select not today time
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('owner', 'owner'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        time.sleep(3)
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        time.sleep(3)
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C58302(self):
        # guest user_schedule recurring_registered RFID、FP and pincode_pincode state "loaded"_change to owner
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('龙燕 沈'))  # click 龙燕 沈
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_owner_access)  # 改为管理员
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('owner', 'owner'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58311(self):
        # owner user_registered RFID、FP and pincode__pincode state "loaded"_change to guest temporary
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('龙燕 沈'))  # click 龙燕 沈
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_general_access)  # 改为普通用户
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_temp)  # temp
        click_element(userManagement_invite_temp_start_date)  # 点击临时开始日期
        click_element(userManagement_invite_temp_future_time)  # 选择以后的时间，每个月最后一天
        click_element(notification_main_confirm_time)  # 确定
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('owner', 'owner'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        time.sleep(3)
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        time.sleep(3)
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C58312(self):
        # guest user_schedule temporary_registered RFID、FP and pincode_pincode state "loaded"_change to owner
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('龙燕 沈'))  # click 龙燕 沈
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_owner_access)  # 改为管理员
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('owner', 'owner'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58319(self):
        # guest user_schedule recurring_registered RFID、FP and pincode_pincode state "disabled"_change to owner
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('龙燕 沈'))  # click 龙燕 沈
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_general_access)  # 改为普通用户
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_recurring)  # recurring
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('龙燕 沈'))  # click 龙燕 沈
        click_element(userManagement_edit_pin_set)
        click_element(userManagement_edit_disable_pin)   # 禁用密码
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('龙燕 沈'))  # click 龙燕 沈
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_owner_access)  # 改为管理员
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('owner', 'owner'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C58325(self):
        # owner user_modify and enable pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('admin2 shen'))  # click admin2 shen
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_owner_access)  # 改为管理员
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('admin2 shen'))  # click admin2 shen
        click_element(userManagement_edit_pin_set)
        click_element(userManagement_edit_disable_pin)
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('admin2 shen'))  # click admin2 shen
        assert_result_text(userManagement_edit_pin_set, '(已停用)')  # 已停用
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        send_keys(userManagement_invite_pin_input, '222222')
        click_element(userManagement_invite_change_pin)  # modify
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58330(self):
        # guest user_schedule always_modify and enable pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('admin2 shen'))  # click admin2 shen
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_general_access)  # 改为普通用户
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('admin2 shen'))  # click admin2 shen
        click_element(userManagement_edit_pin_set)
        click_element(userManagement_edit_disable_pin)
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('admin2 shen'))  # click admin2 shen
        assert_result_text(userManagement_edit_pin_set, '(已停用)')  # 已停用
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        send_keys(userManagement_invite_pin_input, '333333')
        click_element(userManagement_invite_change_pin)  # modify
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58350(self):
        # pincode only user_schedule recurring_modify and enable pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('PINCODE ONLY'))  # click PINCODE ONLY
        click_element(userManagement_edit_pin_set)
        click_element(userManagement_edit_disable_pin)
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('PINCODE ONLY'))  # click admin2 shen
        assert_result_text(userManagement_edit_pin_set, '(已停用)')  # 已停用
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        send_keys(userManagement_invite_pin_input, '444444')
        click_element(userManagement_invite_change_pin)  # modify
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58360(self):
        # guest user_registered pincode_schedule temporary_change schedule to always_modify and enable pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('admin2 shen'))  # click admin2 shen
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_temp)  # temp
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('admin2 shen'))  # click admin2 shen
        click_element(userManagement_edit_pin_set)
        click_element(userManagement_edit_disable_pin)
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('admin2 shen'))  # click admin2 shen
        assert_result_text(userManagement_edit_pin_set, '(已停用)')  # 已停用
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_always)  # always
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        send_keys(userManagement_invite_pin_input, '555666')
        click_element(userManagement_invite_change_pin)  # modify
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58366(self):
        # guest user_registered pincode、RFID and FP_schedule temporary_change schedule to always_modify and enable pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('龙燕 沈'))  # click 龙燕 沈
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_general_access)  # 改为普通用户
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_temp)  # temp
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('龙燕 沈'))  # click 龙燕 沈
        assert_result_text(userManagement_edit_pin_set, '(已停用)')  # 已停用
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_always)  # always
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        send_keys(userManagement_invite_pin_input, '666777')
        click_element(userManagement_invite_change_pin)  # modify
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_edit_manage_access_button, after_action_sleep=10)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('owner', 'owner'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58372(self):
        # pincode only user_registered pincode_schedule temporary_change schedule to always_modify and enable pincode
        click_element(userManagement_main_user_tab)  # invite
        click_element(UserManagement_user_admin.format('PINCODE ONLY'))  # click PINCODE ONLY
        click_element(userManagement_edit_pin_set)
        click_element(userManagement_edit_disable_pin)
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(UserManagement_user_admin.format('PINCODE ONLY'))  # click PINCODE ONLY
        assert_result_text(userManagement_edit_pin_set, '(已停用)')  # 已停用
        click_element(userManagement_invite_pincode_set)  # click pincode
        click_element(userManagement_invite_change_pin)  # enable
        send_keys(userManagement_invite_pin_input, '777888')
        click_element(userManagement_invite_change_pin)  # modify
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_invite_only_user_schedule)  # schedule set
        click_element(userManagement_invite_schedule_always)  # always
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        try:
            requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        except:
            pass
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58381(self):
        # owner user_schedule always_registered RFID and pincode_unregister RFID
        delete_FP('龙燕 沈')
        click_element(UserManagement_user_admin.format('龙燕 沈'))  # click 龙燕 沈
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_owner_access)  # 改为管理员
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        delete_RFID('龙燕 沈')
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().finger_open('owner', 'owner'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        time.sleep(3)
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C58382(self):
        # owner user_schedule always_registered RFID and FP_unregister FP
        delete_user_yale('龙燕 沈')
        try:
            if check_element(UserManagement_user_normal.format('龙燕 沈'), wait_time=5, check_type=False) is False:
                invite_user_yale('admin', '15216762426', '龙燕 沈')  # 邀请管理员
        except NameError:
            pass
        register_RFID('龙燕 沈')
        click_element(UserManagement_back_bar_button)  # BACK
        register_FP('龙燕 沈', 'last', 'last')
        click_element(UserManagement_back_bar_button)  # BACK
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('last', 'last'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        delete_FP('龙燕 沈')
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('last', 'last'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        time.sleep(3)
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58385(self):
        # guest user_schedule always_registered RFID and pincode_unregister RFID
        pin = register_PINCODE('龙燕 沈')  # add PINCODE
        click_element(UserManagement_user_admin.format('龙燕 沈'))  # click 龙燕 沈
        pin = get_element(userManagement_edit_pin_set).text
        click_element(userManagement_invite_user_access)
        click_element(userManagement_invite_general_access)  # 改为普通用户
        click_element(userManagement_edit_manage_access_button)  # 保存
        click_element(userManagement_edit_manage_access_button)  # 保存
        delete_RFID('龙燕 沈')
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked

    @adb_log
    def test_C58386(self):
        # owner user_schedule always_registered RFID and FP_unregister FP
        delete_user_yale('龙燕 沈')
        try:
            if check_element(UserManagement_user_normal.format('龙燕 沈'), wait_time=5, check_type=False) is False:
                invite_user_yale('normal', '15216762426', '龙燕 沈')  # 邀请普通用户
        except NameError:
            pass
        register_RFID('龙燕 沈')
        click_element(UserManagement_back_bar_button)  # BACK
        register_FP('龙燕 沈', '88', '88')
        click_element(UserManagement_back_bar_button)  # BACK
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('88', '88'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
        click_element(lock_main_user_lock_button)  # lock
        assert_result_displayed(lock_main_lock_close_status, True)  # Locked
        delete_FP('龙燕 沈')
        click_element(userManagement_main_home_tab)  # lock manage
        while 1:
            try:
                assert_result_displayed(lock_main_lock_close_status, True)
            except:
                click_element(lock_main_user_lock_button)  # lock
            else:
                break
        requests.put(url=API().finger_open('88', '88'))  # UNLOCK BY FP
        assert_result_displayed(lock_main_lock_close_status, True)  # still locked
        time.sleep(3)
        requests.put(url=API().card_open())  # UNLOCK BY RFID
        assert_result_displayed(lock_main_lock_open_status, True)  # unlocked

    @adb_log
    def test_C58396(self):
        # contactless key only user_schedule recurring_registered RFID_unregister RFID
        delete_RFID('龙燕 沈')
        invite_RFID_only()
        assert_result_displayed(UserManagement_user_normal.format('RFID ONLY'), True)  # RFID ONLY出现在用户列表
        delete_RFID('RFID ONLY', 'RFID only user')
        assert_result_displayed(UserManagement_user_normal.format('RFID ONLY'), False)  # RFID ONLY从用户列表消失

    @adb_log
    def test_C58398(self):
        # FP only user_schedule always_registered FP_unregister FP
        delete_user_yale('RFID FAILURE')
        delete_user_yale('RFID TIMEOUT')
        scroll_screen_up()
        while 1:
            try:
                if check_element(UserManagement_user_normal.format('FP ONLY'), wait_time=5, check_type=False) is False:
                    break
                else:
                    click_element(UserManagement_user_admin.format('FP ONLY'))  # click RFID ONLY
                    click_element(userManagement_edit_delete_button)  # delete user
                    click_element(userManagement_invite_confirm)  # click confirm
            except NameError:
                pass
        invite_FP_only('100', '100')
        assert_result_displayed(UserManagement_user_normal.format('FP ONLY'), True)  # FP ONLY出现在用户列表
        delete_FP('FP ONLY', 'FP only user')
        assert_result_displayed(UserManagement_user_normal.format('FP ONLY'), False)  # FP ONLY从用户列表消失

    # @adb_log
    # def test_C58409(self):
    #     # factory reset_remove all user's credentials and schedule
    #     delete_user_yale('龙燕 沈')
    #     try:
    #         if check_element(UserManagement_user_normal.format('龙燕 沈'), wait_time=5, check_type=False) is False:
    #             invite_user_yale('admin', '15216762426', '龙燕 沈')  # 邀请管理员
    #     except NameError:
    #         pass
    #     pin = register_PINCODE('龙燕 沈')  # add PINCODE
    #     register_RFID('龙燕 沈')
    #     click_element(UserManagement_back_bar_button)  # BACK
    #     register_FP('龙燕 沈', '99', '99')
    #     click_element(UserManagement_back_bar_button)  # BACK
    #     click_element(userManagement_main_home_tab)  # lock manage
    #     check_lock_connection()  # 检查锁的连接
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置按钮
    #     click_element(lock_lockSetting_reset)  # 点击出厂设置
    #     click_element(lock_lockSetting_reset_button)  # 点击恢复出厂设置按钮
    #     click_element(lock_lockSetting_reset_confirm)  # 点击重置
    #     if check_element(lock_lockSetting_reset_confirm, wait_time=20, check_type=False) is False:
    #         pass
    #     else:
    #         click_element(lock_lockSetting_reset_confirm)  # 点击好的
    #         click_element(lock_lockSetting_reset_button)  # 点击恢复出厂设置按钮
    #         click_element(lock_lockSetting_reset_confirm)  # 点击重置
    #     requests.put(url=API().finger_open('99', '99'))  # UNLOCK BY FP 听锁的声音判断是否解绑成功
    #     time.sleep(5)
    #     requests.put(url=API().card_open())  # UNLOCK BY RFID 听锁的声音判断是否解绑成功
    #     time.sleep(5)
    #     requests.put(url=API().pin_open(pin))  # UNLOCK BY pincode 听锁的声音判断是否解绑成功


    def cases(self):
        return list(filter(lambda m: m.startswith("test_C") and callable(getattr(self, m)), dir(self)))
