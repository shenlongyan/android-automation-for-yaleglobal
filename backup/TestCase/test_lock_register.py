import logging
import threading
import unittest
from Configuration.OperationMethod import *
from Configuration import StartUp
from Redis.VerificationCode import verification_code_phone, verification_code_email


class TestRegister(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = StartUp.driver
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H_%M_%S',
                            filename=FolderList.logging_name,
                            filemode='a')
        logging.getLogger()
        time.sleep(1)

    def add_img(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

    def setUp(self) -> None:
        os.system('adb shell svc wifi enable')
        bt_switch('on')
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    # @adb_log
    # def test_register_tc001(self):
    #     if app_select == 'panpan':
    #         def app_install():
    #             subprocess.Popen('adb install {}'.format(APPInfo.version_new), stdout=subprocess.PIPE)
    #
    #         def confirm_install():
    #             try:
    #                 subprocess.Popen(click_element(soft_button_install_confirm, wait_time=300), stdout=subprocess.PIPE)
    #             except TypeError:
    #                 pass
    #             else:
    #                 pass
    #
    #         os.system('adb uninstall {}'.format(APKInfo.package_name))
    #         # 更新LastUser
    #         file_name = 'Configuration/LastUser.txt'
    #         file_name = open(file_name, 'w')
    #         file_name.write('/')
    #         file_name.close()
    #         threads = []
    #         t1 = threading.Thread(target=app_install)
    #         t2 = threading.Thread(target=confirm_install)
    #         threads.append(t1)
    #         threads.append(t2)
    #         for x in threads:
    #             x.start()
    #         x.join()
    #         os.system('adb shell am start {}/{}'.format(APKInfo.package_name, APKInfo.activity_name))
    #         assert_result_displayed(register_button_welcome_register, True)  # 显示开始体验
    #     elif app_select == 'yale':
    #         # 全新安装测试
    #         pass
    #     else:
    #         assert False
    #
    # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    # @adb_log
    # def test_register_tc003(self):
    #     if app_select == 'panpan':
    #         def app_install():
    #             subprocess.Popen('adb install {}'.format(APPInfo.version_old), stdout=subprocess.PIPE)
    #
    #         def confirm_install():
    #             try:
    #                 subprocess.Popen(click_element(soft_button_install_confirm, wait_time=300), stdout=subprocess.PIPE)
    #             except TypeError:
    #                 pass
    #             else:
    #                 pass
    #
    #         os.system('adb uninstall {}'.format(APKInfo.package_name))
    #         # 更新LastUser
    #         file_name = 'Configuration/LastUser.txt'
    #         file_name = open(file_name, 'w')
    #         file_name.write('/')
    #         file_name.close()
    #         threads = []
    #         t1 = threading.Thread(target=app_install)
    #         t2 = threading.Thread(target=confirm_install)
    #         threads.append(t1)
    #         threads.append(t2)
    #         for x in threads:
    #             x.start()
    #         x.join()
    #         os.system('adb install -r {}'.format(APPInfo.version_new))
    #         os.system('adb shell am start {}/{}'.format(APKInfo.package_name, APKInfo.activity_name))
    #         assert_result_displayed(register_button_welcome_register, True)  # 显示开始体验
    #     elif app_select == 'yale':
    #         # 第一次安装
    #         pass
    #     else:
    #         assert False
    #
    # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    # @adb_log
    # def test_register_tc004(self):
    #     if app_select == 'panpan':
    #         click_element(register_button_privacy, after_action_sleep=1)  # 取消【同意隐私条款】
    #         click_element(register_button_welcome_register)  # 点击【开始体验】
    #         assert_result_text(register_assert_alert, register_assert_alert_privacy)  # 提示【请先同意隐私条款】
    #         time.sleep(3)
    #         click_element(register_button_login)  # 点击【登录】
    #         assert_result_text(register_assert_alert, register_assert_alert_privacy)  # 提示【请先同意隐私条款】
    #         time.sleep(3)
    #         click_element(register_button_privacy)  # 勾选【同意隐私条款】
    #         click_element(register_button_welcome_register)  # 点击【开始体验】
    #         assert_result_displayed(register_compound_phone_area, True)  # 进入注册页面
    #         click_element(register_button_back)  # 点击【返回】
    #         click_element(register_button_login)  # 点击【登录】
    #         assert_result_displayed(login_assert_lost_phone, True)  # 进入登录页面
    #         click_element(register_button_back)  # 点击【返回】
    #         click_element(register_button_privacy_link, after_action_sleep=8)  # 点击隐私协议
    #         assert_result_text(register_assert_alert_privacy_detail, register_assert_alert_privacy_detail_title)  # 显示标题
    #     elif app_select == 'yale':
    #         pass
    #     else:
    #         assert False

    # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    # @adb_log
    # def test_register_tc005(self):
    #     if app_select == 'panpan':
    #         click_element(register_button_welcome_register)  # 点击【开始体验】
    #         click_element(register_compound_phone_area)  # 点击【手机区号】
    #         click_element(register_button_phone_area_select)  # 选择【Afghanistan】
    #         assert_result_displayed(register_button_phone_area_select, True)  # 手机区号显示【+93】
    #         assert_result_displayed(register_compound_password_show, True)  # 显示【显示】
    #     elif app_select == 'yale':
    #         pass
    #     else:
    #         assert False

    # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    # @adb_log
    # def test_register_tc007(self):
    #     if app_select == 'panpan':
    #         def app_install():
    #             subprocess.Popen('adb install {}'.format(APPInfo.version_new), stdout=subprocess.PIPE)
    #
    #         def confirm_install():
    #             try:
    #                 subprocess.Popen(click_element(soft_button_install_confirm, wait_time=300), stdout=subprocess.PIPE)
    #             except TypeError:
    #                 pass
    #             else:
    #                 pass
    #
    #         os.system('adb uninstall {}'.format(APKInfo.package_name))
    #         # 更新LastUser
    #         file_name = 'Configuration/LastUser.txt'
    #         file_name = open(file_name, 'w')
    #         file_name.write('/')
    #         file_name.close()
    #         threads = []
    #         t1 = threading.Thread(target=app_install)
    #         t2 = threading.Thread(target=confirm_install)
    #         threads.append(t1)
    #         threads.append(t2)
    #         for x in threads:
    #             x.start()
    #         x.join()
    #         os.system('adb shell am start {}/{}'.format(APKInfo.package_name, APKInfo.activity_name))
    #         login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
    #         logout()
    #     elif app_select == 'yale':
    #         pass
    #     else:
    #         assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C357(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 欢迎页UI测试
            assert_result_displayed(register_main_invited, True)       # 显示【收到邀请】
            assert_result_displayed(register_main_register, True)  # 显示【注册】
            assert_result_displayed(register_main_login, True)  # 显示【登录】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C358(self):
        if app_select == 'panpan':
            def app_install():
                subprocess.Popen('adb install {}'.format(APPInfo.version_new), stdout=subprocess.PIPE)

            def confirm_install():
                try:
                    subprocess.Popen(click_element(soft_button_install_confirm, wait_time=300), stdout=subprocess.PIPE)
                except TypeError:
                    pass
                else:
                    pass

            os.system('adb uninstall {}'.format(APKInfo.package_name))
            # 更新LastUser
            file_name = 'Configuration/LastUser.txt'
            file_name = open(file_name, 'w')
            file_name.write('/')
            file_name.close()
            threads = []
            t1 = threading.Thread(target=app_install)
            t2 = threading.Thread(target=confirm_install)
            threads.append(t1)
            threads.append(t2)
            for x in threads:
                x.start()
            x.join()
            os.system('adb shell am start {}/{}'.format(APKInfo.package_name, APKInfo.activity_name))
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
            logout()
        elif app_select == 'yale':
            # 收到邀请按钮
            click_element(register_main_invited)  # 点击【收到邀请】
            assert_result_displayed(register_register_start, True)  # 显示【开始】按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C359(self):
        if app_select == 'panpan':
            click_element(register_button_welcome_register)  # 点击【开始体验】
            send_keys(register_button_register_phone, UserInfo.First.phone)  # 输入手机号
            send_keys(register_button_register_name, UserInfo.First.name)  # 输入昵称
            send_keys(register_button_register_email, '12345')  # 输入邮箱
            send_keys(register_button_register_password, UserInfo.First.password)  # 输入密码
            assert_result_enable(register_button_sure, False)  # 不可点击【确定】
        elif app_select == 'yale':
            # 注册按钮
            click_element( register_main_register)  # 点击【注册】
            assert_result_displayed(register_register_start, True)  # 显示【开始】按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C360(self):
        if app_select == 'panpan':
            click_element(register_button_welcome_register)  # 点击【开始体验】
            send_keys(register_button_register_phone, UserInfo.First.phone)  # 输入手机号
            send_keys(register_button_register_name, UserInfo.First.name)  # 输入昵称
            send_keys(register_button_register_email, UserInfo.First.email)  # 输入邮箱
            click_element(register_button_register_password)  # 点击【密码】
            driver.press_keycode(10)  # 模拟键盘输入，否则不会出现提示
            send_keys(register_button_register_password, '123')  # 输入密码
            assert_result_text(register_assert_snackbar_text, register_assert_password_length)  # 密码长度提示
            clear_text_content(register_button_register_password)  # 清除密码输入
            send_keys(register_button_register_password, '123456789')  # 输入密码
            assert_result_text(register_assert_snackbar_text, register_assert_password_complexity)  # 密码复杂度提示
        elif app_select == 'yale':
            # 登录按钮
            click_element(register_main_login)  # 点击【登录】
            assert_result_displayed(login_login_submit, True)  # 显示【登录】按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C239(self):
        if app_select == 'panpan':
            user_info = phone_number_get_not_registered()
            click_element(register_button_welcome_register)  # 点击【开始体验】
            send_keys(register_button_register_phone, user_info[0])  # 输入手机号
            send_keys(register_button_register_name, user_info[1])  # 输入昵称
            send_keys(register_button_register_email, user_info[2])  # 输入邮箱
            send_keys(register_button_register_password, user_info[3])  # 输入密码
            click_element(register_button_sure)  # 点击确定
            # 手机
            assert_result_displayed(register_compound_resend_verify, True)  # 显示重新发送验证码倒计时
            assert_result_text(register_sent, register_sent_text_phone.format(user_info[0]))  # 已发送验证码
            send_keys(register_button_verify_input, '000000')  # 输入错误验证码
            click_element(register_button_sure, after_action_sleep=5)  # 点击提交，验证错误后验证码会晃动，期间无法输入
            send_keys(register_button_verify_input, verification_code_phone(user_info[0]))  # 直接输入正确验证码
            click_element(register_button_sure)  # 点击提交
            # 邮箱
            assert_result_displayed(register_compound_resend_verify, True)  # 显示重新发送验证码倒计时
            assert_result_text(register_sent, register_sent_text_email.format(user_info[2]))  # 已发送验证码
            send_keys(register_button_verify_input, '000000')  # 输入错误验证码
            click_element(register_button_sure, after_action_sleep=5)  # 点击提交，验证错误后验证码会晃动，期间无法输入
            send_keys(register_button_verify_input, verification_code_email(user_info[2]))  # 直接输入正确验证码
            click_element(register_button_sure)  # 点击提交
            assert_result_text(homePage_homePage_main, homepage_assert_home_page_name.format(user_info[0]))  # 显示xx的家
            logout()
        elif app_select == 'yale':
            # 收到邀请UI测试
            click_element(register_main_invited)  # 点击【收到邀请】
            click_element(register_register_start)  # 点击【开始】
            assert_result_displayed(register_policy_privacy, True)  # 显示【隐私政策】
            assert_result_displayed(register_policy_service, True)  # 显示【服务条件】
            assert_result_displayed(register_policy_license, True)  # 显示【终端用户许可协议】
            click_element(register_policy_agree)  # 点击【我同意】
            assert_result_displayed(login_login_create_account, True)  # 显示【创建账号】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C240(self):
        if app_select == 'panpan':
            user_info = phone_number_get_not_registered()
            click_element(register_button_welcome_register)  # 点击【开始体验】
            send_keys(register_button_register_phone, user_info[0])  # 输入手机号
            send_keys(register_button_register_name, user_info[1])  # 输入昵称
            send_keys(register_button_register_email, user_info[2])  # 输入邮箱
            send_keys(register_button_register_password, user_info[3])  # 输入密码
            click_element(register_button_sure)  # 点击确定
            # 手机
            assert_result_displayed(register_compound_resend_verify, True)  # 显示重新发送验证码倒计时
            assert_result_text(register_sent, register_sent_text_phone.format(user_info[0]))  # 已发送验证码
            send_keys(register_button_verify_input, '000000')  # 输入错误验证码
            click_element(register_button_sure, after_action_sleep=60)  # 点击提交，验证错误后验证码会晃动，期间无法输入
            click_element(register_compound_resend_verify, after_action_sleep=5)  # 重新发送验证码
            send_keys(register_button_verify_input, verification_code_phone(user_info[0]))  # 直接输入正确验证码
            click_element(register_button_sure)  # 点击提交
            # 邮箱
            assert_result_displayed(register_compound_resend_verify, True)  # 显示重新发送验证码倒计时
            assert_result_text(register_sent, register_sent_text_email.format(user_info[2]))  # 已发送验证码
            send_keys(register_button_verify_input, '000000')  # 输入错误验证码
            click_element(register_button_sure, after_action_sleep=60)  # 点击提交，验证错误后验证码会晃动，期间无法输入
            click_element(register_compound_resend_verify, after_action_sleep=5)  # 重新发送验证码
            send_keys(register_button_verify_input, verification_code_email(user_info[2]))  # 直接输入正确验证码
            click_element(register_button_sure)  # 点击提交
            assert_result_text(homePage_homePage_main, homepage_assert_home_page_name.format(user_info[0]))  # 显示xx的家
            logout()
        elif app_select == 'yale':
            # 收到邀请图片按钮
            click_element(register_main_invited)  # 点击【收到邀请】
            assert_result_displayed(register_register_start, True)  # 显示【开始】按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C241(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 收到邀请返回按钮
            click_element(register_main_invited)  # 点击【收到邀请】
            driver.press_keycode(4)
            assert_result_displayed(register_main_invited, True)       # 显示欢迎页【收到邀请】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C242(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            #  开始按钮
            click_element(register_main_invited)  # 点击【收到邀请】
            click_element(register_register_start)  # 点击【开始】
            assert_result_displayed(register_policy_privacy, True)  # 显示【隐私政策】
            assert_result_displayed(register_policy_service, True)  # 显示【服务条件】
            assert_result_displayed(register_policy_license, True)  # 显示【终端用户许可协议】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C243(self):
        if app_select == 'panpan':
            user_info = user_register()
            assert_result_text(homePage_homePage_main, homepage_assert_home_page_name.format(user_info[1]))  # 显示xx的家
            logout()
        elif app_select == 'yale':
            # 返回按钮
            pass
        else:
            assert False

    # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    # @adb_log
    # def test_register_tc017(self):
    #     if app_select == 'panpan':
    #         click_element(register_button_welcome_register)  # 点击【开始体验】
    #         send_keys(register_button_register_phone, UserInfo.First.phone)  # 输入手机号
    #         send_keys(register_button_register_name, UserInfo.First.name)  # 输入昵称
    #         send_keys(register_button_register_email, UserInfo.First.email)  # 输入邮箱
    #         send_keys(register_button_register_password, UserInfo.First.password)  # 输入密码
    #         click_element(register_button_sure)  # 点击确定
    #         assert_result_text(register_assert_alert, register_assert_alert_register_phone)  # 提示【当前手机号已注册】
    #     elif app_select == 'yale':
    #         # 隐私条款
    #         pass
    #         # click_element(register_main_invited)  # 点击【收到邀请】
    #         # click_element(register_invited_image)  # 点击【邀请图片】
    #         # click_element(register_register_start)  # 点击【开始】
    #         # click_element(register_policy_privacy)  # 点击【隐私政策】
    #         # image_compare('Capture/privacy_h5.png', register_policy_link)  # 对比图片验证跳转到隐私条款H5页面
    #         # click_element(register_policy_back)   # 点击【返回】
    #         # click_element(register_policy_service)  # 点击【服务条件】
    #         # image_compare('Capture/service_h5.png', register_policy_link)  # 对比图片验证跳转到服务条件H5页面
    #         # click_element(register_policy_back)  # 点击【返回】
    #         # click_element(register_policy_license)  # 点击【终端用户许可协议】
    #         # image_compare('Capture/license_h5.png', register_policy_link)  # 对比图片验证跳转到终端用户许可协议H5页面
    #         # click_element(register_policy_back)  # 点击【返回】
    #     else:
    #         assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C245(self):
        if app_select == 'panpan':
            user_info = phone_number_get_not_registered()
            click_element(register_button_welcome_register)  # 点击【开始体验】
            send_keys(register_button_register_phone, user_info[0])  # 输入手机号
            send_keys(register_button_register_name, UserInfo.First.name)  # 输入昵称
            send_keys(register_button_register_email, UserInfo.First.email)  # 输入邮箱
            send_keys(register_button_register_password, UserInfo.First.password)  # 输入密码
            click_element(register_button_sure)  # 点击确定
            assert_result_text(register_assert_alert, register_assert_alert_register_email)  # 提示【当前邮箱已注册】
        elif app_select == 'yale':
            # 我同意按钮
            click_element(register_main_invited)  # 点击【收到邀请】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            assert_result_displayed(login_login_create_account, True)  # 显示【创建账号】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C246(self):
        if app_select == 'panpan':
           pass
        elif app_select == 'yale':
            # 注册页UI测试
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start) # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            click_element(register_register_phone_area)  # 点击【手机区号】
            click_element(register_phone_area_select)  # 选择【Afghanistan】
            assert_result_displayed(register_phone_area_select, True)  # 手机区号显示【+93】
            assert_result_displayed(register_register_first_name, True)  # 显示【名】
            assert_result_displayed(register_register_last_name, True)  # 显示【姓】
            assert_result_displayed(register_register_mail, True)  # 显示【邮箱地址】
            assert_result_displayed(register_register_phone, True)  # 显示【手机号】
            assert_result_displayed(register_register_password, True)  # 显示【密码】
            assert_result_displayed(register_register_password_visible, True)  # 显示【密码不可见】
            click_element(register_register_password_visible)
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C247(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 注册返回按钮
            click_element(register_main_register)  # 点击【注册】
            driver.press_keycode(4)  # 点击【返回】
            assert_result_displayed(register_main_register, True)  # 显示【注册】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C248(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 昵称为空
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱
            send_keys(register_register_phone, UserInfo.Four.phone)   # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)   # 输入密码
            click_element(register_register_next)  # 点击下一步
            assert_result_text(register_register_alert_firstname, register_register_alert_first_name)  # 提示【空的名】
            assert_result_text(register_register_alert_lastname, register_register_alert_last_name)  # 提示【空的姓】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_register_tc022(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 昵称超过12位
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
            send_keys(register_register_first_name, UserInfo.Four.name_first_12)  # 输入名超过12位
            send_keys(register_register_last_name, UserInfo.Four.name_last_12)    # 输入姓超过12位
            click_element(register_register_next)  # 点击下一步
            assert_result_displayed(register_userIcon_take_photo, True)  # 显示下一页【拍照】按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_register_tc023(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 昵称含空格
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
            send_keys(register_register_first_name, UserInfo.Four.name_first_space)  # 输入名含空格
            send_keys(register_register_last_name, UserInfo.Four.name_last_space)    # 输入姓含空格
            click_element(register_register_next)  # 点击下一步
            assert_result_displayed(register_userIcon_take_photo, True)  # 显示下一页【拍照】按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_register_tc024(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 昵称含特殊字符
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
            send_keys(register_register_first_name, UserInfo.Four.name_first_special)  # 输入名含特殊字符
            send_keys(register_register_last_name, UserInfo.Four.name_last_special)  # 输入姓含特殊字符
            click_element(register_register_next)  # 点击下一步
            assert_result_displayed(register_userIcon_take_photo, True)  # 显示下一页【拍照】按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C249(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 邮箱地址非法
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email_wrong)  # 输入非法邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            click_element(register_register_next)  # 点击下一步
            assert_result_text(register_register_alert_email, register_register_alert_email_error)  # 提示邮箱非法
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C250(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 手机号地区测试
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            assert_result_text(register_register_phone_area_text, register_register_phone_area_default)  # 默认+86
            click_element(register_register_phone_area)  # 点击地区区号
            send_keys(register_phone_area_search, UserInfo.Four.area)  # 搜索国家区号
            assert_result_text(register_phone_area_search_match, UserInfo.Four.area)  # 匹配搜索结果
            click_element(register_phone_area_search_result)  # 点击搜索结果
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C251(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 手机号为空
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            click_element(register_register_next)  # 点击下一步
            assert_result_displayed(register_userIcon_take_photo, False)  # 未显示下一页【拍照】按钮
        else:
            assert False
#
    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C252(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 手机号小于11位
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone_wrong)  # 输入手机号小于11位
            click_element(register_register_next)  # 点击下一步
            assert_result_displayed(register_userIcon_take_photo, False)  # 未显示下一页【拍照】按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C253(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 手机号大于11位
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone_wrong_15)  # 输入手机号大于11位
            assert_result_text(register_register_phone, '15216762426')  # 手机号输入框只显示11位,实际显示了13位
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C254(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 手机号为空格
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone_space)  # 输入手机号为空格
            click_element(register_register_next)  # 点击下一步
            assert_result_displayed(register_userIcon_take_photo, False)  # 未显示下一页【拍照】按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C255(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 手机号含中文
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, '152我16762426')  # 输入手机号含中文
            assert_result_text(register_register_phone, '15216762426')  # 验证手机号不含中文
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C256(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 手机号含英文
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, '152xyz16762426')  # 输入手机号含英文
            assert_result_text(register_register_phone, '15216762426')  # 验证手机号不含英文,手机实际显示1529991676242
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C257(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 手机号含特殊字符
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, '152!@#16762426')  # 输入手机号含特殊字符
            assert_result_text(register_register_phone, '15216762426')  # 验证手机号不含特殊字符,手机实际显示152!@#1676242
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C258(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 密码为空
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            click_element(register_register_next)  # 点击下一步
            assert_result_displayed(register_userIcon_take_photo, False)  # 未显示下一页【拍照】按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C259(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 密码小于8位
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password_short)  # 输入密码小于8位
            click_element(register_register_next)  # 点击下一步
            assert_result_text(register_register_assert_password_strength, register_register_assert_alert_password)  # 提示密码非法
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C260(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 密码大于8位但不符合规则
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password_wrong)  # 密码大于8位但不符合规则
            click_element(register_register_next)  # 点击下一步
            assert_result_text(register_register_assert_password_strength, register_register_assert_alert_password)  # 提示密码非法
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C262(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 8位密码注册
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 8位密码注册
            click_element(register_register_next)  # 点击下一步
            assert_result_displayed(register_userIcon_take_photo, True)  # 显示下一页【拍照】按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C263(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 20位密码注册
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password_20)  # 20位密码注册
            click_element(register_register_next)  # 点击下一步
            assert_result_displayed(register_userIcon_take_photo, True)  # 显示下一页【拍照】按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C261(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 特殊字符密码注册
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password_special)  # 输入全部特殊字符
            click_element(register_register_next)  # 点击下一步
            assert_result_text(register_register_assert_password_strength, register_register_assert_alert_password)  # 提示密码非法
        else:
            assert False

    # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    # @adb_log
    # def C264(self):
    #     if app_select == 'panpan':
    #        pass
    #     elif app_select == 'yale':
    #         # 密码显示或隐藏默认状态测试
    #         click_element(register_main_register)  # 点击【注册】
    #         click_element(register_register_start)  # 点击【开始】
    #         click_element(register_policy_agree)  # 点击【我同意】
    #         send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
    #         assert_result_text(register_register_password, UserInfo.Four.password)  # 验证密码默认隐藏
    #         click_element(register_register_password_visible)
    #         assert_result_text(register_register_password, UserInfo.Four.password)   # 验证密码显示
    #
    #     else:
    #         assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C266(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 正常注册流程测试
            user_info = phone_number_create()
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, user_info[1])  # 输入名
            send_keys(register_register_last_name, user_info[1])  # 输入姓
            send_keys(register_register_mail, user_info[2])  # 输入邮箱
            send_keys(register_register_phone, user_info[0])  # 输入手机号
            send_keys(register_register_password, user_info[3])  # 输入密码
            click_element(register_register_next)  # 点击下一步
            click_element(register_userIcon_skip, after_action_sleep=5)  # 点击跳过这步
            send_keys(register_verifyCode_code_input, verification_code_phone(user_info[0]))  # 输入正确手机验证码
            send_keys(register_verifyCode_code_input, verification_code_email(user_info[2]))  # 输入正确邮箱验证码
            assert_result_displayed(homePage_homePage_add_device, True)  # 注册成功进入首页显示添加设备按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C268(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证码错误
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
            click_element(register_register_next)  # 点击下一步
            click_element(register_userIcon_skip)  # 点击跳过这步
            send_keys(register_verifyCode_code_input, '000000')   # 输入错误验证码
            assert_result_text(register_verifyCode_code_input, '')  # 验证输入错误验证码后自动清除验证码
        else:
            assert False

    @adb_log
    def test_C269(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 手机验证码重发
            user_info = phone_number_create()
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, user_info[1])  # 输入名
            send_keys(register_register_last_name, user_info[1])  # 输入姓
            send_keys(register_register_mail, user_info[2])  # 输入邮箱
            send_keys(register_register_phone, user_info[0])  # 输入手机号
            send_keys(register_register_password, user_info[3])  # 输入密码
            click_element(register_register_next)  # 点击下一步
            click_element(register_userIcon_skip, after_action_sleep=5)  # 点击跳过这步
            send_keys(register_verifyCode_code_input, '123456')  # 输入错误验证码
            click_element(register_verifyCode_no_code)  # 点击未收到验证码
            click_element(register_verifyCode_send_again)  # 点击【再发一次】按钮
            click_element(register_verifyCode_send_again_ok)  # 点击好的
            send_keys(register_verifyCode_code_input, verification_code_phone(user_info[0]))  # 输入正确手机验证码
            send_keys(register_verifyCode_code_input, verification_code_email(user_info[2]))  # 输入正确邮箱验证码
            assert_result_displayed(homePage_homePage_add_device, True)  # 注册成功进入首页显示添加设备按钮
        else:
            assert False

    @adb_log
    def test_C270(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 邮箱验证码重发
            user_info = phone_number_create()
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, user_info[1])  # 输入名
            send_keys(register_register_last_name, user_info[1])  # 输入姓
            send_keys(register_register_mail, user_info[2])  # 输入邮箱
            send_keys(register_register_phone, user_info[0])  # 输入手机号
            send_keys(register_register_password, user_info[3])  # 输入密码
            click_element(register_register_next)  # 点击下一步
            click_element(register_userIcon_skip, after_action_sleep=5)  # 点击跳过这步
            send_keys(register_verifyCode_code_input, verification_code_phone(user_info[0]))  # 输入正确手机验证码
            send_keys(register_verifyCode_code_input, '222222')  # 输入错误验证码
            click_element(register_verifyCode_no_code)  # 点击未收到验证码
            click_element(register_verifyCode_send_again)  # 点击【再发一次】按钮
            click_element(register_verifyCode_send_again_ok)  # 点击好的
            send_keys(register_verifyCode_code_input, verification_code_email(user_info[2]))  # 输入正确邮箱验证码
            assert_result_displayed(homePage_homePage_add_device, True)  # 注册成功进入首页显示添加设备按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C271(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 更改电话取消按钮
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
            click_element(register_register_next)  # 点击下一步
            click_element(register_userIcon_skip, after_action_sleep=60)  # 点击跳过这步
            click_element(register_verifyCode_no_code)  # 60s后点击未收到验证码
            assert_result_displayed(register_verifyCode_change_phone, True)  # 显示【更改电话】按钮
            assert_result_displayed(register_verifyCode_send_again, True)  # 显示【再发一次】按钮
            click_element(register_verifyCode_change_phone)  # 点击更改电话
            assert_result_text(register_verifyCode_change_phone_title, register_verifyCode_assert_change_phone_title ) #更新电话号码弹框
            click_element(register_verifyCode_cancel_change)  # 取消更改电话
            assert_result_displayed(register_verifyCode_code_view, True)  # 回到验证码输入页
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C272(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 更改电话保存按钮
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
            click_element(register_register_next)  # 点击下一步
            click_element(register_userIcon_skip, after_action_sleep=60)  # 点击跳过这步
            click_element(register_verifyCode_no_code)  # 60s后点击未收到验证码
            click_element(register_verifyCode_change_phone)  # 点击更改电话
            click_element(register_verifyCode_cancel_change)  # 取消更改电话
            click_element(register_verifyCode_no_code)  # 点击未收到验证码
            click_element(register_verifyCode_change_phone)  # 点击更改电话
            send_keys(register_verifyCode_change_phone_new, UserInfo.Four.phone_new)  #更改手机号
            click_element(register_verifyCode_change_phone_save)  # 点击保存
            assert_result_text(register_verifyCode_send_again_title, register_verifyCode_assert_send_again_title) #弹框重新发送手机验证码
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C273(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 更改电话好的按钮
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
            click_element(register_register_next)  # 点击下一步
            click_element(register_userIcon_skip, after_action_sleep=60)  # 点击跳过这步
            click_element(register_verifyCode_no_code)  # 60s后点击未收到验证码
            click_element(register_verifyCode_change_phone)  # 点击更改电话
            click_element(register_verifyCode_cancel_change)  # 取消更改电话
            click_element(register_verifyCode_no_code)  # 点击未收到验证码
            click_element(register_verifyCode_change_phone)  # 点击更改电话
            send_keys(register_verifyCode_change_phone_new, UserInfo.Four.phone_new)  #更改手机号
            click_element(register_verifyCode_change_phone_save)  # 点击保存
            assert_result_text(register_verifyCode_send_again_title, register_verifyCode_assert_send_again_title) #弹框重新发送手机验证码
            click_element(register_verifyCode_send_again_ok)  # 点击更改电话好的按钮
            assert_result_displayed(register_verifyCode_code_view, True)  # 返回手机验证页
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C274(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 更改电话验证码测试
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
            click_element(register_register_next)  # 点击下一步
            click_element(register_userIcon_skip, after_action_sleep=60)  # 点击跳过这步
            click_element(register_verifyCode_no_code)  # 60s后点击未收到验证码
            click_element(register_verifyCode_change_phone)  # 点击更改电话
            click_element(register_verifyCode_cancel_change)  # 取消更改电话
            click_element(register_verifyCode_no_code)  # 点击未收到验证码
            click_element(register_verifyCode_change_phone)  # 点击更改电话
            send_keys(register_verifyCode_change_phone_new, UserInfo.Four.phone_new)  #更改手机号
            click_element(register_verifyCode_change_phone_save)  # 点击保存
            click_element(register_verifyCode_send_again_ok)  # 点击更改电话好的按钮
            send_keys(register_verifyCode_code_input, verification_code_phone(UserInfo.Four.phone_new)) # 输入更改手机后的验证码
            send_keys(register_verifyCode_code_input, verification_code_email(UserInfo.Four.email))   # 输入正确邮箱验证码
            assert_result_displayed(homePage_homePage_add_device, True)  # 注册成功进入首页显示添加设备按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C277(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 上传头像页UI测试
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
            click_element(register_register_next)  # 点击下一步
            assert_result_displayed(register_UserIcon_show_image, True)  # 显示头像
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C278(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 跳过这一步按钮
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
            click_element(register_register_next)  # 点击下一步
            click_element(register_userIcon_skip)  # 点击跳过这步
            assert_result_displayed(register_verifyCode_code_input, True)  # 显示手机验证码
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C279(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 从图库中选择按钮
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
            click_element(register_register_next)  # 点击下一步
            click_element(register_userIcon_select_from_album)  # 点击从图片中选择
            click_element(register_UserIcon_select_image)  # 选择手机相册中第一张图片
            click_element(register_UserIcon_upload_image_next)  # 点击下一步
            assert_result_displayed(register_verifyCode_code_input, True)  # 显示手机验证码
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C280(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 拍照片按钮
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
            click_element(register_register_next)  # 点击下一步
            click_element(register_userIcon_take_photo)  # 点击拍照
            click_element(register_UserIcon_shutter_button)   # 调用手机拍照功能
            click_element(register_UserIcon_sure_button)  # 确定上传头像
            click_element(register_UserIcon_upload_image_next)  # 点击下一步
            assert_result_displayed(register_verifyCode_code_input, True)  # 显示手机验证码
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_register_tc057(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 手机号重复注册
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入注册过的手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
            click_element(register_register_next)  # 点击下一步
            click_element(register_userIcon_skip)  # 点击跳过这步
            send_keys(register_verifyCode_code_input, verification_code_phone(UserInfo.Four.phone))  # 输入手机验证码
            send_keys(register_verifyCode_code_input, verification_code_email(UserInfo.Four.email))   # 输入邮箱验证码
            assert_result_displayed(homePage_homePage_add_device, True)  # 注册成功进入首页显示添加设备按钮
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_register_tc058(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 邮箱地址重复注册
            click_element(register_main_register)  # 点击【注册】
            click_element(register_register_start)  # 点击【开始】
            click_element(register_policy_agree)  # 点击【我同意】
            send_keys(register_register_first_name, UserInfo.Four.name_first)  # 输入名
            send_keys(register_register_last_name, UserInfo.Four.name_last)  # 输入姓
            send_keys(register_register_mail, UserInfo.Four.email)  # 输入注册过的邮箱地址
            send_keys(register_register_phone, UserInfo.Four.phone)  # 输入手机号
            send_keys(register_register_password, UserInfo.Four.password)  # 输入密码
            click_element(register_register_next)  # 点击下一步
            click_element(register_userIcon_skip)  # 点击跳过这步
            send_keys(register_verifyCode_code_input, verification_code_phone(UserInfo.Four.phone))  # 输入手机验证码
            send_keys(register_verifyCode_code_input, verification_code_email(UserInfo.Four.email))  # 输入邮箱验证码
            assert_result_displayed(homePage_homePage_add_device, True)  # 注册成功进入首页显示添加设备按钮
        else:
            assert False

    def cases(self):
        return list(filter(lambda m: m.startswith("test_C") and callable(getattr(self, m)), dir(self)))
