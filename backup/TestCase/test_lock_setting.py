import logging
import unittest
from Configuration.OperationMethod import *
from Configuration import StartUp
import random
import os


RunOnWhere = 'Server'
if RunOnWhere == 'Server':
    # 第九处修改 # run on server
    autoLock = os.environ['AutoLock']
    # print('autoLock=' + autoLock)
elif RunOnWhere == 'local':
    # run on local
    autoLock = True
else:
    raise ValueError


class TestLockSetting(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = StartUp.driver
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H_%M_%S',
                            filename=FolderList.logging_name,
                            filemode='a')
        logging.getLogger()
        time.sleep(1)

    def add_img(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

    def setUp(self) -> None:
        os.system('adb shell svc wifi enable')
        bt_switch('on')
        # add_card_yale()
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    @adb_log
    def test_C000(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 设置前置条件
            try:
                if check_element(userManagement_main_home_tab, wait_time=5, check_type=False) is False:
                    add_lock_yale_new()
                    # click_element(lock_main_lock_home_button)  # 点击home按钮
            except NameError:
                pass
            # else:
                # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            invite_user_yale('admin', UserInfo.AdminJoyce.phone2, UserInfo.AdminJoyce.name2)  # 邀请管理员B
        else:
            assert False

    @adb_log
    def test_C574(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            add_lock()
            wifi_switch('close')
            time.sleep(28)  # 30秒重新连接
            assert_result_displayed(lock_assert_lock_status_connecting, True, wait_time=40)  # 显示【正在连接】
            wifi_switch('open')
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 检查蓝牙连接
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            try:
                if check_element(userManagement_main_home_tab, wait_time=5, check_type=False) is False:
                    add_lock_yale_new()
                    # click_element(lock_main_lock_home_button)  # 点击home按钮
            except NameError:
                pass
            check_lock_connection()
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
        else:
            assert False

    @adb_log
    def test_C575(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查断开蓝牙连接时的状态
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            bt_switch('off')
            assert_result_displayed(lock_lockSetting_assert_bt_not_open, True)  # 显示【蓝牙未打开】
            # logout('house')
            bt_switch('on')
        else:
            assert False

    @adb_log
    def test_C576(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            add_lock()
            bt_switch('off')
            assert_result_displayed(lock_assert_bt_not_open, True)  # 显示【蓝牙未打开】
            bt_switch('on')
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 检查蓝牙断开重连
            check_lock_connection()
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            bt_switch('off')
            assert_result_displayed(lock_lockSetting_assert_bt_not_open, True)  # 显示【蓝牙未打开】
            bt_switch('on')
            check_lock_connection()
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C579(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
            add_lock()
            assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
            time.sleep(28)  # 30秒重新连接
            assert_result_displayed(lock_assert_lock_status_connecting, True, wait_time=40)  # 显示【正在连接】
            assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
            click_element(lock_button_open_lock)  # 点击【开锁】
            assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
            assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 检查锁的自动重连
            time.sleep(30)  # 30秒断开连接
            # assert_result_displayed(lock_main_lock_connect_status, True)  # 显示【连接中】
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【锁关闭】
            click_element(lock_main_user_lock_button)
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【锁开启】
        else:
            assert False

    @adb_log
    def test_C580(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            add_lock()
            auto_lock('off')
            wifi_switch('close')
            click_element(user_button_lock)  # 点击【锁】
            assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
            click_element(lock_button_open_lock, after_action_sleep=10)  # 点击【开锁】
            assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
            click_element(lock_button_open_lock)  # 点击【关锁】
            assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
            wifi_switch('open')
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 检查开关锁的页面显示
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            check_lock_connection()
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【锁关闭】
            assert_result_displayed(homePage_else_my_house_name, True)  # 显示住宅名称
            assert_result_displayed(lock_main_my_lock_name, True)  # 显示锁名称
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C581(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
            add_lock()
            invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
            add_pin(UserInfo.Second.name, UserInfo.Second.pin)
            add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb)
            wifi_switch('close')
            click_element(user_button_lock)  # 点击【锁】
            assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
            time.sleep(3)
            requests.put(url=API.request_api_finger_open.format(UserInfo.Second.name, UserInfo.Second.finger_thumb))  # 指纹
            assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
            wifi_switch('open')
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 检查开关锁页的home按钮(三点)
            check_lock_connection()   # 检查锁的状态
            driver.press_keycode(4)  # 点击【返回】
            assert_result_displayed(lock_main_room_list, True)  # 显示房间列表
        else:
            assert False

    @adb_log
    def test_C582(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
            add_lock()
            auto_lock('off')
            add_pin(UserInfo.First.name, UserInfo.First.pin)
            wifi_switch('off')
            assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
            # Todo 密码开门
            assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
            time.sleep(10)
            click_element(lock_button_open_lock)  # 点击【开锁】
            assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
            wifi_switch('on')
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 验证开关锁页面状态同步_指纹
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # add_finger_yale(UserInfo.Four.name, UserInfo.Four.name, UserInfo.Four.name)  # 添加指纹
            register_FP(UserInfo.Four.name, UserInfo.Four.name, UserInfo.Four.name)    # 注册指纹
            click_element(UserManagement_back_bar_button)  # BACK
            click_element(userManagement_main_home_tab)  # lock manage
            while 1:
                try:
                    assert_result_displayed(lock_main_lock_close_status, True)
                except:
                    click_element(lock_main_user_lock_button)  # lock
                else:
                    break
            try:
                requests.put(url=API().finger_open(UserInfo.Four.name, UserInfo.Four.name))  # 指纹开门
            except:
                pass
            assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
            time.sleep(3)
            if autoLock is True:
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            else:
                click_element(lock_main_lottie)  # 连接成功，点击开锁
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
        else:
            assert False

    @adb_log
    def test_C583(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            add_lock()
            invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
            add_pin(UserInfo.Second.name, UserInfo.Second.pin)
            add_card(UserInfo.Second.name)
            click_element(user_button_lock)  # 点击锁
            assert_result_displayed(lock_assert_lock_status_closing, True)  # 显示【门锁关闭】
            requests.put(url=API().request_api_card_open)  # 门卡开门
            assert_result_displayed(lock_assert_lock_status_opening, True)  # 显示【门锁打开】
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 验证开关锁页面状态同步_门卡
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # add_card_yale(UserInfo.Four.name)   # 添加门卡
            register_RFID(UserInfo.Four.name)   # 注册门卡
            click_element(UserManagement_back_bar_button)  # BACK
            click_element(userManagement_main_home_tab)  # lock manage
            while 1:
                try:
                    assert_result_displayed(lock_main_lock_close_status, True)
                except:
                    click_element(lock_main_user_lock_button)  # lock
                else:
                    break
            try:
                requests.put(url=API().card_open())  # 门卡开门
            except:
                pass
            assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
            time.sleep(3)
            if autoLock is True:
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            else:
                click_element(lock_main_lottie)  # 连接成功，点击开锁
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
        else:
            assert False

    @adb_log
    def test_C584(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证开关锁页面状态同步_密码
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # password_register(UserInfo.Four.name, UserInfo.Four.phone)  # 设置密码
            pin = register_PINCODE(UserInfo.Four.name)    # 注册密码
            click_element(userManagement_main_home_tab)  # lock manage
            while 1:
                try:
                    assert_result_displayed(lock_main_lock_close_status, True)
                except:
                    click_element(lock_main_user_lock_button)  # lock
                else:
                    break
            try:
                requests.put(url=API().pin_open(pin))  # 密码开门
            except:
                pass
            assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
            time.sleep(3)
            if autoLock is True:
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            else:
                click_element(lock_main_lottie)  # 连接成功，点击开锁
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
        else:
            assert False

    @adb_log
    def test_C588(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证自动上锁打开时的快速开关锁
            check_lock_connection()
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            click_element(lock_main_user_lock_button)  # 点击锁
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            for i in range(5):
                time.sleep(3)
                click_element(lock_main_user_lock_button)  # 点击锁
                assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            # logout('house')
        else:
            assert False

    @unittest.skipIf(app_select == 'yale', 'Not stable')
    @adb_log
    def test_lock_setting_tc016(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证自动上锁关闭时的快速开关锁
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            auto_lock_switch('关闭')  # 关闭自动上锁
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            click_element(lock_main_user_lock_button)  # 点击锁
            time.sleep(10)
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】然后容易变成无法连接
            click_element(lock_main_user_lock_button)  # 点击锁
            time.sleep(5)
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            for i in range(5):
                click_element(lock_main_user_lock_button)  # 点击锁
                time.sleep(5)
                assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
                click_element(lock_main_user_lock_button)  # 点击锁
                time.sleep(5)
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C590(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证管理员蓝牙开关锁
            check_lock_connection()
            # if autoLock is True:
            #     auto_lock_switch('开启')  # 开启自动上锁
            # else:
            #     pass
            click_element(lock_main_user_lock_button)   # 点击锁
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【锁开启】
            if autoLock is True:
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            else:
                click_element(lock_main_lottie)  # 连接成功，点击开锁
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
        else:
            assert False

    @adb_log
    def test_C591(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证管理员指纹开关锁
            register_FP(UserInfo.AdminJoyce.name2, UserInfo.AdminJoyce.name2, UserInfo.AdminJoyce.name2)  # 注册指纹
            click_element(UserManagement_back_bar_button)  # BACK
            click_element(userManagement_main_home_tab)  # lock manage
            while 1:
                try:
                    assert_result_displayed(lock_main_lock_close_status, True)
                except:
                    click_element(lock_main_user_lock_button)  # lock
                else:
                    break
            try:
                requests.put(url=API().finger_open(UserInfo.AdminJoyce.name2, UserInfo.AdminJoyce.name2))  # 指纹开门
            except:
                pass
            assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
            if autoLock is True:
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            else:
                click_element(lock_main_lottie)  # 连接成功，点击开锁
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
        else:
            assert False

    @adb_log
    def test_C592(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证管理员门卡开关锁
            # delete_card_yale()  # 删除门卡
            delete_RFID(UserInfo.Four.name)   # 删除门卡
            register_RFID(UserInfo.AdminJoyce.name2)   # 注册门卡
            click_element(UserManagement_back_bar_button)  # BACK
            click_element(userManagement_main_home_tab)  # lock manage
            while 1:
                try:
                    assert_result_displayed(lock_main_lock_close_status, True)
                except:
                    click_element(lock_main_user_lock_button)  # lock
                else:
                    break
            try:
                requests.put(url=API().card_open())  # 门卡开门
            except:
                pass
            assert_result_displayed(lock_main_lock_open_status, True)  # unlocked
            time.sleep(3)
            if autoLock is True:
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            else:
                click_element(lock_main_lottie)  # 连接成功，点击开锁
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
        else:
            assert False

    @adb_log
    def test_C593(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证管理员密码开关锁
            password_register(UserInfo.AdminJoyce.name2, UserInfo.AdminJoyce.phone2)  # 设置密码
            click_element(userManagement_main_home_tab)  # lock manage
            while 1:
                try:
                    assert_result_displayed(lock_main_lock_close_status, True)
                except:
                    click_element(lock_main_user_lock_button)  # lock
                else:
                    break
            try:
                requests.put(url=API().password_open(UserInfo.AdminJoyce.phone2))  # 密码开门
            except:
                pass
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            if autoLock is True:
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            else:
                click_element(lock_main_lottie)  # 连接成功，点击开锁
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
        else:
            assert False

    @adb_log
    def test_C594(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 邀请普通用户A
            # login(UserInfo.AdminJoyce.phone, UserInfo.AdminJoyce.email, UserInfo.AdminJoyce.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            invite_user_yale('normal', UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.name)  # 邀请普通用户A
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_edit_house_owner)  # 点击编辑房屋管理员
            scroll_and_click(lock_setting_admin_scroll_house_owner, lock_setting_admin_permission_check.format(UserInfo.NormalJoyce.name))   # 勾选普通用户A
            click_element(lock_setting_owner_back_button)  # 返回
            logout('house')
            # 验证普通用户蓝牙开关锁
            login(UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.email, UserInfo.NormalJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            click_element(lock_main_user_lock_button)   # 点击锁
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【锁开启】
            if autoLock is True:
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            else:
                click_element(lock_main_lottie)  # 连接成功，点击开锁
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            logout('house')
        else:
            assert False

    @adb_log
    def test_C595(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证普通用户指纹开关锁
            # login(UserInfo.AdminJoyce.phone, UserInfo.AdminJoyce.email, UserInfo.AdminJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(UserManagement_user_list)  # 点击【用户列表】
            try:
                if check_element(UserManagement_user_normal.format(UserInfo.NormalJoyce.name), wait_time=5, check_type=False) is False:
                    invite_user_yale('normal', UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.name)  # 邀请普通用户A
            except NameError:
                pass
            register_FP(UserInfo.NormalJoyce.name, UserInfo.NormalJoyce.name, UserInfo.NormalJoyce.name)
            # add_finger_yale(UserInfo.NormalJoyce.name, UserInfo.NormalJoyce.name, UserInfo.NormalJoyce.name, user_level='normal')
            click_element(UserManagement_back_bar_button)  # BACK
            logout('house')
            login(UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.email, UserInfo.NormalJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()   # 检查锁的连接
            try:
                requests.put(url=API().finger_open(UserInfo.NormalJoyce.name, UserInfo.NormalJoyce.name))  # 指纹开门
            except:
                pass
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            if autoLock is True:
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            else:
                click_element(lock_main_lottie)  # 连接成功，点击开锁
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            logout('house')
        else:
            assert False

    @adb_log
    def test_C596(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证普通用户门卡开关锁
            # login(UserInfo.AdminJoyce.phone, UserInfo.AdminJoyce.email, UserInfo.AdminJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            delete_RFID('admin2 shen')
            register_RFID('normal shen')
            # add_card_yale(UserInfo.NormalJoyce.name, user_level='normal')  # 添加门卡
            click_element(UserManagement_back_bar_button)  # BACK
            logout('house')
            login(UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.email, UserInfo.NormalJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            try:
                requests.put(url=API().card_open())  # 门卡开门
            except:
                pass
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            if autoLock is True:
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            else:
                click_element(lock_main_lottie)  # 连接成功，点击开锁
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            logout('house')
        else:
            assert False

    @adb_log
    def test_C597(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证普通用户密码开关锁
            # login(UserInfo.AdminJoyce.phone, UserInfo.AdminJoyce.email, UserInfo.AdminJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            password_register(UserInfo.NormalJoyce.name, UserInfo.NormalJoyce.phone)  # 设置密码
            logout('house')
            login(UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.email, UserInfo.NormalJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            try:
                requests.put(url=API().password_open(UserInfo.NormalJoyce.phone))  # 密码开门
            except:
                pass
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            if autoLock is True:
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            else:
                click_element(lock_main_lottie)  # 连接成功，点击开锁
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            logout('house')
        else:
            assert False

    @adb_log
    def test_C600(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证锁的设置页UI
            click_element(lock_setting_setting_button)  # 点击设置按钮
            assert_result_displayed(lock_setting_house_setting, True)  # 显示【房屋设置】
            assert_result_displayed(lock_setting_lock_setting, True)  # 显示【锁设置】
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C601(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证家的名字和照片页面进入
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_house_name_image)   # 点击房屋名称和照片
            assert_result_displayed(lock_setting_house_name, True)  # 显示房屋名字
            assert_result_displayed(lock_setting_house_image_button, True)  # 显示房屋照片按钮
            driver.press_keycode(4)
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C602(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证家的名字和照片页面返回
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_house_name_image)   # 点击房屋名称和照片
            assert_result_displayed(lock_setting_house_name, True)  # 显示房屋名字
            assert_result_displayed(lock_setting_house_image_button, True)  # 显示房屋照片按钮
            click_element(lock_setting_back_button)  # 点击返回
            assert_result_displayed(lock_setting_house_setting, True)  # 返回房屋设置页
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C603(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证锁的设置页主页按钮(左上角)
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(homePage_homePage_menu_in_other_page)  # 点击菜单
            assert_result_displayed(homePage_menu_user_icon, True)  # 显示用户头像
            click_element(homePage_menu_user_icon)  # 点击用户头像
            click_element(homePage_user_logout)  # 点击【退出】
            # click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @adb_log
    def test_C605(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证锁的设置在设置页显示
            click_element(lock_setting_setting_button)  # 点击设置按钮
            assert_result_displayed(lock_setting_lock_name, True)  # 显示锁名字
            assert_result_displayed(lock_setting_lock_icon, True)  # 显示锁图标
            assert_result_displayed(lock_setting_lock_setting, True)  # 显示锁设置按钮
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C606(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证家的设置页名字和照片显示页UI
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_house_name_image)   # 点击房屋名称和照片
            assert_result_displayed(lock_setting_house_name, True)  # 显示房屋名字
            assert_result_displayed(lock_setting_house_image_button, True)  # 显示房屋照片按钮
            driver.press_keycode(4)
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C607(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证家的设置页家的编辑页名称设置
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_house_name_image)  # 点击房屋名称和照片
            click_element(lock_setting_house_name)     # 点击房屋名字
            # send_keys(lock_setting_house_name_edit, '')   # 编辑住宅名为空
            click_element(lock_setting_save_button)    # 点击保存
            assert_result_text(lock_setting_alert_house_name, lock_setting_alert_name_notnull)  # 提示房间名字不能为空
            driver.press_keycode(4)
        else:
            assert False

    @adb_log
    def test_C608(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            add_lock()
            click_element(user_button_setting)  # 点击【锁】
            click_element(user_button_setting_lock)  # 点击【锁的设置】
            assert_result_text(lock_assert_lock_setting_name, 'selected_lock_name')
            click_element(homepage_button_detail_back)  # 点击【返回】
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 验证家的设置页家的编辑页名称修改
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_house_name_image)  # 点击房屋名称和照片
            click_element(lock_setting_house_name)  # 点击房屋名字
            send_keys(lock_setting_house_name_edit, '啦啦啦')  # 修改住宅名
            click_element(lock_setting_save_button)  # 点击保存按钮
            click_element(lock_setting_save_name)  # 保存修改
            click_element(lock_setting_house_name)  # 点击房屋名字
            send_keys(lock_setting_house_name_edit, '老洋房')   # 修改住宅名
            click_element(lock_setting_save_button)  # 点击保存按钮
            assert_result_text(lock_setting_house_name, '老洋房')  # 房间名字修改成功
            click_element(lock_setting_save_name)  # 保存修改
            click_element(lock_setting_back_button)  # 点击返回
            click_element(homePage_homePage_menu_in_other_page)    # 点击菜单
            click_element(homePage_menu_home)   # 点击首页
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            assert_result_text(homePage_else_my_house_name, '老洋房')  # 房间名字修改成功
        else:
            assert False

    @adb_log
    def test_C609(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            add_lock()
            click_element(user_button_setting)  # 点击【锁】
            click_element(user_button_setting_lock)  # 点击【锁的设置】
            click_element(lock_button_lock_setting_name)  # 点击【锁的名字】
            click_element(lock_button_setting_name_input)  # 点击锁名输入框
            clear_text_content(lock_button_setting_name_input)  # 清除输入
            send_keys(lock_button_setting_name_input, 'selected_lock_name_new')
            click_element(user_button_user_pin_save)  # 点击【保存】
            assert_result_text(lock_assert_lock_setting_name, 'selected_lock_name_new')
            click_element(lock_button_lock_setting_name)  # 点击【锁的名字】
            click_element(lock_button_setting_name_input)  # 点击锁名输入框
            clear_text_content(lock_button_setting_name_input)  # 清除输入
            send_keys(lock_button_setting_name_input, 'selected_lock_name')
            click_element(user_button_user_pin_save)  # 点击【保存】
            click_element(homepage_button_detail_back)  # 点击【返回】
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 验证家的设置页名称取消编辑
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_house_name_image)  # 点击房屋名称和照片
            click_element(lock_setting_house_name)  # 点击房屋名字
            send_keys(lock_setting_house_name_edit, '别墅')   # 修改住宅名
            click_element(lock_setting_cancel_button)  # 点击取消按钮
            assert_result_text(lock_setting_house_name, '老洋房')  # 房间显示原有名字
            click_element(lock_setting_back_button)  # 点击返回
            assert_result_text(homePage_else_my_house_name, '老洋房')  # 房间显示原有名字
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C610(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证家的图片编辑，拍照
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_house_name_image)  # 点击房屋名称和照片
            click_element(lock_setting_house_image_button)  # 点击房屋照片按钮
            click_element(homePage_else_take_house_photo)  # 点击拍一张照片
            click_element(register_UserIcon_shutter_button)  # 按下快门
            click_element(register_UserIcon_sure_button)  # 确定
            assert_result_displayed(lock_setting_house_image_button, True)  # 显示房屋照片按钮
        else:
            assert False

    @adb_log
    def test_C611(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证家的图片编辑，系统相册
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_house_name_image)  # 点击房屋名称和照片
            click_element(lock_setting_house_image_button)  # 点击房屋照片按钮
            click_element(homePage_else_choose_house_image)  # 点击选择图片
            click_element(homePage_else_image_from_phone)   # 选择相册第一张照片
            assert_result_displayed(lock_setting_house_image_button, True)  # 显示房屋照片按钮
        else:
            assert False

    @adb_log
    def test_C612(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证家的图片再次编辑
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_house_name_image)  # 点击房屋名称和照片
            click_element(lock_setting_house_image_button)  # 点击房屋照片按钮
            click_element(homePage_else_choose_house_image)  # 点击选择图片
            click_element(lock_setting_another_phone_image)   # 选择相册第三张照片
            assert_result_displayed(lock_setting_house_image_button, True)  # 显示房屋照片按钮
        else:
            assert False

    @adb_log
    def test_C613(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证普通用户编辑家的名称
            click_element(UserManagement_user_list)  # 点击【用户列表】
            try:
                if check_element(UserManagement_user_normal.format(UserInfo.NormalJoyce.name), wait_time=5, check_type=False) is False:
                    invite_user_yale('normal', UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.name)  # 邀请普通用户A
            except NameError:
                pass
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_edit_house_owner)  # 点击编辑房屋管理员
            while 1:
                try:
                    driver.find_element_by_xpath(lock_setting_admin_permission_check.format(UserInfo.NormalJoyce.name))
                except:
                    scroll_screen_up()  # 上滑
                else:
                    break
            if driver.find_element_by_xpath(lock_setting_admin_permission_check.format(UserInfo.NormalJoyce.name)).get_attribute('checked') == 'false':
                click_element(lock_setting_admin_permission_check.format(UserInfo.NormalJoyce.name))  # 勾选普通用户A
                click_element(lock_setting_owner_back_button)  # 返回
            else:
                click_element(lock_setting_owner_back_button)  # 返回
            logout('house')
            login(UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.email, UserInfo.NormalJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_house_name_image)  # 点击房屋名称和照片
            click_element(lock_setting_house_name)  # 点击房屋名字
            send_keys(lock_setting_house_name_edit, '')  # 修改住宅名为空
            click_element(lock_setting_save_button)  # 点击保存按钮
            assert_result_text(lock_setting_alert_house_name, lock_setting_alert_name_notnull)  # 提示房间名字不能为空
            driver.press_keycode(4)
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C614(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            add_lock()
            click_element(user_button_setting)  # 点击【锁】
            click_element(user_button_setting_lock)  # 点击【锁的设置】
            click_element(lock_button_setting_smart_alert)  # 点击【智能提醒设置】
            click_element(lock_button_setting_add_smart_alert)  # 点击【新增智能提醒】
            assert_result_enable(lock_compound_lock_setting_smart_save, False)  # 不可点【保存】
            click_element(lock_button_lock_setting_smart_cancel)  # 点击【取消】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 验证普通用户编辑家的背景图
            # login(UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.email, UserInfo.NormalJoyce.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_house_name_image)  # 点击房屋名称和照片
            click_element(lock_setting_house_image_button)  # 点击房屋照片按钮
            click_element(homePage_else_choose_house_image)  # 点击选择图片
            click_element(homePage_else_image_from_phone)   # 选择相册第一张照片
            assert_result_displayed(lock_setting_house_image_button, True)  # 显示房屋照片按钮
            click_element(lock_setting_back_button)  # 点击返回
            logout('house')
        else:
            assert False

    @adb_log
    def test_C615(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证住宅详情页的网络断开情况
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            wifi_switch('close')
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_house_name_image)  # 点击房屋名称和照片
            assert_result_text(lock_setting_alert_house_name, lock_setting_alert_wifi_close)  # 提示获取设置时出错
            click_element(lock_setting_house_image_button)  # 点击房屋照片按钮
            click_element(homePage_else_take_house_photo)  # 点击拍一张照片
            click_element(register_UserIcon_shutter_button)  # 按下快门
            click_element(register_UserIcon_sure_button)  # 确定
            assert_result_displayed(lock_setting_upload_image_failure, True)  # 提示上传失败
            wifi_switch('open')
        else:
            assert False

    @adb_log
    def test_C616(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
            add_lock()
            auto_lock('off')
            invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
            invite_user('guest', UserInfo.Third.phone, UserInfo.Third.name, True, 'always')
            add_pin(UserInfo.Second.name, UserInfo.Second.pin)
            add_card(UserInfo.Second.name)
            add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb)
            add_pin(UserInfo.Third.name, UserInfo.Third.pin)
            click_element(user_button_setting)  # 点击【设定】
            click_element(user_button_setting_lock)  # 点击【锁的设置】
            click_element(lock_button_setting_smart_alert)  # 点击【智能提醒设置】
            click_element(lock_button_setting_add_smart_alert)  # 点击【添加智能提醒】
            click_element(lock_button_lock_setting_smart_event_choose)  # 点击【选择事件】
            click_element(lock_button_lock_setting_smart_event_user)  # 点击【门锁被某个用户解锁或上锁】
            click_element(homepage_button_select_room_or_user.format(UserInfo.Second.name))  # 选择管理员
            click_element(lock_button_lock_setting_smart_event_user_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_event_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_time_choose)  # 点击【选择时间】
            click_element(lock_button_lock_setting_smart_time_always)  # 点击【任何时间】
            click_element(lock_button_lock_setting_smart_time_save)  # 点击【保存】
            click_element(lock_compound_lock_setting_smart_save)  # 点击【保存】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】
            # logout('house')
            # 蓝牙开门通知验证
            bt_open_and_notification(user1_phone=UserInfo.Second.phone,
                                     user1_email=UserInfo.Second.email,
                                     user1_password=UserInfo.Second.password,
                                     user2_phone=UserInfo.First.phone,
                                     user2_email=UserInfo.First.email,
                                     user2_password=UserInfo.First.password,
                                     login_type='phone',
                                     notification_result=True,
                                     home_name=HouseInfo.name)

            # 蓝牙关门通知验证
            bt_close_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            auto_lock('on')
            # logout('house')
            # 门卡开门通知验证
            card_open_and_notification(user1_phone=UserInfo.Second.phone,
                                       user1_email=UserInfo.Second.email,
                                       user1_password=UserInfo.Second.password,
                                       user2_phone=UserInfo.First.phone,
                                       user2_email=UserInfo.First.email,
                                       user2_password=UserInfo.First.password,
                                       login_type='phone',
                                       notification_result=True,
                                       home_name=HouseInfo.name)
            # logout('house')
            # 指纹开门通知验证
            finger_open_and_notification(user1_phone=UserInfo.Second.phone,
                                         user1_email=UserInfo.Second.email,
                                         user1_password=UserInfo.Second.password,
                                         user1_name=UserInfo.Second.name,
                                         user1_id=UserInfo.Second.finger_thumb,
                                         user2_phone=UserInfo.First.phone,
                                         user2_email=UserInfo.First.email,
                                         user2_password=UserInfo.First.password,
                                         login_type='phone',
                                         notification_result=True,
                                         home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Third.phone,
                                      user1_email=UserInfo.Third.email,
                                      user1_password=UserInfo.Third.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 验证家的管理员权限页显示
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_edit_house_owner)  # 点击编辑房屋管理员
            while 1:
                try:
                    driver.find_element_by_xpath(lock_setting_admin_permission_name.format(UserInfo.Four.name))
                except:
                    scroll_screen_up()  # 上滑
                else:
                    break
            assert_result_displayed(lock_setting_admin_permission_check.format(UserInfo.Four.name), False)  # 登录账号的管理员没有权限勾选框
            click_element(lock_setting_owner_back_button)  # 返回
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C617(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
            add_lock()
            auto_lock('off')
            invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
            invite_user('guest', UserInfo.Third.phone, UserInfo.Third.name, True, 'always')
            add_pin(UserInfo.Second.name, UserInfo.Second.pin,)
            add_card(UserInfo.Second.name,)
            add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb)
            add_pin(UserInfo.Third.name, UserInfo.Third.pin)
            click_element(user_button_setting)  # 点击【设定】
            click_element(user_button_setting_lock)  # 点击【锁的设置】
            click_element(lock_button_setting_smart_alert)  # 点击【智能提醒设置】
            click_element(lock_button_setting_add_smart_alert)  # 点击【添加智能提醒】
            click_element(lock_button_lock_setting_smart_event_choose)  # 点击【选择事件】
            click_element(lock_button_lock_setting_smart_event_user)  # 点击【门锁被某个用户解锁或上锁】
            click_element(homepage_button_select_room_or_user.format(UserInfo.Second.name))  # 选择管理员
            click_element(lock_button_lock_setting_smart_event_user_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_event_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_time_choose)  # 点击【选择时间】
            click_element(lock_button_lock_setting_smart_time_recur)  # 点击【在一个时间段内】
            get_weekday()
            click_element(time_button_alert_end_time)  # 点击【结束时间】
            click_element(time_button_invite_keyboard)  # 点击【键盘】
            target_time = get_time(need_minute=10)  # 获取时间
            click_element(time_button_invite_input_hour)  # 点击【小时】
            clear_text_content(time_button_invite_input_hour)  # 清除输入
            send_keys(time_button_invite_input_hour, target_time[0], hide_keyboard=False)  # 输入小时
            click_element(time_button_invite_input_minute)  # 点击【分钟】
            clear_text_content(time_button_invite_input_minute)  # 清除输入
            send_keys(time_button_invite_input_minute, target_time[1], hide_keyboard=False)  # 输入分钟
            click_element(time_button_invite_temp_save)  # 点击【确定】
            click_element(lock_button_lock_setting_smart_time_save)  # 点击【保存】
            click_element(lock_compound_lock_setting_smart_save)  # 点击【保存】
            start_time = time.time()  # 获取开始时间
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】
            # logout('house')
            # 蓝牙开门通知验证
            bt_open_and_notification(user1_phone=UserInfo.Second.phone,
                                     user1_email=UserInfo.Second.email,
                                     user1_password=UserInfo.Second.password,
                                     user2_phone=UserInfo.First.phone,
                                     user2_email=UserInfo.First.email,
                                     user2_password=UserInfo.First.password,
                                     login_type='phone',
                                     notification_result=True,
                                     home_name=HouseInfo.name)

            # 蓝牙关门通知验证
            bt_close_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            auto_lock('on')
            # logout('house')
            # 门卡开门通知验证
            card_open_and_notification(user1_phone=UserInfo.Second.phone,
                                       user1_email=UserInfo.Second.email,
                                       user1_password=UserInfo.Second.password,
                                       user2_phone=UserInfo.First.phone,
                                       user2_email=UserInfo.First.email,
                                       user2_password=UserInfo.First.password,
                                       login_type='phone',
                                       notification_result=True,
                                       home_name=HouseInfo.name)
            # logout('house')
            # 指纹开门通知验证
            finger_open_and_notification(user1_phone=UserInfo.Second.phone,
                                         user1_email=UserInfo.Second.email,
                                         user1_password=UserInfo.Second.password,
                                         user1_name=UserInfo.Second.name,
                                         user1_id=UserInfo.Second.finger_thumb,
                                         user2_phone=UserInfo.First.phone,
                                         user2_email=UserInfo.First.email,
                                         user2_password=UserInfo.First.password,
                                         login_type='phone',
                                         notification_result=True,
                                         home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Third.phone,
                                      user1_email=UserInfo.Third.email,
                                      user1_password=UserInfo.Third.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            auto_lock('off')
            # logout('house')
            end_time = time.time()  # 获取结束时间
            # 提醒时间超范围外
            wait_time = int(target_time[2])*60 - int(end_time - start_time) + 60  # 总的等待时间，减去已经花费的时间，再多等一分钟
            if wait_time <= 0:
                pass
            else:
                time.sleep(int(target_time[2])*60 - int(end_time - start_time))
            # 蓝牙开门通知验证
            bt_open_and_notification(user1_phone=UserInfo.Second.phone,
                                     user1_email=UserInfo.Second.email,
                                     user1_password=UserInfo.Second.password,
                                     user2_phone=UserInfo.First.phone,
                                     user2_email=UserInfo.First.email,
                                     user2_password=UserInfo.First.password,
                                     login_type='phone',
                                     notification_result=False,
                                     home_name=HouseInfo.name)

            # 蓝牙关门通知验证
            bt_close_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=False,
                                      home_name=HouseInfo.name)
            auto_lock('on')
            # logout('house')
            # 门卡开门通知验证
            card_open_and_notification(user1_phone=UserInfo.Second.phone,
                                       user1_email=UserInfo.Second.email,
                                       user1_password=UserInfo.Second.password,
                                       user2_phone=UserInfo.First.phone,
                                       user2_email=UserInfo.First.email,
                                       user2_password=UserInfo.First.password,
                                       login_type='phone',
                                       notification_result=False,
                                       home_name=HouseInfo.name)
            # logout('house')
            # 指纹开门通知验证
            finger_open_and_notification(user1_phone=UserInfo.Second.phone,
                                         user1_email=UserInfo.Second.email,
                                         user1_password=UserInfo.Second.password,
                                         user1_name=UserInfo.Second.name,
                                         user1_id=UserInfo.Second.finger_thumb,
                                         user2_phone=UserInfo.First.phone,
                                         user2_email=UserInfo.First.email,
                                         user2_password=UserInfo.First.password,
                                         login_type='phone',
                                         notification_result=False,
                                         home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Third.phone,
                                      user1_email=UserInfo.Third.email,
                                      user1_password=UserInfo.Third.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=False,
                                      home_name=HouseInfo.name)
            factory_lock()
            logout()

        elif app_select == 'yale':
            # 验证家的管理员页面返回
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_edit_house_owner)  # 点击编辑房屋管理员
            click_element(lock_setting_owner_back_button)   # 点击房屋管理员返回键
            assert_result_displayed(lock_setting_edit_house_owner, True)  # 显示编辑房屋管理员
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C618(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
            add_lock()
            auto_lock('off')
            invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
            add_pin(UserInfo.Second.name, UserInfo.Second.pin)
            add_card(UserInfo.Second.name)
            add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb)
            click_element(user_button_setting)  # 点击【设定】
            click_element(user_button_setting_lock)  # 点击【锁的设置】
            click_element(lock_button_setting_smart_alert)  # 点击【智能提醒设置】
            click_element(lock_button_setting_add_smart_alert)  # 点击【添加智能提醒】
            click_element(lock_button_lock_setting_smart_event_choose)  # 点击【选择事件】
            click_element(lock_button_lock_setting_smart_event_user)  # 点击【门锁被某个用户解锁或上锁】
            click_element(homepage_button_select_room_or_user.format(UserInfo.Second.name))  # 选择管理员
            click_element(lock_button_lock_setting_smart_event_user_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_event_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_time_choose)  # 点击【选择时间】
            click_element(lock_button_lock_setting_smart_time_always)  # 点击【任何时间】
            click_element(lock_button_lock_setting_smart_time_save)  # 点击【保存】
            click_element(lock_compound_lock_setting_smart_save)  # 点击【保存】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】
            # logout('house')
            # 蓝牙开门通知验证
            bt_open_and_notification(user1_phone=UserInfo.Second.phone,
                                     user1_email=UserInfo.Second.email,
                                     user1_password=UserInfo.Second.password,
                                     user2_phone=UserInfo.First.phone,
                                     user2_email=UserInfo.First.email,
                                     user2_password=UserInfo.First.password,
                                     login_type='phone',
                                     notification_result=True,
                                     home_name=HouseInfo.name)

            # 蓝牙关门通知验证
            bt_close_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            auto_lock('on')
            # logout('house')
            # 门卡开门通知验证
            card_open_and_notification(user1_phone=UserInfo.Second.phone,
                                       user1_email=UserInfo.Second.email,
                                       user1_password=UserInfo.Second.password,
                                       user2_phone=UserInfo.First.phone,
                                       user2_email=UserInfo.First.email,
                                       user2_password=UserInfo.First.password,
                                       login_type='phone',
                                       notification_result=True,
                                       home_name=HouseInfo.name)
            # logout('house')
            # 指纹开门通知验证
            finger_open_and_notification(user1_phone=UserInfo.Second.phone,
                                         user1_email=UserInfo.Second.email,
                                         user1_password=UserInfo.Second.password,
                                         user1_name=UserInfo.Second.name,
                                         user1_id=UserInfo.Second.finger_thumb,
                                         user2_phone=UserInfo.First.phone,
                                         user2_email=UserInfo.First.email,
                                         user2_password=UserInfo.First.password,
                                         login_type='phone',
                                         notification_result=True,
                                         home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 验证家的管理员默认权限
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_edit_house_owner)  # 点击编辑房屋管理员
            assert_result_displayed(lock_setting_admin_permission_check.format(UserInfo.Four.name), False)
            click_element(lock_setting_owner_back_button)  # 返回
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C619(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
            add_lock()
            auto_lock('off')
            invite_user('Admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
            add_pin(UserInfo.Second.name, UserInfo.Second.pin,)
            add_card(UserInfo.Second.name,)
            add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb)
            click_element(user_button_setting)  # 点击【设定】
            click_element(user_button_setting_lock)  # 点击【锁的设置】
            click_element(lock_button_setting_smart_alert)  # 点击【智能提醒设置】
            click_element(lock_button_setting_add_smart_alert)  # 点击【添加智能提醒】
            click_element(lock_button_lock_setting_smart_event_choose)  # 点击【选择事件】
            click_element(lock_button_lock_setting_smart_event_user)  # 点击【门锁被某个用户解锁或上锁】
            click_element(homepage_button_select_room_or_user.format(UserInfo.Second.name))  # 选择管理员
            click_element(lock_button_lock_setting_smart_event_user_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_event_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_time_choose)  # 点击【选择时间】
            click_element(lock_button_lock_setting_smart_time_recur)  # 点击【在一个时间段内】
            get_weekday()
            click_element(time_button_alert_end_time)  # 点击【结束时间】
            click_element(time_button_invite_keyboard)  # 点击【键盘】
            target_time = get_time(need_minute=10)  # 获取时间
            click_element(time_button_invite_input_hour)  # 点击【小时】
            clear_text_content(time_button_invite_input_hour)  # 清除输入
            send_keys(time_button_invite_input_hour, target_time[0], hide_keyboard=False)  # 输入小时
            click_element(time_button_invite_input_minute)  # 点击【分钟】
            clear_text_content(time_button_invite_input_minute)  # 清除输入
            send_keys(time_button_invite_input_minute, target_time[1], hide_keyboard=False)  # 输入分钟
            click_element(time_button_invite_temp_save)  # 点击【确定】
            click_element(lock_button_lock_setting_smart_time_save)  # 点击【保存】
            click_element(lock_compound_lock_setting_smart_save)  # 点击【保存】
            start_time = time.time()  # 获取开始时间
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】
            # logout('house')
            # 蓝牙开门通知验证
            bt_open_and_notification(user1_phone=UserInfo.Second.phone,
                                     user1_email=UserInfo.Second.email,
                                     user1_password=UserInfo.Second.password,
                                     user2_phone=UserInfo.First.phone,
                                     user2_email=UserInfo.First.email,
                                     user2_password=UserInfo.First.password,
                                     login_type='phone',
                                     notification_result=True,
                                     home_name=HouseInfo.name)

            # 蓝牙关门通知验证
            bt_close_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            auto_lock('on')
            # logout('house')
            # 门卡开门通知验证
            card_open_and_notification(user1_phone=UserInfo.Second.phone,
                                       user1_email=UserInfo.Second.email,
                                       user1_password=UserInfo.Second.password,
                                       user2_phone=UserInfo.First.phone,
                                       user2_email=UserInfo.First.email,
                                       user2_password=UserInfo.First.password,
                                       login_type='phone',
                                       notification_result=True,
                                       home_name=HouseInfo.name)
            # logout('house')
            # 指纹开门通知验证
            finger_open_and_notification(user1_phone=UserInfo.Second.phone,
                                         user1_email=UserInfo.Second.email,
                                         user1_password=UserInfo.Second.password,
                                         user1_name=UserInfo.Second.name,
                                         user1_id=UserInfo.Second.finger_thumb,
                                         user2_phone=UserInfo.First.phone,
                                         user2_email=UserInfo.First.email,
                                         user2_password=UserInfo.First.password,
                                         login_type='phone',
                                         notification_result=True,
                                         home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            auto_lock('off')
            # logout('house')
            end_time = time.time()  # 获取结束时间
            # 提醒时间超范围外
            wait_time = int(target_time[2])*60 - int(end_time - start_time) + 60  # 总的等待时间，减去已经花费的时间，再多等一分钟
            if wait_time <= 0:
                pass
            else:
                time.sleep(int(target_time[2])*60 - int(end_time - start_time))
            # 蓝牙开门通知验证
            bt_open_and_notification(user1_phone=UserInfo.Second.phone,
                                     user1_email=UserInfo.Second.email,
                                     user1_password=UserInfo.Second.password,
                                     user2_phone=UserInfo.First.phone,
                                     user2_email=UserInfo.First.email,
                                     user2_password=UserInfo.First.password,
                                     login_type='phone',
                                     notification_result=False,
                                     home_name=HouseInfo.name)

            # 蓝牙关门通知验证
            bt_close_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=False,
                                      home_name=HouseInfo.name)
            auto_lock('on')
            # logout('house')
            # 门卡开门通知验证
            card_open_and_notification(user1_phone=UserInfo.Second.phone,
                                       user1_email=UserInfo.Second.email,
                                       user1_password=UserInfo.Second.password,
                                       user2_phone=UserInfo.First.phone,
                                       user2_email=UserInfo.First.email,
                                       user2_password=UserInfo.First.password,
                                       login_type='phone',
                                       notification_result=False,
                                       home_name=HouseInfo.name)
            # logout('house')
            # 指纹开门通知验证
            finger_open_and_notification(user1_phone=UserInfo.Second.phone,
                                         user1_email=UserInfo.Second.email,
                                         user1_password=UserInfo.Second.password,
                                         user1_name=UserInfo.Second.name,
                                         user1_id=UserInfo.Second.finger_thumb,
                                         user2_phone=UserInfo.First.phone,
                                         user2_email=UserInfo.First.email,
                                         user2_password=UserInfo.First.password,
                                         login_type='phone',
                                         notification_result=False,
                                         home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 验证家的普通用户默认权限
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            invite_user_yale('normal', UserInfo.NormalJoyce.phone2, UserInfo.NormalJoyce.name2)  # 邀请普通用户B
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_edit_house_owner)  # 点击编辑房屋管理员
            while 1:
                try:
                    driver.find_element_by_xpath(lock_setting_admin_permission_name.format(UserInfo.NormalJoyce.name2))
                except:
                    scroll_screen_up()  # 上滑
                else:
                    break
            assert driver.find_element_by_xpath(lock_setting_admin_permission_check.format(UserInfo.NormalJoyce.name2)).get_attribute('checked') == 'false'
            click_element(lock_setting_owner_back_button)  # 返回
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C620(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
            add_lock()
            auto_lock('off')
            invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
            add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
            add_card(UserInfo.Second.name, 'normal')
            add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb, 'normal')
            click_element(user_button_setting)  # 点击【设定】
            click_element(user_button_setting_lock)  # 点击【锁的设置】
            click_element(lock_button_setting_smart_alert)  # 点击【智能提醒设置】
            click_element(lock_button_setting_add_smart_alert)  # 点击【添加智能提醒】
            click_element(lock_button_lock_setting_smart_event_choose)  # 点击【选择事件】
            click_element(lock_button_lock_setting_smart_event_user)  # 点击【门锁被某个用户解锁或上锁】
            click_element(homepage_button_select_room_or_user.format(UserInfo.Second.name))  # 选择管理员
            click_element(lock_button_lock_setting_smart_event_user_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_event_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_time_choose)  # 点击【选择时间】
            click_element(lock_button_lock_setting_smart_time_always)  # 点击【任何时间】
            click_element(lock_button_lock_setting_smart_time_save)  # 点击【保存】
            click_element(lock_compound_lock_setting_smart_save)  # 点击【保存】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】
            # logout('house')
            # 蓝牙开门通知验证
            bt_open_and_notification(user1_phone=UserInfo.Second.phone,
                                     user1_email=UserInfo.Second.email,
                                     user1_password=UserInfo.Second.password,
                                     user2_phone=UserInfo.First.phone,
                                     user2_email=UserInfo.First.email,
                                     user2_password=UserInfo.First.password,
                                     login_type='phone',
                                     notification_result=True,
                                     home_name=HouseInfo.name)

            # 蓝牙关门通知验证
            bt_close_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            auto_lock('on')
            # logout('house')
            # 门卡开门通知验证
            card_open_and_notification(user1_phone=UserInfo.Second.phone,
                                       user1_email=UserInfo.Second.email,
                                       user1_password=UserInfo.Second.password,
                                       user2_phone=UserInfo.First.phone,
                                       user2_email=UserInfo.First.email,
                                       user2_password=UserInfo.First.password,
                                       login_type='phone',
                                       notification_result=True,
                                       home_name=HouseInfo.name)
            # logout('house')
            # 指纹开门通知验证
            finger_open_and_notification(user1_phone=UserInfo.Second.phone,
                                         user1_email=UserInfo.Second.email,
                                         user1_password=UserInfo.Second.password,
                                         user1_name=UserInfo.Second.name,
                                         user1_id=UserInfo.Second.finger_thumb,
                                         user2_phone=UserInfo.First.phone,
                                         user2_email=UserInfo.First.email,
                                         user2_password=UserInfo.First.password,
                                         login_type='phone',
                                         notification_result=True,
                                         home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            factory_lock()
            logout()

        elif app_select == 'yale':
            # 验证锁普通用户家的管理员权限启用
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(UserManagement_user_list)  # 点击【用户列表】
            try:
                if check_element(UserManagement_user_normal.format(UserInfo.NormalJoyce.name2), wait_time=5, check_type=False) is False:
                    invite_user_yale('normal', UserInfo.NormalJoyce.phone2, UserInfo.NormalJoyce.name2)  # 邀请普通用户B
            except NameError:
                pass
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_edit_house_owner)  # 点击编辑房屋管理员
            while 1:
                try:
                    driver.find_element_by_xpath(lock_setting_admin_permission_check.format(UserInfo.NormalJoyce.name2))
                except:
                    scroll_screen_up()  # 上滑
                else:
                    break
            click_element(lock_setting_admin_permission_check.format(UserInfo.NormalJoyce.name2))  # 勾选普通用户
            assert driver.find_element_by_xpath(lock_setting_admin_permission_check.format(UserInfo.NormalJoyce.name2)).get_attribute('checked') == 'true'  # 勾选成功
            click_element(lock_setting_owner_back_button)  # 返回
            logout('house')
            login(UserInfo.NormalJoyce.phone2, UserInfo.NormalJoyce.email2, UserInfo.NormalJoyce.password2, 'phone')  # 普通用户2登录
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            assert_result_displayed(lock_setting_setting_button, True)  # 显示设置按钮
            click_element(lock_setting_setting_button)  # 点击设置按钮
            assert_result_displayed(lock_setting_house_setting, True)  # 显示【房屋设置】
            assert_result_displayed(lock_setting_lock_setting, False)  # 不显示【锁设置】
            logout('house')
        else:
            assert False

    @adb_log
    def test_C621(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
            add_lock()
            auto_lock('off')
            invite_user('normal', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
            add_pin(UserInfo.Second.name, UserInfo.Second.pin, 'normal')
            add_card(UserInfo.Second.name, 'normal')
            add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb, 'normal')
            click_element(user_button_setting)  # 点击【设定】
            click_element(user_button_setting_lock)  # 点击【锁的设置】
            click_element(lock_button_setting_smart_alert)  # 点击【智能提醒设置】
            click_element(lock_button_setting_add_smart_alert)  # 点击【添加智能提醒】
            click_element(lock_button_lock_setting_smart_event_choose)  # 点击【选择事件】
            click_element(lock_button_lock_setting_smart_event_user)  # 点击【门锁被某个用户解锁或上锁】
            click_element(homepage_button_select_room_or_user.format(UserInfo.Second.name))  # 选择管理员
            click_element(lock_button_lock_setting_smart_event_user_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_event_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_time_choose)  # 点击【选择时间】
            click_element(lock_button_lock_setting_smart_time_recur)  # 点击【在一个时间段内】
            get_weekday()
            click_element(time_button_alert_end_time)  # 点击【结束时间】
            click_element(time_button_invite_keyboard)  # 点击【键盘】
            target_time = get_time(need_minute=10)  # 获取时间
            click_element(time_button_invite_input_hour)  # 点击【小时】
            clear_text_content(time_button_invite_input_hour)  # 清除输入
            send_keys(time_button_invite_input_hour, target_time[0], hide_keyboard=False)  # 输入小时
            click_element(time_button_invite_input_minute)  # 点击【分钟】
            clear_text_content(time_button_invite_input_minute)  # 清除输入
            send_keys(time_button_invite_input_minute, target_time[1], hide_keyboard=False)  # 输入分钟
            click_element(time_button_invite_temp_save)  # 点击【确定】
            click_element(lock_button_lock_setting_smart_time_save)  # 点击【保存】
            click_element(lock_compound_lock_setting_smart_save)  # 点击【保存】
            start_time = time.time()  # 获取开始时间
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】
            # logout('house')
            # 蓝牙开门通知验证
            bt_open_and_notification(user1_phone=UserInfo.Second.phone,
                                     user1_email=UserInfo.Second.email,
                                     user1_password=UserInfo.Second.password,
                                     user2_phone=UserInfo.First.phone,
                                     user2_email=UserInfo.First.email,
                                     user2_password=UserInfo.First.password,
                                     login_type='phone',
                                     notification_result=True,
                                     home_name=HouseInfo.name)

            # 蓝牙关门通知验证
            bt_close_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            auto_lock('on')
            # logout('house')
            # 门卡开门通知验证
            card_open_and_notification(user1_phone=UserInfo.Second.phone,
                                       user1_email=UserInfo.Second.email,
                                       user1_password=UserInfo.Second.password,
                                       user2_phone=UserInfo.First.phone,
                                       user2_email=UserInfo.First.email,
                                       user2_password=UserInfo.First.password,
                                       login_type='phone',
                                       notification_result=True,
                                       home_name=HouseInfo.name)
            # logout('house')
            # 指纹开门通知验证
            finger_open_and_notification(user1_phone=UserInfo.Second.phone,
                                         user1_email=UserInfo.Second.email,
                                         user1_password=UserInfo.Second.password,
                                         user1_name=UserInfo.Second.name,
                                         user1_id=UserInfo.Second.finger_thumb,
                                         user2_phone=UserInfo.First.phone,
                                         user2_email=UserInfo.First.email,
                                         user2_password=UserInfo.First.password,
                                         login_type='phone',
                                         notification_result=True,
                                         home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            auto_lock('off')
            # logout('house')
            end_time = time.time()  # 获取结束时间
            # 提醒时间超范围外
            wait_time = int(target_time[2])*60 - int(end_time - start_time) + 60  # 总的等待时间，减去已经花费的时间，再多等一分钟
            if wait_time <= 0:
                pass
            else:
                time.sleep(int(target_time[2])*60 - int(end_time - start_time))
            # 蓝牙开门通知验证
            bt_open_and_notification(user1_phone=UserInfo.Second.phone,
                                     user1_email=UserInfo.Second.email,
                                     user1_password=UserInfo.Second.password,
                                     user2_phone=UserInfo.First.phone,
                                     user2_email=UserInfo.First.email,
                                     user2_password=UserInfo.First.password,
                                     login_type='phone',
                                     notification_result=False,
                                     home_name=HouseInfo.name)

            # 蓝牙关门通知验证
            bt_close_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=False,
                                      home_name=HouseInfo.name)
            auto_lock('on')
            # logout('house')
            # 门卡开门通知验证
            card_open_and_notification(user1_phone=UserInfo.Second.phone,
                                       user1_email=UserInfo.Second.email,
                                       user1_password=UserInfo.Second.password,
                                       user2_phone=UserInfo.First.phone,
                                       user2_email=UserInfo.First.email,
                                       user2_password=UserInfo.First.password,
                                       login_type='phone',
                                       notification_result=False,
                                       home_name=HouseInfo.name)
            # logout('house')
            # 指纹开门通知验证
            finger_open_and_notification(user1_phone=UserInfo.Second.phone,
                                         user1_email=UserInfo.Second.email,
                                         user1_password=UserInfo.Second.password,
                                         user1_name=UserInfo.Second.name,
                                         user1_id=UserInfo.Second.finger_thumb,
                                         user2_phone=UserInfo.First.phone,
                                         user2_email=UserInfo.First.email,
                                         user2_password=UserInfo.First.password,
                                         login_type='phone',
                                         notification_result=False,
                                         home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=False,
                                      home_name=HouseInfo.name)
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 验证锁普通用户家的管理员权限变更
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_edit_house_owner)  # 点击编辑房屋管理员
            while 1:
                try:
                    driver.find_element_by_xpath(lock_setting_admin_permission_name.format(UserInfo.NormalJoyce.name2))
                except:
                    scroll_screen_up()  # 上滑
                else:
                    break
            if driver.find_element_by_xpath(lock_setting_admin_permission_check.format(UserInfo.NormalJoyce.name2)).get_attribute('checked') == 'true':
                click_element(lock_setting_admin_permission_check.format(UserInfo.NormalJoyce.name2))  # 取消勾选普通用户
                click_element(lock_setting_owner_back_button)  # 返回
                # logout('house')
            else:
                click_element(lock_setting_owner_back_button)  # 返回
            logout('house')
            login(UserInfo.NormalJoyce.phone2, UserInfo.NormalJoyce.email2, UserInfo.NormalJoyce.password2, 'phone')  # 普通用户登录
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            assert_result_displayed(lock_setting_setting_button, False)  # 不显示设置按钮
            logout('house')
        else:
            assert False

    @adb_log
    def test_C622(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证锁管理员家的管理员权限变更
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_edit_house_owner)  # 点击编辑房屋管理员
            while 1:
                try:
                    driver.find_element_by_xpath(lock_setting_admin_permission_name.format(UserInfo.AdminJoyce.name2))
                except:
                    scroll_screen_up()  # 上滑
                else:
                    break
            if driver.find_element_by_xpath(lock_setting_admin_permission_check.format(UserInfo.AdminJoyce.name2)).get_attribute('checked') == 'true':
                click_element(lock_setting_admin_permission_check.format(UserInfo.AdminJoyce.name2))  # 取消勾选管理员
                click_element(lock_setting_owner_back_button)  # 返回
                # logout('house')
            else:
                click_element(lock_setting_owner_back_button)  # 返回
            logout('house')
            login(UserInfo.AdminJoyce.phone2, UserInfo.AdminJoyce.email2, UserInfo.AdminJoyce.password2, 'phone')  # 管理员B登录
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(lock_setting_setting_button)  # 点击设置按钮
            assert_result_displayed(lock_setting_house_setting, False)  # 不显示【房屋设置】
            assert_result_displayed(lock_setting_lock_setting, True)  # 显示【锁设置】
            logout('house')
        else:
            assert False

    @adb_log
    def test_C623(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证锁管理员移除后家的管理员权限保持
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')  # 管理员A登录
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(UserManagement_user_list)  # 点击【用户列表】
            click_element(UserManagement_user_admin.format(UserInfo.AdminJoyce.name2))  # 点击管理员
            click_element(UserManagement_delete_user)   # 点击删除用户
            click_element(lock_setting_save_button)    # 确认删除
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_edit_house_owner)  # 点击编辑房屋管理员
            # while 1:
            #     try:
            #         driver.find_element_by_xpath(lock_setting_admin_permission_name.format(UserInfo.AdminJoyce.name2))
            #     except:
            #         scroll_screen_up()  # 上滑
            #     else:
            #         break
            # assert driver.find_element_by_xpath(lock_setting_admin_permission_check.format(UserInfo.AdminJoyce.name2)).get_attribute('checked') == 'false'
            assert_result_displayed(lock_setting_admin_permission_name.format(UserInfo.AdminJoyce.name2), False)
            click_element(lock_setting_owner_back_button)  # 返回
            logout('house')
            login(UserInfo.AdminJoyce.phone2, UserInfo.AdminJoyce.email2, UserInfo.AdminJoyce.password2, 'phone')  # 管理员B登录
            assert_result_displayed(homePage_homePage_home_list, False)  # 不显示住宅
            logout()
            # # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')  # 管理员A登录
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            # invite_user_yale('admin', UserInfo.AdminJoyce.phone2, UserInfo.AdminJoyce.name2)  # 重置邀请
            # # logout('house')
        else:
            assert False

    @adb_log
    def test_C624(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证普通用户移除后家的管理员权限保持
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')  # 管理员A登录
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(UserManagement_user_list)  # 点击【用户列表】
            click_element(UserManagement_user_admin.format(UserInfo.NormalJoyce.name2))  # 点击普通用户B
            click_element(UserManagement_delete_user)   # 点击删除用户
            click_element(lock_setting_save_button)    # 确认删除
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_edit_house_owner)  # 点击编辑房屋管理员
            # while 1:
            #     try:
            #         driver.find_element_by_xpath(lock_setting_admin_permission_name.format(UserInfo.NormalJoyce.name2))
            #     except:
            #         scroll_screen_up()  # 上滑
            #     else:
            #         break
            # assert driver.find_element_by_xpath(lock_setting_admin_permission_check.format(UserInfo.NormalJoyce.name2)).get_attribute('checked') == 'false'
            assert_result_displayed(lock_setting_admin_permission_name.format(UserInfo.NormalJoyce.name2), False)
            click_element(lock_setting_owner_back_button)  # 返回
            logout('house')
            login(UserInfo.NormalJoyce.phone2, UserInfo.NormalJoyce.email2, UserInfo.NormalJoyce.password2, 'phone')  # 普通用户B登录
            assert_result_displayed(homePage_homePage_home_list, False)  # 不显示住宅
            logout()
            # # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')  # 管理员A登录
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            # invite_user_yale('normal', UserInfo.NormalJoyce.phone2, UserInfo.NormalJoyce.name2)  # 重置邀请
            # # logout('house')
        else:
            assert False

    @adb_log
    def test_C625(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证家的管理员能否删除自己
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_edit_house_owner)  # 点击编辑房屋管理员
            while 1:
                try:
                    driver.find_element_by_xpath(lock_setting_admin_permission_name.format(UserInfo.Four.name))
                except:
                    scroll_screen_up()  # 上滑
                else:
                    break
            assert_result_displayed(lock_setting_admin_permission_check.format(UserInfo.Four.name), False)  # 登录账号的管理员没有权限勾选框
            click_element(lock_setting_owner_back_button)  # 返回
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C626(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证家的管理员设置页网络断开情况
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(lock_setting_setting_button)  # 点击设置按钮
            wifi_switch('close')  # 断网
            click_element(lock_setting_edit_house_owner)  # 点击编辑房屋管理员
            assert_result_displayed(lock_setting_admin_permission_name.format(UserInfo.Four.name), False)  # 不显示房屋管理员
            click_element(lock_setting_owner_back_button)  # 返回
            wifi_switch('open')  # 联网
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C627(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            add_lock()
            auto_lock('off')
            invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
            add_pin(UserInfo.Second.name, UserInfo.Second.pin)
            add_card(UserInfo.Second.name)
            add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb)
            click_element(user_button_setting)  # 点击【设定】
            click_element(user_button_setting_lock)  # 点击【锁的设置】
            click_element(lock_button_setting_smart_alert)  # 点击【智能提醒设置】
            click_element(lock_button_setting_add_smart_alert)  # 点击【添加智能提醒】
            click_element(lock_button_lock_setting_smart_event_choose)  # 点击【选择事件】
            click_element(lock_button_lock_setting_smart_event_user)  # 点击【门锁被某个用户解锁或上锁】
            click_element(homepage_button_select_room_or_user.format(UserInfo.Second.name))  # 选择管理员
            click_element(lock_button_lock_setting_smart_event_user_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_event_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_time_choose)  # 点击【选择时间】
            click_element(lock_button_lock_setting_smart_time_recur)  # 点击【在一个时间段内】
            get_weekday()
            click_element(time_button_alert_end_time)  # 点击【结束时间】
            click_element(time_button_invite_keyboard)  # 点击【键盘】
            target_time = get_time(need_minute=10)  # 获取时间
            click_element(time_button_invite_input_hour)  # 点击【小时】
            clear_text_content(time_button_invite_input_hour)  # 清除输入
            send_keys(time_button_invite_input_hour, target_time[0], hide_keyboard=False)  # 输入小时
            click_element(time_button_invite_input_minute)  # 点击【分钟】
            clear_text_content(time_button_invite_input_minute)  # 清除输入
            send_keys(time_button_invite_input_minute, target_time[1], hide_keyboard=False)  # 输入分钟
            click_element(time_button_invite_temp_save)  # 点击【确定】
            click_element(lock_button_lock_setting_smart_time_save)  # 点击【保存】
            click_element(lock_compound_lock_setting_smart_save)  # 点击【保存】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】
            # logout('house')
            # 蓝牙开门通知验证
            bt_open_and_notification(user1_phone=UserInfo.Second.phone,
                                     user1_email=UserInfo.Second.email,
                                     user1_password=UserInfo.Second.password,
                                     user2_phone=UserInfo.First.phone,
                                     user2_email=UserInfo.First.email,
                                     user2_password=UserInfo.First.password,
                                     login_type='phone',
                                     notification_result=True,
                                     home_name=HouseInfo.name)

            # 蓝牙关门通知验证
            bt_close_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            auto_lock('on')
            # logout('house')
            # 门卡开门通知验证
            card_open_and_notification(user1_phone=UserInfo.Second.phone,
                                       user1_email=UserInfo.Second.email,
                                       user1_password=UserInfo.Second.password,
                                       user2_phone=UserInfo.First.phone,
                                       user2_email=UserInfo.First.email,
                                       user2_password=UserInfo.First.password,
                                       login_type='phone',
                                       notification_result=True,
                                       home_name=HouseInfo.name)
            # logout('house')
            # 指纹开门通知验证
            finger_open_and_notification(user1_phone=UserInfo.Second.phone,
                                         user1_email=UserInfo.Second.email,
                                         user1_password=UserInfo.Second.password,
                                         user1_name=UserInfo.Second.name,
                                         user1_id=UserInfo.Second.finger_thumb,
                                         user2_phone=UserInfo.First.phone,
                                         user2_email=UserInfo.First.email,
                                         user2_password=UserInfo.First.password,
                                         login_type='phone',
                                         notification_result=True,
                                         home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            auto_lock('off')
            # 修改时间
            click_element(user_button_setting)  # 点击【设定】
            click_element(user_button_setting_lock)  # 点击【锁的设置】
            click_element(lock_button_setting_smart_alert)  # 点击【智能提醒设置】
            click_element(lock_button_lock_setting_smart_time_edit)  # 点击【编辑时间】
            click_element(lock_button_lock_setting_smart_time_choose)  # 点击选择时间
            click_element(time_button_invite_recur_end_time)  # 点击【在一个时间段内】
            get_weekday(select_type=False)  # 取消【今天】
            click_element(lock_button_lock_setting_smart_time_save)  # 点击【保存】
            click_element(lock_compound_lock_setting_smart_save)  # 点击【保存】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】
            # logout('house')
            # 蓝牙开门通知验证
            bt_open_and_notification(user1_phone=UserInfo.Second.phone,
                                     user1_email=UserInfo.Second.email,
                                     user1_password=UserInfo.Second.password,
                                     user2_phone=UserInfo.First.phone,
                                     user2_email=UserInfo.First.email,
                                     user2_password=UserInfo.First.password,
                                     login_type='phone',
                                     notification_result=False,
                                     home_name=HouseInfo.name)

            # 蓝牙关门通知验证
            bt_close_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=False,
                                      home_name=HouseInfo.name)
            auto_lock('on')
            # logout('house')
            # 门卡开门通知验证
            card_open_and_notification(user1_phone=UserInfo.Second.phone,
                                       user1_email=UserInfo.Second.email,
                                       user1_password=UserInfo.Second.password,
                                       user2_phone=UserInfo.First.phone,
                                       user2_email=UserInfo.First.email,
                                       user2_password=UserInfo.First.password,
                                       login_type='phone',
                                       notification_result=False,
                                       home_name=HouseInfo.name)
            # logout('house')
            # 指纹开门通知验证
            finger_open_and_notification(user1_phone=UserInfo.Second.phone,
                                         user1_email=UserInfo.Second.email,
                                         user1_password=UserInfo.Second.password,
                                         user1_name=UserInfo.Second.name,
                                         user1_id=UserInfo.Second.finger_thumb,
                                         user2_phone=UserInfo.First.phone,
                                         user2_email=UserInfo.First.email,
                                         user2_password=UserInfo.First.password,
                                         login_type='phone',
                                         notification_result=False,
                                         home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=False,
                                      home_name=HouseInfo.name)
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 检查锁的设置详情页显示
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(lock_setting_setting_button)  # 点击设置按钮
            assert_result_displayed(lock_lockSetting_lock_icon, True)  # 显示锁icon
            click_element(lock_setting_lock_setting)  # 点击锁设置
            assert_result_displayed(lock_lockSetting_reset, True)  # 显示出厂设置
            driver.press_keycode(4)
            click_element(lock_main_lock_manager)
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C628(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            add_lock()
            auto_lock('off')
            invite_user('admin', UserInfo.Second.phone, UserInfo.Second.name, True, 'always')
            add_pin(UserInfo.Second.name, UserInfo.Second.pin)
            add_card(UserInfo.Second.name)
            add_finger(UserInfo.Second.name, UserInfo.Second.name, UserInfo.Second.finger_thumb)
            click_element(user_button_setting)  # 点击【设定】
            click_element(user_button_setting_lock)  # 点击【锁的设置】
            click_element(lock_button_setting_smart_alert)  # 点击【智能提醒设置】
            click_element(lock_button_setting_add_smart_alert)  # 点击【添加智能提醒】
            click_element(lock_button_lock_setting_smart_event_choose)  # 点击【选择事件】
            click_element(lock_button_lock_setting_smart_event_user)  # 点击【门锁被某个用户解锁或上锁】
            click_element(homepage_button_select_room_or_user.format(UserInfo.Second.name))  # 选择管理员
            click_element(lock_button_lock_setting_smart_event_user_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_event_save)  # 点击【完成】
            click_element(lock_button_lock_setting_smart_time_choose)  # 点击【选择时间】
            click_element(lock_button_lock_setting_smart_time_recur)  # 点击【在一个时间段内】
            get_weekday()
            click_element(time_button_alert_end_time)  # 点击【结束时间】
            click_element(time_button_invite_keyboard)  # 点击【键盘】
            target_time = get_time(need_minute=10)  # 获取时间
            click_element(time_button_invite_input_hour)  # 点击【小时】
            clear_text_content(time_button_invite_input_hour)  # 清除输入
            send_keys(time_button_invite_input_hour, target_time[0], hide_keyboard=False)  # 输入小时
            click_element(time_button_invite_input_minute)  # 点击【分钟】
            clear_text_content(time_button_invite_input_minute)  # 清除输入
            send_keys(time_button_invite_input_minute, target_time[1], hide_keyboard=False)  # 输入分钟
            click_element(time_button_invite_temp_save)  # 点击【确定】
            click_element(lock_button_lock_setting_smart_time_save)  # 点击【保存】
            click_element(lock_compound_lock_setting_smart_save)  # 点击【保存】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】
            # logout('house')
            # 蓝牙开门通知验证
            bt_open_and_notification(user1_phone=UserInfo.Second.phone,
                                     user1_email=UserInfo.Second.email,
                                     user1_password=UserInfo.Second.password,
                                     user2_phone=UserInfo.First.phone,
                                     user2_email=UserInfo.First.email,
                                     user2_password=UserInfo.First.password,
                                     login_type='phone',
                                     notification_result=True,
                                     home_name=HouseInfo.name)

            # 蓝牙关门通知验证
            bt_close_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            auto_lock('on')
            # logout('house')
            # 门卡开门通知验证
            card_open_and_notification(user1_phone=UserInfo.Second.phone,
                                       user1_email=UserInfo.Second.email,
                                       user1_password=UserInfo.Second.password,
                                       user2_phone=UserInfo.First.phone,
                                       user2_email=UserInfo.First.email,
                                       user2_password=UserInfo.First.password,
                                       login_type='phone',
                                       notification_result=True,
                                       home_name=HouseInfo.name)
            # logout('house')
            # 指纹开门通知验证
            finger_open_and_notification(user1_phone=UserInfo.Second.phone,
                                         user1_email=UserInfo.Second.email,
                                         user1_password=UserInfo.Second.password,
                                         user1_name=UserInfo.Second.name,
                                         user1_id=UserInfo.Second.finger_thumb,
                                         user2_phone=UserInfo.First.phone,
                                         user2_email=UserInfo.First.email,
                                         user2_password=UserInfo.First.password,
                                         login_type='phone',
                                         notification_result=True,
                                         home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=True,
                                      home_name=HouseInfo.name)
            auto_lock('off')
            # 删除智能提醒
            click_element(user_button_setting)  # 点击【设定】
            click_element(user_button_setting_lock)  # 点击【锁的设置】
            click_element(lock_button_setting_smart_alert)  # 点击【智能提醒设置】
            click_element(lock_button_lock_setting_smart_time_edit)  # 点击【编辑时间】
            click_element(lock_button_lock_setting_smart_cancel)  # 点击【删除】
            click_element(user_button_factory_reset_and_delete_yes)  # 点击【删除】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】
            # logout('house')
            # 蓝牙开门通知验证
            bt_open_and_notification(user1_phone=UserInfo.Second.phone,
                                     user1_email=UserInfo.Second.email,
                                     user1_password=UserInfo.Second.password,
                                     user2_phone=UserInfo.First.phone,
                                     user2_email=UserInfo.First.email,
                                     user2_password=UserInfo.First.password,
                                     login_type='phone',
                                     notification_result=False,
                                     home_name=HouseInfo.name)

            # 蓝牙关门通知验证
            bt_close_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=False,
                                      home_name=HouseInfo.name)
            auto_lock('on')
            # logout('house')
            # 门卡开门通知验证
            card_open_and_notification(user1_phone=UserInfo.Second.phone,
                                       user1_email=UserInfo.Second.email,
                                       user1_password=UserInfo.Second.password,
                                       user2_phone=UserInfo.First.phone,
                                       user2_email=UserInfo.First.email,
                                       user2_password=UserInfo.First.password,
                                       login_type='phone',
                                       notification_result=False,
                                       home_name=HouseInfo.name)
            # logout('house')
            # 指纹开门通知验证
            finger_open_and_notification(user1_phone=UserInfo.Second.phone,
                                         user1_email=UserInfo.Second.email,
                                         user1_password=UserInfo.Second.password,
                                         user1_name=UserInfo.Second.name,
                                         user1_id=UserInfo.Second.finger_thumb,
                                         user2_phone=UserInfo.First.phone,
                                         user2_email=UserInfo.First.email,
                                         user2_password=UserInfo.First.password,
                                         login_type='phone',
                                         notification_result=False,
                                         home_name=HouseInfo.name)
            # logout('house')
            # 密码开门通知验证
            pin_open_and_notification(user1_phone=UserInfo.Second.phone,
                                      user1_email=UserInfo.Second.email,
                                      user1_password=UserInfo.Second.password,
                                      user2_phone=UserInfo.First.phone,
                                      user2_email=UserInfo.First.email,
                                      user2_password=UserInfo.First.password,
                                      login_type='phone',
                                      notification_result=False,
                                      home_name=HouseInfo.name)
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 检查锁的名字显示
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_lock_setting)  # 点击锁设置
            assert_result_displayed(lock_lockSetting_lock_name, True)  # 显示锁的名字
            driver.press_keycode(4)
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C629(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查锁的名字首次设置
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_lock_setting)  # 点击锁设置
            click_element(lock_lockSetting_lock_name)  # 点击锁的名字
            send_keys(lock_lockSetting_lock_name_edit, '后门')  # 编辑锁的名字
            click_element(lock_setting_save_button)  # 保存
            assert_result_text(lock_lockSetting_lock_name_show, '后门')  # 显示锁的名字
            driver.press_keycode(4)
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C630(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查锁的名字修改
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_lock_setting)  # 点击锁设置
            click_element(lock_lockSetting_lock_name)  # 点击锁的名字
            send_keys(lock_lockSetting_lock_name_edit, '后门update')  # 编辑锁的名字
            click_element(lock_setting_save_button)  # 保存
            assert_result_text(lock_lockSetting_lock_name_show, '后门UPDATE')  # 显示锁的名字
            driver.press_keycode(4)
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C631(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查锁的名字设置为空防呆
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_lock_setting)  # 点击锁设置
            click_element(lock_lockSetting_lock_name)  # 点击锁的名字
            send_keys(lock_lockSetting_lock_name_edit, '')  # 编辑锁的名字为空
            click_element(lock_setting_save_button)  # 保存
            assert_result_text(lock_lockSetting_alert_error, '锁的名字不能为空')  # 提示锁的名字不能为空
            driver.press_keycode(4)
            click_element(lock_setting_lock_setting)  # 点击锁设置
            if driver.find_element_by_xpath(lock_lockSetting_lock_name_show).text == '':
                assert False
        else:
            assert False

    @adb_log
    def test_C632(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查锁的声音默认设置
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_lock_setting)  # 点击锁设置
            click_element(lock_lockSetting_device_volume)  # 点击设备设置
            assert_result_displayed(lock_lockSetting_volume_setting, True)  # 显示音量设置
            time.sleep(20)
            assert_result_text(lock_lockSetting_default_volume, '高')   # 默认音量高
            # logout()
        else:
            assert False

    @adb_log
    def test_C633(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查锁的声音_高的设置
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_lock_setting)  # 点击锁设置
            click_element(lock_lockSetting_device_volume)  # 点击设备设置
            click_element(lock_lockSetting_volume_setting)   # 点击音量设置
            click_element(lock_lockSetting_volume_high)  # 设置高音量
            # assert_result_text(lock_lockSetting_alert_update_volume, '更新音量…')   # 提示更新音量
            assert_result_text(lock_lockSetting_default_volume, '高')  # 显示高
        else:
            assert False

    @adb_log
    def test_C634(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            add_lock()
            click_element(user_button_setting)  # 点击【设定】
            click_element(user_button_setting_lock)  # 点击【锁的设置】
            click_element(lock_button_setting_product_info)  # 点击【产品信息】
            assert_result_text(lock_assert_lock_setting_product_serial, selected_lock_id)
            assert_result_text(lock_assert_lock_setting_product_model, 'selected_lock_model')
            check_lock_info(lock_assert_lock_setting_product_module)  # 模块版本号不为空
            check_lock_info(lock_assert_lock_setting_product_hardware)  # 硬件版本号不为空
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 检查锁的声音_低的设置
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_lock_setting)  # 点击锁设置
            click_element(lock_lockSetting_device_volume)  # 点击设备设置
            click_element(lock_lockSetting_volume_setting)   # 点击音量设置
            click_element(lock_lockSetting_volume_low)  # 设置低音量
            assert_result_text(lock_lockSetting_default_volume, '低')  # 显示低
            # logout()
        else:
            assert False

    @adb_log
    def test_C635(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查锁的声音关闭时的设置
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_lock_setting)  # 点击锁设置
            click_element(lock_lockSetting_device_volume)  # 点击设备设置
            click_element(lock_lockSetting_volume_setting)  # 点击音量设置
            click_element(lock_lockSetting_volume_silent)  # 设置静音
            assert_result_text(lock_lockSetting_default_volume, '静音')  # 显示静音
            # logout()
        else:
            assert False

    @adb_log
    def test_C636(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁默认设置
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            # assert_result_displayed(lock_main_lock_close_status, True)  # 显示【锁关闭】
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_lock_setting)  # 点击锁设置
            time.sleep(30)   # 盼盼特有
            click_element(lock_lockSetting_auto_lock)  # 点击自动重新上锁
            time.sleep(5)
            assert driver.find_element_by_id(lock_lockSetting_auto_lock_switch).get_attribute('checked') == 'true'
            click_element(lock_lockSetting_header_back_button)
            driver.press_keycode(4)
            # logout('house')
        else:
            assert False

    @adb_log
    def test_lock_setting_tc064(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁按钮记忆功能
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【锁关闭】
            auto_lock_switch('关闭')
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_lock_setting)  # 点击锁设置
            time.sleep(15)
            assert_result_text(lock_lockSetting_auto_lock_status, '关闭')  # 自动上锁关闭
            driver.press_keycode(4)
            click_element(lock_main_lock_manager)  # 点击锁按钮
            check_lock_connection()
            auto_lock_switch('开启')
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_lock_setting)  # 点击锁设置
            time.sleep(15)
            assert_result_text(lock_lockSetting_auto_lock_status, '开启')  # 自动上锁打开
            driver.press_keycode(4)
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C638(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁开启时管理员的蓝牙解锁
            pass
        else:
            assert False

    @adb_log
    def test_C639(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁开启时管理员的指纹解锁
            pass
        else:
            assert False

    @adb_log
    def test_C640(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁开启时管理员的密码解锁
            pass
        else:
            assert False

    @adb_log
    def test_C641(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁开启时管理员的门卡解锁
            pass
        else:
            assert False

    @adb_log
    def test_C642(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁开启时普通用户的蓝牙解锁
            pass
        else:
            assert False

    @adb_log
    def test_C643(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁开启时普通用户的指纹解锁
            pass
        else:
            assert False

    @adb_log
    def test_C644(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁开启时普通用户的密码解锁
            pass
        else:
            assert False

    @adb_log
    def test_C645(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁开启时普通用户的门卡解锁
            pass
        else:
            assert False

    @adb_log
    def test_C646(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁开启时访客的密码解锁
            # login(UserInfo.AdminJoyce.phone, UserInfo.AdminJoyce.email, UserInfo.AdminJoyce.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(userManagement_main_user_tab)  # 点击【用户】
            click_element(userManagement_invite_invite)  # 点击邀请用户
            click_element(userManagement_invite_invite_guest)  # 点击【访客邀请】
            send_keys(userManagement_invite_guest_first_name, UserInfo.GuestJoyce.first_name)  # 输入名
            send_keys(userManagement_invite_guest_last_name, UserInfo.GuestJoyce.last_name)  # 输入姓
            click_element(userManagement_invite_continue_button)  # 继续
            click_element(userManagement_invite_create_pin)
            clear_text_content(userManagement_invite_pin_input)
            send_keys(userManagement_invite_pin_input, UserInfo.GuestJoyce.phone[-6:])  # 输入密码
            click_element(userManagement_invite_change_pin, after_action_sleep=5)
            click_element(userManagement_invite_save_pin, after_action_sleep=15)
            click_element(userManagement_main_home_tab)  # lock manage
            check_lock_connection()
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            try:
                requests.put(url=API().password_open(UserInfo.GuestJoyce.phone))  # 密码开门
            except:
                pass
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            if autoLock is True:
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            else:
                click_element(lock_main_lottie)  # 连接成功，点击开锁
                assert_result_displayed(lock_main_lock_close_status, True)  # 显示锁关闭
            delete_guest(UserInfo.GuestJoyce.name)   # 删除访客
        else:
            assert False

    @adb_log
    def test_C647(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁关闭时管理员的蓝牙解锁
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            if autoLock is True:
                auto_lock_switch('关闭')  # 关闭自动上锁
            else:
                pass
            click_element(lock_main_user_lock_button)   # 点击锁
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【锁开启】
            time.sleep(5)
            check_lock_connection()
            time.sleep(5)
            click_element(lock_main_user_lock_button)  # 点击锁
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【锁关闭】
        else:
            assert False

    @adb_log
    def test_C648(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁关闭时管理员的指纹解锁
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            add_finger_yale(UserInfo.Four.name, UserInfo.Four.name, UserInfo.Four.name)
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            requests.put(url=API().finger_open(UserInfo.Four.name, UserInfo.Four.name))  # 指纹开锁
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            requests.put(url=API().finger_open(UserInfo.Four.name, UserInfo.Four.name))  # 指纹关锁
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C649(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁关闭时管理员的密码解锁
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            password_register(UserInfo.Four.name, UserInfo.Four.phone)  # 设置密码
            click_element(lock_main_lock_manager)  # 点击锁
            # check_lock_connection()
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            requests.put(url=API().password_open(UserInfo.Four.phone))  # 密码开锁
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            time.sleep(5)
            requests.put(url=API().password_open(UserInfo.Four.phone))  # 密码关锁
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C650(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁关闭时管理员的门卡解锁
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            add_card_yale(UserInfo.Four.name)  # 添加门卡
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            requests.put(url=API().card_open())  # 门卡开锁
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            requests.put(url=API().card_open())  # 门卡关锁
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            delete_card_yale(UserInfo.Four.name)  # 删除门卡
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C651(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁关闭时普通用户的蓝牙解锁
            logout('house')
            login(UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.email, UserInfo.NormalJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            click_element(lock_main_user_lock_button)   # 点击锁
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【锁开启】
            # assert_result_displayed(lock_main_lock_close_status, False)  # 不显示【锁关闭】
            time.sleep(5)
            click_element(lock_main_user_lock_button)  # 点击锁
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【锁关闭】
            logout('house')
        else:
            assert False

    @adb_log
    def test_C652(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁关闭时普通用户的指纹解锁
            login(UserInfo.AdminJoyce.phone, UserInfo.AdminJoyce.email, UserInfo.AdminJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            add_finger_yale(UserInfo.NormalJoyce.name, UserInfo.NormalJoyce.name, UserInfo.NormalJoyce.name, user_level='normal')
            # logout('house')
            login(UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.email, UserInfo.NormalJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            # check_lock_connection()
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            requests.put(url=API().finger_open(UserInfo.NormalJoyce.name, UserInfo.NormalJoyce.name))  # 指纹开锁
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            requests.put(url=API().finger_open(UserInfo.NormalJoyce.name, UserInfo.NormalJoyce.name))  # 指纹关锁
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C653(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁关闭时普通用户的密码解锁
            login(UserInfo.AdminJoyce.phone, UserInfo.AdminJoyce.email, UserInfo.AdminJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            password_register(UserInfo.NormalJoyce.name, UserInfo.NormalJoyce.phone, user_level='normal')  # 设置密码
            # logout('house')
            login(UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.email, UserInfo.NormalJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            # check_lock_connection()
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            requests.put(url=API().password_open(UserInfo.NormalJoyce.phone))  # 密码开门
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            time.sleep(5)
            requests.put(url=API().password_open(UserInfo.NormalJoyce.phone))  # 密码关门
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C654(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁关闭时普通用户的门卡解锁
            login(UserInfo.AdminJoyce.phone, UserInfo.AdminJoyce.email, UserInfo.AdminJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            add_card_yale(UserInfo.NormalJoyce.name, user_level='normal')  # 添加门卡
            # logout('house')
            login(UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.email, UserInfo.NormalJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            # check_lock_connection()
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            requests.put(url=API().card_open())  # 门卡开门
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            requests.put(url=API().card_open())  # 门卡关门
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            # logout('house')
            login(UserInfo.AdminJoyce.phone, UserInfo.AdminJoyce.email, UserInfo.AdminJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            delete_card_yale(UserInfo.NormalJoyce.name)  # 删除门卡
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C655(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查自动上锁关闭时访客的密码解锁
            # login(UserInfo.AdminJoyce.phone, UserInfo.AdminJoyce.email, UserInfo.AdminJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            click_element(userManagement_main_user_tab)  # 点击【用户】
            click_element(userManagement_invite_invite)  # 点击邀请用户
            click_element(userManagement_invite_invite_guest)  # 点击【访客邀请】
            send_keys(userManagement_invite_guest_first_name, UserInfo.GuestJoyce.first_name)  # 输入名
            send_keys(userManagement_invite_guest_last_name, UserInfo.GuestJoyce.last_name)  # 输入姓
            click_element(userManagement_invite_continue_button)  # 继续
            click_element(userManagement_invite_create_pin)
            clear_text_content(userManagement_invite_pin_input)
            send_keys(userManagement_invite_pin_input, UserInfo.GuestJoyce.phone[-6:])  # 输入密码
            click_element(userManagement_invite_change_pin, after_action_sleep=5)
            click_element(userManagement_invite_save_pin, after_action_sleep=15)
            click_element(lock_main_lock_manager)
            # check_lock_connection()
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            requests.put(url=API().password_open(UserInfo.GuestJoyce.phone))  # 密码开门
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            time.sleep(5)
            requests.put(url=API().password_open(UserInfo.GuestJoyce.phone))  # 密码关门
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            delete_guest(UserInfo.GuestJoyce.name)   # 删除访客
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C667(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证app恢复出厂设置时蓝牙关闭的情况
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()  # 检查锁的连接
            # assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            bt_switch('off')
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_lock_setting)  # 点击锁设置
            # assert_result_displayed(lock_lockSetting_loading, True)  # 显示【刷新中】
            # time.sleep(60)
            click_element(lock_lockSetting_reset)   # 点击出厂设置
            click_element(lock_lockSetting_reset_button)    # 点击恢复出厂设置按钮
            click_element(lock_lockSetting_reset_confirm)  # 点击重置
            assert_result_displayed(lock_lockSetting_connection_error, True)  # 显示连接故障
            click_element(register_verifyCode_send_again_ok)  # 点击好的(盼盼))   # 取消连接
            driver.press_keycode(4)
            driver.press_keycode(4)
            # logout('house')
            bt_switch('on')
        else:
            assert False

    @adb_log
    def test_C671(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查设备信息
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            click_element(lock_setting_setting_button)  # 点击设置按钮
            click_element(lock_setting_lock_setting)  # 点击锁设置
            scroll_and_click(lock_lockSetting_scroll_view, lock_lockSetting_device_info)  # 上滑点击设备信息
            check_lock_info(lock_lockSetting_module_serial)   # 显示模块序列号
            check_lock_info(lock_lockSetting_module_version)  # 显示模块版本
            check_lock_info(lock_lockSetting_lock_model)    # 显示锁的型号
            # assert_result_text(lock_lockSetting_module_serial, selected_module_serial)   # 显示模块序列号
            # assert_result_text(lock_lockSetting_module_version, selected_module_version)  # 显示模块版本
            # assert_result_text(lock_lockSetting_lock_model, selected_lock_model)  # 显示锁的型号
            # assert_result_text(lock_lockSetting_lock_firmware_version, selected_lock_firmware_version)  # 显示锁的固件版本
            driver.press_keycode(4)
            click_element(userManagement_invite_guest_back_button)  # 返回
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C674(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查无网络时管理员连接情况
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            # click_element(homePage_homePage_home_list)  # 点击住宅图片
            wifi_switch('close')  # 断网
            check_lock_connection()
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
            click_element(lock_main_user_lock_button)  # 点击锁
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            wifi_switch('open')  # 联网
        else:
            assert False

    @adb_log
    def test_C675(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查无网络时普通用户连接情况
            logout('house')
            time.sleep(15)
            login(UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.email, UserInfo.NormalJoyce.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            wifi_switch('close')  # 断网
            while 1:
                try:
                    assert_result_displayed(lock_lockSetting_wifi_lost, True)  # 提示似乎已断开与互联网的连接
                    break
                except:
                    click_element(lock_main_user_lock_button)  # 点击锁
            wifi_switch('open')  # 联网
            logout('house')
        else:
            assert False

    @adb_log
    def test_C7413(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 绑定bridge
            login(UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.email, UserInfo.NormalJoyce.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 菜单
            click_element(homePage_menu_add_device)  # 添加设备
            click_element(homePage_else_add_bridge_text)  # 添加智能附件
            click_element(homePage_else_venus_setup)  # 设置网关
            click_element(homePage_else_venus_setup)  # 网关已经插好了
            click_element(homePage_else_venus_setup)  # 灯是绿色的
            click_element(homePage_else_venus_setup)  # 找到了网关
            click_element(homePage_else_venus_house)  # 选择房间和锁
            click_element(homePage_else_select_network)  # 选择房间和锁
            click_element(homePage_else_network_continue)  # 继续
            send_keys(homePage_else_input_password, 'assa2020')  # 输入密码
            click_element(homePage_else_allow_open_bt)  # 允许打开网络
            click_element(homePage_else_venus_finish)  # 关闭
            click_element(userManagement_invite_confirm)  # 确认退出
            # # logout('house')
        else:
            assert False

    @adb_log
    def test_C7536(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 开关锁_bridge
            # # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            bt_switch('off')  # 关闭蓝牙
            click_element(lock_main_user_lock_button)  # 点击锁
            assert_result_displayed(lock_main_lock_open_status, True)  # 显示【门锁打开】
            time.sleep(10)
            click_element(lock_main_user_lock_button)  # 点击锁
            assert_result_displayed(lock_main_lock_close_status, True)  # 显示【门锁关闭】
        else:
            assert False

    @adb_log
    def test_D000(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 恢复出厂设置 #660
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_home_list)  # 点击住宅图片
            # click_element(UserManagement_user_list)  # 点击【用户列表】
            # click_element(UserManagement_user_admin.format(UserInfo.NormalJoyce.name))  # 点击普通用户A
            # click_element(UserManagement_user_fingerprint)  # 点击【指纹】
            # click_element(UserManagement_remove_card)  # 删除指纹
            # time.sleep(8)
            # click_element(UserManagement_delete_user)   # 点击删除用户
            # click_element(lock_setting_save_button)    # 确认删除
            # click_element(lock_main_lock_manager)  # 点击锁
            factory_lock_yale()
            logout()
        else:
            assert False

    def cases(self):
        return list(filter(lambda m: m.startswith("test_C") and callable(getattr(self, m)), dir(self)))