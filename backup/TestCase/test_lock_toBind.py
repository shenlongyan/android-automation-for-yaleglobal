import logging
import unittest
from Configuration.OperationMethod import *
from Configuration import StartUp
import random


class TestAddDevice(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = StartUp.driver
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H_%M_%S',
                            filename=FolderList.logging_name,
                            filemode='a')
        logging.getLogger()
        time.sleep(1)

    def add_img(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

    def setUp(self) -> None:
        os.system('adb shell svc wifi enable')
        bt_switch('on')
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    @adb_log
    def test_C000(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查是否解绑
            login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            try:
                if check_element(homePage_homePage_home_list, wait_time=5, check_type=False) is False:
                    pass
                else:
                    click_element(homePage_homePage_home_list)  # 点击住宅图片
                    factory_lock_yale()
            except NameError:
                pass
            logout()
        else:
            assert False

    @adb_log
    def test_C573(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查添加设备入口
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            assert_result_displayed(homePage_homePage_main, True)  # 显示添加设备主页
            assert_result_displayed(homePage_homePage_add_device, True)  # 显示添加设备按钮
            # logout()
        else:
            assert False

    @adb_log
    def test_C529(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 添加设备页UI
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            assert_result_displayed(homePage_else_add_device_text, True)  # 显示PANPAN智能门锁
            # logout()
        else:
            assert False

    @adb_log
    def test_C530(self):
        if app_select == 'panpan':
            user_register()
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(add_button_device_1)  # 点击【盼盼智能门锁】
            assert_result_displayed(add_compound_device_1, True)  # 显示【FC2A】
            click_element(add_button_device_list_close)  # 点击【关闭】
            click_element(add_button_device_2)  # 点击【盼盼智能锁柜】
            assert_result_displayed(add_assert_device_2, True)  # 显示【设置步骤】
            click_element(register_button_back)  # 点击【返回】
            click_element(add_button_device_3)  # 点击【盼盼智能附件】
            assert_result_text(register_assert_alert, add_assert_device_3)  # 显示【请先安装智能门锁】
            click_element(add_button_device_4)  # 点击【DOORMAX智能设备】
            assert_result_displayed(add_compound_device_4, True)  # 显示【DD110】
            click_element(add_button_device_list_close)  # 点击【关闭】
            click_element(add_button_device_5)  # 点击【KEYLOCK智能设备】
            assert_result_displayed(add_compound_device_5, True)  # 显示【315E】
            click_element(add_button_device_list_close)  # 点击【关闭】
            logout('other')
        elif app_select == 'yale':
            # 检查智能门锁区域跳转
            pass
        else:
            assert False

    @adb_log
    def test_C534(self):
        if app_select == 'panpan':
            user_register()
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(add_button_device_1)  # 点击【PanPan智能设备】
            click_element(add_compound_device_1)  # 点击【FC2A】
            click_element(add_button_start_install)  # 点击【安装指南】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(add_button_installed)  # 点击【已安装】
            click_element(register_button_back)  # 点击【返回】
            logout('other')
        elif app_select == 'yale':
            # 检查选择门锁页UI
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # click [can't scan the code?]
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            assert_result_displayed(homePage_else_set_device_text, True)  # 显示PANPAN智能门锁
            # logout()
        else:
            assert False

    @adb_log
    def test_C535(self):
        if app_select == 'panpan':
            user_register()
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(add_button_device_1)  # 点击【PanPan智能设备】
            click_element(add_compound_device_1)  # 点击【FC2A】
            click_element(add_button_installed)  # 点击【已安装】
            click_element(register_button_back)  # 点击【返回】
            logout('other')
        elif app_select == 'yale':
            # 检查选择门锁页跳转
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            assert_result_displayed(homePage_else_lock_start, True)  # 显示安装智能锁开始按钮
            driver.press_keycode(4)  # 点击【返回】
            assert_result_displayed(homePage_else_set_device_text, True)  # 显示选择PANPAN智能门锁
            # logout()
        else:
            assert False

    @adb_log
    def test_C536(self):
        if app_select == 'panpan':
            user_register()
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(add_button_device_5)  # 点击【KEYLOCK智能设备】
            click_element(add_compound_device_5)  # 点击【315E】
            click_element(add_button_installed)  # 点击【已安装】
            click_element(add_button_start_connect)  # 点击【开始连接】
            assert_result_displayed(add_compound_start_setup, True)  # 显示【开始设置】
            click_element(homepage_button_detail_back)  # 点击【返回】
            logout('other')
        elif app_select == 'yale':
            # 检查安装智能门锁引导页UI
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # click [can't scan the code?]
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            assert_result_displayed(homePage_else_add_lock_text, True)  # 跳转至安装智能锁页面
            # logout()
        else:
            assert False

    @adb_log
    def test_C537(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查安装智能门锁功能键
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            assert_result_displayed(homePage_else_link_lock_text, True)  # 跳转至连接智能锁界面
            # logout()
        else:
            assert False

    @adb_log
    def test_C538(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 检查连接智能锁页UI
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            assert_result_displayed(homePage_else_link_lock_text, True)  # 显示连接智能锁提示
            assert_result_displayed(homePage_else_lock_start, True)  # 有开始按钮
            # logout()
        else:
            assert False

    @adb_log
    def test_C539(self):
        if app_select == 'panpan':
            user_register()
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(add_button_device_1)  # 点击【PanPan智能设备】
            click_element(add_compound_device_1)  # 点击【FC2A】
            click_element(add_button_installed)  # 点击【已安装】
            click_element(add_button_start_connect)  # 点击【开始连接】
            assert_result_displayed(add_compound_start_setup, True)  # 显示【开始设置】
            click_element(homepage_button_detail_back)  # 点击【返回】
            logout('other')
        elif app_select == 'yale':
            # 检查连接智能锁页按键功能
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            assert_result_displayed(homePage_else_lock_install, True)  # 显示开始安装按钮
            # logout()
        else:
            assert False

    @adb_log
    def test_C540(self):
        if app_select == 'panpan':
            user_register()
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(add_button_device_1)  # 点击【PanPan智能设备】
            click_element(add_compound_device_1)  # 点击【FC2A】
            click_element(add_button_installed)  # 点击【已安装】
            click_element(add_button_start_connect)  # 点击【开始连接】
            click_element(add_compound_start_setup)  # 点击【开始设置】
            # 初次安装时有此验证
            try:
                assert_result_displayed(soft_button_location_permission, assert_type=False, wait_time=30) is False
            except:
                click_element(soft_button_location_permission)
                click_element(add_button_double_confirm)  # 请求定位权限
                click_element(soft_button_location_permission)
            else:
                pass
            assert_result_displayed(add_assert_select_lock_or_photo, True)  # 显示【选择一把锁】
            driver.press_keycode(4)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】
            logout('other')
        elif app_select == 'yale':
            # 检查选择锁页按键功能
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            assert_result_displayed(homePage_else_scan_smartlock_progress, True)  # 显示扫描智能锁进度
            # logout()
        else:
            assert False

    @adb_log
    def test_C541(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击菜单
            click_element(homePage_menu_add_device)  # 点击【添加设备】
            click_element(add_button_device_1)  # 点击【PanPan智能设备】
            click_element(add_compound_device_1)  # 点击【FC2A】
            click_element(add_button_installed)  # 点击【已安装】
            click_element(add_button_start_connect)  # 点击【开始连接】
            click_element(add_compound_start_setup)  # 点击【开始设置】
            # 初次安装时有此验证
            try:
                assert_result_displayed(soft_button_location_permission, assert_type=False, wait_time=30) is False
            except:
                click_element(soft_button_location_permission)
                click_element(add_button_double_confirm)  # 请求定位权限
                click_element(soft_button_location_permission)
            else:
                pass
            scroll_and_rescan(add_assert_select_lock_or_photo, add_compound_room_or_lock_name.format(selected_lock_id))
            assert_result_enable(add_button_create_lock_name, False)  # 不可点击【下一步】
            send_keys(add_button_lock_name_input_with_house, '1')  # 输入1
            assert_result_enable(add_button_create_lock_name, True)  # 可点击【下一步】
            clear_text_content(add_button_lock_name_input_with_house)  # 清除输入
            send_keys(add_button_lock_name_input_with_house, '123456789012')  # 输入123456789012
            assert_result_enable(add_button_create_lock_name, True)  # 可点击【下一步】
            clear_text_content(add_button_lock_name_input_with_house)  # 清除输入
            send_keys(add_button_lock_name_input_with_house, 'selected_lock_name')  # 输入锁名
            click_element(add_button_create_lock_name)  # 点击【下一步】
            assert_result_text(add_assert_select_house, add_assert_select_house_text)  # 显示【选择一个住宅】
            click_element(homepage_button_detail_back)  # 点击【返回】
            logout('other')
        elif app_select == 'yale':
            # 检查选择锁页关闭按钮功能
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            driver.swipe(50, 100, 100, 200)   # 滑动选择锁页关闭按钮
            assert_result_displayed(homePage_else_link_lock_text, True)  # 返回连接智能锁页
            # logout()
        else:
            assert False

    @adb_log
    def test_C542(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证扫描页UI
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            assert_result_displayed(homePage_else_scan_smartlock_progress, True)  # 显示扫描智能锁进度
            # logout()
        else:
            assert False

    @adb_log
    def test_C543(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证选择锁功能
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            assert_result_displayed(homePage_else_choose_lock_name, True)   # 显示锁名称输入框
            assert_result_displayed(homePage_else_add_lock_continue, True)  # 显示继续按钮
            # logout()
        else:
            assert False

    @adb_log
    def test_C545(self):
        if app_select == 'panpan':
            user_register()
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(add_button_device_1)  # 点击【PanPan智能设备】
            click_element(add_compound_device_1)  # 点击【FC2A】
            click_element(add_button_installed)  # 点击【已安装】
            click_element(add_button_start_connect)  # 点击【开始连接】
            click_element(add_compound_start_setup)  # 点击【开始设置】
            # 初次安装时有此验证
            try:
                assert_result_displayed(soft_button_location_permission, assert_type=False, wait_time=30) is False
            except:
                click_element(soft_button_location_permission)
                click_element(add_button_double_confirm)  # 请求定位权限
                click_element(soft_button_location_permission)
            else:
                pass
            scroll_and_rescan(add_assert_select_lock_or_photo, add_compound_room_or_lock_name.format(selected_lock_id))
            assert_result_displayed(add_compound_house_image, True)  # 显示【添加图片】
            click_element(homepage_button_detail_back)  # 点击【返回】
            logout('other')
        elif app_select == 'yale':
            # 验证多把锁扫描
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            assert_result_displayed(homePage_else_find_many_locks, True)   # 显示找到多把锁弹窗
            # logout()
        else:
            assert False

    @adb_log
    def test_C547(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证关闭蓝牙后扫描
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            bt_switch('off')
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            click_element(homePage_else_reject_open_bt)  # 拒绝打开蓝牙
            assert_result_displayed(homePage_else_scan_smartlock_failure, True)  # 显示扫描失败
            click_element(userManagement_invite_popup_confirm)  # 点击重试
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            click_element(homePage_else_allow_open_bt)  # 允许打开蓝牙
            click_element(userManagement_invite_popup_confirm)  # 点击重试
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            assert_result_displayed(homePage_else_find_many_locks, True)  # 显示找到多把锁弹窗
        else:
            assert False

    @adb_log
    def test_C548(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证选择智能锁功能
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            assert_result_displayed(homePage_else_choose_lock_name, True)   # 显示锁名称输入框
            # logout()
        else:
            assert False

    @adb_log
    def test_C549(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            add_lock()
            click_element(add_button_menu_in_house)  # 点击【菜单】
            click_element(homePage_menu_add_device)  # 点击【添加设备】
            click_element(add_button_device_1)  # 点击【PanPan智能设备】
            click_element(add_compound_device_1)  # 点击【FC2A】
            click_element(add_button_installed)  # 点击【已安装】
            click_element(add_button_start_connect)  # 点击【开始连接】
            click_element(add_compound_start_setup)  # 点击【开始设置】
            # 初次安装时有此验证
            try:
                assert_result_displayed(soft_button_location_permission, assert_type=False, wait_time=30) is False
            except:
                click_element(soft_button_location_permission)
                click_element(add_button_double_confirm)  # 请求定位权限
                click_element(soft_button_location_permission)
            else:
                pass
            assert_result_displayed(scroll_and_rescan(add_assert_select_lock_or_photo, add_compound_room_or_lock_name.format(selected_lock_id)),
                                    False)  # 不显示已添加的锁
            driver.press_keycode(4)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homePage_homePage_menu_in_other_page)  # 点击【菜单】
            click_element(homePage_menu_home)  # 点击【首页】
            click_element(homepage_button_select_room_or_user.format(HouseInfo.name))  # 点击房间
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 验证默认住宅名
            logout()
            user_register_yale()   # 注册
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            assert_result_text(homePage_else_house_new_name, homePage_else_default_house_name)  # 验证默认住宅名Mi Casa
            # logout()
        else:
            assert False

    @adb_log
    def test_C550(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_add_device)  # 点击【添加设备】
            click_element(add_button_device_1)  # 点击【PanPan智能设备】
            click_element(add_compound_device_1)  # 点击【FC2A】
            click_element(add_button_installed)  # 点击【已安装】
            click_element(add_button_start_connect)  # 点击【开始连接】
            bt_switch('off')
            click_element(add_compound_start_setup)  # 点击【开始设置】
            assert_result_displayed(add_assert_bt_need_open, True)  # 提示请打开蓝牙后进行搜索
            bt_switch('on')
            click_element(homepage_button_detail_back)  # 点击【返回】
            logout('other')
        elif app_select == 'yale':
            # 验证添加住宅页UI
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            assert_result_displayed(homePage_else_choose_name_radiobutton, True)  # 添加一个新住宅单选按钮
            assert_result_displayed(homePage_else_choose_house_continue, True)   # 显示【继续】按钮
            # logout()
        else:
            assert False

    @adb_log
    def test_C551(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证添加住宅页功能
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            click_element(homePage_else_choose_name_radiobutton)  # 点击添加一个新住宅单选按钮
            click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
            assert_result_displayed(homePage_else_house_new_name, True)  # 显示住宅名字输入框
            assert_result_displayed(homePage_else_house_new_image, True)  # 显示上传住宅图片按钮
            # logout()
        else:
            assert False

    @adb_log
    def test_C552(self):
        if app_select == 'panpan':
            user_register()
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(add_button_device_1)  # 点击【PanPan智能设备】
            click_element(add_compound_device_1)  # 点击【FC2A】
            click_element(add_button_installed)  # 点击【已安装】
            click_element(add_button_start_connect)  # 点击【开始连接】
            click_element(add_compound_start_setup)  # 点击【开始设置】
            # 初次安装时有此验证
            try:
                assert_result_displayed(soft_button_location_permission, assert_type=False, wait_time=30) is False
            except:
                click_element(soft_button_location_permission)
                click_element(add_button_double_confirm)  # 请求定位权限
                click_element(soft_button_location_permission)
            else:
                pass
            scroll_and_rescan(add_assert_select_lock_or_photo, add_compound_room_or_lock_name.format(selected_lock_id))
            click_element(add_compound_house_image)  # 点击【添加照片】
            assert_result_displayed(add_assert_select_lock_or_photo, True)  # 显示【添加照片】
            driver.press_keycode(4)  # 点击【返回】
            send_keys(add_button_house_name_input, HouseInfo.name)  # 输入房间名
            send_keys(add_button_lock_name_input_no_house, selected_lock_id)  # 输入锁名
            click_element(add_button_create_house_and_lock)  # 点击【下一步】
            click_element(homePage_else_sure_or_try)  # 点击【试试】
            click_element(user_button_setting)  # 点击【设定】
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 验证添加住宅页添加图片功能
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            click_element(homePage_else_choose_name_radiobutton)  # 点击添加一个新住宅单选按钮
            click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
            click_element(homePage_else_house_new_image)  # 点击上传住宅图片按钮
            click_element(homePage_else_choose_house_image)  # 点击选择图片
            click_element(homePage_else_image_from_phone)   # 选择相册第一张照片
            assert_result_displayed(homePage_else_house_image_view, True)  # 显示照片
            # logout()
        else:
            assert False

    @adb_log
    def test_C553(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证添加住宅页拍照功能
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            click_element(homePage_else_choose_name_radiobutton)  # 点击添加一个新住宅单选按钮
            click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
            click_element(homePage_else_house_new_image)  # 点击上传住宅图片按钮
            click_element(homePage_else_take_house_photo)  # 点击拍一张照片
            click_element(register_UserIcon_shutter_button)  # 按下快门
            click_element(register_UserIcon_sure_button)  # 确定
            assert_result_displayed(homePage_else_house_image_view, True)  # 显示照片
            # logout()
        else:
            assert False

    @adb_log
    def test_C554(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证添加住宅页住宅名编辑功能
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            click_element(homePage_else_choose_name_radiobutton)  # 点击添加一个新住宅单选按钮
            click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
            send_keys(homePage_else_house_new_name, random.choice('abcdefghijklmnopqrstuvwxyz0123456789'))
            # logout()
        else:
            assert False

    @adb_log
    def test_C555(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证添加住宅页继续按钮
            pass
        else:
            assert False

    @adb_log
    def test_C556(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
            add_lock()
            factory_lock()
            logout()
        elif app_select == 'yale':
            # 验证添加住宅页默认住宅名
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            click_element(homePage_else_choose_name_radiobutton)  # 点击添加一个新住宅单选按钮
            click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
            assert_result_text(homePage_else_house_new_name,  homePage_else_default_house_name)  # 默认住宅名Mi Casa
            # logout()
        else:
            assert False

    @adb_log
    def test_C557(self):
        if app_select == 'panpan':
            login(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
            click_element(homePage_homePage_menu_in_home_page)  # 点击菜单
            click_element(homePage_menu_add_device)  # 点击【添加设备】
            click_element(add_button_device_1)  # 点击【PanPan智能设备】
            click_element(add_compound_device_1)  # 点击【FC2A】
            click_element(add_button_installed)  # 点击【已安装】
            click_element(add_button_start_connect)  # 点击【开始连接】
            click_element(add_compound_start_setup)  # 点击【开始设置】
            # 初次安装时有此验证
            try:
                assert_result_displayed(soft_button_location_permission, assert_type=False, wait_time=30) is False
            except:
                click_element(soft_button_location_permission)
                click_element(add_button_double_confirm)  # 请求定位权限
                click_element(soft_button_location_permission)
            else:
                pass
            scroll_and_rescan(add_assert_select_lock_or_photo, add_compound_room_or_lock_name.format(selected_lock_id))
            send_keys(add_button_lock_name_input_with_house, 'selected_lock_name')  # 输入锁名
            click_element(add_button_create_lock_name)  # 点击【下一步】
            click_element(add_button_add_room)  # 点击【添加住宅】
            click_element(homepage_button_detail_back)  # 点击【返回】
            logout('other')
        elif app_select == 'yale':
            # 验证添加住宅页空住宅名
            pass
        else:
            assert False

    @adb_log
    def test_C558(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 有住宅时设置智能锁页面UI
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install, after_action_sleep=20)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            try:
                click_element(homePage_else_all_house)     # 除了添加新住宅外，其他按钮为已经添加过的住宅
            except:
                assert False
        else:
            assert False

    @adb_log
    def test_C559(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 有住宅时添加新住宅
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            click_element(homePage_else_all_house)   # 除了添加新住宅外，其他按钮为已经添加过的住宅
            click_element(homePage_else_choose_name_radiobutton)     # 有住宅时添加新住宅
            click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
            assert_result_displayed(homePage_else_house_new_name, True)  # 显示住宅名字输入框
            assert_result_displayed(homePage_else_house_new_image, True)  # 显示上传住宅图片按钮
            # logout()
        else:
            assert False

    @adb_log
    def test_C560(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 有住宅时添加住宅图片功能
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            click_element(homePage_else_all_house)  # 除了添加新住宅外，其他按钮为已经添加过的住宅
            click_element(homePage_else_choose_name_radiobutton)  # 有住宅时添加新住宅
            click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
            click_element(homePage_else_house_new_image)  # 点击上传住宅图片按钮
            click_element(homePage_else_choose_house_image)  # 点击选择图片
            click_element(homePage_else_image_from_phone)   # 选择相册第一张照片
            assert_result_displayed(homePage_else_house_image_view, True)  # 显示照片
            # logout()
        else:
            assert False

    @adb_log
    def test_C561(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 有住宅时添加住宅页拍照功能
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            click_element(homePage_else_all_house)  # 除了添加新住宅外，其他按钮为已经添加过的住宅
            click_element(homePage_else_choose_name_radiobutton)  # 有住宅时添加新住宅
            click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
            click_element(homePage_else_house_new_image)  # 点击上传住宅图片按钮
            click_element(homePage_else_take_house_photo)  # 点击拍一张照片
            click_element(register_UserIcon_shutter_button)  # 按下快门
            click_element(register_UserIcon_sure_button)  # 确定
            assert_result_displayed(homePage_else_house_image_view, True)  # 显示照片
            # logout()
        else:
            assert False

    @adb_log
    def test_C562(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 有住宅时住宅编辑功能
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            click_element(homePage_else_all_house)  # 除了添加新住宅外，其他按钮为已经添加过的住宅
            click_element(homePage_else_choose_name_radiobutton)  # 有住宅时添加新住宅
            click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
            send_keys(homePage_else_house_new_name, random.choice('abcdefghijklmnopqrstuvwxyz0123456789'))
            # logout()
        else:
            assert False

    @adb_log
    def test_C563(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 有住宅时添加住宅页继续按钮
            pass
        else:
            assert False

    @adb_log
    def test_C564(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 有住宅时添加住宅住宅名防呆
            pass
        else:
            assert False

    @adb_log
    def test_C565(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 添加住宅页的返回按钮
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            click_element(homePage_else_choose_name_radiobutton)  # 有住宅时添加新住宅
            click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
            driver.press_keycode(4)  # 点击【返回】
            assert_result_displayed(homePage_else_link_lock_text, True)  # 返回连接智能锁页
        else:
            assert False

    @adb_log
    def test_C566(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 验证锁命名功能
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            assert_result_text(homePage_else_choose_lock_name, homePage_else_default_lock_name)  # 锁默认命名前门
        else:
            assert False

    @adb_log
    def test_C567(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 锁命名位数防呆
            pass
        else:
            assert False

    @adb_log
    def test_C568(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 锁命名格式防呆
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            send_keys(homePage_else_choose_lock_name, '   ')  # 锁命名为空格
            click_element(homePage_else_choose_name_radiobutton)  # 添加新住宅
            click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
            assert_result_displayed(homePage_else_house_new_name, True)  # 显示住宅名字输入框
            assert_result_displayed(homePage_else_house_new_image, True)  # 显示上传住宅图片按钮
        else:
            assert False

    @adb_log
    def test_C569(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 锁命名页的关闭按钮
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install, after_action_sleep=20)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            # driver.swipe(50, 100, 100, 200)  # 滑动选择锁页关闭按钮
            driver.keyevent(4)
            assert_result_displayed(homePage_else_link_lock_text, True)  # 返回连接智能锁页
        else:
            assert False

    @adb_log
    def test_C570(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 锁命名页的继续按钮-正常添加锁流程
            # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            click_element(homePage_else_add_device_text)  # 点击设置智能门锁
            click_element(homePage_else_manual_scan)  # 手动选择我的设备
            click_element(homePage_else_set_device_text)  # 点击选择智能门锁
            click_element(homePage_else_set_device_image)  # 点击智能门锁第一张图片
            click_element(homePage_else_lock_start)  # 点击安装智能锁【开始】按钮
            click_element(homePage_else_lock_start)  # 点击连接智能锁【开始】按钮
            click_element(homePage_else_lock_install)  # 点击开始安装按钮
            scroll_and_rescan(homePage_else_find_many_locks, homePage_else_compound_lock_name.format(selected_lock_id))  # 选择锁
            click_element(homePage_else_choose_house_continue)  # 点击【继续】按钮
            time.sleep(60)
            click_element(homePage_else_lock_start)  # 点击【试试】按钮(盼盼才有)
            assert_result_displayed(homePage_else_my_house_name, True)  # 显示我的住宅名
            # factory_lock_yale()
            # logout()
        else:
            assert False

    def cases(self):
        return list(filter(lambda m: m.startswith("test_C") and callable(getattr(self, m)), dir(self)))