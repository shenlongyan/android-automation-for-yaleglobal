import logging
import threading
import unittest
from Configuration.OperationMethod import *
from Configuration import StartUp
from Redis.VerificationCode import verification_code_phone, verification_code_email
import os

RunOnWhere = 'Server'
if RunOnWhere == 'Server':
    # 第十处修改 # run on server
    autoLock = os.environ['AutoLock']
    # print('autoLock=' + autoLock)
elif RunOnWhere == 'local':
    # run on local
    autoLock = True
else:
    raise ValueError


class TestSmartAlert(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = StartUp.driver
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H_%M_%S',
                            filename=FolderList.logging_name,
                            filemode='a')
        logging.getLogger()
        time.sleep(1)

    def add_img(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

    def setUp(self) -> None:
        os.system('adb shell svc wifi enable')
        bt_switch('on')
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    @adb_log
    def test_C000(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            # 设置前置条件
            try:
                click_element(homePage_homePage_home_list)  # 点击住宅图片
            except:
                pass
            try:
                if check_element(userManagement_main_home_tab, wait_time=5, check_type=False) is False:
                    add_lock_yale()
                    click_element(lock_main_lock_home_button)  # 点击home按钮
            except NameError:
                pass
            # else:
            #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
            check_lock_connection()
            # logout('house')
        else:
            assert False

    @adb_log
    def test_C826(self):
        # 首次智能提醒设置首页显示及返回按钮
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)   # 点击智能提醒
        assert_result_displayed(lock_lockSetting_add_alert_button, True)   # 显示添加智能提醒+按钮
        click_element(lock_lockSetting_add_alert_back)  # 点击返回按钮
        assert_result_displayed(lock_lockSetting_alarm, True)  # 返回锁设置页

    @adb_log
    def test_C828(self):
        # 创建智能提醒按钮
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)   # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)   # 点击+按钮
        assert_result_displayed(lock_lockSetting_choose_event, True)  # 显示选择活动
        click_element(lock_lockSetting_add_alert_back)  # 点击返回按钮
        assert_result_displayed(lock_lockSetting_add_alert_button, True)  # 返回智能提醒首页

    @adb_log
    def test_C829(self):
        # 取消按钮
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)   # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)   # 点击+按钮
        click_element(lock_lockSetting_add_alert_back)  # 点击返回按钮
        assert_result_displayed(lock_lockSetting_add_alert_button, True)  # 返回智能提醒首页

    @adb_log
    def test_C830(self):
        # 保存按钮不可点击
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)   # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)   # 点击+按钮
        assert_result_displayed(lock_lockSetting_alert_save_button, True)   # 显示保存按钮
        assert_result_enable(lock_lockSetting_alert_save_button, False)  # 保存按钮不可点击

    @adb_log
    def test_C831(self):
        # 选择时间栏不可点击
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)  # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
        assert_result_displayed(lock_lockSetting_choose_time, True)  # 显示选择时间
        assert_result_enable(lock_lockSetting_choose_time, False)  # 选择时间栏不可点击

    @adb_log
    def test_C832(self):
        # 选择事件栏
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)  # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
        click_element(lock_lockSetting_choose_event)   # 点击选择活动
        assert_result_displayed(lock_lockSetting_lock_by_special, True)  # 显示门由特定用户锁定或解锁
        assert_result_enable(lock_lockSetting_choose_alert_done, False)  # 完成按钮不可点击

    @adb_log
    def test_C833(self):
        # 选择开关锁事件
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)  # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
        click_element(lock_lockSetting_choose_event)  # 点击选择活动
        click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
        assert_result_displayed(UserManagement_user_admin.format(UserInfo.Four.name), True)  # 显示用户列表

    @adb_log
    def test_C834(self):
        # 不可为访客创建智能提醒
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        invite_user_yale('guest', UserInfo.GuestJoyce.phone, UserInfo.GuestJoyce.name2, 'always', 'default')   # 邀请访客
        click_element(lock_main_lock_manager)  # 点击锁
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)  # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
        click_element(lock_lockSetting_choose_event)  # 点击选择活动
        click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
        assert_result_displayed(lock_lockSetting_visitor_not_show, False)  # 不显示访客栏
        assert_result_displayed(UserManagement_user_admin.format('guest zhu'), False)  # 不显示访客

    @adb_log
    def test_C835(self):
        # 选择用户
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)  # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
        click_element(lock_lockSetting_choose_event)  # 点击选择活动
        click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
        click_element(UserManagement_user_admin.format(UserInfo.Four.name))  # 点击登录账号
        assert_result_text(lock_lockSetting_lock_by_special, "{}关锁或解锁".format(UserInfo.Four.name))  # 显示特定用户关锁或解锁

    @adb_log
    def test_C836(self):
        # 完成按钮
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)  # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
        click_element(lock_lockSetting_choose_event)  # 点击选择活动
        click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
        click_element(UserManagement_user_admin.format(UserInfo.Four.name))  # 点击登录账号
        assert_result_text(lock_lockSetting_lock_by_special, "{}关锁或解锁".format(UserInfo.Four.name))  # 显示特定用户关锁或解锁
        click_element(lock_lockSetting_delete_button)   # 点击取消
        assert_result_displayed(lock_lockSetting_add_alert_button, True)  # 返回智能提醒设置首页

    @adb_log
    def test_C837(self):
        # 新建智能提醒页_选择时间栏
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)  # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
        click_element(lock_lockSetting_choose_event)  # 点击选择活动
        click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
        click_element(UserManagement_user_admin.format(UserInfo.Four.name))  # 点击登录账号
        click_element(lock_lockSetting_choose_time)  # 点击选择时间
        assert_result_displayed(lock_lockSetting_any_time, True)  # 显示任何时候
        assert_result_displayed(lock_lockSetting_fixed_time, True)  # 显示在一个时间范围内

    @adb_log
    def test_C838(self):
        # 选择任何时间
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)  # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
        click_element(lock_lockSetting_choose_event)  # 点击选择活动
        click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
        click_element(UserManagement_user_admin.format(UserInfo.Four.name))  # 点击登录账号
        click_element(lock_lockSetting_choose_time)  # 点击选择时间
        click_element(lock_lockSetting_any_time)  # 选择任何时候
        assert_result_displayed(lock_lockSetting_any_time, True)  # 显示任何时候

    @adb_log
    def test_C839(self):
        # 选择一段时间
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)  # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
        click_element(lock_lockSetting_choose_event)  # 点击选择活动
        click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
        click_element(UserManagement_user_admin.format(UserInfo.Four.name))  # 点击登录账号
        click_element(lock_lockSetting_choose_time)  # 点击选择时间
        click_element(lock_lockSetting_fixed_time)  # 选择一段时间
        click_element(lock_lockSetting_alert_save_button)  # 保存
        assert_result_displayed(lock_lockSetting_smart_alert_text, True)  # 显示智能提醒内容

    @adb_log
    def test_C840(self):
        # 选择一段时间-取消按钮
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)  # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
        click_element(lock_lockSetting_choose_event)  # 点击选择活动
        click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
        click_element(UserManagement_user_admin.format(UserInfo.Four.name))  # 点击登录账号
        click_element(lock_lockSetting_choose_time)  # 点击选择时间
        click_element(lock_lockSetting_fixed_time)  # 选择一段时间
        click_element(lock_lockSetting_delete_button)  # 点击取消按钮
        assert_result_displayed(lock_lockSetting_add_alert_button, True)  # 返回智能提醒设置首页

    @adb_log
    def test_C841(self):
        # 创建一个智能提醒
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        time.sleep(5)
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)  # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
        click_element(lock_lockSetting_choose_event)  # 点击选择活动
        click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
        click_element(UserManagement_user_admin.format(UserInfo.Four.name))  # 点击登录账号
        click_element(lock_lockSetting_choose_time)  # 点击选择时间
        click_element(lock_lockSetting_any_time)  # 选择任何时候
        click_element(lock_lockSetting_alert_save_button)  # 保存
        assert_result_displayed(lock_lockSetting_smart_alert_text, True)  # 显示智能提醒内容

    @adb_log
    def test_C842(self):
        # 给已有智能提醒的用户再次创建
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)  # 点击设置按钮
        click_element(lock_setting_lock_setting)  # 点击锁设置
        click_element(lock_lockSetting_alarm)  # 点击智能提醒
        click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
        click_element(lock_lockSetting_choose_event)  # 点击选择活动
        click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
        assert_result_displayed(UserManagement_user_admin.format(UserInfo.Four.name), True)  # 登录账号应不在用户列表中,目前在

    @adb_log
    def test_C843(self):
        # 删除智能提醒按钮
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)    # 点击住宅图片
        check_lock_connection()
        click_element(lock_setting_setting_button)    # 点击设置按钮
        click_element(lock_setting_lock_setting)    # 点击锁设置
        click_element(lock_lockSetting_alarm)    # 点击智能提醒
        click_element(lock_lockSetting_smart_alert_text)   # 点击智能提醒内容
        click_element(lock_lockSetting_delete_button)    # 点击删除按钮
        click_element(userManagement_invite_popup_confirm)   # 点击弹窗中的删除
        assert_result_displayed(lock_lockSetting_add_alert_button, True)  # 返回智能提醒设置首页

    # @adb_log
    # def test_C844(self):
    #     # 智能提醒取消删除
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     click_element(UserManagement_user_list)  # 点击【用户列表】
    #     try:
    #         if check_element(UserManagement_user_normal.format(UserInfo.NormalJoyce.name), wait_time=5, check_type=False) is False:
    #             invite_user_yale('normal', UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.name)  # 邀请普通用户A
    #     except NameError:
    #         pass
    #     click_element(lock_main_lock_manager)  # 点击锁按钮
    #     check_lock_connection()
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
    #     click_element(lock_lockSetting_choose_event)  # 点击选择活动
    #     click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
    #     click_element(UserManagement_user_admin.format(UserInfo.NormalJoyce.name))  # 点击普通用户A
    #     click_element(lock_lockSetting_choose_time)  # 点击选择时间
    #     click_element(lock_lockSetting_fixed_time)  # 选择一段时间
    #     click_element(lock_lockSetting_alert_save_button)  # 保存
    #     click_element(lock_lockSetting_smart_alert_text)  # 点击智能提醒内容
    #     click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #     click_element(userManagement_invite_popup_cancel)  # 点击弹窗中的取消
    #     assert_result_displayed(lock_lockSetting_delete_button, True)  # 未删除成功，仍显示删除按钮
    #
    # @adb_log
    # def test_C845(self):
    #     # 管理员_蓝牙_任意时间
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()    # 检查锁的连接
    #     if autoLock is True:
    #         auto_lock_switch('关闭')  # 关闭自动上锁
    #     else:
    #        pass
    #     add_smart_alert(UserInfo.Four.name)  # 创建智能提醒
    #     while 1:
    #         try:
    #             assert_result_displayed(lock_main_lock_close_status, True)
    #         except:
    #             click_element(lock_main_user_lock_button)  # lock
    #         else:
    #             break
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_open_bt, True)  # 蓝牙开锁推送
    #     click_element(notification_main_clear_button)      # 清除所有通知
    #     check_lock_connection()
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_close_bt, True)  # 蓝牙关锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     # logout('house')
    #
    # @adb_log
    # def test_C846(self):
    #     # 管理员_蓝牙_指定时间内有推送
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     if autoLock is True:
    #         auto_lock_switch('关闭')  # 关闭自动上锁
    #     else:
    #         pass
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting, after_action_sleep=10)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     try:
    #         if check_element(lock_lockSetting_smart_alert_text, wait_time=20, check_type=False) is False:
    #             pass
    #         else:
    #             click_element(lock_lockSetting_smart_alert_text)  # 点击智能提醒内容
    #             click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #             click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #     except NameError:
    #         pass
    #     click_element(lock_lockSetting_add_alert_back, after_action_sleep=15)  # 返回
    #     click_element(login_login_back_button)  # 返回
    #     click_element(lock_main_lock_manager)  # 点击锁按钮
    #     check_lock_connection()
    #     add_smart_alert(UserInfo.Four.name, time_type='fixedtime')  # 创建现在的智能提醒
    #     while 1:
    #         try:
    #             assert_result_displayed(lock_main_lock_close_status, True)
    #         except:
    #             click_element(lock_main_user_lock_button)  # lock
    #         else:
    #             break
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_open_bt, True)  # 蓝牙开锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_close_bt, True)  # 蓝牙关锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     # 管理员_蓝牙_指定时间外无推送
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     try:
    #         if check_element(lock_lockSetting_smart_alert_text, wait_time=20, check_type=False) is False:
    #             pass
    #         else:
    #             click_element(lock_lockSetting_smart_alert_text)  # 点击智能提醒内容
    #             click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #             click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #     except NameError:
    #         pass
    #     click_element(lock_lockSetting_add_alert_back, after_action_sleep=15)  # 返回
    #     click_element(login_login_back_button)  # 返回
    #     click_element(lock_main_lock_manager)  # 点击锁按钮
    #     check_lock_connection()
    #     add_smart_alert(UserInfo.Four.name, time_type='fixedtime', what_time='future')  # 创建以后的智能提醒
    #     while 1:
    #         try:
    #             assert_result_displayed(lock_main_lock_close_status, True)
    #         except:
    #             click_element(lock_main_user_lock_button)  # lock
    #         else:
    #             break
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_open_bt, False, wait_time=5)  # 蓝牙开锁无推送
    #     os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
    #     click_element("//*[@text='Yale Access']")
    #     # check_lock_connection()
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_close_bt, False, wait_time=5)  # 蓝牙关锁无推送
    #     os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
    #     click_element("//*[@text='Yale Access']")
    #     # logout('house')
    #
    # @adb_log
    # def test_C853(self):
    #     # 管理员智能提醒删除
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     try:
    #         if check_element(lock_lockSetting_smart_alert_text, wait_time=20, check_type=False) is False:
    #             click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
    #             click_element(lock_lockSetting_choose_event)  # 点击选择活动
    #             click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
    #             click_element(UserManagement_user_admin.format(UserInfo.Four.name))  # 点击登录账号
    #             click_element(lock_lockSetting_choose_time)  # 点击选择时间
    #             click_element(lock_lockSetting_any_time)  # 选择任何时候
    #             click_element(lock_lockSetting_alert_save_button)  # 保存
    #         else:
    #             pass
    #     except NameError:
    #         pass
    #     else:
    #         click_element(lock_lockSetting_smart_alert_text)  # 点击智能提醒内容
    #         click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #         click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #         click_element(lock_lockSetting_add_alert_back, after_action_sleep=15)  # 返回
    #         click_element(login_login_back_button)  # 返回
    #     click_element(lock_main_lock_manager)  # 点击锁按钮
    #     while 1:
    #         try:
    #             assert_result_displayed(lock_main_lock_close_status, True)
    #         except:
    #             click_element(lock_main_user_lock_button)  # lock
    #         else:
    #             break
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_open_bt, False)  # 蓝牙开锁无推送
    #     os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
    #     click_element("//*[@text='Yale Access']")
    #     check_lock_connection()
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_close_bt, False)  # 蓝牙关锁无推送
    #     os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
    #     click_element("//*[@text='Yale Access']")
    #     # logout('house')
    #
    # @adb_log
    # def test_C854(self):
    #     # 管理员智能提醒修改时间
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     try:
    #         if check_element(lock_lockSetting_smart_alert_text, wait_time=20, check_type=False) is False:
    #             pass
    #         else:
    #             click_element(lock_lockSetting_smart_alert_text)  # 点击智能提醒内容
    #             click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #             click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #     except NameError:
    #         pass
    #     click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
    #     click_element(lock_lockSetting_choose_event)  # 点击选择活动
    #     click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
    #     click_element(UserManagement_user_admin.format(UserInfo.Four.name))  # 点击登录账号
    #     click_element(lock_lockSetting_choose_time)  # 点击选择时间
    #     click_element(lock_lockSetting_any_time)  # 选择任何时候
    #     click_element(lock_lockSetting_alert_save_button)  # 保存
    #     click_element(lock_lockSetting_smart_alert_text)  # 点击智能提醒内容
    #     click_element(lock_lockSetting_any_time)   # 点击修改时间
    #     click_element(lock_lockSetting_fixed_time)   # 选择在一个时间范围内
    #     click_element(notification_main_fix_time_su)  # 选择周日推送
    #     click_element(notification_main_fix_time_mo)
    #     click_element(notification_main_fix_time_tu)
    #     click_element(notification_main_fix_time_we)
    #     click_element(notification_main_fix_time_th)
    #     click_element(notification_main_fix_time_fr)
    #     click_element(lock_lockSetting_alert_save_button)  # 保存
    #     click_element(lock_lockSetting_add_alert_back, after_action_sleep=15)  # 返回
    #     click_element(login_login_back_button)  # 返回
    #     click_element(lock_main_lock_manager)  # 点击锁按钮
    #     check_lock_connection()
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_open_bt, False)  # 蓝牙开锁无推送
    #     os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
    #     click_element("//*[@text='Yale Access']")
    #     check_lock_connection()
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_close_bt, False)  # 蓝牙关锁无推送
    #     os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
    #     click_element("//*[@text='Yale Access']")
    #     # logout('house')
    #
    # @adb_log
    # def test_C855(self):
    #     # 管理员->普通用户
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     invite_user_yale('admin', UserInfo.AdminJoyce.phone2, UserInfo.AdminJoyce.name2)  # 邀请管理员B
    #     click_element(lock_main_lock_manager)  # 点击锁
    #     check_lock_connection()
    #     add_smart_alert(UserInfo.AdminJoyce.name2)   # 设置接收B的开关锁推送
    #     click_element(UserManagement_user_list)  # 点击【用户列表】
    #     click_element(UserManagement_user_admin.format(UserInfo.AdminJoyce.name2))  # 点击用户名
    #     click_element(userManagement_invite_user_access)    # 点击权限等级
    #     click_element(userManagement_invite_general_access)  # 点击普通用户
    #     click_element(userManagement_edit_manage_access_button)  # 点击保存
    #     click_element(userManagement_edit_manage_access_button)  # 发送
    #     click_element(lock_main_lock_manager)  # 点击锁
    #     check_lock_connection()
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     assert_result_displayed(lock_lockSetting_specific_smart_alert.format(UserInfo.AdminJoyce.name2), True)  # 变成普通用户智能提醒保留
    #
    # @adb_log
    # def test_C856(self):
    #     # 删除管理员后验证智能提醒
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     delete_user_yale(UserInfo.AdminJoyce.name2)  # 删除普通用户B
    #     invite_user_yale('admin', UserInfo.AdminJoyce.phone2, UserInfo.AdminJoyce.name2)  # 邀请管理员B
    #     click_element(lock_main_lock_manager)  # 点击锁
    #     check_lock_connection()
    #     add_smart_alert(UserInfo.AdminJoyce.name2)  # 设置接收B的开关锁推送
    #     delete_user_yale(UserInfo.AdminJoyce.name2)  # 删除管理员B
    #     click_element(lock_main_lock_manager)  # 点击锁
    #     check_lock_connection()
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     assert_result_displayed(lock_lockSetting_specific_smart_alert.format(UserInfo.AdminJoyce.name2), False)  # 该智能提醒被删除
    #
    # @adb_log
    # def test_C883(self):
    #     # 普通用户_蓝牙_任意时间
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()
    #     invite_user_yale('normal', UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.name)  # 邀请普通用户A
    #     # try:
    #     #     click_element(userManagement_invite_send_button)
    #     # except:
    #     #     pass
    #     click_element(lock_main_lock_manager)  # 点击锁
    #     check_lock_connection()
    #     add_smart_alert(UserInfo.NormalJoyce.name)  # 设置接收普通用户A的开关锁推送
    #
    # @adb_log
    # def test_C884(self):
    #     # 普通用户_蓝牙_指定时间
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     invite_user_yale('normal', UserInfo.NormalJoyce.phone2, UserInfo.NormalJoyce.name2)  # 邀请普通用户B
    #     click_element(lock_main_lock_manager)  # 点击锁
    #     check_lock_connection()
    #     add_smart_alert(UserInfo.NormalJoyce.name2, time_type='fixedtime')  # 设置接收普通用户B的开关锁推送
    #
    # @adb_log
    # def test_C891(self):
    #     # 普通用户智能提醒删除
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     try:
    #         if check_element(lock_lockSetting_specific_smart_alert.format(UserInfo.NormalJoyce.name2), wait_time=20, check_type=False) is False:
    #             click_element(lock_lockSetting_add_alert_button)  # 点击+按钮
    #             click_element(lock_lockSetting_choose_event)  # 点击选择活动
    #             click_element(lock_lockSetting_lock_by_special)  # 点击门由特定用户锁定或解锁
    #             click_element(UserManagement_user_admin.format(UserInfo.NormalJoyce.name2))  # 点击普通用户B
    #             click_element(lock_lockSetting_choose_time)  # 点击选择时间
    #             click_element(lock_lockSetting_any_time)  # 选择任何时候
    #             click_element(lock_lockSetting_alert_save_button)  # 保存
    #         else:
    #             pass
    #     except NameError:
    #         pass
    #     click_element(lock_lockSetting_specific_smart_alert.format(UserInfo.NormalJoyce.name2))  # 点击智能提醒内容
    #     click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #     click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #     assert_result_displayed(lock_lockSetting_specific_smart_alert.format(UserInfo.NormalJoyce.name2), False)  # 该智能提醒被删除
    #
    # @adb_log
    # def test_C893(self):
    #     # 普通用户->管理员
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     click_element(lock_main_lock_manager)  # 点击锁
    #     check_lock_connection()
    #     add_smart_alert(UserInfo.NormalJoyce.name2)   # 设置接收B的开关锁推送
    #     click_element(UserManagement_user_list)  # 点击【用户列表】
    #     click_element(UserManagement_user_admin.format(UserInfo.NormalJoyce.name2))  # 点击用户名
    #     click_element(userManagement_invite_user_access)    # 点击权限等级
    #     click_element(userManagement_invite_owner_access)  # 点击管理员
    #     click_element(userManagement_edit_manage_access_button)  # 点击保存
    #     click_element(userManagement_edit_manage_access_button)  # 发送
    #     click_element(lock_main_lock_manager)  # 点击锁
    #     check_lock_connection()
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     assert_result_displayed(lock_lockSetting_specific_smart_alert.format(UserInfo.NormalJoyce.name2), True)  # 该智能提醒保留
    #
    # @adb_log
    # def test_C894(self):
    #     # 修改普通用户schedule后验证其开关锁智能提醒
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     add_smart_alert(UserInfo.NormalJoyce.name)  # 设置接收普通用户A的开关锁推送
    #     click_element(UserManagement_user_list)  # 点击【用户列表】
    #     click_element(UserManagement_user_admin.format(UserInfo.NormalJoyce.name))  # 点击用户名
    #     click_element(userManagement_invite_create_schedule)   # 点击使用时间
    #     click_element(userManagement_invite_schedule_temp)  # 选择临时使用
    #     click_element(userManagement_edit_manage_access_button)  # 点击保存
    #     click_element(userManagement_edit_manage_access_button)  # 发送
    #     click_element(lock_main_lock_manager)  # 点击锁
    #     check_lock_connection()
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     assert_result_displayed(lock_lockSetting_specific_smart_alert.format(UserInfo.NormalJoyce.name), False)  # 该智能提醒被删除
    #
    # @adb_log
    # def test_C895(self):
    #     # 删除普通用户后验证智能提醒
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     add_smart_alert(UserInfo.NormalJoyce.name)  # 设置接收普通用户A的开关锁推送
    #     delete_user_yale(UserInfo.NormalJoyce.name)  # 删除普通用户A
    #     click_element(lock_main_lock_manager)  # 点击锁
    #     check_lock_connection()
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     assert_result_displayed(lock_lockSetting_specific_smart_alert.format(UserInfo.NormalJoyce.name), False)  # 该智能提醒被删除
    #
    # @adb_log
    # def test_C896(self):
    #     # 多条智能提醒(只含任意时间)
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     click_element(UserManagement_user_list)  # 点击【用户列表】
    #     try:
    #         if check_element(UserManagement_user_normal.format(UserInfo.NormalJoyce.name), wait_time=5, check_type=False) is False:
    #             invite_user_yale('normal', UserInfo.NormalJoyce.phone, UserInfo.NormalJoyce.name)  # 邀请普通用户A
    #     except NameError:
    #         pass
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     while 1:
    #         try:
    #             if check_element(lock_lockSetting_smart_alert_text, wait_time=3, check_type=False) is False:
    #                 break
    #             else:
    #                 click_element(lock_lockSetting_smart_alert_text)  # 点击智能提醒内容
    #                 click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #                 click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #         except NameError:
    #             pass
    #     click_element(lock_lockSetting_add_alert_back, after_action_sleep=15)  # 返回
    #     click_element(login_login_back_button)  # 返回
    #     click_element(lock_main_lock_manager)  # 点击锁按钮
    #     # check_lock_connection()
    #     add_smart_alert(UserInfo.Four.name)  # 为登录账号创建任意时间智能提醒
    #     add_smart_alert(UserInfo.NormalJoyce.name)  # 设置接收普通用户A的任意时间开关锁推送
    #     while 1:
    #         try:
    #             assert_result_displayed(lock_main_lock_close_status, True)
    #         except:
    #             click_element(lock_main_user_lock_button)  # lock
    #         else:
    #             break
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_open_bt, True)  # 蓝牙开锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     check_lock_connection()
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_close_bt, True)  # 蓝牙关锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     # logout('house')
    #
    # @adb_log
    # def test_C897(self):
    #     # 多条智能提醒(只含指定时间)
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     while 1:
    #         try:
    #             if check_element(lock_lockSetting_smart_alert_text, wait_time=3, check_type=False) is False:
    #                 break
    #             else:
    #                 click_element(lock_lockSetting_smart_alert_text)  # 点击智能提醒内容
    #                 click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #                 click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #         except NameError:
    #             pass
    #     click_element(lock_lockSetting_add_alert_back, after_action_sleep=15)  # 返回
    #     click_element(login_login_back_button)  # 返回
    #     click_element(lock_main_lock_manager)  # 点击锁按钮
    #     check_lock_connection()
    #     add_smart_alert(UserInfo.Four.name, time_type='fixedtime')  # 设置接收登录账号指定时间开关锁推送
    #     add_smart_alert(UserInfo.NormalJoyce.name, time_type='fixedtime')  # 设置接收普通用户指定时间开关锁推送
    #     while 1:
    #         try:
    #             assert_result_displayed(lock_main_lock_close_status, True)
    #         except:
    #             click_element(lock_main_user_lock_button)  # lock
    #         else:
    #             break
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_open_bt, True)  # 蓝牙开锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     check_lock_connection()
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_close_bt, True)  # 蓝牙关锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     # logout('house')
    #
    # @adb_log
    # def test_C898(self):
    #     # 多条智能提醒(含任意/指定时间)
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     while 1:
    #         try:
    #             if check_element(lock_lockSetting_smart_alert_text, wait_time=3, check_type=False) is False:
    #                 break
    #             else:
    #                 click_element(lock_lockSetting_smart_alert_text)  # 点击智能提醒内容
    #                 click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #                 click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #         except NameError:
    #             pass
    #     click_element(lock_lockSetting_add_alert_back, after_action_sleep=15)  # 返回
    #     click_element(login_login_back_button)  # 返回
    #     click_element(lock_main_lock_manager)  # 点击锁按钮
    #     check_lock_connection()
    #     add_smart_alert(UserInfo.Four.name, time_type='fixedtime')  # 设置接收登录账号指定时间开关锁推送
    #     add_smart_alert(UserInfo.NormalJoyce.name)  # 设置接收普通用户任意时间开关锁推送
    #     # if check_element(lock_main_lock_close_status, wait_time=5, check_type=False) is False:
    #     #     click_element(lock_main_user_lock_button)  # 点击锁
    #     #     time.sleep(5)
    #     # else:
    #     #     pass
    #     while 1:
    #         try:
    #             assert_result_displayed(lock_main_lock_close_status, True)
    #         except:
    #             click_element(lock_main_user_lock_button)  # lock
    #         else:
    #             break
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_open_bt, True)  # 蓝牙开锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     check_lock_connection()
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_close_bt, True)  # 蓝牙关锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     # logout('house')
    #
    # @adb_log
    # def test_C899(self):
    #     # 多条智能提醒(只含管理员)
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     invite_user_yale('admin', UserInfo.AdminJoyce.phone2, UserInfo.AdminJoyce.name2)  # 邀请管理员B
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     while 1:
    #         try:
    #             if check_element(lock_lockSetting_smart_alert_text, wait_time=3, check_type=False) is False:
    #                 break
    #             else:
    #                 click_element(lock_lockSetting_smart_alert_text)  # 点击智能提醒内容
    #                 click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #                 click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #         except NameError:
    #             pass
    #     click_element(lock_lockSetting_add_alert_back, after_action_sleep=15)  # 返回
    #     click_element(login_login_back_button)  # 返回
    #     click_element(lock_main_lock_manager)  # 点击锁按钮
    #     check_lock_connection()
    #     add_smart_alert(UserInfo.Four.name, time_type='fixedtime')  # 设置接收登录账号指定时间开关锁推送
    #     add_smart_alert(UserInfo.AdminJoyce.name2)  # 设置接收管理员B任意时间开关锁推送
    #     while 1:
    #         try:
    #             assert_result_displayed(lock_main_lock_close_status, True)
    #         except:
    #             click_element(lock_main_user_lock_button)  # lock
    #         else:
    #             break
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_open_bt, True)  # 蓝牙开锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     check_lock_connection()
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_close_bt, True)  # 蓝牙关锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     # logout('house')
    #
    # @adb_log
    # def test_C901(self):
    #     # 多条智能提醒(含管理员/普通用户)
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     click_element(userManagement_main_user_tab)  # 点击【用户】
    #     try:
    #         if check_element(UserManagement_user_normal.format(UserInfo.NormalJoyce.name2), wait_time=5, check_type=False) is False:
    #             invite_user_yale('normal', UserInfo.NormalJoyce.phone2, UserInfo.NormalJoyce.name2)  # 邀请普通用户B
    #     except NameError:
    #         pass
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     while 1:
    #         try:
    #             if check_element(lock_lockSetting_smart_alert_text, wait_time=3, check_type=False) is False:
    #                 break
    #             else:
    #                 click_element(lock_lockSetting_smart_alert_text)  # 点击智能提醒内容
    #                 click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #                 click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #         except NameError:
    #             pass
    #     click_element(lock_lockSetting_add_alert_back, after_action_sleep=15)  # 返回
    #     click_element(login_login_back_button)  # 返回
    #     click_element(lock_main_lock_manager)  # 点击锁按钮
    #     check_lock_connection()
    #     add_smart_alert(UserInfo.Four.name)  # 设置接收登录账号任意时间开关锁推送
    #     add_smart_alert(UserInfo.NormalJoyce.name, time_type='fixedtime')  # 设置接收普通用户A指定时间开关锁推送
    #     add_smart_alert(UserInfo.NormalJoyce.name2)  # 设置接收普通用户B任意时间开关锁推送
    #     while 1:
    #         try:
    #             assert_result_displayed(lock_main_lock_close_status, True)
    #         except:
    #             click_element(lock_main_user_lock_button)  # lock
    #         else:
    #             break
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_open_bt, True)  # 蓝牙开锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     check_lock_connection()
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_close_bt, True)  # 蓝牙关锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     # logout('house')
    #
    # @adb_log
    # def test_C902(self):
    #      # 一条智能提醒删除
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     while 1:
    #         try:
    #             if check_element(lock_lockSetting_smart_alert_text, wait_time=3, check_type=False) is False:
    #                 break
    #             else:
    #                 click_element(lock_lockSetting_smart_alert_text)  # 点击智能提醒内容
    #                 click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #                 click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #         except NameError:
    #             pass
    #     click_element(lock_lockSetting_add_alert_back, after_action_sleep=15)  # 返回
    #     click_element(login_login_back_button)  # 返回
    #     click_element(lock_main_lock_manager)  # 点击锁按钮
    #     check_lock_connection()
    #     add_smart_alert(UserInfo.NormalJoyce.name)  # 设置接收普通用户A指定时间开关锁推送
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     click_element(lock_lockSetting_specific_smart_alert.format(UserInfo.NormalJoyce.name))  # 点击智能提醒内容
    #     click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #     click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #     assert_result_displayed(lock_lockSetting_specific_smart_alert.format(UserInfo.NormalJoyce.name), False)  # 该智能提醒被删除
    #
    # @adb_log
    # def test_C903(self):
    #      # 多条智能提醒中部分删除
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     check_lock_connection()  # 检查锁的连接
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     while 1:
    #         try:
    #             if check_element(lock_lockSetting_smart_alert_text, wait_time=3, check_type=False) is False:
    #                 break
    #             else:
    #                 click_element(lock_lockSetting_smart_alert_text)  # 点击智能提醒内容
    #                 click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #                 click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #         except NameError:
    #             pass
    #     click_element(lock_lockSetting_add_alert_back, after_action_sleep=15)  # 返回
    #     click_element(login_login_back_button)  # 返回
    #     click_element(lock_main_lock_manager)  # 点击锁按钮
    #     check_lock_connection()
    #     add_smart_alert(UserInfo.Four.name)  # 设置接收登录账号任意时间开关锁推送
    #     add_smart_alert(UserInfo.NormalJoyce.name, time_type='fixedtime')  # 设置接收普通用户A指定时间开关锁推送
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     click_element(lock_lockSetting_specific_smart_alert.format(UserInfo.NormalJoyce.name))  # 点击智能提醒内容
    #     click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #     click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #     click_element(lock_lockSetting_add_alert_back, after_action_sleep=15)  # 返回
    #     click_element(login_login_back_button)  # 返回
    #     click_element(lock_main_lock_manager)  # 点击锁按钮
    #     check_lock_connection()
    #     while 1:
    #         try:
    #             assert_result_displayed(lock_main_lock_close_status, True)
    #         except:
    #             click_element(lock_main_user_lock_button)  # lock
    #         else:
    #             break
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_open_bt, True)  # 蓝牙开锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     check_lock_connection()
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_close_bt, True)  # 蓝牙关锁推送
    #     click_element(notification_main_clear_button)  # 清除所有通知
    #     # logout('house')
    #
    # @adb_log
    # def test_C904(self):
    #      # 多条智能提醒全部删除
    #     # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
    #     # click_element(homePage_homePage_home_list)  # 点击住宅图片
    #     click_element(lock_setting_setting_button)  # 点击设置按钮
    #     click_element(lock_setting_lock_setting)  # 点击锁设置
    #     click_element(lock_lockSetting_alarm)  # 点击智能提醒
    #     while 1:
    #         try:
    #             if check_element(lock_lockSetting_smart_alert_text, wait_time=3, check_type=False) is False:
    #                 break
    #             else:
    #                 click_element(lock_lockSetting_smart_alert_text)  # 点击智能提醒内容
    #                 click_element(lock_lockSetting_delete_button)  # 点击删除按钮
    #                 click_element(userManagement_invite_popup_confirm)  # 点击弹窗中的删除
    #         except NameError:
    #             pass
    #     click_element(lock_lockSetting_add_alert_back, after_action_sleep=15)  # 返回
    #     click_element(login_login_back_button)  # 返回
    #     click_element(lock_main_lock_manager)  # 点击锁按钮
    #     check_lock_connection()
    #     if check_element(lock_main_lock_close_status, wait_time=5, check_type=False) is False:
    #         click_element(lock_main_user_lock_button)  # 点击锁
    #         time.sleep(5)
    #     else:
    #         pass
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_open_bt, False)  # 无蓝牙开锁推送
    #     os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
    #     click_element("//*[@text='Yale Access']")
    #     time.sleep(8)
    #     click_element(lock_main_user_lock_button)  # 点击锁
    #     driver.open_notifications()  # 打开消息推送
    #     assert_result_displayed(notification_main_alert_close_bt, False)  # 无蓝牙关锁推送
    #     os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
    #     click_element("//*[@text='Yale Access']")
        # logout('house')


    @adb_log
    def test_D000(self):
        # 恢复出厂设置
        # login(UserInfo.Four.phone, UserInfo.Four.email, UserInfo.Four.password, 'phone')
        # click_element(homePage_homePage_home_list)  # 点击住宅图片
        factory_lock_yale()
        logout()

    def cases(self):
        return list(filter(lambda m: m.startswith("test_C") and callable(getattr(self, m)), dir(self)))

