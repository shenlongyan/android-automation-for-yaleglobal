import logging
import unittest
from Configuration.OperationMethod import *
from Configuration import StartUp


class TestHomePage(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver = StartUp.driver
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H_%M_%S',
                            filename=FolderList.logging_name,
                            filemode='a')
        logging.getLogger()
        time.sleep(1)

    def add_img(self):
        # 在是python3.x 中，如果在这里初始化driver ，因为3.x版本 unittest 运行机制不同，会导致用力失败时截图失败
        self.imgs.append(self.driver.get_screenshot_as_base64())
        return True

    def setUp(self) -> None:
        os.system('adb shell svc wifi enable')
        bt_switch('on')
        self.imgs = []
        self.addCleanup(self.cleanup)

    def cleanup(self):
        pass

    # @unittest.skipIf(app_select == 'yale', 'Depending on devices')  # 不同机型步骤不一致
    # @adb_log
    # def test_home_page_tc001(self):
    #     if app_select == 'panpan':
    #         user_register_yale()
    #         assert_result_displayed(homePage_homePage_no_rooms, True)  # 没有房间名
    #         logout()
    #     elif app_select == 'yale':
    #         login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
    #         more_setting_cn = '/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[3]'
    #         more_setting_en = '/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[4]'
    #         language_setting = '/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[2]/android.widget.RelativeLayout'
    #         language_setting_detail = '/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]'
    #         language_select_cn = '/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[1]'
    #         language_select_en = '/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.LinearLayout[3]'
    #         # 切换英语语言
    #         os.system('adb shell am start com.android.settings/com.android.settings.Settings')
    #         for i in range(0, 2):
    #             start_x = driver.get_window_size()['width'] * 0.5
    #             start_y = driver.get_window_size()['height'] * 0.7
    #             end_x = start_x
    #             end_y = driver.get_window_size()['height'] * 0.3
    #             driver.swipe(int(start_x), int(start_y), int(end_x), int(end_y), 500)
    #         click_element(more_setting_cn)
    #         click_element(language_setting)
    #         click_element(language_setting_detail)
    #         click_element(language_select_en)
    #         # 切换App
    #         os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
    #         click_element("//*[@text='Yale Access']")
    #         assert_result_text('com.aaecosys.apac_leo:id/keychain_empty_title', 'Welcome!')  # 显示英语
    #         # 切换中文语言
    #         os.system('adb shell am start com.android.settings/com.android.settings.Settings')
    #         click_element(more_setting_en)
    #         click_element(language_setting)
    #         click_element(language_setting_detail)
    #         click_element(language_select_cn)
    #         # 切换App
    #         os.system('adb shell input keyevent KEYCODE_APP_SWITCH')
    #         click_element("//*[@text='Yale Access']")
    #         logout()
    #     else:
    #         assert False

    @adb_log
    def test_C492(self):
        if app_select == 'panpan':
            user_register_yale()
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            assert_result_text(homepage_assert_menu_detail_title, homePage_else_add_device_text)  # 显示智能设备
            logout('other')
        elif app_select == 'yale':
            user_register_yale()
            assert_result_displayed(homePage_homePage_no_rooms, True)  # 没有房间名
            logout()
        else:
            assert False

    @adb_log
    def test_C493(self):
        if app_select == 'panpan':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
            assert_result_displayed(homepage_compound_menu_shop, True)  # 显示【商店】
            assert_result_displayed(homePage_menu_help, True)  # 显示【帮助】
            assert_result_displayed(homePage_menu_add_device, True)  # 显示【添加设备】
            assert_result_displayed(homePage_user_logout, True)  # 显示【登出】
            assert_result_text(homePage_menu_user_name, UserInfo.First.name)  # 显示用户名称
            click_element(homePage_menu_home)  # 点击【首页】
            logout()
        elif app_select == 'yale':
            login_yale(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            self.driver.keyevent(4)  # 返回首页
            logout()
        else:
            assert False

    @adb_log
    def test_C494(self):
        if app_select == 'panpan':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_home)  # 点击【首页】
            assert_result_displayed(homePage_homePage_main, True)  # 显示首页
            logout()
        elif app_select == 'yale':
            user_register_yale()
            click_element(homePage_homePage_add_device)  # 点击【添加设备】
            logout()
        else:
            assert False

    @adb_log
    def test_C495(self):
        if app_select == 'panpan':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homepage_compound_menu_shop)  # 点击【商店】
            assert_result_text(homepage_assert_menu_detail_title, homepage_assert_menu_shop_text)  # 显示敬请期待
            click_element(homePage_homePage_menu_in_other_page)  # 点击【菜单】
            click_element(homePage_menu_home)  # 点击【首页】
            assert_result_displayed(homePage_homePage_main, True)  # 显示首页
            logout()
        elif app_select == 'yale':
            login_yale(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'email')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            assert_result_displayed(homePage_menu_home, True)  # 显示【首页】
            assert_result_displayed(homePage_menu_help, True)  # 显示【帮助】
            assert_result_displayed(homePage_menu_add_device, True)  # 显示【添加设备】
            self.driver.keyevent(4)  # 返回首页
            logout()
        else:
            assert False

    @adb_log
    def test_C496(self):
        if app_select == 'panpan':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_help)  # 点击【帮助】
            assert_result_text(homepage_assert_menu_detail_title, homePage_menu_help_text)  # 显示联系我们
            click_element(homePage_homePage_menu_in_other_page)  # 点击【菜单】
            click_element(homePage_menu_home)  # 点击【首页】
            assert_result_displayed(homePage_homePage_main, True)  # 显示首页
            logout()
        elif app_select == 'yale':
            login_yale(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户】
            assert_result_displayed(homePage_user_name, True)  # 显示用户名字
            assert_result_displayed(homePage_user_mail, True)  # 显示用户邮箱
            assert_result_displayed(homePage_user_phone, True)  # 显示用户电话
            assert_result_displayed(homePage_user_logout, True)  # 显示【登出】
            assert_result_displayed(homePage_user_edit, True)  # 显示【编辑】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @adb_log
    def test_C497(self):
        if app_select == 'panpan':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_add_device)  # 点击【添加设备】
            assert_result_text(homepage_assert_menu_detail_title, homePage_else_add_device_text)  # 显示智能设备
            click_element(homePage_homePage_menu_in_other_page)  # 点击【菜单】
            click_element(homePage_menu_home)  # 点击【首页】
            assert_result_displayed(homePage_homePage_main, True)  # 显示首页
            logout()
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_home)  # 点击【首页】
            self.driver.keyevent(4)  # 返回首页
            assert_result_displayed(homePage_homePage_menu_in_home_page, True)  # 显示首页
            logout()
        else:
            assert False

    @adb_log
    def test_C498(self):
        if app_select == 'panpan':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_user_logout)  # 点击【登出】
            click_element(homePage_else_cancel)  # 点击【取消】
            click_element(homePage_user_logout)  # 点击【登出】
            click_element(homePage_else_sure_or_try)   # 点击【确定】
            assert_result_displayed(register_button_welcome_register, True)  # 显示启动首页
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_help)  # 点击【帮助】
            # assert_result_displayed(homePage_menu_help_text, True)  # 显示【联系我们】
            self.driver.keyevent(4)  # 返回首页
            logout()
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'systeme bug')
    @adb_log
    def test_C499(self):
        if app_select == 'panpan':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_userIcon_modify_icon)  # 点击【头像】
            # Todo 初次安装的验证 & System Bug
            click_element(homePage_userIcon_select_from_album)  # 点击【相册】
            click_element(homePage_userIcon_select_first_photo)  # 点击【照片】
            click_element(homepage_button_detail_back)  # 点击【返回】
            logout()
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            time.sleep(1)
            click_element(homePage_menu_add_device)  # 点击【添加设备】
            # time.sleep(5)  # 等待loading
            assert_result_displayed(homePage_else_add_device_text, True)  # 显示【智能设备】
            logout()
        else:
            assert False

    @adb_log
    def test_C500(self):
        if app_select == 'panpan':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_name)  # 点击修改名字
            clear_text_content(homepage_button_detail_modify_name_input)  # 清除文字
            send_keys(homepage_button_detail_modify_name_input, 'darkbeing001')  # 输入名字
            click_element(homepage_button_detail_modify_name_save)  # 点击【保存】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            assert_result_text(homePage_menu_user_name, 'darkbeing001')
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_name)  # 点击修改名字
            clear_text_content(homepage_button_detail_modify_name_input)  # 清除文字
            send_keys(homepage_button_detail_modify_name_input, UserInfo.First.name)  # 输入名字
            click_element(homepage_button_detail_modify_name_save)  # 点击【保存】
            click_element(homepage_button_detail_back)  # 点击【返回】
            logout()
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_logout)  # 点击【退出】
            assert_result_displayed(homePage_else_cancel, True)  # 显示【取消】
            assert_result_displayed(homePage_else_sure_or_try, True)  # 显示【确定】
            click_element(homePage_else_cancel)  # 点击【取消】
            click_element(homePage_homePage_menu_in_home_page)  # 点击【返回】
            logout()
        else:
            assert False

    @adb_log
    def test_C501(self):
        if app_select == 'panpan':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'email')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_name)  # 点击修改名字
            clear_text_content(homepage_button_detail_modify_name_input)  # 清除文字
            send_keys(homepage_button_detail_modify_name_input, '# ')  # 输入名字
            click_element(homepage_button_detail_modify_name_save)  # 点击【保存】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】， 需要返回两次
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            assert_result_text(homePage_menu_user_name, UserInfo.First.name)
            click_element(homePage_menu_home)  # 点击【首页】
            logout()
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
            assert_result_displayed(register_main_login, True)  # 显示【登录】
        else:
            assert False

    @adb_log
    def test_C502(self):
        if app_select == 'panpan':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_edit_change_password)  # 点击修改密码
            click_element(login_button_forget_password_new_1)  # 点击【密码】
            driver.press_keycode(10)  # 模拟键盘输入，否则不会出现提示
            send_keys(login_button_forget_password_new_1, '123')  # 输入密码
            assert_result_text(register_assert_snackbar_text, register_assert_password_length)  # 密码长度提示
            assert_result_enable(login_compound_forget_password_finish, False)  # 不可点击【提交】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】， 需要返回两次
            logout()
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_logout)  # 点击【退出】
            assert_result_displayed(homePage_else_cancel, True)  # 显示【取消】
            assert_result_displayed(homePage_else_sure_or_try, True)  # 显示【确定】
            click_element(homePage_else_cancel)  # 点击【取消】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @adb_log
    def test_C503(self):
        if app_select == 'panpan':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_edit_change_password)  # 点击修改密码
            click_element(login_button_forget_password_new_1)  # 点击【密码】
            driver.press_keycode(10)  # 模拟键盘输入，否则不会出现提示
            send_keys(login_button_forget_password_new_1, 'abcdefghijkl')  # 输入密码
            assert_result_text(register_assert_snackbar_text, register_assert_password_complexity)  # 密码复杂度提示
            assert_result_enable(login_compound_forget_password_finish, False)  # 不可点击【提交】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】， 需要返回两次
            logout()
        elif app_select == 'yale':
            login_yale(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户】
            assert_result_displayed(homePage_user_name, True)  # 显示用户名字
            assert_result_displayed(homePage_user_mail, True)  # 显示用户邮箱
            assert_result_displayed(homePage_user_phone, True)  # 显示用户电话
            assert_result_displayed(homePage_user_logout, True)  # 显示【登出】
            assert_result_displayed(homePage_user_edit, True)  # 显示【编辑】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @adb_log
    def test_C504(self):
        if app_select == 'panpan':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_edit_change_password)  # 点击修改密码
            click_element(login_button_forget_password_new_1)  # 点击【密码】
            send_keys(login_button_forget_password_new_1, 'Abcd1234')  # 输入密码
            assert_result_enable(login_compound_forget_password_finish, False)  # 不可点击【提交】
            send_keys(login_button_forget_password_new_2, 'Abcd12345')  # 输入密码
            assert_result_enable(login_compound_forget_password_finish, False)  # 不可点击【提交】
            click_element(homepage_button_detail_back)  # 点击【返回】
            click_element(homepage_button_detail_back)  # 点击【返回】， 需要返回两次
            logout()
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_logout)  # 点击【退出】
            assert_result_displayed(homePage_else_cancel, True)  # 显示【取消】
            assert_result_displayed(homePage_else_sure_or_try, True)  # 显示【确定】
            click_element(homePage_else_cancel)  # 点击【取消】
            click_element(homePage_homePage_menu_in_home_page)  # 点击【返回】
            logout()
        else:
            assert False

    @adb_log
    def test_C505(self):
        if app_select == 'panpan':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_edit_change_password)  # 点击修改密码
            click_element(login_button_forget_password_new_1)  # 点击【密码】
            send_keys(login_button_forget_password_new_1, UserInfo.First.password_new)  # 输入密码
            assert_result_enable(login_compound_forget_password_finish, False)  # 不可点击【提交】
            send_keys(login_button_forget_password_new_2, UserInfo.First.password_new)  # 输入密码
            click_element(login_compound_forget_password_finish)  # 点击保存
            #  恢复之前的密码
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password_new, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_edit_change_password)  # 点击修改密码
            click_element(login_button_forget_password_new_1)  # 点击【密码】
            send_keys(login_button_forget_password_new_1, UserInfo.First.password)  # 输入密码
            assert_result_enable(login_compound_forget_password_finish, False)  # 不可点击【提交】
            send_keys(login_button_forget_password_new_2, UserInfo.First.password)  # 输入密码
            click_element(login_compound_forget_password_finish)  # 点击保存
        elif app_select == 'yale':
            login_yale(UserInfo.Second.phone, UserInfo.Second.email, UserInfo.Second.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户】
            assert_result_displayed(homePage_user_name, True)  # 显示用户名字
            assert_result_displayed(homePage_user_mail, True)  # 显示用户邮箱
            assert_result_displayed(homePage_user_phone, True)  # 显示用户电话
            assert_result_displayed(homePage_user_logout, True)  # 显示【登出】
            assert_result_displayed(homePage_user_edit, True)  # 显示【编辑】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C506(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_edit)  # 点击【编辑】
            assert_result_displayed(homePage_edit_change_password, True)  # 显示【修改密码】
            click_element(homePage_edit_account)  # 点击【管理账户】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    # @adb_log
    # def test_C507(self):
    #     if app_select == 'panpan':
    #         pass
    #     elif app_select == 'yale':
    #         user_register_yale()
    #         click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
    #         click_element(homePage_menu_user_icon)  # 点击【用户信息】
    #         click_element(homePage_userIcon_modify_icon)  # 点击【编辑头像】
    #         click_element(homePage_userIcon_open_camera)  # 点击【拍一张照片】
    #         click_element(homePage_userIcon_take_photo)  # 点击【拍摄】
    #         click_element(homePage_userIcon_take_photo_done)  # 点击【完成】
    #         assert_result_displayed(homePage_userIcon_icon_updating, True)  # 正在上传
    #         image_compare('Capture/yale_user_icon.png', homePage_user_icon)  # 对比图片
    #         assert_result_displayed(homePage_user_logout, True)  # 上传成功，弹框消失
    #         click_element(homePage_user_logout)  # 点击【退出】
    #         click_element(homePage_else_sure_or_try)  # 点击【确定】
    #     else:
    #         assert False

    # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    # @adb_log
    # def test_C508(self):
    #     if app_select == 'panpan':
    #         pass
    #     elif app_select == 'yale':
    #         user_register_yale()
    #         click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
    #         click_element(homePage_menu_user_icon)  # 点击【用户信息】
    #         click_element(homePage_userIcon_modify_icon)  # 点击【编辑头像】
    #         click_element(homePage_userIcon_select_from_album)  # 点击【选择图片】
    #         click_element(homePage_userIcon_select_first_photo)  # 选择第一张图片
    #         assert_result_displayed(homePage_userIcon_icon_updating, True)  # 30秒内完成上传
    #         image_compare('Capture/yale_menu_icon.png', homePage_user_icon)  # 对比图片
    #         click_element(homePage_user_logout)  # 点击【退出】
    #         click_element(homePage_else_sure_or_try)  # 点击【确定】
    #     else:
    #         assert False

    # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    # @adb_log
    # def test_C509(self):
    #     if app_select == 'panpan':
    #         pass
    #     elif app_select == 'yale':
    #         login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
    #         click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
    #         click_element(homePage_menu_user_icon)  # 点击【用户信息】
    #         click_element(homePage_userIcon_modify_icon)  # 点击【头像】
    #         assert_result_displayed(homePage_userIcon_open_camera, True)  # 显示拍一张照片
    #         self.driver.keyevent(4)  # 返回
    #         click_element(homePage_userIcon_modify_icon)  # 点击【头像】
    #         click_element(homePage_userIcon_select_from_album)  # 点击【选择图片】
    #         assert_result_displayed(homePage_userIcon_select_first_photo, True)  # 显示图片
    #         driver.press_keycode(4)  # 点击【返回】
    #         click_element(homePage_user_logout)  # 点击【退出】
    #         click_element(homePage_else_sure_or_try)  # 点击【确定】
    #     else:
    #         assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C510(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_name)  # 点击【名字】
            assert_result_displayed(homePage_use_name_modify_title, True)  # 显示【你叫什么名字】
            assert_result_displayed(homePage_userName_first_name, True)  # 显示【名输入框】
            assert_result_displayed(homePage_userName_last_name, True)  # 显示【姓输入框】
            click_element(homePage_else_sure_or_try)  # 点击保存
            assert_result_text(homePage_user_name, UserInfo.First.name)  # 检查名字
            click_element(homePage_homePage_menu_in_home_page)  # 点击【返回】
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            assert_result_text(homePage_menu_user_name, UserInfo.First.name)  # 检查名称
            click_element(homePage_menu_home)  # 点击【首页】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C511(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_name)  # 点击【名字】
            send_keys(homePage_userName_first_name, UserInfo.First.name_new_space_first)  # 输入名
            send_keys(homePage_userName_last_name, UserInfo.First.name_new_space_last)  # 输入姓
            click_element(homePage_else_sure_or_try)  # 点击【保存】
            assert_result_text(homePage_user_name, UserInfo.First.name_new_space)  # 检查名称
            click_element(homePage_homePage_menu_in_home_page)  # 点击【返回】
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            assert_result_text(homePage_menu_user_name, UserInfo.First.name_new_space)  # 检查名称
            # 修改回原来的名字
            click_element(homePage_menu_home)  # 点击【首页】
            click_element(homePage_user_name)  # 点击【名字】
            send_keys(homePage_userName_first_name, UserInfo.First.name_first)  # 输入名
            send_keys(homePage_userName_last_name, UserInfo.First.name_last)  # 输入姓
            click_element(homePage_else_sure_or_try)  # 点击【保存】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C512(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_name)  # 点击【名字】
            send_keys(homePage_userName_first_name, UserInfo.First.name_new_number_first)  # 输入名
            send_keys(homePage_userName_last_name, UserInfo.First.name_new_number_last)  # 输入姓
            click_element(homePage_else_sure_or_try)  # 点击【保存】
            assert_result_text(homePage_user_name, UserInfo.First.name_new_number)  # 检查名称
            click_element(homePage_homePage_menu_in_home_page)  # 点击【返回】
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            assert_result_text(homePage_menu_user_name, UserInfo.First.name_new_number)  # 检查名称
            # 修改回原来的名字
            click_element(homePage_menu_home)  # 点击【首页】
            click_element(homePage_user_name)  # 点击【名字】
            send_keys(homePage_userName_first_name, UserInfo.First.name_first)  # 输入名
            send_keys(homePage_userName_last_name, UserInfo.First.name_last)  # 输入姓
            click_element(homePage_else_sure_or_try)  # 点击【保存】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C513(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_name)  # 点击【名字】
            send_keys(homePage_userName_first_name, UserInfo.First.name_new_symbol_first)  # 输入名
            send_keys(homePage_userName_last_name, UserInfo.First.name_new_symbol_last)  # 输入姓
            click_element(homePage_else_sure_or_try)  # 点击【保存】
            assert_result_text(homePage_user_name, UserInfo.First.name_new_symbol)  # 检查名称
            click_element(homePage_homePage_menu_in_home_page)  # 点击【返回】
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            assert_result_text(homePage_menu_user_name, UserInfo.First.name_new_symbol)  # 检查名称
            # 修改回原来的名字
            click_element(homePage_menu_home)  # 点击【首页】
            click_element(homePage_user_name)  # 点击【名字】
            send_keys(homePage_userName_first_name, UserInfo.First.name_first)  # 输入名
            send_keys(homePage_userName_last_name, UserInfo.First.name_last)  # 输入姓
            click_element(homePage_else_sure_or_try)  # 点击【保存】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C514(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_name)  # 点击【名字】
            send_keys(homePage_userName_first_name, UserInfo.First.name_new_all_first)  # 输入名
            send_keys(homePage_userName_last_name, UserInfo.First.name_new_all_last)  # 输入姓
            click_element(homePage_else_sure_or_try)  # 点击【保存】
            assert_result_text(homePage_user_name, UserInfo.First.name_new_all)  # 检查名称
            click_element(homePage_homePage_menu_in_home_page)  # 点击【返回】
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            assert_result_text(homePage_menu_user_name, UserInfo.First.name_new_all)  # 检查名称
            # 修改回原来的名字
            click_element(homePage_menu_home)  # 点击【首页】
            click_element(homePage_user_name)  # 点击【名字】
            send_keys(homePage_userName_first_name, UserInfo.First.name_first)  # 输入名
            send_keys(homePage_userName_last_name, UserInfo.First.name_last)  # 输入姓
            click_element(homePage_else_sure_or_try)  # 点击【保存】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C515(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_name)  # 点击【名字】
            send_keys(homePage_userName_first_name, UserInfo.First.name_new_all_first)  # 输入名
            send_keys(homePage_userName_last_name, UserInfo.First.name_new_all_last)  # 输入姓
            click_element(homePage_else_cancel)  # 点击【取消】
            assert_result_text(homePage_user_name, UserInfo.First.name)  # 检查名称
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C516(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_edit)  # 点击【编辑】
            click_element(homePage_edit_change_password)  # 点击【修改密码】
            click_element(homePage_edit_change_password_close)  # 点击【关闭】
            click_element(homePage_edit_account)  # 点击【管理账户】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C517(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_edit)  # 点击【编辑】
            click_element(homePage_edit_change_password)  # 点击【修改密码】
            send_keys(homePage_edit_password_first, UserInfo.First.password_length)  # 输入不足长度密码
            send_keys(homePage_edit_password_second, UserInfo.First.password_length)  # 输入不足长度密码
            assert_result_displayed(homePage_edit_change_password_save, False)  # 不显示【保存】
            click_element(homePage_edit_change_password_close)  # 点击【关闭】
            click_element(homePage_edit_account)  # 点击【管理账户】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C518(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_edit)  # 点击【编辑】
            click_element(homePage_edit_change_password)  # 点击【修改密码】
            send_keys(homePage_edit_password_first, UserInfo.First.password_upper)  # 输入大写密码
            send_keys(homePage_edit_password_second, UserInfo.First.password_upper)  # 输入大写密码
            assert_result_displayed(homePage_edit_change_password_save, False)  # 不显示【保存】
            click_element(homePage_edit_change_password_close)  # 点击【关闭】
            click_element(homePage_edit_account)  # 点击【管理账户】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C519(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_edit)  # 点击【编辑】
            click_element(homePage_edit_change_password)  # 点击【修改密码】
            send_keys(homePage_edit_password_first, UserInfo.First.password_lower)  # 输入小写密码
            send_keys(homePage_edit_password_second, UserInfo.First.password_lower)  # 输入小写密码
            assert_result_displayed(homePage_edit_change_password_save, False)  # 不显示【保存】
            click_element(homePage_edit_change_password_close)  # 点击【关闭】
            click_element(homePage_edit_account)  # 点击【管理账户】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C520(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_edit)  # 点击【编辑】
            click_element(homePage_edit_change_password)  # 点击【修改密码】
            send_keys(homePage_edit_password_first, UserInfo.First.password_number)  # 输入数字密码
            send_keys(homePage_edit_password_second, UserInfo.First.password_number)  # 输入数字密码
            assert_result_displayed(homePage_edit_change_password_save, False)  # 不显示【保存】
            click_element(homePage_edit_change_password_close)  # 点击【关闭】
            click_element(homePage_edit_account)  # 点击【管理账户】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C521(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_edit)  # 点击【编辑】
            click_element(homePage_edit_change_password)  # 点击【修改密码】
            send_keys(homePage_edit_password_first, UserInfo.First.password_english)  # 输入英语密码
            send_keys(homePage_edit_password_second, UserInfo.First.password_english)  # 输入英语密码
            # assert_result_displayed(homePage_edit_change_password_save, False)  # 不显示【保存】
            click_element(homePage_edit_change_password_save)  # 点击保存
            assert_result_displayed(homePage_edit_change_password_save, True)  # 仍然在保存页面
            click_element(homePage_edit_change_password_close)  # 点击【关闭】
            click_element(homePage_edit_account)  # 点击【管理账户】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C522(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_edit)  # 点击【编辑】
            click_element(homePage_edit_change_password)  # 点击【修改密码】
            send_keys(homePage_edit_password_first, UserInfo.First.password_symbol)  # 输入符号密码
            send_keys(homePage_edit_password_second, UserInfo.First.password_symbol)  # 输入符号密码
            assert_result_displayed(homePage_edit_change_password_save, False)  # 不显示【保存】
            click_element(homePage_edit_change_password_close)  # 点击【关闭】
            click_element(homePage_edit_account)  # 点击【管理账户】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C523(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_edit)  # 点击【编辑】
            click_element(homePage_edit_change_password)  # 点击【修改密码】
            send_keys(homePage_edit_password_first, UserInfo.First.password)  # 输入不同密码
            send_keys(homePage_edit_password_second, UserInfo.First.password_new)  # 输入不同密码
            assert_result_displayed(homePage_edit_change_password_save, False)  # 不显示【保存】
            click_element(homePage_edit_change_password_close)  # 点击【关闭】
            click_element(homePage_edit_account)  # 点击【管理账户】
            click_element(homePage_user_logout)  # 点击【退出】
            click_element(homePage_else_sure_or_try)  # 点击【确定】
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C524(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            for i in range(0, 3):
                if i != 0:  # 用于初始化密码，以及相同密码修改的确认
                    password = UserInfo.First.password
                else:  # 用于确认密码修改
                    password = UserInfo.First.password_new
                click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
                click_element(homePage_menu_user_icon)  # 点击【用户信息】
                click_element(homePage_user_edit)  # 点击【编辑】
                click_element(homePage_edit_change_password)  # 点击【修改密码】
                send_keys(homePage_edit_password_first, password)  # 输入密码
                send_keys(homePage_edit_password_second, password)  # 输入密码
                click_element(homePage_edit_change_password_save)  # 点击【保存】
            logout()
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C525(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_edit)  # 点击【编辑】
            click_element(homePage_edit_change_password)  # 点击【修改密码】
            send_keys(homePage_edit_password_first, UserInfo.First.password_new)  # 输入新密码
            assert_result_displayed(homePage_edit_change_password_save, False)  # 不显示【保存】
            send_keys(homePage_edit_password_second, UserInfo.First.password_new)  # 输入新密码
            click_element(homePage_edit_change_password_save)  # 点击【保存】
            logout()
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password_new, 'phone')
            # 恢复默认密码
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_edit)  # 点击【编辑】
            click_element(homePage_edit_change_password)  # 点击【修改密码】
            send_keys(homePage_edit_password_first, UserInfo.First.password)  # 输入密码
            send_keys(homePage_edit_password_second, UserInfo.First.password)  # 输入密码
            click_element(homePage_edit_change_password_save)  # 点击【保存】
            logout()
        else:
            assert False

    @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    @adb_log
    def test_C526(self):
        if app_select == 'panpan':
            pass
        elif app_select == 'yale':
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password, 'phone')
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_edit)  # 点击【编辑】
            click_element(homePage_edit_change_password)  # 点击【修改密码】
            send_keys(homePage_edit_password_first, UserInfo.First.password_new)  # 输入新密码
            assert_result_displayed(homePage_edit_change_password_save, False)  # 不显示【保存】
            send_keys(homePage_edit_password_second, UserInfo.First.password_new)  # 输入新密码
            click_element(homePage_edit_change_password_save)  # 点击【保存】
            logout()
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password_new, 'phone')
            logout()
            login_yale(UserInfo.First.phone, UserInfo.First.email, UserInfo.First.password_new, 'email')
            # 恢复默认密码
            click_element(homePage_homePage_menu_in_home_page)  # 点击【菜单】
            click_element(homePage_menu_user_icon)  # 点击【用户信息】
            click_element(homePage_user_edit)  # 点击【编辑】
            click_element(homePage_edit_change_password)  # 点击【修改密码】
            send_keys(homePage_edit_password_first, UserInfo.First.password)  # 输入密码
            send_keys(homePage_edit_password_second, UserInfo.First.password)  # 输入密码
            click_element(homePage_edit_change_password_save)  # 点击【保存】
            logout()
        else:
            assert False

    def cases(self):
        return list(filter(lambda m: m.startswith("test_C") and callable(getattr(self, m)), dir(self)))

    # @unittest.skipIf(app_select == 'panpan', 'no case in panpan')
    # @unittest.skipIf(app_select == 'yale', 'multi device')
    # @adb_log
    # def test_home_page_tc037(self):
    #     if app_select == 'panpan':
    #         pass
    #     elif app_select == 'yale':
    #         pass
    #     else:
    #         assert False
